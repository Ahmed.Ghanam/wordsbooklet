﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="App.cs" company="None">
// Last Modified: 10/10/2018
// Author: Ahmed Ghanam.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace WordsBooklet.UserInterface
{
    using System;
    using System.Globalization;
    using System.Threading;
    using System.Windows;
    using System.Windows.Markup;
    using System.Windows.Threading;

    using Telerik.Windows.Controls;

    using WordsBooklet.BusinessLayer.Infrastructure;
    using WordsBooklet.UserInterface.Resources.Strings;

    using Major = WordsBooklet.UserInterface.Windows.Major;

    /// <inheritdoc cref="Application" />
    /// <summary>
    ///     Interaction logic for words booklet application.
    /// </summary>
    public partial class App
    {
        /// <inheritdoc cref="Application" />
        /// <summary>
        ///     Initializes a new instance of the <see cref="App" /> class.
        /// </summary>
        /// <exception cref="InvalidOperationException">
        ///     More than one instance of the <see cref="Application" /> class is created per <see cref="AppDomain" />.
        /// </exception>
        public App()
        {
            this.DispatcherUnhandledException += UnhandledException;
        }

        /// <inheritdoc cref="Application" />
        /// <summary>
        ///     Raises the <see cref="Application.Startup" /> event.
        /// </summary>
        /// <param name="e">A <see cref="StartupEventArgs" /> that contains the event data.</param>
        [STAThread]
        protected override void OnStartup(StartupEventArgs e)
        {
            // Call base implementation.
            base.OnStartup(e);

            // Set shutdown mode.
            this.SetShutdownMode();

            // Set culture for whole application.
            SetApplicationCulture();

            // Display the main window.
            new Major { FlowDirection = CommonSettings.FlowDirection }.Show();

            // Set the localization settings for telerik controls.
            LocalizationManager.Manager = new LocalizationManager { ResourceManager = Telerik.ResourceManager };
        }

        /// <summary>
        ///     Sets the culture settings.
        /// </summary>
        private static void SetApplicationCulture()
        {
            // Set the thread culture information.
            if (CommonSettings.CultureInfo != null)
            {
                Thread.CurrentThread.CurrentCulture = CommonSettings.CultureInfo;
                Thread.CurrentThread.CurrentUICulture = CommonSettings.CultureInfo;
            }

            // Override metadata to allow validation culture sequence.
            FrameworkElement.LanguageProperty.OverrideMetadata(
                typeof(FrameworkElement),
                new FrameworkPropertyMetadata(XmlLanguage.GetLanguage(CultureInfo.CurrentCulture.IetfLanguageTag)));
        }

        /// <summary>
        ///     Occurs when an exception is thrown but not handled.
        /// </summary>
        /// <param name="sender">The <paramref name="sender" />.</param>
        /// <param name="e">The <see cref="DispatcherUnhandledExceptionEventArgs" /> instance containing the event data.</param>
        private static void UnhandledException(
            [NotNull] object sender,
            [NotNull] DispatcherUnhandledExceptionEventArgs e)
        {
            // Prevent application crashing.
            e.Handled = true;
        }

        /// <summary>
        ///     Sets the shutdown mode for the entire application.
        /// </summary>
        private void SetShutdownMode()
        {
            this.ShutdownMode = ShutdownMode.OnMainWindowClose;
        }
    }
}