﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="DockingFactory.cs" company="None">
// Last Modified: 02/10/2018
// Author: Ahmed Ghanam.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace WordsBooklet.UserInterface.Infrastructure
{
    using System;
    using System.Linq;

    using Telerik.Windows.Controls;
    using Telerik.Windows.Controls.Docking;

    /// <inheritdoc />
    /// <summary>
    ///     This factory helps PanesSource property usage in MVVM scenarios. It
    ///     creates new Telerik.Windows.Controls.RadPane instances, check
    ///     whether an item is its own container or not and adds the new
    ///     generated Telerik.Windows.Controls.RadPane to the
    ///     Telerik.Windows.Controls.RadDocking control.
    /// </summary>
    /// <seealso cref="DockingPanesFactory" />
    internal class DockingFactory : DockingPanesFactory
    {
        /// <inheritdoc />
        /// <summary>
        ///     Adds the <paramref name="pane" /> to the <paramref name="radDocking" /> layout.
        ///     If there is no available containers to generate the new content
        ///     use the <paramref name="radDocking" />'s <see cref="RadDocking.GeneratedItemsFactory" />
        ///     to create additional <see cref="RadSplitContainer" />s and <see cref="RadPaneGroup" />s.
        /// </summary>
        /// <param name="radDocking">
        ///     The <see cref="RadDocking" /> a <see cref="RadPane" /> instance is being added to.
        /// </param>
        /// <param name="pane">The <see cref="RadPane" /> to add.</param>
        /// <exception cref="ArgumentNullException"> <see cref="RadDocking.SplitItems" />  is <see langword="null" />.</exception>
        /// <exception cref="InvalidOperationException">The item to add already has a different logical parent. </exception>
        protected override void AddPane(RadDocking radDocking, RadPane pane)
        {
            // Get the main panes group.
            var panesGroup = radDocking.SplitItems.FirstOrDefault(control => control.GetType() == typeof(RadPaneGroup));

            // Add the new pane to the existing pane collection.
            if (panesGroup != null)
            {
                var mainPaneGroup = panesGroup as RadPaneGroup;
                mainPaneGroup?.Items.Add(pane);
            }
            else
            {
                base.AddPane(radDocking, pane);
            }
        }
    }
}