﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="NounsPlayer.cs" company="None">
// Last Modified: 07/10/2018
// Author: Ahmed Ghanam.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace WordsBooklet.UserInterface.Windows
{
    using Telerik.Windows.Controls;

    /// <inheritdoc cref="RadPane" />
    /// <summary>
    ///     Represents the interaction logic for playing the sound streams of nouns headers.
    /// </summary>
    /// <seealso cref="RadPane" />
    public partial class NounsPlayer
    {
        /// <inheritdoc cref="RadPane" />
        /// <summary>
        ///     Initializes a new instance of the <see cref="NounsPlayer" /> class.
        /// </summary>
        public NounsPlayer()
        {
            this.InitializeComponent();
        }
    }
}