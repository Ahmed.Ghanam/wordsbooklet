﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="VariationsSearch.cs" company="None">
// Last Modified: 13/10/2018
// Author: Ahmed Ghanam.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace WordsBooklet.UserInterface.Windows
{
    using Telerik.Windows.Controls;

    /// <inheritdoc cref="RadWindow" />
    /// <summary>
    ///     Represents the interaction logic for searching about variations headers.
    /// </summary>
    /// <inheritdoc cref="RadWindow" />
    public partial class VariationsSearch
    {
        /// <inheritdoc cref="RadWindow" />
        /// <summary>
        ///     Initializes a new instance of the <see cref="VariationsSearch" /> class.
        /// </summary>
        public VariationsSearch()
        {
            this.InitializeComponent();
        }
    }
}