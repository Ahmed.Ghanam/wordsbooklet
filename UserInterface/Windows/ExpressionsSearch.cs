﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ExpressionsSearch.cs" company="None">
// Last Modified: 12/10/2018
// Author: Ahmed Ghanam.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace WordsBooklet.UserInterface.Windows
{
    using Telerik.Windows.Controls;

    /// <inheritdoc cref="RadWindow" />
    /// <summary>
    ///     Represents the interaction logic for searching about expressions headers.
    /// </summary>
    /// <inheritdoc cref="RadWindow" />
    public partial class ExpressionsSearch
    {
        /// <inheritdoc cref="RadWindow" />
        /// <summary>
        ///     Initializes a new instance of the <see cref="ExpressionsSearch" /> class.
        /// </summary>
        public ExpressionsSearch()
        {
            this.InitializeComponent();
        }
    }
}