﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Major.cs" company="None">
// Last Modified: 04/10/2018
// Author: Ahmed Ghanam.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace WordsBooklet.UserInterface.Windows
{
    using Telerik.Windows.Controls;

    /// <inheritdoc cref="RadWindow" />
    /// <summary>
    ///     Represents the interaction logic for the major window.
    /// </summary>
    public partial class Major
    {
        /// <inheritdoc cref="RadWindow" />
        /// <summary>
        ///     Initializes a new instance of the <see cref="Major" /> class.
        /// </summary>
        public Major()
        {
            this.InitializeComponent();
        }
    }
}