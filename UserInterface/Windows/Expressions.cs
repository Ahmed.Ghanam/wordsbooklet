﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Expressions.cs" company="None">
// Last Modified: 12/10/2018
// Author: Ahmed Ghanam.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace WordsBooklet.UserInterface.Windows
{
    using Telerik.Windows.Controls;

    /// <inheritdoc cref="RadPane" />
    /// <summary>
    ///     Represents the interaction logic for handling the expressions header and details.
    /// </summary>
    /// <seealso cref="RadPane" />
    public partial class Expressions
    {
        /// <inheritdoc cref="RadPane" />
        /// <summary>
        ///     Initializes a new instance of the <see cref="Expressions" /> class.
        /// </summary>
        public Expressions()
        {
            this.InitializeComponent();
        }
    }
}