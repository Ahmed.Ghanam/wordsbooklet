﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Variations.cs" company="None">
// Last Modified: 12/10/2018
// Author: Ahmed Ghanam.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace WordsBooklet.UserInterface.Windows
{
    using Telerik.Windows.Controls;

    /// <inheritdoc cref="RadPane" />
    /// <summary>
    ///     Represents the interaction logic for handling the variations header and details.
    /// </summary>
    /// <seealso cref="RadPane" />
    public partial class Variations
    {
        /// <inheritdoc cref="RadPane" />
        /// <summary>
        ///     Initializes a new instance of the <see cref="Variations" /> class.
        /// </summary>
        public Variations()
        {
            this.InitializeComponent();
        }
    }
}