﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="NounsSearch.cs" company="None">
// Last Modified: 06/10/2018
// Author: Ahmed Ghanam.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace WordsBooklet.UserInterface.Windows
{
    using Telerik.Windows.Controls;

    /// <inheritdoc cref="RadWindow" />
    /// <summary>
    ///     Represents the interaction logic for searching about nouns headers.
    /// </summary>
    /// <inheritdoc cref="RadWindow" />
    public partial class NounsSearch
    {
        /// <inheritdoc cref="RadWindow" />
        /// <summary>
        ///     Initializes a new instance of the <see cref="NounsSearch" /> class.
        /// </summary>
        public NounsSearch()
        {
            this.InitializeComponent();
        }
    }
}