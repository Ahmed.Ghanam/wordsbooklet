﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Wordbooks.cs" company="None">
// Last Modified: 06/10/2018
// Author: Ahmed Ghanam.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace WordsBooklet.UserInterface.Windows
{
    using Telerik.Windows.Controls;

    /// <inheritdoc cref="RadPane" />
    /// <summary>
    ///     Represents the interaction logic for the wordbooks.
    /// </summary>
    /// <seealso cref="RadPane" />
    public partial class Wordbooks
    {
        /// <inheritdoc cref="RadPane" />
        /// <summary>
        ///     Initializes a new instance of the <see cref="Wordbooks" /> class.
        /// </summary>
        public Wordbooks()
        {
            this.InitializeComponent();
        }
    }
}