﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="VariationsPlayer.cs" company="None">
// Last Modified: 13/10/2018
// Author: Ahmed Ghanam.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace WordsBooklet.UserInterface.Windows
{
    using Telerik.Windows.Controls;

    /// <inheritdoc cref="RadPane" />
    /// <summary>
    ///     Represents the interaction logic for playing the sound streams of variations headers.
    /// </summary>
    /// <seealso cref="RadPane" />
    public partial class VariationsPlayer
    {
        /// <inheritdoc cref="RadPane" />
        /// <summary>
        ///     Initializes a new instance of the <see cref="VariationsPlayer" /> class.
        /// </summary>
        public VariationsPlayer()
        {
            this.InitializeComponent();
        }
    }
}