﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Verbs.cs" company="None">
// Last Modified: 11/10/2018
// Author: Ahmed Ghanam.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace WordsBooklet.UserInterface.Windows
{
    using Telerik.Windows.Controls;

    /// <inheritdoc cref="RadPane" />
    /// <summary>
    ///     Represents the interaction logic for handling the verbs header and details.
    /// </summary>
    /// <seealso cref="RadPane" />
    public partial class Verbs
    {
        /// <inheritdoc cref="RadPane" />
        /// <summary>
        ///     Initializes a new instance of the <see cref="Verbs" /> class.
        /// </summary>
        public Verbs()
        {
            this.InitializeComponent();
        }
    }
}