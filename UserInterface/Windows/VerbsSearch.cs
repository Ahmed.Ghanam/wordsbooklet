﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="VerbsSearch.cs" company="None">
// Last Modified: 11/10/2018
// Author: Ahmed Ghanam.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace WordsBooklet.UserInterface.Windows
{
    using Telerik.Windows.Controls;

    /// <inheritdoc cref="RadWindow" />
    /// <summary>
    ///     Represents the interaction logic for searching about verbs headers.
    /// </summary>
    /// <inheritdoc cref="RadWindow" />
    public partial class VerbsSearch
    {
        /// <inheritdoc cref="RadWindow" />
        /// <summary>
        ///     Initializes a new instance of the <see cref="VerbsSearch" /> class.
        /// </summary>
        public VerbsSearch()
        {
            this.InitializeComponent();
        }
    }
}