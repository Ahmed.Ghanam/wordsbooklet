﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ExpressionsPlayer.cs" company="None">
// Last Modified: 12/10/2018
// Author: Ahmed Ghanam.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace WordsBooklet.UserInterface.Windows
{
    using Telerik.Windows.Controls;

    /// <inheritdoc cref="RadPane" />
    /// <summary>
    ///     Represents the interaction logic for playing the sound streams of expressions headers.
    /// </summary>
    /// <seealso cref="RadPane" />
    public partial class ExpressionsPlayer
    {
        /// <inheritdoc cref="RadPane" />
        /// <summary>
        ///     Initializes a new instance of the <see cref="ExpressionsPlayer" /> class.
        /// </summary>
        public ExpressionsPlayer()
        {
            this.InitializeComponent();
        }
    }
}