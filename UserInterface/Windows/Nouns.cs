﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Nouns.cs" company="None">
// Last Modified: 06/10/2018
// Author: Ahmed Ghanam.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace WordsBooklet.UserInterface.Windows
{
    using Telerik.Windows.Controls;

    /// <inheritdoc cref="RadPane" />
    /// <summary>
    ///     Represents the interaction logic for the nouns header.
    /// </summary>
    /// <seealso cref="RadPane" />
    public partial class Nouns
    {
        /// <inheritdoc cref="RadPane" />
        /// <summary>
        ///     Initializes a new instance of the <see cref="Nouns" /> class.
        /// </summary>
        public Nouns()
        {
            this.InitializeComponent();
        }
    }
}