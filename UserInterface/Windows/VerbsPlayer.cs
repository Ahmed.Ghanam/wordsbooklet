﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="VerbsPlayer.cs" company="None">
// Last Modified: 11/10/2018
// Author: Ahmed Ghanam.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace WordsBooklet.UserInterface.Windows
{
    using Telerik.Windows.Controls;

    /// <inheritdoc cref="RadPane" />
    /// <summary>
    ///     Represents the interaction logic for playing the sound streams of verbs headers.
    /// </summary>
    /// <seealso cref="RadPane" />
    public partial class VerbsPlayer
    {
        /// <inheritdoc cref="RadPane" />
        /// <summary>
        ///     Initializes a new instance of the <see cref="VerbsPlayer" /> class.
        /// </summary>
        public VerbsPlayer()
        {
            this.InitializeComponent();
        }
    }
}