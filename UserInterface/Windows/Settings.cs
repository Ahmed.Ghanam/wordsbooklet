﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Settings.cs" company="None">
// Last Modified: 10/10/2018
// Author: Ahmed Ghanam.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace WordsBooklet.UserInterface.Windows
{
    using Telerik.Windows.Controls;

    /// <inheritdoc cref="RadPane" />
    /// <summary>
    ///     Represents the interaction logic for the system settings.
    /// </summary>
    /// <seealso cref="RadPane" />
    public partial class Settings
    {
        /// <inheritdoc cref="RadPane" />
        /// <summary>
        ///     Initializes a new instance of the <see cref="Settings" /> class.
        /// </summary>
        public Settings()
        {
            this.InitializeComponent();
        }
    }
}