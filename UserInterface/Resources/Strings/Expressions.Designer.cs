﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace WordsBooklet.UserInterface.Resources.Strings {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "15.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    public class Expressions {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal Expressions() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("WordsBooklet.UserInterface.Resources.Strings.Expressions", typeof(Expressions).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to تعبير عربي.
        /// </summary>
        public static string ArabicExpressions {
            get {
                return ResourceManager.GetString("ArabicExpressions", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to عدد التعبيرات.
        /// </summary>
        public static string ExpressionsCount {
            get {
                return ResourceManager.GetString("ExpressionsCount", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to معلومات التعبيرات.
        /// </summary>
        public static string ExpressionsInformation {
            get {
                return ResourceManager.GetString("ExpressionsInformation", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to تعبير نرويجي.
        /// </summary>
        public static string NorwegianExpressions {
            get {
                return ResourceManager.GetString("NorwegianExpressions", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to تحديث بيانات التعبيرات.
        /// </summary>
        public static string ReloadExpressionsHeaders {
            get {
                return ResourceManager.GetString("ReloadExpressionsHeaders", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to البحث عن التعبيرات.
        /// </summary>
        public static string SearchingExpressions {
            get {
                return ResourceManager.GetString("SearchingExpressions", resourceCulture);
            }
        }
    }
}
