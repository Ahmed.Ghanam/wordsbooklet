﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="VerbsDetailViewModel.cs" company="None">
// Last Modified: 10/10/2018
// Author: Ahmed Ghanam.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace WordsBooklet.BusinessLayer.ViewModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.Diagnostics;
    using System.IO;
    using System.Linq;
    using System.Media;
    using System.Reflection;
    using System.Threading;
    using System.Threading.Tasks;
    using System.Windows;

    using WordsBooklet.BusinessLayer.Infrastructure;
    using WordsBooklet.BusinessLayer.Models;

    /// <inheritdoc />
    /// <summary>
    ///     Handling the <seealso cref="VerbsDetail" /> data model.
    /// </summary>
    /// <seealso cref="NotificationsLayer{T}" />
    public sealed class VerbsDetailViewModel : NotificationsLayer<VerbsDetail>
    {
        /// <summary>
        ///     Represents the properties names of the changed verbs.
        /// </summary>
        [NotNull]
        private readonly List<string> changedVerbsList = new List<string>();

        /// <summary>
        ///     The cognitive services instance.
        /// </summary>
        [CanBeNull]
        private CognitiveServices cognitiveServices;

        /// <summary>
        ///     Represents the <see langword="public" /> properties of current data Model.
        /// </summary>
        [CanBeNull]
        private PropertyInfo[] modelProperties;

        /// <summary>
        ///     Represents the current data Model type.
        /// </summary>
        [CanBeNull]
        private Type modelType;

        /// <inheritdoc />
        /// <summary>
        ///     Initializes a new instance of the <see cref="VerbsDetailViewModel" /> class.
        /// </summary>
        public VerbsDetailViewModel()
        {
        }

        /// <inheritdoc />
        /// <summary>
        ///     Initializes a new instance of the <see cref="VerbsDetailViewModel" /> class.
        /// </summary>
        /// <param name="verbsDetail">The verbs detail to be set as the inner Model.</param>
        internal VerbsDetailViewModel([NotNull] VerbsDetail verbsDetail)
        {
            this.SetInnerModel(verbsDetail);
        }

        /// <summary>
        ///     Gets or sets the character set of verb in infinitive using the Arabic language.
        /// </summary>
        /// <value>
        ///     The character set of verb in infinitive using the Arabic language.
        /// </value>
        [CanBeNull]
        public string InfinitiveArabicCharacterSet
        {
            get => this.Model.InfinitiveArabicCharacterSet;
            set
            {
                this.Model.InfinitiveArabicCharacterSet = value;

                this.OnPropertyChanged(
                    this.Model.InfinitiveArabicCharacterSet,
                    nameof(this.Model.InfinitiveArabicCharacterSet));
            }
        }

        /// <summary>
        ///     Gets or sets the character set of verb in infinitive using the Norwegian (Bokmål) language.
        /// </summary>
        /// <value>
        ///     The character set of verb in infinitive using the Norwegian (Bokmål) language.
        /// </value>
        [CanBeNull]
        public string InfinitiveNorwegianCharacterSet
        {
            get => this.Model.InfinitiveNorwegianCharacterSet;
            set
            {
                this.Model.InfinitiveNorwegianCharacterSet = value;

                this.OnPropertyChanged(
                    this.Model.InfinitiveNorwegianCharacterSet,
                    nameof(this.Model.InfinitiveNorwegianCharacterSet));
            }
        }

        /// <summary>
        ///     Gets or sets the character set of the example using the Norwegian (Bokmål) language.
        /// </summary>
        /// <value>
        ///     The character set of the example using the Norwegian (Bokmål) language.
        /// </value>
        [CanBeNull]
        public string NorwegianExampleCharacterSet
        {
            get => this.Model.NorwegianExampleCharacterSet;
            set
            {
                this.Model.NorwegianExampleCharacterSet = value;

                this.OnPropertyChanged(
                    this.Model.NorwegianExampleCharacterSet,
                    nameof(this.Model.NorwegianExampleCharacterSet));
            }
        }

        /// <summary>
        ///     Gets or sets the character set of verb in past participle using the Arabic language.
        /// </summary>
        /// <value>
        ///     The character set of verb in past participle using the Arabic language.
        /// </value>
        [CanBeNull]
        public string PastParticipleArabicCharacterSet
        {
            get => this.Model.PastParticipleArabicCharacterSet;
            set
            {
                this.Model.PastParticipleArabicCharacterSet = value;

                this.OnPropertyChanged(
                    this.Model.PastParticipleArabicCharacterSet,
                    nameof(this.Model.PastParticipleArabicCharacterSet));
            }
        }

        /// <summary>
        ///     Gets or sets the character set of verb in past participle using the Norwegian (Bokmål) language.
        /// </summary>
        /// <value>
        ///     The character set of verb in past participle using the Norwegian (Bokmål) language.
        /// </value>
        [CanBeNull]
        public string PastParticipleNorwegianCharacterSet
        {
            get => this.Model.PastParticipleNorwegianCharacterSet;
            set
            {
                this.Model.PastParticipleNorwegianCharacterSet = value;

                this.OnPropertyChanged(
                    this.Model.PastParticipleNorwegianCharacterSet,
                    nameof(this.Model.PastParticipleNorwegianCharacterSet));
            }
        }

        /// <summary>
        ///     Gets or sets the character set of verb in present using the Arabic language.
        /// </summary>
        /// <value>
        ///     The character set of verb in present using the Arabic language.
        /// </value>
        [CanBeNull]
        public string PresentArabicCharacterSet
        {
            get => this.Model.PresentArabicCharacterSet;
            set
            {
                this.Model.PresentArabicCharacterSet = value;

                this.OnPropertyChanged(
                    this.Model.PresentArabicCharacterSet,
                    nameof(this.Model.PresentArabicCharacterSet));
            }
        }

        /// <summary>
        ///     Gets or sets the character set of verb in present using the Norwegian (Bokmål) language.
        /// </summary>
        /// <value>
        ///     The character set of verb in present using the Norwegian (Bokmål) language.
        /// </value>
        [CanBeNull]
        public string PresentNorwegianCharacterSet
        {
            get => this.Model.PresentNorwegianCharacterSet;
            set
            {
                this.Model.PresentNorwegianCharacterSet = value;

                this.OnPropertyChanged(
                    this.Model.PresentNorwegianCharacterSet,
                    nameof(this.Model.PresentNorwegianCharacterSet));
            }
        }

        /// <summary>
        ///     Gets or sets the character set of verb in past tense using the Arabic language.
        /// </summary>
        /// <value>
        ///     The character set of verb in past tense using the Arabic language.
        /// </value>
        [CanBeNull]
        public string PreteriteArabicCharacterSet
        {
            get => this.Model.PreteriteArabicCharacterSet;
            set
            {
                this.Model.PreteriteArabicCharacterSet = value;

                this.OnPropertyChanged(
                    this.Model.PreteriteArabicCharacterSet,
                    nameof(this.Model.PreteriteArabicCharacterSet));
            }
        }

        /// <summary>
        ///     Gets or sets the character set of verb in past tense using the Norwegian (Bokmål) language.
        /// </summary>
        /// <value>
        ///     The character set of verb in past tense using the Norwegian (Bokmål) language.
        /// </value>
        [CanBeNull]
        public string PreteriteNorwegianCharacterSet
        {
            get => this.Model.PreteriteNorwegianCharacterSet;
            set
            {
                this.Model.PreteriteNorwegianCharacterSet = value;

                this.OnPropertyChanged(
                    this.Model.PreteriteNorwegianCharacterSet,
                    nameof(this.Model.PreteriteNorwegianCharacterSet));
            }
        }

        /// <summary>
        ///     Gets or sets the pronunciations times to repeat pronunciation of current verb.
        /// </summary>
        /// <value>
        ///     The pronunciations times to repeat pronunciation of current verb.
        /// </value>
        public byte PronunciationTimes
        {
            get => this.Model.PronunciationTimes;
            set
            {
                this.Model.PronunciationTimes = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets the verb serial number in the current verbs collection.
        /// </summary>
        /// <value>
        ///     The verb serial number in the current verbs collection.
        /// </value>
        public short SerialNumber
        {
            get => this.Model.SerialNumber;
            internal set
            {
                this.Model.SerialNumber = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets the speed of the speaking rate.
        /// </summary>
        /// <value>
        ///     The speed of the speaking rate.
        /// </value>
        [NotNull]
        public string SpeakingRate
        {
            get => this.Model.SpeakingRate;
            set
            {
                this.Model.SpeakingRate = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets a value indicating whether <see langword="this" />
        ///     instance should play the sound once it is available or not.
        /// </summary>
        /// <value>
        ///     <see langword="true" /> if <see langword="this" /> instance should
        ///     play the sound once it is available; otherwise, <see langword="false" />.
        /// </value>
        private bool AutoPlaySound { get; set; }

        /// <summary>
        ///     Converts verbs from the inner data Model to sound streams and pronounce them asynchronously.
        /// </summary>
        /// <param name="cognitiveServicesInstance">The cognitive services instance.</param>
        /// <returns>
        ///     A <see cref="Task{TResult}" /> represents the converted data Model.
        /// </returns>
        /// <exception cref="ValidationException">The required verbs are null or empty.</exception>
        [ItemNotNull]
        internal async Task<VerbsDetail> ConvertInnerModelAsync([NotNull] CognitiveServices cognitiveServicesInstance)
        {
            // Initialize the cognitive services.
            if (this.cognitiveServices == null)
            {
                this.cognitiveServices = cognitiveServicesInstance;
            }

            // Convert The Verb In Infinitive Using Arabic To Sound Stream.
            this.Model.InfinitiveArabicPronunciation =
                await this.ConvertInnerModelAsync(
                    Languages.Arabic,
                    nameof(this.Model.InfinitiveArabicCharacterSet),
                    this.Model.InfinitiveArabicCharacterSet,
                    this.Model.InfinitiveArabicPronunciation).ConfigureAwait(false);

            // Validate The Verb In Infinitive Using Arabic.
            if (this.Model.InfinitiveArabicPronunciation.Length == 0)
            {
                throw new ValidationException();
            }

            // Convert The Verb In Infinitive Using Norwegian To Sound Stream.
            this.Model.InfinitiveNorwegianPronunciation =
                await this.ConvertInnerModelAsync(
                    Languages.Bokmål,
                    nameof(this.Model.InfinitiveNorwegianCharacterSet),
                    this.Model.InfinitiveNorwegianCharacterSet,
                    this.Model.InfinitiveNorwegianPronunciation).ConfigureAwait(false);

            // Validate The Verb In Infinitive Using Norwegian.
            if (this.Model.InfinitiveNorwegianPronunciation.Length == 0)
            {
                throw new ValidationException();
            }

            // Convert The Verb In Present Using Arabic To Sound Stream.
            this.Model.PresentArabicPronunciation =
                await this.ConvertInnerModelAsync(
                    Languages.Arabic,
                    nameof(this.Model.PresentArabicCharacterSet),
                    this.Model.PresentArabicCharacterSet,
                    this.Model.PresentArabicPronunciation).ConfigureAwait(false);

            // Convert The Verb In Present Using Norwegian To Sound Stream.
            this.Model.PresentNorwegianPronunciation =
                await this.ConvertInnerModelAsync(
                    Languages.Bokmål,
                    nameof(this.Model.PresentNorwegianCharacterSet),
                    this.Model.PresentNorwegianCharacterSet,
                    this.Model.PresentNorwegianPronunciation).ConfigureAwait(false);

            // Convert The Verb In Past Tense Using Arabic To Sound Stream.
            this.Model.PreteriteArabicPronunciation =
                await this.ConvertInnerModelAsync(
                    Languages.Arabic,
                    nameof(this.Model.PreteriteArabicCharacterSet),
                    this.Model.PreteriteArabicCharacterSet,
                    this.Model.PreteriteArabicPronunciation).ConfigureAwait(false);

            // Convert Verb In Past Tense Using Norwegian To Sound Stream.
            this.Model.PreteriteNorwegianPronunciation =
                await this.ConvertInnerModelAsync(
                    Languages.Bokmål,
                    nameof(this.Model.PreteriteNorwegianCharacterSet),
                    this.Model.PreteriteNorwegianCharacterSet,
                    this.Model.PreteriteNorwegianPronunciation).ConfigureAwait(false);

            // Convert The Verb In Past Participate Using Arabic To Sound Stream.
            this.Model.PastParticipleArabicPronunciation =
                await this.ConvertInnerModelAsync(
                    Languages.Bokmål,
                    nameof(this.Model.PastParticipleArabicCharacterSet),
                    this.Model.PastParticipleArabicCharacterSet,
                    this.Model.PastParticipleArabicPronunciation).ConfigureAwait(false);

            // Convert The Verb In Past Participate Using Norwegian To Sound Stream.
            this.Model.PastParticipleNorwegianPronunciation =
                await this.ConvertInnerModelAsync(
                    Languages.Bokmål,
                    nameof(this.Model.PastParticipleNorwegianCharacterSet),
                    this.Model.PastParticipleNorwegianCharacterSet,
                    this.Model.PastParticipleNorwegianPronunciation).ConfigureAwait(false);

            // Convert The Norwegian Example To Sound Stream.
            this.Model.NorwegianExamplePronunciation =
                await this.ConvertInnerModelAsync(
                    Languages.Bokmål,
                    nameof(this.Model.NorwegianExampleCharacterSet),
                    this.Model.NorwegianExampleCharacterSet,
                    this.Model.NorwegianExamplePronunciation).ConfigureAwait(false);

            // Return the inner Model holding all filled properties.
            return this.Model;
        }

        /// <summary>
        ///     Determines whether the user confirmed to delete the inner Model or not.
        /// </summary>
        /// <returns>
        ///     <see langword="true" /> if the user confirmed to delete the inner Model; otherwise, <see langword="false" />.
        /// </returns>
        internal bool IsUserConfirmedDeletingModel()
        {
            // Holds the value of the confirmation.
            var isDeletingConfirmed = false;

            // Ask the user to confirm deleting the inner Model using the main thread.
            Application.Current.Dispatcher.Invoke(
                () => { isDeletingConfirmed = this.IsUserConfirmedDeletingModel(this.Model); });

            // Return the value of the confirmation.
            return isDeletingConfirmed;
        }

        /// <summary>
        ///     Tests the speaking rate of the inner model asynchronously.
        /// </summary>
        /// <param name="cognitiveServicesInstance">The cognitive services instance.</param>
        /// <returns>
        ///     A <see cref="Task" /> represents the testing operation.
        /// </returns>
        /// <exception cref="ValidationException">The required verbs are null or empty.</exception>
        internal async Task TestSpeakingRateAsync([NotNull] CognitiveServices cognitiveServicesInstance)
        {
            // Auto play the sound once results are available.
            this.AutoPlaySound = true;

            try
            {
                // Convert the inner Model to a sound streams if its not converted, and play the sound stream.
                await this.ConvertInnerModelAsync(cognitiveServicesInstance).ConfigureAwait(false);
            }
            finally
            {
                // Stop auto playing the sound once results are available.
                this.AutoPlaySound = false;
            }
        }

        /// <inheritdoc />
        /// <summary>
        ///     Releases unmanaged and - optionally - managed resources.
        /// </summary>
        /// <param name="disposing">
        ///     <see langword="true" /> to release both managed and unmanaged resources; <see langword="false" /> to release only
        ///     unmanaged resources.
        /// </param>
        /// <exception cref="AggregateException">
        ///     An aggregate exception containing all the exceptions thrown by the registered
        ///     callbacks on the associated <see cref="CancellationToken" />.
        /// </exception>
        protected override void Dispose(bool disposing)
        {
            // Call base implementation, Always first.
            base.Dispose(disposing);

            if (!disposing)
            {
                return;
            }

            // Clear Model type.
            this.modelType = null;

            // Clear Model properties collection.
            this.modelProperties = null;

            // Remove the value of cognitive services instance.
            this.cognitiveServices = null;

            // Clear the changed properties names list.
            this.changedVerbsList.Clear();
        }

        /// <inheritdoc />
        /// <summary>
        ///     Called when a property value changed.
        /// </summary>
        /// <param name="sender">The <paramref name="sender" />.</param>
        /// <param name="e">The <see cref="PropertyChangedEventArgs" /> instance containing the event data.</param>
        protected override void OnPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            switch (e.PropertyName)
            {
                // Changing any verb will result in re-producing its audio stream only.
                case nameof(this.Model.InfinitiveArabicCharacterSet):
                case nameof(this.Model.InfinitiveNorwegianCharacterSet):
                case nameof(this.Model.PresentArabicCharacterSet):
                case nameof(this.Model.PresentNorwegianCharacterSet):
                case nameof(this.Model.PreteriteArabicCharacterSet):
                case nameof(this.Model.PreteriteNorwegianCharacterSet):
                case nameof(this.Model.PastParticipleArabicCharacterSet):
                case nameof(this.Model.PastParticipleNorwegianCharacterSet):
                case nameof(this.Model.NorwegianExampleCharacterSet):

                    // Remove the changed verb from changed verbs list, don't hold empty properties.
                    if (this.changedVerbsList.Contains(e.PropertyName))
                    {
                        this.changedVerbsList.Remove(e.PropertyName);
                    }

                    // Get the value of the changed property.
                    var changedPropertyValue = this.GetPropertyValue(e.PropertyName);

                    // If the changed verb is not null, insert it into the changed verbs list.
                    if (!string.IsNullOrEmpty(changedPropertyValue))
                    {
                        this.changedVerbsList.Add(e.PropertyName);
                    }

                    break;

                // Changing speaking rate will result in re-producing the audio stream for all verbs.
                case nameof(this.Model.SpeakingRate):

                    // Clear the changed verbs list.
                    this.changedVerbsList.Clear();

                    // Get properties names which is a string and not empty or null.
                    var propertiesCarryingValues = this.GetPropertiesNameCarryingValues();

                    // Add properties names to the changed verbs list.
                    this.changedVerbsList.AddRange(propertiesCarryingValues);

                    break;
            }
        }

        /// <summary>
        ///     Converts the passed verb from string to a sound stream and play it asynchronously.
        /// </summary>
        /// <param name="speakingLanguage">The speaking language.</param>
        /// <param name="targetPropertyName">The verb property name.</param>
        /// <param name="targetPropertyValue">The verb property value.</param>
        /// <param name="currentSoundStreamBytes">The current sound stream.</param>
        /// <returns>
        ///     A <see cref="Task{TResult}" /> represents the sound stream as a byte array.
        /// </returns>
        private async Task<byte[]> ConvertInnerModelAsync(
            Languages speakingLanguage,
            [NotNull] string targetPropertyName,
            [NotNull] string targetPropertyValue,
            [NotNull] byte[] currentSoundStreamBytes)
        {
            // Throw exception if the user canceled the whole task.
            this.CancellationToken.ThrowIfCancellationRequested();

            // Do not convert empty verbs.
            if (string.IsNullOrWhiteSpace(targetPropertyValue) || this.cognitiveServices == null)
            {
                return Array.Empty<byte>();
            }

            // Do not convert not changed verbs.
            if (this.changedVerbsList.Contains(targetPropertyName))
            {
                // Start measuring the pronouncing time.
                var pronouncingElapsedTime = Stopwatch.StartNew();

                // Build and send a pronunciation request.
                await this.cognitiveServices
                    .ConvertToSoundAsync(targetPropertyValue.Trim(), this.Model.SpeakingRate, speakingLanguage)
                    .ContinueWith(
                        async conversionTask =>
                            {
                                // Stop measuring the pronouncing time.
                                pronouncingElapsedTime.Stop();

                                // Hold the conversion results.
                                currentSoundStreamBytes = conversionTask.Result;

                                // If no conversion results, return.
                                if (currentSoundStreamBytes.Length == 0)
                                {
                                    return;
                                }

                                // Remove the converted verb from the changed verbs collection.
                                this.changedVerbsList.Remove(targetPropertyName);

                                // Measuring the total pronouncing time opposite the ability to pronounce 40 words per minute.
                                var waitingMilliseconds = (int)(1500 - pronouncingElapsedTime.ElapsedMilliseconds);

                                // Applying the rule 'User can only pronounce 40 words per minute'.
                                await Task.Delay(
                                    waitingMilliseconds > 0 ? waitingMilliseconds : 0,
                                    this.CancellationToken).ConfigureAwait(false);
                            },
                        this.CancellationToken,
                        TaskContinuationOptions.OnlyOnRanToCompletion,
                        TaskScheduler.Current).Unwrap().ConfigureAwait(false);
            }

            // Return the conversion results if the user did not request playing sounds.
            if (!this.AutoPlaySound)
            {
                return currentSoundStreamBytes;
            }

            // Play the sound if the user requested playing sounds and it is not empty.
            if (currentSoundStreamBytes.Length > 0)
            {
                await this.PlaySoundStreamAsync(currentSoundStreamBytes).ConfigureAwait(false);
            }

            // Return the conversion results.
            return currentSoundStreamBytes;
        }

        /// <summary>
        ///     Gets properties names which carrying values.
        /// </summary>
        /// <returns>
        ///     A <see cref="IEnumerable{T}" /> holding properties names.
        /// </returns>
        [NotNull]
        private IEnumerable<string> GetPropertiesNameCarryingValues()
        {
            // Initialize the Model type.
            if (this.modelType == null)
            {
                this.modelType = this.Model.GetType();
            }

            // Initialize the public Model properties.
            if (this.modelProperties == null)
            {
                this.modelProperties = this.modelType.GetProperties();
            }

            // Return every property matching rules.
            return (from modelPropertyInfo in this.modelProperties
                    where modelPropertyInfo.CanWrite && modelPropertyInfo.PropertyType == typeof(string)
                                                     && !string.IsNullOrWhiteSpace(
                                                         Convert.ToString(modelPropertyInfo.GetValue(this.Model)))
                                                     && !Convert.ToString(modelPropertyInfo.GetValue(this.Model))
                                                         .Contains("%")
                    select modelPropertyInfo.Name).ToList();
        }

        /// <summary>
        ///     Gets the value of the passed property name.
        /// </summary>
        /// <param name="propertyName">The property name.</param>
        /// <returns>
        ///     The value of the passed property name.
        /// </returns>
        [NotNull]
        private string GetPropertyValue([NotNull] string propertyName)
        {
            // Initialize the Model type.
            if (this.modelType == null)
            {
                this.modelType = this.Model.GetType();
            }

            // Get property value using reflection.
            var matchedPropertyInfo = this.modelType.GetProperty(propertyName);
            var valueOfMatchedPropertyInfo = matchedPropertyInfo?.GetValue(this.Model);
            return Convert.ToString(valueOfMatchedPropertyInfo);
        }

        /// <summary>
        ///     Plays the passed sound stream asynchronously.
        /// </summary>
        /// <param name="verbAsByteArray">The verb as <see langword="byte" /> array.</param>
        /// <returns>
        ///     A <see cref="Task" /> represent the playing operation.
        /// </returns>
        [NotNull]
        private Task PlaySoundStreamAsync([NotNull] byte[] verbAsByteArray)
        {
            // Throw exception if the user canceled the whole conversion task.
            this.CancellationToken.ThrowIfCancellationRequested();

            // Do not play the sound if the stream is empty.
            if (verbAsByteArray.Length == 0)
            {
                return Task.CompletedTask;
            }

            // Play the sound using a separate task.
            return Task.Factory.StartNew(
                () =>
                    {
                        using (var soundPlayer = new SoundPlayer(new MemoryStream(verbAsByteArray)))
                        {
                            soundPlayer.PlaySync();
                        }
                    },
                this.CancellationToken);
        }

        /// <summary>
        ///     Sets the inner Model using the passed VerbsDetail.
        /// </summary>
        /// <param name="verbsDetail">The verbs detail.</param>
        private void SetInnerModel([NotNull] VerbsDetail verbsDetail)
        {
            // Sets the inner Model.
            this.Model = verbsDetail;

            // Notify both the user interface and changed verbs list.
            this.OnPropertyChanged(string.Empty);
        }
    }
}