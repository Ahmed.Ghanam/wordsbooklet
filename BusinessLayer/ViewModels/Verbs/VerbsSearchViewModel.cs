﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="VerbsSearchViewModel.cs" company="None">
// Last Modified: 11/10/2018
// Author: Ahmed Ghanam.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace WordsBooklet.BusinessLayer.ViewModels
{
    using System;
    using System.Collections.ObjectModel;
    using System.Data.Entity;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Input;

    using WordsBooklet.BusinessLayer.Infrastructure;
    using WordsBooklet.BusinessLayer.Infrastructure.Notifications.Resources;
    using WordsBooklet.BusinessLayer.Models;

    /// <inheritdoc />
    /// <summary>
    ///     Searching about a <seealso cref="VerbsHeader" /> for purposes of selecting or deleting.
    /// </summary>
    /// <seealso cref="NotificationsLayer{T}" />
    public sealed class VerbsSearchViewModel : NotificationsLayer<VerbsHeader>
    {
        /// <summary>
        ///     The back field for the <see cref="DialogResult" /> property.
        /// </summary>
        private bool? dialogResult;

        /// <summary>
        ///     The back field for the <see cref="NoResults" /> property.
        /// </summary>
        private bool noResults;

        /// <summary>
        ///     The back field for the <see cref="SearchEndDate" /> property.
        /// </summary>
        [CanBeNull]
        private DateTime? searchEndDate;

        /// <summary>
        ///     The back field for the <see cref="SearchName" /> property.
        /// </summary>
        [CanBeNull]
        private string searchName;

        /// <summary>
        ///     The back field for the <see cref="SearchResults" /> property.
        /// </summary>
        [CanBeNull]
        private ObservableCollection<VerbsHeader> searchResults;

        /// <summary>
        ///     The back field for the <see cref="SearchStartDate" /> property.
        /// </summary>
        [CanBeNull]
        private DateTime? searchStartDate;

        /// <summary>
        ///     The back field for the <see cref="SearchWordbook" /> property.
        /// </summary>
        [CanBeNull]
        private Wordbook searchWordbook;

        /// <summary>
        ///     The back field for the <see cref="VerbsWordbooks" /> property.
        /// </summary>
        [CanBeNull]
        private ObservableCollection<Wordbook> verbsWordbooks;

        /// <summary>
        ///     Gets a command to delete a verbs header and details asynchronously.
        /// </summary>
        /// <value>
        ///     The command to delete a verbs header and details asynchronously.
        /// </value>
        [NotNull]
        public AsyncCommand DeleteVerbsHeaderCommand => new AsyncCommand(this.DeleteNounsHeaderAsync);

        /// <summary>
        ///     Gets or sets the DialogResult value for displaying <see langword="this" /> instance as a dialog.
        /// </summary>
        /// <value>
        ///     The DialogResult value for displaying <see langword="this" /> instance as a dialog.
        /// </value>
        public bool? DialogResult
        {
            get => this.dialogResult;

            set
            {
                this.dialogResult = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets a command to load the Wordbooks which can hold verbs from the data source asynchronously.
        /// </summary>
        /// <value>
        ///     The command to load the Wordbooks which can hold verbs from the data source asynchronously.
        /// </value>
        [NotNull]
        public AsyncCommand LoadWordbooksForVerbsCommand => new AsyncCommand(this.LoadWordbooksForVerbsAsync);

        /// <summary>
        ///     Gets a value indicating whether there is matches to the search or not.
        /// </summary>
        /// <value>
        ///     <see langword="false" /> if there is matches to the search; otherwise, <see langword="true" />.
        /// </value>
        public bool NoResults
        {
            get => this.noResults;

            private set
            {
                this.noResults = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets a command to return the identifier of the selected verbs header and to close the current dialog.
        /// </summary>
        /// <value>
        ///     The command to return the identifier of the selected verbs header and to close the current dialog.
        /// </value>
        [NotNull]
        public ICommand ReturnSelectedIdentifierCommand => new RelayCommand(this.ReturnSelectedIdentifier);

        /// <summary>
        ///     Gets or sets the end date for searching.
        /// </summary>
        /// <value>
        ///     The end date for searching.
        /// </value>
        [CanBeNull]
        public DateTime? SearchEndDate
        {
            get => this.searchEndDate;

            set
            {
                this.searchEndDate = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets the name for searching.
        /// </summary>
        /// <value>
        ///     The name for searching.
        /// </value>
        [CanBeNull]
        public string SearchName
        {
            get => this.searchName;

            set
            {
                this.searchName = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets the search results of using the search inputs
        /// </summary>
        /// <value>
        ///     The search results of using the search inputs.
        /// </value>
        [CanBeNull]
        public ObservableCollection<VerbsHeader> SearchResults
        {
            get => this.searchResults;

            private set
            {
                this.searchResults = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets the start date for searching.
        /// </summary>
        /// <value>
        ///     The start date for searching.
        /// </value>
        [CanBeNull]
        public DateTime? SearchStartDate
        {
            get => this.searchStartDate;

            set
            {
                this.searchStartDate = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets a command to display the search results using the search inputs.
        /// </summary>
        /// <value>
        ///     The command to display the search results using the search inputs.
        /// </value>
        [NotNull]
        public AsyncCommand SearchVerbsHeadersCommand => new AsyncCommand(this.SearchVerbsHeadersAsync);

        /// <summary>
        ///     Gets or sets the Wordbook for searching.
        /// </summary>
        /// <value>
        ///     The Wordbook for searching.
        /// </value>
        [CanBeNull]
        public Wordbook SearchWordbook
        {
            get => this.searchWordbook;

            set
            {
                this.searchWordbook = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets the wordbooks which can hold verbs which stored into the data source.
        /// </summary>
        /// <value>
        ///     The wordbooks which can hold verbs which stored into the data source.
        /// </value>
        [CanBeNull]
        public ObservableCollection<Wordbook> VerbsWordbooks
        {
            get => this.verbsWordbooks;

            private set
            {
                this.verbsWordbooks = value;
                this.OnPropertyChanged();
            }
        }

        /// <inheritdoc />
        /// <summary>
        ///     Releases unmanaged and - optionally - managed resources.
        /// </summary>
        /// <param name="disposing">
        ///     <see langword="true" /> to release both managed and unmanaged resources; <see langword="false" /> to release only
        ///     unmanaged resources.
        /// </param>
        /// <exception cref="AggregateException">
        ///     An aggregate exception containing all the exceptions thrown by the registered
        ///     callbacks on the associated <see cref="CancellationToken" />.
        /// </exception>
        protected override void Dispose(bool disposing)
        {
            // Call base implementation, Always first.
            base.Dispose(disposing);

            if (!disposing)
            {
                return;
            }

            // Dispose related commands.
            this.DeleteVerbsHeaderCommand.Dispose();
            this.SearchVerbsHeadersCommand.Dispose();
            this.LoadWordbooksForVerbsCommand.Dispose();
        }

        /// <summary>
        ///     Builds the query to search for verbs headers.
        /// </summary>
        /// <returns>
        ///     A <seealso cref="IQueryable{T}" /> represents the search query.
        /// </returns>
        [NotNull]
        private IQueryable<VerbsHeader> BuildSearchQuery()
        {
            // By default select all verbs headers.
            var searchingQuery = from verbsHeaders in this.DataEntities.VerbsHeaders select verbsHeaders;

            // Add searching by the verb category.
            if (this.SearchWordbook != null)
            {
                searchingQuery =
                    searchingQuery.Where(verbsHeaders => verbsHeaders.WordbookId == this.SearchWordbook.Id);
            }

            // Add searching by the name.
            if (!string.IsNullOrWhiteSpace(this.SearchName))
            {
                searchingQuery = searchingQuery.Where(verbsHeaders => verbsHeaders.Name.Contains(this.SearchName));
            }

            // Add searching by the start date for the creation date.
            if (this.SearchStartDate.HasValue)
            {
                searchingQuery = searchingQuery.Where(
                    verbsHeaders => verbsHeaders.CreationDateTime.Date >= this.SearchStartDate.Value.Date);
            }

            // Add searching by the end date for the creation date.
            if (this.SearchEndDate.HasValue)
            {
                searchingQuery = searchingQuery.Where(
                    verbsHeaders => verbsHeaders.CreationDateTime.Date <= this.SearchEndDate.Value.Date);
            }

            // Return the built select query.
            return searchingQuery;
        }

        /// <summary>
        ///     Deletes a singular verbs header and its details asynchronously.
        /// </summary>
        /// <param name="verbsHeader">The verbs header.</param>
        /// <returns>
        ///     A <see cref="Task" /> represents the deleting operation.
        /// </returns>
        [NotNull]
        private Task DeleteNounsHeaderAsync([NotNull] object verbsHeader)
        {
            // Cast the passed object to a VerbsHeader.
            var verbsHeaderModel = (VerbsHeader)verbsHeader;

            // Ask the user to confirm deleting the data model.
            return this.DeleteAsync(verbsHeaderModel).ContinueWith(
                deletingTask =>
                    {
                        // Remove the deleted VerbsHeader from SearchResults collection.
                        if (deletingTask.Result)
                        {
                            Application.Current.Dispatcher.Invoke(
                                () => { this.SearchResults?.Remove(verbsHeaderModel); });
                        }
                    },
                this.CancellationToken,
                TaskContinuationOptions.OnlyOnRanToCompletion,
                TaskScheduler.Current);
        }

        /// <summary>
        ///     Loads the Wordbooks which can hold verbs from the data source asynchronously.
        /// </summary>
        /// <returns>
        ///     A <see cref="Task" /> represents the loading operation.
        /// </returns>
        private async Task LoadWordbooksForVerbsAsync()
        {
            // Notify the user interface.
            this.LongRunningTaskDescription = NotificationsLayer.PreparingUserInterfaceMessage;
            this.IsPerformingLongRunningTask = true;

            try
            {
                await this.DataEntities.Wordbooks
                    .Where(wordbook => wordbook.Type == (byte)WordbookTypes.Verb && !wordbook.IsFolder)
                    .ToListAsync(this.CancellationToken).ContinueWith(
                        readingTask =>
                            {
                                this.VerbsWordbooks = new ObservableCollection<Wordbook>(readingTask.Result);
                            },
                        this.CancellationToken,
                        TaskContinuationOptions.OnlyOnRanToCompletion,
                        TaskScheduler.Current).ConfigureAwait(false);
            }
            finally
            {
                this.IsPerformingLongRunningTask = false;
                this.LongRunningTaskDescription = string.Empty;
            }
        }

        /// <summary>
        ///     Closes the dialog and return the identifier of the selected VerbsHeader.
        /// </summary>
        /// <param name="verbsHeader">The selected VerbsHeader.</param>
        private void ReturnSelectedIdentifier([CanBeNull] object verbsHeader)
        {
            // Assign the selected verbs header to the generic model property.
            this.Model = (VerbsHeader)verbsHeader ?? throw new InvalidOperationException();

            // Set the dialog result value.
            this.DialogResult = true;

            // Close current dialog and return the identifier.
            DialogService.CloseDialog(this.Model.Id);
        }

        /// <summary>
        ///     Searches for verbs headers using the search inputs asynchronously.
        /// </summary>
        /// <returns>
        ///     A <see cref="Task" /> represents the searching operation.
        /// </returns>
        private async Task SearchVerbsHeadersAsync()
        {
            // Notify the user interface.
            this.LongRunningTaskDescription = NotificationsLayer.SearchingForDataMessage;
            this.IsPerformingLongRunningTask = true;

            try
            {
                // Build the search query and search for data.
                await this.BuildSearchQuery().ToListAsync(this.CancellationToken).ContinueWith(
                    searchingTask =>
                        {
                            this.NoResults = searchingTask.Result.Count == 0;
                            this.SearchResults = searchingTask.Result.Count == 0
                                                     ? null
                                                     : new ObservableCollection<VerbsHeader>(searchingTask.Result);
                        },
                    this.CancellationToken,
                    TaskContinuationOptions.OnlyOnRanToCompletion,
                    TaskScheduler.Current).ConfigureAwait(false);
            }
            finally
            {
                // Notify the user interface.
                this.IsPerformingLongRunningTask = false;
                this.LongRunningTaskDescription = string.Empty;
            }
        }
    }
}