﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="VerbsPlayerViewModel.cs" company="None">
// Last Modified: 11/10/2018
// Author: Ahmed Ghanam.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace WordsBooklet.BusinessLayer.ViewModels
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Collections.Specialized;
    using System.Data.Entity;
    using System.IO;
    using System.Linq;
    using System.Media;
    using System.Threading;
    using System.Threading.Tasks;
    using System.Windows;

    using Microsoft.Win32;

    using WordsBooklet.BusinessLayer.Infrastructure;
    using WordsBooklet.BusinessLayer.Infrastructure.Notifications.Resources;
    using WordsBooklet.BusinessLayer.Models;

    /// <inheritdoc />
    /// <summary>
    ///     Playing and saving the sound of the <seealso cref="VerbsHeader" /> data model.
    /// </summary>
    /// <seealso cref="NotificationsLayer{T}" />
    public class VerbsPlayerViewModel : NotificationsLayer<VerbsHeader>
    {
        /// <summary>
        ///     The back field for the <see cref="BeginUtteringVerbsDetail" /> property.
        /// </summary>
        [CanBeNull]
        private VerbsDetail beginUtteringVerbsDetail;

        /// <summary>
        ///     The back field for the <see cref="IsUtteringPaused" /> property.
        /// </summary>
        private bool isUtteringPaused;

        /// <summary>
        ///     The back field for the <see cref="IsUtteringVerbsDetail" /> property.
        /// </summary>
        private bool isUtteringVerbsDetail;

        /// <summary>
        ///     The back field for the <see cref="StoredVerbsHeaders" /> property.
        /// </summary>
        [CanBeNull]
        private ObservableCollection<VerbsHeader> storedVerbsHeaders;

        /// <summary>
        ///     The back field for the <see cref="UsePastParticipleArabicForm" /> property.
        /// </summary>
        private bool usePastParticipleArabicForm;

        /// <summary>
        ///     The back field for the <see cref="UsePastParticipleNorwegianForm" /> property.
        /// </summary>
        private bool usePastParticipleNorwegianForm;

        /// <summary>
        ///     The back field for the <see cref="UsePresentArabicForm" /> property.
        /// </summary>
        private bool usePresentArabicForm;

        /// <summary>
        ///     The back field for the <see cref="UsePresentNorwegianForm" /> property.
        /// </summary>
        private bool usePresentNorwegianForm;

        /// <summary>
        ///     The back field for the <see cref="UsePreteriteArabicForm" /> property.
        /// </summary>
        private bool usePreteriteArabicForm;

        /// <summary>
        ///     The back field for the <see cref="UsePreteriteNorwegianForm" /> property.
        /// </summary>
        private bool usePreteriteNorwegianForm;

        /// <inheritdoc />
        /// <summary>
        ///     Initializes a new instance of the <see cref="VerbsPlayerViewModel" /> class.
        /// </summary>
        public VerbsPlayerViewModel()
        {
            // Set the SelectedVerbsHeaders collection changed event handler.
            this.SelectedVerbsHeaders.CollectionChanged += this.SelectedVerbsHeadersChangedAsync;
        }

        /// <summary>
        ///     Gets the verbs detail which is being uttered now.
        /// </summary>
        /// <value>
        ///     The verbs detail which is being uttered now.
        /// </value>
        [CanBeNull]
        public VerbsDetail BeginUtteringVerbsDetail
        {
            get => this.beginUtteringVerbsDetail;

            private set
            {
                this.beginUtteringVerbsDetail = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets a value indicating whether <see langword="this" /> instance is uttering a verbs detail or not.
        /// </summary>
        /// <value>
        ///     <see langword="true" /> if <see langword="this" /> instance is uttering a verbs detail otherwise,
        ///     <see langword="false" />.
        /// </value>
        public bool IsUtteringVerbsDetail
        {
            get => this.isUtteringVerbsDetail;

            private set
            {
                this.isUtteringVerbsDetail = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets a command to load the verbs headers from the data source asynchronously.
        /// </summary>
        /// <value>
        ///     The command to load the verbs headers from the data source asynchronously.
        /// </value>
        [NotNull]
        public AsyncCommand LoadVerbsHeadersCommand => new AsyncCommand(this.LoadVerbsHeadersAsync);

        /// <summary>
        ///     Gets a command to pause uttering the selected verbs details.
        /// </summary>
        /// <value>
        ///     The command to pause uttering the selected verbs details.
        /// </value>
        [NotNull]
        public RelayCommand PauseUtteringCommand =>
            new RelayCommand(
                canExecute => this.IsUtteringVerbsDetail && !this.IsUtteringPaused,
                execute =>
                    {
                        using (var cancellationTokenSource = new CancellationTokenSource())
                        {
                            this.IsUtteringPaused = true;
                            cancellationTokenSource.Cancel();
                            this.CancellationToken = cancellationTokenSource.Token;
                        }
                    });

        /// <summary>
        ///     Gets a command to resume uttering the selected verbs details asynchronously.
        /// </summary>
        /// <value>
        ///     The command to resume uttering the selected verbs details asynchronously.
        /// </value>
        [NotNull]
        public AsyncCommand ResumeUtteringCommand =>
            new AsyncCommand(
                canExecute => this.IsUtteringPaused && !this.IsUtteringVerbsDetail && this.SelectedVerbsDetails.Any(),
                this.UtterVerbsDetailsAsync);

        /// <summary>
        ///     Gets a command to save the selected verbs details as a sound file asynchronously.
        /// </summary>
        /// <value>
        ///     The command to save the selected verbs details as a sound file asynchronously.
        /// </value>
        [NotNull]
        public AsyncCommand SaveAsSoundFileCommand =>
            new AsyncCommand(canExecute => this.SelectedVerbsDetails.Any(), this.SaveAsSoundFileAsync);

        /// <summary>
        ///     Gets the verbs details of the selected verbs headers.
        /// </summary>
        /// <value>
        ///     The verbs details of the selected verbs headers.
        /// </value>
        [NotNull]
        public ObservableCollection<VerbsDetail> SelectedVerbsDetails { get; } =
            new ObservableCollection<VerbsDetail>();

        /// <summary>
        ///     Gets or sets the verbs headers which user has select to utter.
        /// </summary>
        /// <value>
        ///     The verbs headers which user has select to utter.
        /// </value>
        [NotNull]
        public ObservableCollection<VerbsHeader> SelectedVerbsHeaders { get; set; } =
            new ObservableCollection<VerbsHeader>();

        /// <summary>
        ///     Gets the verbs headers stored into the data source.
        /// </summary>
        /// <value>
        ///     The verbs headers stored into the data source.
        /// </value>
        [CanBeNull]
        public ObservableCollection<VerbsHeader> StoredVerbsHeaders
        {
            get => this.storedVerbsHeaders;

            private set
            {
                this.storedVerbsHeaders = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets a value indicating whether the Arabic past participle form of the verb is used or not.
        /// </summary>
        /// <value>
        ///     <see langword="true" /> if the Arabic past participle form of the verb is used; otherwise, <see langword="false" />
        ///     .
        /// </value>
        public bool UsePastParticipleArabicForm
        {
            get => this.usePastParticipleArabicForm;
            set
            {
                this.usePastParticipleArabicForm = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets a value indicating whether the Norwegian past participle form of the verb is used or not.
        /// </summary>
        /// <value>
        ///     <see langword="true" /> if the Norwegian past participle form of the verb is used; otherwise,
        ///     <see langword="false" />.
        /// </value>
        public bool UsePastParticipleNorwegianForm
        {
            get => this.usePastParticipleNorwegianForm;
            set
            {
                this.usePastParticipleNorwegianForm = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets a value indicating whether the Arabic present form of the verb is used or not.
        /// </summary>
        /// <value>
        ///     <see langword="true" /> if the Arabic present form of the verb is used; otherwise, <see langword="false" />.
        /// </value>
        public bool UsePresentArabicForm
        {
            get => this.usePresentArabicForm;
            set
            {
                this.usePresentArabicForm = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets a value indicating whether the Norwegian present form of the verb is used or not.
        /// </summary>
        /// <value>
        ///     <see langword="true" /> if the Norwegian present form of the verb is used; otherwise, <see langword="false" />.
        /// </value>
        public bool UsePresentNorwegianForm
        {
            get => this.usePresentNorwegianForm;
            set
            {
                this.usePresentNorwegianForm = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets a value indicating whether the Arabic past tense form of the verb is used or not.
        /// </summary>
        /// <value>
        ///     <see langword="true" /> if the Arabic past tense form of the verb is used; otherwise, <see langword="false" />.
        /// </value>
        public bool UsePreteriteArabicForm
        {
            get => this.usePreteriteArabicForm;
            set
            {
                this.usePreteriteArabicForm = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets a value indicating whether the Norwegian past tense form of the verb is used or not.
        /// </summary>
        /// <value>
        ///     <see langword="true" /> if the Norwegian past tense form of the verb is used; otherwise, <see langword="false" />.
        /// </value>
        public bool UsePreteriteNorwegianForm
        {
            get => this.usePreteriteNorwegianForm;
            set
            {
                this.usePreteriteNorwegianForm = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets a command to utter the selected verbs details from beginning asynchronously.
        /// </summary>
        /// <value>
        ///     The command to utter the selected verbs details from beginning asynchronously.
        /// </value>
        [NotNull]
        public AsyncCommand UtterVerbsDetailsCommand =>
            new AsyncCommand(
                canExecute => !this.IsUtteringVerbsDetail && this.SelectedVerbsDetails.Any(),
                this.UtterVerbsDetailsFromBeginningAsync);

        /// <summary>
        ///     Gets or sets the cancellation token for controlling tasks.
        /// </summary>
        /// <value>
        ///     The cancellation token for controlling tasks.
        /// </value>
        private new CancellationToken CancellationToken { get; set; }

        /// <summary>
        ///     Gets or sets a value indicating whether the user has paused the uttering or not.
        /// </summary>
        /// <value>
        ///     <see langword="true" /> if the user has paused the uttering; otherwise, <see langword="false" />.
        /// </value>
        private bool IsUtteringPaused
        {
            get => this.isUtteringPaused;

            set
            {
                this.isUtteringPaused = value;
                this.OnPropertyChanged();
            }
        }

        /// <inheritdoc />
        /// <summary>
        ///     Releases unmanaged and - optionally - managed resources.
        /// </summary>
        /// <param name="disposing">
        ///     <see langword="true" /> to release both managed and unmanaged resources; <see langword="false" /> to release only
        ///     unmanaged resources.
        /// </param>
        /// <exception cref="ObjectDisposedException">This <see cref="CancellationTokenSource" /> has been disposed.</exception>
        /// <exception cref="AggregateException">
        ///     An aggregate exception containing all the exceptions thrown by the registered
        ///     callbacks on the associated <see cref="CancellationToken" />.
        /// </exception>
        protected override void Dispose(bool disposing)
        {
            // Call base implementation, Always first.
            base.Dispose(disposing);

            if (!disposing)
            {
                return;
            }

            using (var cancellationTokenSource = new CancellationTokenSource())
            {
                cancellationTokenSource.Cancel();
                this.CancellationToken = cancellationTokenSource.Token;
            }

            // Dispose related commands.
            this.ResumeUtteringCommand.Dispose();
            this.SaveAsSoundFileCommand.Dispose();
            this.LoadVerbsHeadersCommand.Dispose();
            this.UtterVerbsDetailsCommand.Dispose();

            // Remove event listeners and clear the verbs detail collection.
            this.SelectedVerbsHeaders.CollectionChanged -= this.SelectedVerbsHeadersChangedAsync;
        }

        /// <summary>
        ///     Creates a sound file using the <see langword="byte" /> arrays of the selected verbs details asynchronously.
        /// </summary>
        /// <param name="soundFilePath">The sound file path.</param>
        /// <returns>
        ///     A <see cref="Task" /> represents the creation operation.
        /// </returns>
        [NotNull]
        private Task CreateSoundFileAsync([NotNull] string soundFilePath)
        {
            return Task.Factory.StartNew(
                () =>
                    {
                        // Compress all sounds streams into a single array.
                        var soundFilesStreams = new List<byte[]>();
                        foreach (var verbsListDetail in this.SelectedVerbsDetails)
                        {
                            soundFilesStreams.Add(verbsListDetail.GetSoundStreams());
                        }

                        // Save byte array as sound file (.WAV).
                        var soundFileWriter = new SoundFilesWriter();
                        soundFileWriter.CreateSoundFile(soundFilePath, soundFilesStreams);
                    },
                base.CancellationToken);
        }

        /// <summary>
        ///     Loads the verbs headers from data source asynchronously.
        /// </summary>
        /// <returns>
        ///     A <see cref="Task" /> represents the loading operation.
        /// </returns>
        private async Task LoadVerbsHeadersAsync()
        {
            // Notify the user interface.
            this.LongRunningTaskDescription = NotificationsLayer.PreparingUserInterfaceMessage;
            this.IsPerformingLongRunningTask = true;

            try
            {
                await this.DataEntities.VerbsHeaders.ToListAsync(base.CancellationToken).ContinueWith(
                    loadingTask =>
                        {
                            this.StoredVerbsHeaders = new ObservableCollection<VerbsHeader>(loadingTask.Result);
                        },
                    base.CancellationToken,
                    TaskContinuationOptions.OnlyOnRanToCompletion,
                    TaskScheduler.Current).ConfigureAwait(false);
            }
            finally
            {
                this.IsPerformingLongRunningTask = false;
                this.LongRunningTaskDescription = string.Empty;
            }
        }

        /// <summary>
        ///     Plays the passed sound stream as a sound asynchronously.
        /// </summary>
        /// <param name="soundStream">The sound as <see langword="byte" /> array.</param>
        /// <returns>
        ///     A <see cref="Task" /> represent the audio playing operation.
        /// </returns>
        [NotNull]
        private Task PlaySoundStreamAsync([CanBeNull] byte[] soundStream)
        {
            // Throw exception if the user canceled the task.
            this.CancellationToken.ThrowIfCancellationRequested();

            // Return a completed task if the passed stream is null.
            if (soundStream == null)
            {
                return Task.CompletedTask;
            }

            // Return a completed task if the passed stream is empty.
            if (soundStream.Length == 0)
            {
                return Task.CompletedTask;
            }

            // Play the sound using a separate task.
            return Task.Factory.StartNew(
                () =>
                    {
                        using (var soundPlayer = new SoundPlayer(new MemoryStream(soundStream)))
                        {
                            soundPlayer.PlaySync();
                        }
                    },
                this.CancellationToken);
        }

        /// <summary>
        ///     Saves the selected verbs details as a sound file asynchronously.
        /// </summary>
        /// <returns>
        ///     A <see cref="Task" /> represents the saving operation.
        /// </returns>
        private async Task SaveAsSoundFileAsync()
        {
            // Notify the user interface.
            this.IsPerformingLongRunningTask = true;

            // Hold the values of SaveFileDialog.
            bool? saveFileDialogResult = false;
            var saveFileDialogFullName = string.Empty;

            try
            {
                // Display the SaveFileDialog on the main thread.
                Application.Current.Dispatcher.Invoke(
                    () =>
                        {
                            var saveFileDialog = new SaveFileDialog
                                                     {
                                                         ValidateNames = true,
                                                         OverwritePrompt = true,
                                                         CheckPathExists = true,
                                                         RestoreDirectory = true,
                                                         Filter = "wav files (*.wav)|*.wav",
                                                         FileName = this.SelectedVerbsHeaders[0].FullName
                                                                    ?? string.Empty
                                                     };
                            saveFileDialogResult = saveFileDialog.ShowDialog();
                            saveFileDialogFullName = saveFileDialog.FileName;
                        });

                // Create the sound file in a separate task.
                if (saveFileDialogResult == true)
                {
                    await this.CreateSoundFileAsync(saveFileDialogFullName).ConfigureAwait(false);
                }
            }
            finally
            {
                this.IsPerformingLongRunningTask = false;
            }
        }

        /// <summary>
        ///     Occurs when the collection of the selected verbs headers changed.
        /// </summary>
        /// <param name="sender">The <paramref name="sender" />.</param>
        /// <param name="e">The <see cref="NotifyCollectionChangedEventArgs" /> instance containing the event data.</param>
        private async void SelectedVerbsHeadersChangedAsync(
            [NotNull] object sender,
            [NotNull] NotifyCollectionChangedEventArgs e)
        {
            // Handle only the new and the old VerbsHeader.
            if (!((e.NewItems?[0] ?? e.OldItems?[0]) is VerbsHeader verbsHeader))
            {
                return;
            }

            // Cancel resuming sound playing.
            this.IsUtteringPaused = false;

            // Notify the user interface.
            this.IsPerformingLongRunningTask = true;

            try
            {
                await Task.Factory.StartNew(
                    () =>
                        {
                            Application.Current.Dispatcher.Invoke(
                                () =>
                                    {
                                        switch (e.Action)
                                        {
                                            case NotifyCollectionChangedAction.Add:
                                                foreach (var verbsDetail in verbsHeader.VerbsDetails)
                                                {
                                                    this.SelectedVerbsDetails.Add(verbsDetail);
                                                }

                                                break;

                                            case NotifyCollectionChangedAction.Remove:
                                                foreach (var verbsDetail in verbsHeader.VerbsDetails)
                                                {
                                                    this.SelectedVerbsDetails.Remove(verbsDetail);
                                                }

                                                break;
                                        }
                                    });

                            // Handle the displaying of verbs forms.
                            this.SetUsedArabicForms(this.SelectedVerbsDetails);
                            this.SetUsedNorwegianForms(this.SelectedVerbsDetails);
                        },
                    base.CancellationToken,
                    TaskCreationOptions.None,
                    TaskScheduler.Current).ConfigureAwait(false);
            }
            finally
            {
                this.IsPerformingLongRunningTask = false;
            }
        }

        /// <summary>
        ///     Reflects the used Arabic forms to the user.
        /// </summary>
        /// <param name="verbsDetails">The verbs details.</param>
        private void SetUsedArabicForms([NotNull] IReadOnlyCollection<VerbsDetail> verbsDetails)
        {
            this.UsePresentArabicForm = verbsDetails.Any(
                verbsDetail => !string.IsNullOrWhiteSpace(verbsDetail.PresentArabicCharacterSet));
            this.UsePreteriteArabicForm = verbsDetails.Any(
                verbsDetail => !string.IsNullOrWhiteSpace(verbsDetail.PreteriteArabicCharacterSet));
            this.UsePastParticipleArabicForm = verbsDetails.Any(
                verbsDetail => !string.IsNullOrWhiteSpace(verbsDetail.PastParticipleArabicCharacterSet));
        }

        /// <summary>
        ///     Reflects the used Norwegian forms to the user.
        /// </summary>
        /// <param name="verbsDetails">The verbs details.</param>
        private void SetUsedNorwegianForms([NotNull] IReadOnlyCollection<VerbsDetail> verbsDetails)
        {
            this.UsePresentNorwegianForm = verbsDetails.Any(
                verbsDetail => !string.IsNullOrWhiteSpace(verbsDetail.PresentNorwegianCharacterSet));
            this.UsePreteriteNorwegianForm = verbsDetails.Any(
                verbsDetail => !string.IsNullOrWhiteSpace(verbsDetail.PreteriteNorwegianCharacterSet));
            this.UsePastParticipleNorwegianForm = verbsDetails.Any(
                verbsDetail => !string.IsNullOrWhiteSpace(verbsDetail.PastParticipleNorwegianCharacterSet));
        }

        /// <summary>
        ///     Utters the verbs in the verbs detail asynchronously.
        /// </summary>
        /// <param name="verbsDetail">The verbs detail.</param>
        /// <returns>
        ///     A <see cref="Task" /> represents the uttering operation.
        /// </returns>
        private async Task UtterVerbsDetailAsync([NotNull] VerbsDetail verbsDetail)
        {
            try
            {
                // Continue if the verb is not uttered as much as declared.
                while (verbsDetail.TimesNumberUttered < verbsDetail.PronunciationTimes)
                {
                    // Notify that the uttering has been resumed.
                    this.IsUtteringPaused = false;

                    // Notify that uttering has been started.
                    this.IsUtteringVerbsDetail = true;

                    // Hold the verb detail which begin uttering.
                    this.BeginUtteringVerbsDetail = verbsDetail;

                    // Play The Verb In Infinitive Using Arabic.
                    await this.PlaySoundStreamAsync(verbsDetail.InfinitiveArabicPronunciation).ConfigureAwait(false);

                    // Play The Verb In Infinitive Using Norwegian.
                    await this.PlaySoundStreamAsync(verbsDetail.InfinitiveNorwegianPronunciation).ConfigureAwait(false);

                    // Play The Verb In Present Using Arabic.
                    await this.PlaySoundStreamAsync(verbsDetail.PresentArabicPronunciation).ConfigureAwait(false);

                    // Play The Verb In Present Using Norwegian.
                    await this.PlaySoundStreamAsync(verbsDetail.PresentNorwegianPronunciation).ConfigureAwait(false);

                    // Play The Verb In Past Tense Using Arabic.
                    await this.PlaySoundStreamAsync(verbsDetail.PreteriteArabicPronunciation).ConfigureAwait(false);

                    // Play The Verb In Past Tense Using Norwegian.
                    await this.PlaySoundStreamAsync(verbsDetail.PreteriteNorwegianPronunciation).ConfigureAwait(false);

                    // Play The Verb In Past Participle Using Arabic.
                    await this.PlaySoundStreamAsync(verbsDetail.PastParticipleArabicPronunciation)
                        .ConfigureAwait(false);

                    // Play The Verb In Past Participle Using Norwegian.
                    await this.PlaySoundStreamAsync(verbsDetail.PastParticipleNorwegianPronunciation)
                        .ConfigureAwait(false);

                    // Play The Verb Example Using Norwegian.
                    await this.PlaySoundStreamAsync(verbsDetail.NorwegianExamplePronunciation).ConfigureAwait(false);

                    // Increase the value of the TimesNumberUttered.
                    verbsDetail.TimesNumberUttered += 1;
                }
            }
            finally
            {
                // Notify that uttering has been done.
                this.IsUtteringVerbsDetail = false;

                // Release the last uttered verb.
                this.BeginUtteringVerbsDetail = null;
            }
        }

        /// <summary>
        ///     Utters the verbs details of the selected verbs headers asynchronously.
        /// </summary>
        /// <returns>
        ///     A <see cref="Task" /> represent the uttering operation.
        /// </returns>
        private async Task UtterVerbsDetailsAsync()
        {
            // Initialize a new instance of the CancellationToken.
            if (this.CancellationToken.IsCancellationRequested)
            {
                this.CancellationToken = new CancellationToken();
            }

            // Loop on the selected verbs headers. 
            foreach (var selectedVerbsHeader in this.SelectedVerbsHeaders)
            {
                // Continue if the header is not uttered as much as declared.
                while (selectedVerbsHeader.TimesNumberUttered < selectedVerbsHeader.PronunciationTimes)
                {
                    // Utter verbs detail in a separated task as much as declared where it has been paused.
                    foreach (var verbsDetail in selectedVerbsHeader.VerbsDetails)
                    {
                        await this.UtterVerbsDetailAsync(verbsDetail).ConfigureAwait(false);
                    }

                    // Increase the value of the TimesNumberUttered.
                    selectedVerbsHeader.TimesNumberUttered += 1;

                    // Reset the value of TimesNumberUttered until the last rotation on the header.
                    if (selectedVerbsHeader.TimesNumberUttered < selectedVerbsHeader.PronunciationTimes)
                    {
                        foreach (var verbsDetail in selectedVerbsHeader.VerbsDetails)
                        {
                            verbsDetail.TimesNumberUttered = 0;
                        }
                    }
                }
            }
        }

        /// <summary>
        ///     Utters the selected verbs details from beginning asynchronously.
        /// </summary>
        /// <returns>
        ///     A <see cref="Task" /> represents the uttering operation.
        /// </returns>
        [NotNull]
        private Task UtterVerbsDetailsFromBeginningAsync()
        {
            // Reset the value of TimesNumberUttered for both headers and details.
            foreach (var verbsHeader in this.SelectedVerbsHeaders)
            {
                verbsHeader.TimesNumberUttered = 0;
                foreach (var verbsDetail in verbsHeader.VerbsDetails)
                {
                    verbsDetail.TimesNumberUttered = 0;
                }
            }

            // Utter the verbs details of the selected headers.
            return this.UtterVerbsDetailsAsync();
        }
    }
}