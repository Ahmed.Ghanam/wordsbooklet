﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="VerbsHeaderViewModel.cs" company="None">
// Last Modified: 11/10/2018
// Author: Ahmed Ghanam.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace WordsBooklet.BusinessLayer.ViewModels
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Collections.Specialized;
    using System.ComponentModel;
    using System.Data.Entity;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using System.Windows;

    using WordsBooklet.BusinessLayer.Infrastructure;
    using WordsBooklet.BusinessLayer.Infrastructure.Notifications.Resources;
    using WordsBooklet.BusinessLayer.Models;
    using WordsBooklet.BusinessLayer.ViewModels.Resources;

    /// <inheritdoc />
    /// <summary>
    ///     Handling the <seealso cref="VerbsHeader" /> data model.
    /// </summary>
    /// <seealso cref="NotificationsLayer{T}" />
    public sealed class VerbsHeaderViewModel : NotificationsLayer<VerbsHeader>
    {
        /// <summary>
        ///     The cognitive services instance.
        /// </summary>
        [NotNull]
        private readonly CognitiveServices cognitiveServices;

        /// <summary>
        ///     The back field for the <see cref="IsTestingSpeakingRateOfVerbsDetail" /> property.
        /// </summary>
        private bool isTestingSpeakingRateOfVerbsDetail;

        /// <summary>
        ///     The back field for the <see cref="UsePastParticipleArabicForm" /> property.
        /// </summary>
        private bool usePastParticipleArabicForm;

        /// <summary>
        ///     The back field for the <see cref="UsePastParticipleNorwegianForm" /> property.
        /// </summary>
        private bool usePastParticipleNorwegianForm;

        /// <summary>
        ///     The back field for the <see cref="UsePresentArabicForm" /> property.
        /// </summary>
        private bool usePresentArabicForm;

        /// <summary>
        ///     The back field for the <see cref="UsePresentNorwegianForm" /> property.
        /// </summary>
        private bool usePresentNorwegianForm;

        /// <summary>
        ///     The back field for the <see cref="UsePreteriteArabicForm" /> property.
        /// </summary>
        private bool usePreteriteArabicForm;

        /// <summary>
        ///     The back field for the <see cref="UsePreteriteNorwegianForm" /> property.
        /// </summary>
        private bool usePreteriteNorwegianForm;

        /// <summary>
        ///     The back field for the <see cref="VerbsWordbooks" /> property.
        /// </summary>
        [CanBeNull]
        private ObservableCollection<Wordbook> verbsWordbooks;

        /// <inheritdoc />
        /// <summary>
        ///     Initializes a new instance of the <see cref="VerbsHeaderViewModel" /> class.
        /// </summary>
        /// <exception cref="OverflowException">
        ///     the call back value is less than <see cref="TimeSpan.MinValue" />
        ///     or greater than <see cref="TimeSpan.MaxValue" />
        ///     .-or-
        ///     the call back value is <see cref="Double.PositiveInfinity" />.-or- the call back value is
        ///     <see cref="Double.NegativeInfinity" />.
        /// </exception>
        /// <exception cref="ArgumentOutOfRangeException">
        ///     The number of milliseconds in the value of call back value is negative
        ///     and not equal to <see cref="Timeout.Infinite" />, or is greater than <see cref="Int32.MaxValue" />.
        /// </exception>
        /// <exception cref="ArgumentException"> the call back value is equal to <see cref="Double.NaN" />. </exception>
        /// <exception cref="ArgumentNullException">The call back value parameter is <see langword="null" />. </exception>
        public VerbsHeaderViewModel()
        {
            // Set the initial values of the inner Model properties.
            this.Model.PronunciationTimes = 1;

            // Initialize the cognitive services instance.
            this.cognitiveServices = new CognitiveServices();
            this.cognitiveServices.OnErrorOccurred += this.OnConversionErrorOccurred;

            // Set the VerbsDetailViewModels collection changed event handler.
            this.VerbsDetailViewModels.CollectionChanged += this.VerbsDetailViewModelsChanged;
        }

        /// <summary>
        ///     Gets a command to delete a view model of verbs detail.
        /// </summary>
        /// <value>
        ///     The command to delete a view model of verbs detail.
        /// </value>
        [NotNull]
        public AsyncCommand DeleteVerbsDetailViewModelCommand => new AsyncCommand(this.DeleteVerbsDetailViewModelAsync);

        /// <summary>
        ///     Gets a value indicating whether the user is testing the speaking rate of a verbs detail or not.
        /// </summary>
        /// <value>
        ///     <see langword="true" /> if the user is testing the speaking rate of a verbs detail; otherwise,
        ///     <see langword="false" />.
        /// </value>
        public bool IsTestingSpeakingRateOfVerbsDetail
        {
            get => this.isTestingSpeakingRateOfVerbsDetail;

            private set
            {
                this.isTestingSpeakingRateOfVerbsDetail = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets a command to load the Wordbooks which can hold verbs from the data source asynchronously.
        /// </summary>
        /// <value>
        ///     The command to load the Wordbooks which can hold verbs from the data source asynchronously.
        /// </value>
        [NotNull]
        public AsyncCommand LoadWordbooksForVerbsCommand => new AsyncCommand(this.LoadWordbooksForVerbsAsync);

        /// <summary>
        ///     Gets or sets the current verbs header name.
        /// </summary>
        /// <value>
        ///     The current verbs header name.
        /// </value>
        [CanBeNull]
        public string Name
        {
            get => this.Model.Name;

            set
            {
                this.Model.Name = value;
                this.OnPropertyChanged(this.Model.Name, nameof(this.Model.Name));
            }
        }

        /// <summary>
        ///     Gets or sets the pronunciation times of the current verbs header.
        /// </summary>
        /// <value>
        ///     The pronunciation times of the current verbs header.
        /// </value>
        public byte PronunciationTimes
        {
            get => this.Model.PronunciationTimes;

            set
            {
                this.Model.PronunciationTimes = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets the current verbs header remarks.
        /// </summary>
        /// <value>
        ///     The current verbs header remarks.
        /// </value>
        [CanBeNull]
        public string Remarks
        {
            get => this.Model.Remarks;

            set
            {
                this.Model.Remarks = value;
                this.OnPropertyChanged(this.Model.Remarks, nameof(this.Model.Remarks));
            }
        }

        /// <summary>
        ///     Gets a command to roll the user inputs back.
        /// </summary>
        /// <value>
        ///     The command to roll the user inputs back.
        /// </value>
        [NotNull]
        public RelayCommand RollBackUserInputsCommand =>
            new RelayCommand(
                canExecute => this.HasErrors || !string.IsNullOrEmpty(this.Model.Name)
                                             || !string.IsNullOrEmpty(this.Model.Remarks)
                                             || this.VerbsDetailViewModels.Any(),
                execute => this.RollBackDataChanges());

        /// <summary>
        ///     Gets a command to insert or update the current <seealso cref="VerbsHeader" />.
        /// </summary>
        /// <value>
        ///     The command to insert or update the current <seealso cref="VerbsHeader" />.
        /// </value>
        [NotNull]
        public AsyncCommand SaveVerbsHeaderDataCommand =>
            new AsyncCommand(
                canExecute => this.Model.Wordbook != null && !string.IsNullOrWhiteSpace(this.Model.Name)
                                                          && this.IsModelHasModified()
                                                          && this.VerbsDetailViewModels.Any(
                                                              verbsDetailViewModel =>
                                                                  verbsDetailViewModel.IsModelHasModified()),
                this.SaveVerbsHeaderDataAsync);

        /// <summary>
        ///     Gets a command to open a dialog box to search and select a single verbs header.
        /// </summary>
        /// <value>
        ///     The command to open a dialog box to search and select a single verbs header.
        /// </value>
        [NotNull]
        public AsyncCommand SearchVerbsHeadersCommand => new AsyncCommand(this.SearchVerbsHeadersAsync);

        /// <summary>
        ///     Gets a command to tests the speaking rate of a view model of verbs detail asynchronously.
        /// </summary>
        /// <value>
        ///     The command to tests the speaking rate of a view model of verbs detail
        /// </value>
        [NotNull]
        public AsyncCommand TestSpeakingRateOfVerbsDetailCommand => new AsyncCommand(this.TestSpeakingRateAsync);

        /// <summary>
        ///     Gets or sets a value indicating whether the Arabic past participle form of the verb is used or not.
        /// </summary>
        /// <value>
        ///     <see langword="true" /> if the Arabic past participle form of the verb is used; otherwise, <see langword="false" />.
        /// </value>
        public bool UsePastParticipleArabicForm
        {
            get => this.usePastParticipleArabicForm;
            set
            {
                this.usePastParticipleArabicForm = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets a value indicating whether the Norwegian past participle form of the verb is used or not.
        /// </summary>
        /// <value>
        ///     <see langword="true" /> if the Norwegian past participle form of the verb is used; otherwise,
        ///     <see langword="false" />.
        /// </value>
        public bool UsePastParticipleNorwegianForm
        {
            get => this.usePastParticipleNorwegianForm;
            set
            {
                this.usePastParticipleNorwegianForm = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets a value indicating whether the Arabic present form of the verb is used or not.
        /// </summary>
        /// <value>
        ///     <see langword="true" /> if the Arabic present form of the verb is used; otherwise, <see langword="false" />.
        /// </value>
        public bool UsePresentArabicForm
        {
            get => this.usePresentArabicForm;
            set
            {
                this.usePresentArabicForm = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets a value indicating whether the Norwegian present form of the verb is used or not.
        /// </summary>
        /// <value>
        ///     <see langword="true" /> if the Norwegian present form of the verb is used; otherwise, <see langword="false" />.
        /// </value>
        public bool UsePresentNorwegianForm
        {
            get => this.usePresentNorwegianForm;
            set
            {
                this.usePresentNorwegianForm = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets a value indicating whether the Arabic past tense form of the verb is used or not.
        /// </summary>
        /// <value>
        ///     <see langword="true" /> if the Arabic past tense form of the verb is used; otherwise, <see langword="false" />.
        /// </value>
        public bool UsePreteriteArabicForm
        {
            get => this.usePreteriteArabicForm;
            set
            {
                this.usePreteriteArabicForm = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets a value indicating whether the Norwegian past tense form of the verb is used or not.
        /// </summary>
        /// <value>
        ///     <see langword="true" /> if the Norwegian past tense form of the verb is used; otherwise, <see langword="false" />.
        /// </value>
        public bool UsePreteriteNorwegianForm
        {
            get => this.usePreteriteNorwegianForm;
            set
            {
                this.usePreteriteNorwegianForm = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets the view models of the verbs details which the current header holding.
        /// </summary>
        /// <value>
        ///     The view models of the verbs details which the current header holding.
        /// </value>
        [NotNull]
        public ObservableCollection<VerbsDetailViewModel> VerbsDetailViewModels { get; } =
            new ObservableCollection<VerbsDetailViewModel>();

        /// <summary>
        ///     Gets the wordbooks which can hold verbs which stored into the data source.
        /// </summary>
        /// <value>
        ///     The wordbooks which can hold verbs which stored into the data source.
        /// </value>
        [CanBeNull]
        public ObservableCollection<Wordbook> VerbsWordbooks
        {
            get => this.verbsWordbooks;

            private set
            {
                this.verbsWordbooks = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets the main wordbook of the current verbs header.
        /// </summary>
        /// <value>
        ///     The main wordbook of the current verbs header.
        /// </value>
        [CanBeNull]
        public Wordbook Wordbook
        {
            get => this.Model.Wordbook;

            set
            {
                // Set the selected word category object.
                this.Model.Wordbook = value;

                // Set the selected word category identifier.
                this.Model.WordbookId = this.Model.Wordbook?.Id ?? 0;

                // Validate the user inputs and notify the user interface.
                this.OnPropertyChanged(this.Model.Wordbook, nameof(this.Model.Wordbook));
            }
        }

        /// <inheritdoc />
        /// <summary>
        ///     Releases unmanaged and - optionally - managed resources.
        /// </summary>
        /// <param name="disposing">
        ///     <see langword="true" /> to release both managed and unmanaged resources; <see langword="false" /> to release only
        ///     unmanaged resources.
        /// </param>
        /// <exception cref="ObjectDisposedException">This <see cref="CancellationTokenSource" /> has been disposed.</exception>
        /// <exception cref="AggregateException">
        ///     An aggregate exception containing all the exceptions thrown by the registered
        ///     callbacks on the associated <see cref="CancellationToken" />.
        /// </exception>
        protected override void Dispose(bool disposing)
        {
            // Call base implementation, Always first.
            base.Dispose(disposing);

            if (!disposing)
            {
                return;
            }

            // Dispose related commands.
            this.SearchVerbsHeadersCommand.Dispose();
            this.SaveVerbsHeaderDataCommand.Dispose();
            this.LoadWordbooksForVerbsCommand.Dispose();
            this.DeleteVerbsDetailViewModelCommand.Dispose();
            this.TestSpeakingRateOfVerbsDetailCommand.Dispose();

            // Remove event listeners and dispose the cognitive services instance.
            this.cognitiveServices.OnErrorOccurred -= this.OnConversionErrorOccurred;
            this.cognitiveServices.Dispose();

            // Dispose every view model of verbs detail.
            foreach (var verbsDetailViewModel in this.VerbsDetailViewModels)
            {
                verbsDetailViewModel.Dispose();
            }

            // Remove event listeners.
            this.VerbsDetailViewModels.CollectionChanged -= this.VerbsDetailViewModelsChanged;
        }

        /// <inheritdoc />
        /// <summary>
        ///     Invoked if inserting the data model succeeded.
        /// </summary>
        /// <param name="sender">The control raise the event.</param>
        /// <param name="e">The <see cref="OnSaveChangesEventArgs" /> instance containing the event data.</param>
        /// <exception cref="Exception">A delegate callback throws an exception.</exception>
        protected override void OnInsertingModelSucceeded(object sender, OnSaveChangesEventArgs e)
        {
            // Call the base initialization.
            base.OnInsertingModelSucceeded(sender, e);

            // Prepare the user interface for new inputs.
            this.RollBackUserInputsCommand.Execute();
        }

        /// <inheritdoc />
        /// <summary>
        ///     Called when a property value changed.
        /// </summary>
        /// <param name="sender">The <paramref name="sender" />.</param>
        /// <param name="e">The <see cref="PropertyChangedEventArgs" /> instance containing the event data.</param>
        protected override void OnPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            // Displaying and hiding used formers based on the user selections.
            switch (e.PropertyName)
            {
                case nameof(this.UsePresentArabicForm):
                case nameof(this.UsePreteriteArabicForm):
                case nameof(this.UsePastParticipleArabicForm):
                    this.UsePresentNorwegianForm = this.UsePresentArabicForm || this.UsePresentNorwegianForm;
                    this.UsePreteriteNorwegianForm = this.UsePreteriteArabicForm || this.UsePreteriteNorwegianForm;
                    this.UsePastParticipleNorwegianForm =
                        this.UsePastParticipleArabicForm || this.UsePastParticipleNorwegianForm;
                    break;
            }
        }

        /// <inheritdoc />
        /// <summary>
        ///     Invoked if updating the data model succeeded.
        /// </summary>
        /// <param name="sender">The control raise the event.</param>
        /// <param name="e">The <see cref="OnSaveChangesEventArgs" /> instance containing the event data.</param>
        /// <exception cref="Exception">A delegate callback throws an exception.</exception>
        protected override void OnUpdatingModelSucceeded(object sender, OnSaveChangesEventArgs e)
        {
            // Call the base initialization.
            base.OnUpdatingModelSucceeded(sender, e);

            // Prepare the user interface for new inputs.
            this.RollBackUserInputsCommand.Execute();
        }

        /// <inheritdoc />
        /// <summary>
        ///     Roll the user changes back and set every Model to its original values.
        /// </summary>
        /// <exception cref="AggregateException">
        ///     An aggregate exception containing all the exceptions thrown by the registered
        ///     callbacks on the associated <see cref="CancellationToken" />.
        /// </exception>
        /// <exception cref="ObjectDisposedException">This <see cref="CancellationTokenSource" /> has been disposed.</exception>
        /// <exception cref="Exception">A <see langword="delegate" /> callback throws an exception.</exception>
        protected override void RollBackDataChanges()
        {
            // Roll back any data changes.
            if (this.IsModelHasModified())
            {
                base.RollBackDataChanges();
            }

            // Dispose every VerbsDetailViewModel.
            foreach (var verbsDetailViewModel in this.VerbsDetailViewModels)
            {
                verbsDetailViewModel.Dispose();
            }

            // Clear the VerbsDetailViewModels.
            this.VerbsDetailViewModels.Clear();

            // Initialize a new instance of the corresponding Model with the current customized values.
            this.Model = new VerbsHeader
                             {
                                 Wordbook = this.Model.Wordbook,
                                 CreationDateTime = DateTime.Now,
                                 LastUpdateDateTime = DateTime.Now,
                                 PronunciationTimes = this.Model.PronunciationTimes
                             };

            // Clear any existing validation errors.
            this.ClearValidationErrors();

            // Update the user interface depending on the new Model, and invoke property changed events.
            this.OnPropertyChanged(string.Empty);
        }

        /// <summary>
        ///     Attaches the verbs details to the current Model.
        /// </summary>
        /// <param name="verbsDetails">The verbs details.</param>
        private void AttachVerbsDetails([NotNull] IEnumerable<VerbsDetail> verbsDetails)
        {
            // Add the verbs details to the current Model using the main thread.
            Application.Current.Dispatcher.Invoke(
                () =>
                    {
                        foreach (var verbsDetail in verbsDetails)
                        {
                            this.VerbsDetailViewModels.Add(new VerbsDetailViewModel(verbsDetail));
                        }
                    });
        }

        /// <summary>
        ///     Convert details of verbs to a sound stream and attach them to the current data Model asynchronously.
        /// </summary>
        /// <param name="progress">The action to report the <paramref name="progress" /> to the user.</param>
        /// <returns>
        ///     A <see cref="Task" /> represents the conversion operation.
        /// </returns>
        private async Task ConvertVerbsDetailsToSoundStreamsAsync([NotNull] IProgress<int> progress)
        {
            // Clear existing VerbsDetails.
            this.Model.VerbsDetails.Clear();

            // Defines and reports the conversion progress percentage.
            var convertedVerbIndex = 0;
            progress.Report(convertedVerbIndex);

            // Convert verbs from string to sound stream.
            foreach (var verbsDetailViewModel in this.VerbsDetailViewModels)
            {
                // Every verb will be checked to not convert it multiple times.
                await verbsDetailViewModel.ConvertInnerModelAsync(this.cognitiveServices).ContinueWith(
                    conversionTask =>
                        {
                            // Attach the converted verbs to the VerbsDetails collection.
                            this.Model.VerbsDetails.Add(conversionTask.Result);

                            // Increases and reports the conversion progress percentage.
                            convertedVerbIndex++;
                            progress.Report(convertedVerbIndex);
                        },
                    this.CancellationToken,
                    TaskContinuationOptions.OnlyOnRanToCompletion,
                    TaskScheduler.Current).ConfigureAwait(false);
            }
        }

        /// <summary>
        ///     Deletes a view model of verbs detail asynchronous.
        /// </summary>
        /// <param name="verbsDetailViewModel">The verbs detail view model.</param>
        /// <returns>
        ///     A <see cref="Task" /> represents the deleting operation.
        /// </returns>
        private async Task DeleteVerbsDetailViewModelAsync([NotNull] object verbsDetailViewModel)
        {
            // Cast the passed object to VerbsDetailViewModel.
            var deletingVerbsDetailViewModel = (VerbsDetailViewModel)verbsDetailViewModel;

            // Check the value of required verbs.
            if (string.IsNullOrWhiteSpace(deletingVerbsDetailViewModel.InfinitiveArabicCharacterSet)
                || string.IsNullOrWhiteSpace(deletingVerbsDetailViewModel.InfinitiveNorwegianCharacterSet))
            {
                return;
            }

            // Ask the user to confirm deleting the VerbsDetailViewModel.
            if (!deletingVerbsDetailViewModel.IsUserConfirmedDeletingModel())
            {
                return;
            }

            // Delete the inner Model from the data source.
            if (deletingVerbsDetailViewModel.Model.Id != 0)
            {
                // Notify the user interface.
                this.IsPerformingLongRunningTask = true;

                // Update the VerbsHeader and VerbsDetails.
                this.Model.VerbsCount -= 1;
                this.Model.LastUpdateDateTime = DateTime.Now;
                this.DataEntities.VerbsDetails.Remove(deletingVerbsDetailViewModel.Model);

                try
                {
                    // Save the changes on the data source.
                    await this.DataEntities.SaveChangesAsync(this.CancellationToken).ConfigureAwait(false);
                }
                finally
                {
                    // Notify the user interface.
                    this.IsPerformingLongRunningTask = false;
                }
            }

            // Remove the deleted VerbsDetailViewModel from the collection and notify the user. 
            Application.Current.Dispatcher.Invoke(
                () =>
                    {
                        this.VerbsDetailViewModels.Remove(deletingVerbsDetailViewModel);
                        this.OnDeletingModelSucceeded(
                            this,
                            new OnSaveChangesEventArgs { Entity = deletingVerbsDetailViewModel.Model });
                    });
        }

        /// <summary>
        ///     Checks the name of current verbs header against duplication.
        /// </summary>
        /// <returns>
        ///     A <see cref="Task{TResult}" /> represents the checking operation.
        /// </returns>
        private async Task<bool> IsDuplicatedNameAsync()
        {
            // Hold the checking results.
            var isDuplicatedName = false;

            // Check whether the name is duplicated or not.
            await this.DataEntities.VerbsHeaders
                .Where(
                    verbsHeader => verbsHeader.Id != this.Model.Id && verbsHeader.Name == this.Model.Name.Trim()
                                                                   && verbsHeader.WordbookId == this.Model.WordbookId)
                .ToListAsync(this.CancellationToken).ContinueWith(
                    checkingDuplicationTask =>
                        {
                            // Hold the checking results.
                            isDuplicatedName = checkingDuplicationTask.Result.Any();

                            // Display or remove an error to notify the user.
                            if (isDuplicatedName)
                            {
                                this.SetError(
                                    string.Format(ValidationMessages.DuplicatedName, this.Model.Name),
                                    nameof(this.Model.Name));
                            }
                            else
                            {
                                this.RemoveErrors(nameof(this.Model.Name));
                            }
                        },
                    this.CancellationToken,
                    TaskContinuationOptions.OnlyOnRanToCompletion,
                    TaskScheduler.Current).ConfigureAwait(false);

            // Return the checking results.
            return isDuplicatedName;
        }

        /// <summary>
        ///     Loads the Wordbooks which can hold verbs from the data source asynchronously.
        /// </summary>
        /// <returns>
        ///     A <see cref="Task" /> represents the loading operation.
        /// </returns>
        private async Task LoadWordbooksForVerbsAsync()
        {
            // Notify the user interface.
            this.LongRunningTaskDescription = NotificationsLayer.PreparingUserInterfaceMessage;
            this.IsPerformingLongRunningTask = true;

            try
            {
                await this.DataEntities.Wordbooks
                    .Where(wordbook => wordbook.Type == (byte)WordbookTypes.Verb && !wordbook.IsFolder)
                    .ToListAsync(this.CancellationToken).ContinueWith(
                        readingTask =>
                            {
                                this.VerbsWordbooks = new ObservableCollection<Wordbook>(readingTask.Result);
                            },
                        this.CancellationToken,
                        TaskContinuationOptions.OnlyOnRanToCompletion,
                        TaskScheduler.Current).ConfigureAwait(false);
            }
            finally
            {
                this.IsPerformingLongRunningTask = false;
                this.LongRunningTaskDescription = string.Empty;
            }
        }

        /// <summary>
        ///     Called when a text to speech request has been failed and an error has been occurred.
        /// </summary>
        /// <param name="sender">The <paramref name="sender" />.</param>
        /// <param name="e">The <see cref="GenericEventArgs{Exception}" /> instance containing the event data.</param>
        private void OnConversionErrorOccurred([NotNull] object sender, [NotNull] GenericEventArgs<Exception> e)
        {
            this.DisplayDesktopAlert(
                NotificationsLayer.GeneralExceptionsHeader,
                string.Format(NotificationsLayer.GeneralExceptionsMessage, e.EventData.Message),
                AlertTypes.Error);
        }

        /// <summary>
        ///     Reports the <paramref name="progress" /> percentage for the conversion tasks.
        /// </summary>
        /// <param name="progress">The <paramref name="progress" /> percentage value.</param>
        private void ReportConversionProgress(int progress)
        {
            // Display the index of the converted verb.
            this.LongRunningTaskDescription = string.Format(
                NotificationsLayer.StringToSoundConversionIsInProgressMessage,
                progress,
                this.VerbsDetailViewModels.Count);
        }

        /// <summary>
        ///     Saves the data of the current <seealso cref="VerbsHeader" /> asynchronously.
        /// </summary>
        /// <returns>
        ///     A <see cref="Task" /> represents the Insert or the Update operations.
        /// </returns>
        private async Task SaveVerbsHeaderDataAsync()
        {
            // Notify the user interface.
            this.IsPerformingLongRunningTask = true;

            try
            {
                // Check and notify the user if the name is duplicated.
                if (!await this.IsDuplicatedNameAsync().ConfigureAwait(false))
                {
                    // Update the VerbsCount and LastUpdateDateTime.
                    this.Model.LastUpdateDateTime = DateTime.Now;
                    this.Model.VerbsCount = (short)this.VerbsDetailViewModels.Count;

                    // Convert inner verbs from string to sound streams and attach them to the inner Model.
                    await this.ConvertVerbsDetailsToSoundStreamsAsync(new Progress<int>(this.ReportConversionProgress))
                        .ConfigureAwait(false);

                    // Inserts or updates the data Model asynchronously.
                    switch (this.Model.Id)
                    {
                        case 0:
                            this.Model.CreationDateTime = DateTime.Now;
                            await this.InsertAsync().ConfigureAwait(false);
                            break;

                        default:
                            await this.UpdateAsync().ConfigureAwait(false);
                            break;
                    }
                }
            }
            finally
            {
                // Notify the user interface.
                this.IsPerformingLongRunningTask = false;
                this.LongRunningTaskDescription = string.Empty;
            }
        }

        /// <summary>
        ///     Displays a dialog box to edit a verbs header from the search result.
        /// </summary>
        /// <param name="searchingWindowType">The type of searching window.</param>
        /// <returns>
        ///     A <see cref="Task" /> represents the selecting operation.
        /// </returns>
        private async Task SearchVerbsHeadersAsync([NotNull] object searchingWindowType)
        {
            // Is the selected data model not the current data model.
            var userSelectedDifferentModel = false;

            // Create and display the searching dialog on the main thread.
            Application.Current.Dispatcher.Invoke(
                () =>
                    {
                        // Display the searching data dialog.
                        DialogService.ShowDialog((Type)searchingWindowType);

                        // Roll back current data if the user has selected a different data Model.
                        if (DialogService.SelectedModelId != 0)
                        {
                            if (!this.Model.Id.Equals(DialogService.SelectedModelId))
                            {
                                userSelectedDifferentModel = true;
                                this.RollBackUserInputsCommand.Execute();
                            }
                            else
                            {
                                this.DisplayDesktopAlert(
                                    NotificationsLayer.DataUpdating,
                                    string.Format(NotificationsLayer.CanEditDataModelFromInputFields, this.Model.Name),
                                    AlertTypes.Information);
                            }
                        }
                    });

            // If the selected data model not the current data model.
            if (userSelectedDifferentModel)
            {
                try
                {
                    // Notify the user interface.
                    this.IsPerformingLongRunningTask = true;

                    // Search for data using the selected Model identifier.
                    this.Model = await this.DataEntities.VerbsHeaders.Include(verbsHeader => verbsHeader.VerbsDetails)
                                     .SingleAsync(
                                         verbsHeader => verbsHeader.Id == DialogService.SelectedModelId,
                                         this.CancellationToken).ConfigureAwait(false);

                    // Fill the verbs details view model.
                    this.AttachVerbsDetails(this.Model.VerbsDetails);

                    // Handle the displaying of verbs forms.
                    this.SetUsedArabicForms(this.Model.VerbsDetails);
                    this.SetUsedNorwegianForms(this.Model.VerbsDetails);
                }
                finally
                {
                    // Notify the user interface.
                    this.OnPropertyChanged(string.Empty);
                    this.IsPerformingLongRunningTask = false;
                }
            }
        }

        /// <summary>
        ///     Reflects the used Arabic forms to the user.
        /// </summary>
        /// <param name="verbsDetails">The verbs details.</param>
        private void SetUsedArabicForms([NotNull] IReadOnlyCollection<VerbsDetail> verbsDetails)
        {
            this.UsePresentArabicForm = verbsDetails.Any(
                verbsDetail => !string.IsNullOrWhiteSpace(verbsDetail.PresentArabicCharacterSet));
            this.UsePreteriteArabicForm = verbsDetails.Any(
                verbsDetail => !string.IsNullOrWhiteSpace(verbsDetail.PreteriteArabicCharacterSet));
            this.UsePastParticipleArabicForm = verbsDetails.Any(
                verbsDetail => !string.IsNullOrWhiteSpace(verbsDetail.PastParticipleArabicCharacterSet));
        }

        /// <summary>
        ///     Reflects the used Norwegian forms to the user.
        /// </summary>
        /// <param name="verbsDetails">The verbs details.</param>
        private void SetUsedNorwegianForms([NotNull] IReadOnlyCollection<VerbsDetail> verbsDetails)
        {
            this.UsePresentNorwegianForm = verbsDetails.Any(
                verbsDetail => !string.IsNullOrWhiteSpace(verbsDetail.PresentNorwegianCharacterSet));
            this.UsePreteriteNorwegianForm = verbsDetails.Any(
                verbsDetail => !string.IsNullOrWhiteSpace(verbsDetail.PreteriteNorwegianCharacterSet));
            this.UsePastParticipleNorwegianForm = verbsDetails.Any(
                verbsDetail => !string.IsNullOrWhiteSpace(verbsDetail.PastParticipleNorwegianCharacterSet));
        }

        /// <summary>
        ///     Tests the speaking rate for the passed view model of verbs detail asynchronously.
        /// </summary>
        /// <param name="verbsDetailViewModel">The verbs detail view model.</param>
        /// <returns>
        ///     A <see cref="Task" /> represents the testing operation.
        /// </returns>
        private async Task TestSpeakingRateAsync([NotNull] object verbsDetailViewModel)
        {
            try
            {
                // Notify the user interface.
                this.IsTestingSpeakingRateOfVerbsDetail = true;

                // Cast the passed object to VerbsDetailViewModel.
                var castedVerbsDetailViewModel = (VerbsDetailViewModel)verbsDetailViewModel;

                // Convert the inner Model to a sound streams if its not converted, then play the sound stream.
                await castedVerbsDetailViewModel.TestSpeakingRateAsync(this.cognitiveServices).ConfigureAwait(false);
            }
            finally
            {
                // Notify the user interface.
                this.IsTestingSpeakingRateOfVerbsDetail = false;
            }
        }

        /// <summary>
        ///     Occurs when the verbs detail view models collection changed.
        /// </summary>
        /// <param name="sender">The <paramref name="sender" />.</param>
        /// <param name="e">The <see cref="NotifyCollectionChangedEventArgs" /> instance containing the event data.</param>
        private void VerbsDetailViewModelsChanged([NotNull] object sender, [NotNull] NotifyCollectionChangedEventArgs e)
        {
            switch (e.Action)
            {
                case NotifyCollectionChangedAction.Add:

                    // Escape the first VerbsDetailViewModel.
                    if (e.NewStartingIndex > 0)
                    {
                        // Get the new VerbsDetailViewModels only.
                        if (e.NewItems[0] is VerbsDetailViewModel verbsDetailViewModel
                            && verbsDetailViewModel.Model.Id == 0)
                        {
                            // Repeat values of SpeakingRate and PronunciationTimes properties.
                            verbsDetailViewModel.SpeakingRate =
                                this.VerbsDetailViewModels[e.NewStartingIndex - 1].SpeakingRate;
                            verbsDetailViewModel.PronunciationTimes = this.VerbsDetailViewModels[e.NewStartingIndex - 1]
                                .PronunciationTimes;
                        }
                    }

                    break;
            }

            // Reorder serial number for verbs detail on all actions.
            byte firstSerial = 1;
            foreach (var verbsDetailViewModels in this.VerbsDetailViewModels)
            {
                verbsDetailViewModels.SerialNumber = firstSerial++;
            }
        }
    }
}