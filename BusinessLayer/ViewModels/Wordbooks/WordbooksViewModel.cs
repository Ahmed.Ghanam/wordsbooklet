﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="WordbooksViewModel.cs" company="None">
// Last Modified: 06/10/2018
// Author: Ahmed Ghanam.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace WordsBooklet.BusinessLayer.ViewModels
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.ComponentModel;
    using System.Data.Entity;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;

    using WordsBooklet.BusinessLayer.Infrastructure;
    using WordsBooklet.BusinessLayer.Infrastructure.Notifications.Resources;
    using WordsBooklet.BusinessLayer.Models;
    using WordsBooklet.BusinessLayer.ViewModels.Resources;

    /// <inheritdoc />
    /// <summary>
    ///     Handling the <seealso cref="Wordbook" /> data model.
    /// </summary>
    /// <seealso cref="NotificationsLayer{T}" />
    public sealed class WordbooksViewModel : NotificationsLayer<Wordbook>
    {
        /// <summary>
        ///     The back field for the <see cref="CanChangeChildhoodStatus" /> property.
        /// </summary>
        private bool canChangeChildhoodStatus;

        /// <summary>
        ///     The back field for the <see cref="CanChangeType" /> property.
        /// </summary>
        private bool canChangeType;

        /// <summary>
        ///     The back field for the <see cref="IllegitimateParentalWordbooks" /> property.
        /// </summary>
        [CanBeNull]
        private ObservableCollection<Wordbook> illegitimateParentalWordbooks;

        /// <summary>
        ///     The back field for the <see cref="IllegitimateWordbooks" /> property.
        /// </summary>
        [CanBeNull]
        private ObservableCollection<Wordbook> illegitimateWordbooks;

        /// <inheritdoc />
        /// <summary>
        ///     Initializes a new instance of the <see cref="WordbooksViewModel" /> class.
        /// </summary>
        public WordbooksViewModel()
        {
            // Set the model properties' initial values.
            this.Model.IsFolder = true;
            this.Model.LevelNumber = 1;
            this.Model.PureCodePart = "00";

            // Set the view properties' initial values.
            this.CanChangeType = true;
        }

        /// <summary>
        ///     Gets a value indicating whether the user can change the current wordbook childhood status or not.
        /// </summary>
        /// <value>
        ///     <see langword="true" /> if the user can change the childhood status; otherwise, <see langword="false" />.
        /// </value>
        /// <exception cref="OverflowException" accessor="get">Overflow calculate the value</exception>
        public bool CanChangeChildhoodStatus
        {
            get =>
                this.canChangeChildhoodStatus && this.Model.SubWordbooksCount == 0 && this.Model.RelatedWordsCount == 0;

            private set
            {
                this.canChangeChildhoodStatus = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets a value indicating whether the user can change the current wordbook type or not.
        /// </summary>
        /// <value>
        ///     <see langword="true" /> if the user can change the type; otherwise, <see langword="false" />.
        /// </value>
        /// <exception cref="OverflowException" accessor="get">Overflow calculate the value</exception>
        public bool CanChangeType
        {
            get => this.canChangeType && this.Model.SubWordbooksCount == 0 && this.Model.RelatedWordsCount == 0;

            private set
            {
                this.canChangeType = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets a command to delete a wordbook.
        /// </summary>
        /// <value>
        ///     The command to delete a wordbook.
        /// </value>
        [NotNull]
        public AsyncCommand DeleteWordbookCommand => new AsyncCommand(this.DeleteWordbookAsync);

        /// <summary>
        ///     Gets a command to set the selected wordbook into the edit mode.
        /// </summary>
        /// <value>
        ///     The command to set the selected wordbook into the edit mode.
        /// </value>
        [NotNull]
        public RelayCommand EditWordbookCommand => new RelayCommand(this.SetWordbookIntoEditMode);

        /// <summary>
        ///     Gets a command to generate a code for the current wordbook.
        /// </summary>
        /// <value>
        ///     The command to generate a code for the current wordbook.
        /// </value>
        [NotNull]
        public RelayCommand GenerateFollowingCodeCommand
        {
            get
            {
                return new RelayCommand(executeAction => { this.PureCodePart = this.GenerateFollowingCode(); });
            }
        }

        /// <summary>
        ///     Gets the illegitimate parental wordbooks.
        /// </summary>
        /// <value>
        ///     The illegitimate parental wordbooks.
        /// </value>
        [CanBeNull]
        public ObservableCollection<Wordbook> IllegitimateParentalWordbooks
        {
            get => this.illegitimateParentalWordbooks;

            private set
            {
                this.illegitimateParentalWordbooks = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets the illegitimate wordbooks.
        /// </summary>
        /// <value>
        ///     The illegitimate wordbooks.
        /// </value>
        [CanBeNull]
        public ObservableCollection<Wordbook> IllegitimateWordbooks
        {
            get => this.illegitimateWordbooks;

            private set
            {
                this.illegitimateWordbooks = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets a value indicating whether current wordbook can contain children or not.
        /// </summary>
        /// <value>
        ///     <see langword="true" /> if current wordbook can contain children, otherwise; <see langword="false" />.
        /// </value>
        public bool IsFolder
        {
            get => this.Model.IsFolder;

            set
            {
                this.Model.IsFolder = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets the level number of current wordbook in the tree.
        /// </summary>
        /// <value>
        ///     The level number of current wordbook in the tree.
        /// </value>
        public byte LevelNumber
        {
            get => this.Model.LevelNumber;

            private set
            {
                this.Model.LevelNumber = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets a command to load the illegitimate wordbooks asynchronously.
        /// </summary>
        /// <value>
        ///     The command to load the illegitimate wordbooks asynchronously.
        /// </value>
        [NotNull]
        public AsyncCommand LoadIllegitimateWordbooksCommand => new AsyncCommand(this.LoadIllegitimateWordbooksAsync);

        /// <summary>
        ///     Gets or sets the major wordbook of the current wordbook.
        /// </summary>
        /// <value>
        ///     The major wordbook of the current wordbook.
        /// </value>
        [CanBeNull]
        public Wordbook MajorWordbook
        {
            get => this.Model.MajorWordbook;

            set
            {
                if (value != null)
                {
                    if (this.Model.Id != 0)
                    {
                        // Rule 1: It can not be a parent for itself.
                        if (this.Model.Id.Equals(value.Id))
                        {
                            return;
                        }

                        // Rule 2: It can not be a parent for its parent.
                        if (value.Code.StartsWith(this.Model.Code))
                        {
                            return;
                        }
                    }
                }

                this.Model.MajorWordbook = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets the current wordbook name.
        /// </summary>
        /// <value>
        ///     The current wordbook name.
        /// </value>
        [CanBeNull]
        public string Name
        {
            get => this.Model.Name;

            set
            {
                this.Model.Name = value;
                this.OnPropertyChanged(this.Model.Name, nameof(this.Model.Name));
            }
        }

        /// <summary>
        ///     Gets or sets the current wordbook code part in the full code chain.
        /// </summary>
        /// <value>
        ///     The current wordbook code part in the full code chain.
        /// </value>
        [NotNull]
        public string PureCodePart
        {
            get => this.Model.PureCodePart;

            set
            {
                this.Model.PureCodePart = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets a command to remove the major wordbook of current wordbook.
        /// </summary>
        /// <value>
        ///     The command to remove the major wordbook of current wordbook.
        /// </value>
        [NotNull]
        public RelayCommand RemoveMajorWordbookCommand
        {
            get
            {
                return new RelayCommand(
                    canExecute => this.Model.MajorWordbook != null,
                    executeAction => this.MajorWordbook = null);
            }
        }

        /// <summary>
        ///     Gets a command to roll the user inputs back.
        /// </summary>
        /// <value>
        ///     The command to roll the user inputs back.
        /// </value>
        [NotNull]
        public RelayCommand RollBackUserInputsCommand
        {
            get
            {
                return new RelayCommand(
                    canExecute => this.HasErrors || this.Model.LevelNumber != 1 || this.Model.PureCodePart != "00"
                                  || !string.IsNullOrEmpty(this.Model.Name)
                                  || this.Model.Type != (byte)WordbookTypes.Main,
                    execute => this.RollBackDataChanges());
            }
        }

        /// <summary>
        ///     Gets a command to insert or update the current <seealso cref="Wordbook" />.
        /// </summary>
        /// <value>
        ///     The command to insert or update the current <seealso cref="Wordbook" />.
        /// </value>
        [NotNull]
        public AsyncCommand SaveWordbookDataCommand
        {
            get
            {
                return new AsyncCommand(
                    canExecute => !string.IsNullOrWhiteSpace(this.Model.Name) && this.Model.PureCodePart != "00"
                                                                              && this.IsModelHasModified(),
                    this.SaveWordbookDataAsync);
            }
        }

        /// <summary>
        ///     Gets or sets the current wordbook type.
        /// </summary>
        /// <value>
        ///     The current wordbook type.
        /// </value>
        public WordbookTypes Type
        {
            get => (WordbookTypes)this.Model.Type;

            set
            {
                this.Model.Type = (byte)value;
                this.OnPropertyChanged();
            }
        }

        /// <inheritdoc />
        /// <summary>
        ///     Releases unmanaged and - optionally - managed resources.
        /// </summary>
        /// <param name="disposing">
        ///     <see langword="true" /> to release both managed and unmanaged resources; <see langword="false" /> to release only
        ///     unmanaged resources.
        /// </param>
        /// <exception cref="AggregateException">
        ///     An aggregate exception containing all the exceptions thrown by the registered
        ///     callbacks on the associated <see cref="CancellationToken" />.
        /// </exception>
        protected override void Dispose(bool disposing)
        {
            // Call base implementation, Always first.
            base.Dispose(disposing);

            if (!disposing)
            {
                return;
            }

            // Dispose related commands.
            this.DeleteWordbookCommand.Dispose();
            this.SaveWordbookDataCommand.Dispose();
            this.LoadIllegitimateWordbooksCommand.Dispose();
        }

        /// <inheritdoc />
        /// <summary>
        ///     Invoked if deleting the data model succeeded.
        /// </summary>
        /// <param name="sender">The control raise the event.</param>
        /// <param name="e">The <see cref="OnSaveChangesEventArgs" /> instance containing the event data.</param>
        /// <exception cref="Exception">A delegate callback throws an exception.</exception>
        protected override void OnDeletingModelSucceeded(object sender, OnSaveChangesEventArgs e)
        {
            // Call the base initialization.
            base.OnDeletingModelSucceeded(sender, e);

            // Prepare the user interface for new inputs.
            this.RollBackUserInputsCommand.Execute();

            // Reload the illegitimate wordbook data.
            this.LoadIllegitimateWordbooksCommand.Execute();
        }

        /// <inheritdoc />
        /// <summary>
        ///     Invoked if inserting the data model succeeded.
        /// </summary>
        /// <param name="sender">The control raise the event.</param>
        /// <param name="e">The <see cref="OnSaveChangesEventArgs" /> instance containing the event data.</param>
        /// <exception cref="Exception">A delegate callback throws an exception.</exception>
        protected override void OnInsertingModelSucceeded(object sender, OnSaveChangesEventArgs e)
        {
            // Call the base initialization.
            base.OnInsertingModelSucceeded(sender, e);

            // Prepare the user interface for new inputs.
            this.RollBackUserInputsCommand.Execute();

            // Reload the illegitimate wordbook data.
            this.LoadIllegitimateWordbooksCommand.Execute();
        }

        /// <inheritdoc />
        /// <summary>
        ///     Called when a property value changed.
        /// </summary>
        /// <param name="sender">The <paramref name="sender" />.</param>
        /// <param name="e">The <see cref="PropertyChangedEventArgs" /> instance containing the event data.</param>
        protected override void OnPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            switch (e.PropertyName)
            {
                case nameof(this.Model.Type):
                    this.SetRelatedPropertiesOnTypeChanged();
                    break;

                case nameof(this.Model.MajorWordbook):
                    this.SetRelatedPropertiesOnParentChanged();
                    break;
            }
        }

        /// <inheritdoc />
        /// <summary>
        ///     Invoked if updating the data model succeeded.
        /// </summary>
        /// <param name="sender">The control raise the event.</param>
        /// <param name="e">The <see cref="OnSaveChangesEventArgs" /> instance containing the event data.</param>
        /// <exception cref="Exception">A delegate callback throws an exception.</exception>
        protected override void OnUpdatingModelSucceeded(object sender, OnSaveChangesEventArgs e)
        {
            // Call the base initialization.
            base.OnUpdatingModelSucceeded(sender, e);

            // Prepare the user interface for new inputs.
            this.RollBackUserInputsCommand.Execute();

            // Reload the illegitimate wordbook data.
            this.LoadIllegitimateWordbooksCommand.Execute();
        }

        /// <inheritdoc />
        /// <summary>
        ///     Roll the user changes back and assign the model to its original values.
        /// </summary>
        /// <exception cref="Exception">The <see langword="delegate" /> callback throws an exception.</exception>
        protected override void RollBackDataChanges()
        {
            // Roll back any data changes.
            if (this.IsModelHasModified())
            {
                base.RollBackDataChanges();
            }

            // Initialize a new instance of the corresponding Model with the default values.
            this.Model = new Wordbook { IsFolder = true, PureCodePart = "00", LevelNumber = 1 };

            // Clear any existing validation errors.
            this.ClearValidationErrors();

            // Update the user interface depending on the new Model, and invoke property changed events.
            this.OnPropertyChanged(string.Empty);
            this.OnPropertyChanged(nameof(this.Model.Type));
            this.OnPropertyChanged(nameof(this.Model.MajorWordbook));
        }

        /// <summary>
        ///     Generates the following code from the coding sequence.
        /// </summary>
        /// <param name="codeSequenceZone">The code sequence zone.</param>
        /// <returns>
        ///     The unique following code from the coding sequence
        /// </returns>
        [NotNull]
        private static string GenerateFollowingCode([NotNull] IEnumerable<Wordbook> codeSequenceZone)
        {
            // Get the used codes without the parent code part.
            var usedPureCodes = codeSequenceZone
                .Select(takenCode => takenCode.Code.Remove(0, takenCode.Code.Length - 2))
                .Select(pureCodes => Convert.ToInt32(pureCodes));
            usedPureCodes = usedPureCodes.ToList();

            // Get the first missing code between the coding sequence which starts at '01' and ends at '99'.
            var followingCode = Enumerable.Range(1, 99).Except(usedPureCodes).FirstOrDefault();

            // Rule 1 : If range has no room, return the value '00'.
            // Rule 2 : If the following code is bigger than '9', return it as it is.
            // Rule 3 : If the following code is less than '9', append '0' to its left.
            return followingCode <= 0
                       ? "00"
                       : (followingCode <= 9 ? $"0{Convert.ToString(followingCode)}" : Convert.ToString(followingCode));
        }

        /// <summary>
        ///     Updates the current wordbook code by appending or removing the parent code part.
        /// </summary>
        private void AppendOrRemoveParentCodePart()
        {
            if (this.Model.MajorWordbook != null)
            {
                // Append the parent code part.
                this.Model.Code = this.Model.MajorWordbook.Code + "-" + this.Model.PureCodePart;
            }
            else
            {
                // Remove the parent code part.
                this.Model.Code = this.Model.PureCodePart.Remove(0, this.Model.PureCodePart.Length - 2);
            }

            // If the current wordbook has sub wordbooks, update their codes also.
            if (this.Model.SubWordbooks.Any())
            {
                this.UpdateSubWordbooksCodeRecursively(this.Model.Code, this.Model.SubWordbooks);
            }
        }

        /// <summary>
        ///     Deletes the <paramref name="wordbook" /> asynchronously.
        /// </summary>
        /// <param name="wordbook">The wordbook to delete.</param>
        /// <returns>
        ///     A <see cref="Task" /> represents the deleting operation.
        /// </returns>
        [NotNull]
        private Task DeleteWordbookAsync([NotNull] object wordbook)
        {
            return this.DeleteAsync((Wordbook)wordbook);
        }

        /// <summary>
        ///     Generates the following code for the current wordbook.
        /// </summary>
        /// <returns>
        ///     The following code.
        /// </returns>
        [NotNull]
        private string GenerateFollowingCode()
        {
            // Specify the coding zone.
            var codingZone = this.Model.MajorWordbook != null
                                 ? this.Model.MajorWordbook.SubWordbooks
                                 : this.IllegitimateWordbooks;

            // Rule 1: The first entity takes the code '01' always.
            if (codingZone == null)
            {
                return "01";
            }

            // Rule 1: The first entity takes the code '01' always.
            if (!codingZone.Any())
            {
                return "01";
            }

            // Rule 2: Do not generate a code for an existing wordbook.
            if (codingZone.Contains(this.Model))
            {
                var currentWordbookIndex = codingZone.IndexOf(this.Model);
                var currentWordbookObject = codingZone[currentWordbookIndex];
                return currentWordbookObject.Code.Remove(0, currentWordbookObject.Code.Length - 2);
            }

            // Generate the following code from the coding sequence.
            return GenerateFollowingCode(codingZone);
        }

        /// <summary>
        ///     Gets the illegitimate wordbooks from the data source asynchronously.
        /// </summary>
        /// <returns>
        ///     A <see cref="Task{Wordbook}" /> represents the loading operation.
        /// </returns>
        private async Task<IList<Wordbook>> GetIllegitimateWordbooksAsync()
        {
            return await this.DataEntities.Wordbooks.Where(wordbook => wordbook.ParentId == null).ToListAsync()
                       .ConfigureAwait(false);
        }

        /// <summary>
        ///     Determines whether the code of current wordbook is duplicate or not asynchronously.
        /// </summary>
        /// <returns>
        ///     A <see cref="Task{TResult}" /> represents the results.
        /// </returns>
        private async Task<bool> IsCodeDuplicateAsync()
        {
            // Build the full code for the current wordbook.
            var currentWordbookFullCode = this.Model.MajorWordbook != null
                                              ? $"{this.Model.MajorWordbook.Code}-{this.Model.PureCodePart}"
                                              : this.Model.PureCodePart;

            var duplicatedWordbooks = await this.DataEntities.Wordbooks
                                          .Where(
                                              duplicatedEntities =>
                                                  duplicatedEntities.Id != this.Model.Id
                                                  && duplicatedEntities.ParentId == this.Model.ParentId
                                                  && duplicatedEntities.Code == currentWordbookFullCode).ToListAsync()
                                          .ConfigureAwait(false);

            return duplicatedWordbooks.Any();
        }

        /// <summary>
        ///     Determines whether the name of current wordbook is duplicate or not asynchronously.
        /// </summary>
        /// <returns>
        ///     A <see cref="Task{TResult}" /> represents the results.
        /// </returns>
        private async Task<bool> IsNameDuplicateAsync()
        {
            var duplicatedWordbooks = await this.DataEntities.Wordbooks
                                          .Where(
                                              duplicatedEntities =>
                                                  duplicatedEntities.Id != this.Model.Id
                                                  && duplicatedEntities.Name == this.Model.Name
                                                  && duplicatedEntities.ParentId == this.Model.ParentId).ToListAsync()
                                          .ConfigureAwait(false);
            return duplicatedWordbooks.Any();
        }

        /// <summary>
        ///     Checks the name and code of current wordbook against duplication.
        /// </summary>
        /// <returns>
        ///     A <see cref="Task{TResult}" /> represents the checking operation.
        /// </returns>
        private async Task<bool> IsNameOrCodeDuplicatedAsync()
        {
            // Check the name.
            var isDuplicatedName = await this.IsNameDuplicateAsync().ConfigureAwait(false);
            if (isDuplicatedName)
            {
                this.SetError(
                    string.Format(ValidationMessages.DuplicatedName, this.Model.Name),
                    nameof(this.Model.Name));
            }
            else
            {
                this.RemoveErrors(nameof(this.Model.Name));
            }

            // Check the code.
            var isDuplicatedCode = await this.IsCodeDuplicateAsync().ConfigureAwait(false);
            if (isDuplicatedCode)
            {
                this.SetError(
                    string.Format(ValidationMessages.DuplicatedCode, this.Model.PureCodePart),
                    nameof(this.Model.PureCodePart));
            }
            else
            {
                this.RemoveErrors(nameof(this.Model.PureCodePart));
            }

            return isDuplicatedName || isDuplicatedCode;
        }

        /// <summary>
        ///     Loads the illegitimate wordbooks asynchronously.
        /// </summary>
        /// <returns>
        ///     A <see cref="Task" /> represents the loading operation.
        /// </returns>
        private async Task LoadIllegitimateWordbooksAsync()
        {
            // Notify the user interface.
            this.LongRunningTaskDescription = NotificationsLayer.PreparingUserInterfaceMessage;
            this.IsPerformingLongRunningTask = true;

            try
            {
                // Load the illegitimate wordbooks in a separated task.
                await this.GetIllegitimateWordbooksAsync().ContinueWith(
                    getIllegitimateWordbooksTask =>
                        {
                            // Assign illegitimate wordbooks.
                            this.IllegitimateWordbooks =
                                new ObservableCollection<Wordbook>(getIllegitimateWordbooksTask.Result);

                            // Assign illegitimate parental wordbooks.
                            this.IllegitimateParentalWordbooks = new ObservableCollection<Wordbook>(
                                getIllegitimateWordbooksTask.Result.Where(wordbook => wordbook.IsFolder).ToList());
                        },
                    this.CancellationToken,
                    TaskContinuationOptions.OnlyOnRanToCompletion,
                    TaskScheduler.Current).ConfigureAwait(false);
            }
            finally
            {
                // Notify that the task is done.
                this.IsPerformingLongRunningTask = false;
                this.LongRunningTaskDescription = string.Empty;
            }
        }

        /// <summary>
        ///     Saves the data of the current <seealso cref="Wordbook" /> asynchronously.
        /// </summary>
        /// <returns>
        ///     A <see cref="Task" /> represents the Insert or the Update operations.
        /// </returns>
        private async Task SaveWordbookDataAsync()
        {
            // Notify the user interface.
            this.IsPerformingLongRunningTask = true;

            try
            {
                // Check the name and code of current wordbook.
                if (!await this.IsNameOrCodeDuplicatedAsync().ConfigureAwait(false))
                {
                    // Append or remove the parent wordbook code part.
                    this.AppendOrRemoveParentCodePart();

                    // Asynchronously insert or update the model.
                    switch (this.Model.Id)
                    {
                        case 0:
                            await this.InsertAsync().ConfigureAwait(false);
                            break;

                        default:

                            await this.UpdateAsync().ConfigureAwait(false);
                            break;
                    }
                }
            }
            finally
            {
                this.IsPerformingLongRunningTask = false;
            }
        }

        /// <summary>
        ///     Sets the related properties when the user change the parent of current wordbook.
        /// </summary>
        private void SetRelatedPropertiesOnParentChanged()
        {
            if (this.Model.MajorWordbook == null)
            {
                // Set the current level.
                this.LevelNumber = 1;

                // Set the current parent id.
                this.Model.ParentId = null;

                // User can change the type.
                this.CanChangeType = true;
            }
            else
            {
                // Set the parent identity.
                this.Model.ParentId = this.Model.MajorWordbook.Id;

                // Set the level.
                this.LevelNumber = (byte)(this.Model.MajorWordbook.LevelNumber + 1);

                // Rule 1: The Main wordbook can hold children of different types,
                // otherwise; must hold child of the same type..
                var parentWordbookType = (WordbookTypes)this.Model.MajorWordbook.Type;
                if (parentWordbookType != WordbookTypes.Main)
                {
                    this.Type = (WordbookTypes)this.Model.MajorWordbook.Type;
                }

                // Rule 2: If the parent is of the Type Main, user can change the current wordbook type.
                this.CanChangeType = parentWordbookType == WordbookTypes.Main;
            }
        }

        /// <summary>
        ///     Sets the related properties when the user change the type of current wordbook.
        /// </summary>
        private void SetRelatedPropertiesOnTypeChanged()
        {
            switch (this.Model.Type)
            {
                /* WordbookTypes.Main Rules ::
                    1: The IsFolder property must be true.
                    2: User can not change the value of IsFolder property.
                 */
                case (int)WordbookTypes.Main:
                    this.IsFolder = true;
                    this.CanChangeChildhoodStatus = false;
                    break;

                /* WordbookTypes.Others* Rules ::
                  1: The IsFolder property can be as it is.
                  2: User can change the value of IsFolder property.
               */
                case (int)WordbookTypes.Verb:
                case (int)WordbookTypes.Noun:
                case (int)WordbookTypes.Unlike:
                case (int)WordbookTypes.Expression:
                    this.CanChangeChildhoodStatus = true;
                    break;
            }
        }

        /// <summary>
        ///     Sets the selected <paramref name="wordbook" /> into the input fields.
        /// </summary>
        /// <param name="wordbook">The <paramref name="wordbook" /> to edit</param>
        private void SetWordbookIntoEditMode([NotNull] object wordbook)
        {
            // Cast the passed object to IEntity.
            if (!(wordbook is IEntity selectedWordbook))
            {
                return;
            }

            // If the selected wordbook is in the edit mode, display a notification and return.
            if (this.Model.Id.Equals(selectedWordbook.Id))
            {
                this.DisplayDesktopAlert(
                    NotificationsLayer.DataUpdating,
                    string.Format(NotificationsLayer.CanEditDataModelFromInputFields, this.Model.Name),
                    AlertTypes.Information);
            }
            else
            {
                // Set the inner Model.
                this.Model = (Wordbook)selectedWordbook;

                // Remove the parent code part.
                this.Model.PureCodePart = this.Model.Code.Remove(0, this.Model.Code.Length - 2);

                // Update the user interface depending on the selected Model, and invoke property changed events.
                this.OnPropertyChanged(string.Empty);
                this.OnPropertyChanged(nameof(this.Model.Type));
                this.OnPropertyChanged(nameof(this.Model.MajorWordbook));
            }
        }

        /// <summary>
        ///     Updates the code of <paramref name="subWordbooks" /> by appending or removing the parent code recursively.
        /// </summary>
        /// <param name="parentCode">The parent code.</param>
        /// <param name="subWordbooks">The <paramref name="subWordbooks" />.</param>
        private void UpdateSubWordbooksCodeRecursively(
            [NotNull] string parentCode,
            [NotNull] IEnumerable<Wordbook> subWordbooks)
        {
            foreach (var subWordbook in subWordbooks)
            {
                // Remove the old parent code.
                var childCode = subWordbook.Code.Remove(0, subWordbook.Code.Length - 2);

                // Replace the old code with the new one.
                subWordbook.Code = parentCode + "-" + childCode;

                // Loop to catch the last child and update its code.
                if (subWordbook.SubWordbooks != null)
                {
                    this.UpdateSubWordbooksCodeRecursively(subWordbook.Code, subWordbook.SubWordbooks);
                }
            }
        }
    }
}