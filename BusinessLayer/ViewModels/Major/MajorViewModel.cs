﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MajorViewModel.cs" company="None">
// Last Modified: 01/10/2018
// Author: Ahmed Ghanam.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace WordsBooklet.BusinessLayer.ViewModels
{
    using System;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Windows.Input;

    using Telerik.Windows.Controls;
    using Telerik.Windows.Controls.Docking;

    using WordsBooklet.BusinessLayer.Infrastructure;

    /// <summary>
    ///     Represents the business logic for the Major window.
    /// </summary>
    public sealed class MajorViewModel
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="MajorViewModel" /> class.
        /// </summary>
        public MajorViewModel()
        {
            this.PanesCollection = new ObservableCollection<RadPane>();
        }

        /// <summary>
        ///     Gets a command to display the pane type as <see cref="RadPane" />.
        /// </summary>
        /// <value>
        ///     The command to display the pane type as <see cref="RadPane" />.
        /// </value>
        [NotNull]
        public ICommand DisplayPaneTypeCommand => new RelayCommand(this.DisplayPaneType);

        /// <summary>
        ///     Gets the panes collection in the docking control.
        /// </summary>
        /// <value>
        ///     The panes collection in the docking control.
        /// </value>
        [NotNull]
        public ObservableCollection<RadPane> PanesCollection { get; }

        /// <summary>
        ///     Gets a command to remove the closed pane from panes collection.
        /// </summary>
        /// <value>
        ///     The command to remove the closed pane from panes collection.
        /// </value>
        [NotNull]

        public ICommand RemoveClosedPaneCommand => new RelayCommand(this.RemoveClosedPane);

        /// <summary>
        ///     Displays a pane type as a <see cref="RadPane" />.
        /// </summary>
        /// <param name="paneType">The pane type.</param>
        /// <exception cref="ArgumentNullException">
        ///     The passed pane type is not a <see cref="Type" /> or it is not a <see cref="RadPane" />.
        /// </exception>
        private void DisplayPaneType([NotNull] object paneType)
        {
            // Cast the passed type to Type.
            if (!(paneType is Type paneAsType))
            {
                throw new ArgumentNullException(nameof(paneAsType));
            }

            // Create an instance of the pane type.
            var paneInstance = Activator.CreateInstance(paneAsType);

            // Convert the instance to a RadPane instance.
            if (!(paneInstance is RadPane radPaneInstance))
            {
                throw new ArgumentNullException(nameof(paneInstance));
            }

            // Check if the pane is already exist or not by its Tag.
            var isExistRadPane = this.PanesCollection.FirstOrDefault(pane => pane.Tag.Equals(radPaneInstance.Tag));
            if (isExistRadPane != null)
            {
                // If already exist active it.
                isExistRadPane.IsActive = true;
            }
            else
            {
                // If is not already exist, add it.
                this.PanesCollection.Add(radPaneInstance);
            }
        }

        /// <summary>
        ///     Removes out the closed pane from the panes collection.
        /// </summary>
        /// <param name="eventArgs">The event arguments.</param>
        /// <exception cref="ArgumentNullException">if the event args is empty</exception>
        private void RemoveClosedPane([NotNull] object eventArgs)
        {
            // Get the closed pane.
            var closedPane = ((StateChangeEventArgs)eventArgs).Panes.First();
            if (closedPane == null)
            {
                throw new ArgumentNullException();
            }

            // Dispose the data context object, which will also close all desktop notifications.
            if (closedPane.DataContext is IDisposable dataContextDisposableObject)
            {
                dataContextDisposableObject.Dispose();
            }

            // Remove the closed pane form the panes collection.
            if (this.PanesCollection.Contains(closedPane))
            {
                this.PanesCollection.Remove(closedPane);
            }

            // Remove the closed pane from its parent, it will also dispose it.
            closedPane.RemoveFromParent();
        }
    }
}