﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="VariationsPlayerViewModel.cs" company="None">
// Last Modified: 12/10/2018
// Author: Ahmed Ghanam.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace WordsBooklet.BusinessLayer.ViewModels
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Collections.Specialized;
    using System.Data.Entity;
    using System.IO;
    using System.Linq;
    using System.Media;
    using System.Threading;
    using System.Threading.Tasks;
    using System.Windows;

    using Microsoft.Win32;

    using WordsBooklet.BusinessLayer.Infrastructure;
    using WordsBooklet.BusinessLayer.Infrastructure.Notifications.Resources;
    using WordsBooklet.BusinessLayer.Models;

    /// <inheritdoc />
    /// <summary>
    ///     Playing and saving the sound of the <seealso cref="VariationsHeader" /> data model.
    /// </summary>
    /// <seealso cref="NotificationsLayer{T}" />
    public class VariationsPlayerViewModel : NotificationsLayer<VariationsHeader>
    {
        /// <summary>
        ///     The back field for the <see cref="BeginUtteringVariationsDetail" /> property.
        /// </summary>
        [CanBeNull]
        private VariationsDetail beginUtteringVariationsDetail;

        /// <summary>
        ///     The back field for the <see cref="IsUtteringPaused" /> property.
        /// </summary>
        private bool isUtteringPaused;

        /// <summary>
        ///     The back field for the <see cref="IsUtteringVariationsDetail" /> property.
        /// </summary>
        private bool isUtteringVariationsDetail;

        /// <summary>
        ///     The back field for the <see cref="StoredVariationsHeaders" /> property.
        /// </summary>
        [CanBeNull]
        private ObservableCollection<VariationsHeader> storedVariationsHeaders;

        /// <inheritdoc />
        /// <summary>
        ///     Initializes a new instance of the <see cref="VariationsPlayerViewModel" /> class.
        /// </summary>
        public VariationsPlayerViewModel()
        {
            // Set the SelectedVariationsHeaders collection changed event handler.
            this.SelectedVariationsHeaders.CollectionChanged += this.SelectedVariationsHeadersChangedAsync;
        }

        /// <summary>
        ///     Gets the variations detail which is being uttered now.
        /// </summary>
        /// <value>
        ///     The variations detail which is being uttered now.
        /// </value>
        [CanBeNull]
        public VariationsDetail BeginUtteringVariationsDetail
        {
            get => this.beginUtteringVariationsDetail;

            private set
            {
                this.beginUtteringVariationsDetail = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets a value indicating whether <see langword="this" /> instance is uttering a variations detail or not.
        /// </summary>
        /// <value>
        ///     <see langword="true" /> if <see langword="this" /> instance is uttering a variations detail otherwise,
        ///     <see langword="false" />.
        /// </value>
        public bool IsUtteringVariationsDetail
        {
            get => this.isUtteringVariationsDetail;

            private set
            {
                this.isUtteringVariationsDetail = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets a command to load the variations headers from the data source asynchronously.
        /// </summary>
        /// <value>
        ///     The command to load the variations headers from the data source asynchronously.
        /// </value>
        [NotNull]
        public AsyncCommand LoadVariationsHeadersCommand => new AsyncCommand(this.LoadVariationsHeadersAsync);

        /// <summary>
        ///     Gets a command to pause uttering the selected variations details.
        /// </summary>
        /// <value>
        ///     The command to pause uttering the selected variations details.
        /// </value>
        [NotNull]
        public RelayCommand PauseUtteringCommand =>
            new RelayCommand(
                canExecute => this.IsUtteringVariationsDetail && !this.IsUtteringPaused,
                execute =>
                    {
                        using (var cancellationTokenSource = new CancellationTokenSource())
                        {
                            this.IsUtteringPaused = true;
                            cancellationTokenSource.Cancel();
                            this.CancellationToken = cancellationTokenSource.Token;
                        }
                    });

        /// <summary>
        ///     Gets a command to resume uttering the selected variations details asynchronously.
        /// </summary>
        /// <value>
        ///     The command to resume uttering the selected variations details asynchronously.
        /// </value>
        [NotNull]
        public AsyncCommand ResumeUtteringCommand =>
            new AsyncCommand(
                canExecute => this.IsUtteringPaused && !this.IsUtteringVariationsDetail
                                                    && this.SelectedVariationsDetails.Any(),
                this.UtterVariationsDetailsAsync);

        /// <summary>
        ///     Gets a command to save the selected variations details as a sound file asynchronously.
        /// </summary>
        /// <value>
        ///     The command to save the selected variations details as a sound file asynchronously.
        /// </value>
        [NotNull]
        public AsyncCommand SaveAsSoundFileCommand =>
            new AsyncCommand(canExecute => this.SelectedVariationsDetails.Any(), this.SaveAsSoundFileAsync);

        /// <summary>
        ///     Gets the variations details of the selected variations headers.
        /// </summary>
        /// <value>
        ///     The variations details of the selected variations headers.
        /// </value>
        [NotNull]
        public ObservableCollection<VariationsDetail> SelectedVariationsDetails { get; } =
            new ObservableCollection<VariationsDetail>();

        /// <summary>
        ///     Gets or sets the variations headers which user has selected to utter.
        /// </summary>
        /// <value>
        ///     The variations headers which user has selected to utter.
        /// </value>
        [NotNull]
        public ObservableCollection<VariationsHeader> SelectedVariationsHeaders { get; set; } =
            new ObservableCollection<VariationsHeader>();

        /// <summary>
        ///     Gets the variations headers stored into the data source.
        /// </summary>
        /// <value>
        ///     The variations headers stored into the data source.
        /// </value>
        [CanBeNull]
        public ObservableCollection<VariationsHeader> StoredVariationsHeaders
        {
            get => this.storedVariationsHeaders;

            private set
            {
                this.storedVariationsHeaders = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets a command to utter the selected variations details from beginning asynchronously.
        /// </summary>
        /// <value>
        ///     The command to utter the selected variations details from beginning asynchronously.
        /// </value>
        [NotNull]
        public AsyncCommand UtterVariationsDetailsCommand =>
            new AsyncCommand(
                canExecute => !this.IsUtteringVariationsDetail && this.SelectedVariationsDetails.Any(),
                this.UtterVariationsDetailsFromBeginningAsync);

        /// <summary>
        ///     Gets or sets the cancellation token for controlling tasks.
        /// </summary>
        /// <value>
        ///     The cancellation token for controlling tasks.
        /// </value>
        private new CancellationToken CancellationToken { get; set; }

        /// <summary>
        ///     Gets or sets a value indicating whether the user has paused the uttering or not.
        /// </summary>
        /// <value>
        ///     <see langword="true" /> if the user has paused the uttering; otherwise, <see langword="false" />.
        /// </value>
        private bool IsUtteringPaused
        {
            get => this.isUtteringPaused;

            set
            {
                this.isUtteringPaused = value;
                this.OnPropertyChanged();
            }
        }

        /// <inheritdoc />
        /// <summary>
        ///     Releases unmanaged and - optionally - managed resources.
        /// </summary>
        /// <param name="disposing">
        ///     <see langword="true" /> to release both managed and unmanaged resources; <see langword="false" /> to release only
        ///     unmanaged resources.
        /// </param>
        /// <exception cref="ObjectDisposedException">This <see cref="CancellationTokenSource" /> has been disposed.</exception>
        /// <exception cref="AggregateException">
        ///     An aggregate exception containing all the exceptions thrown by the registered
        ///     callbacks on the associated <see cref="CancellationToken" />.
        /// </exception>
        protected override void Dispose(bool disposing)
        {
            // Call base implementation, Always first.
            base.Dispose(disposing);

            if (!disposing)
            {
                return;
            }

            using (var cancellationTokenSource = new CancellationTokenSource())
            {
                cancellationTokenSource.Cancel();
                this.CancellationToken = cancellationTokenSource.Token;
            }

            // Dispose related commands.
            this.ResumeUtteringCommand.Dispose();
            this.SaveAsSoundFileCommand.Dispose();
            this.LoadVariationsHeadersCommand.Dispose();
            this.UtterVariationsDetailsCommand.Dispose();

            // Remove event listeners and clear the variations detail collection.
            this.SelectedVariationsHeaders.CollectionChanged -= this.SelectedVariationsHeadersChangedAsync;
        }

        /// <summary>
        ///     Creates a sound file using the <see langword="byte" /> arrays of the selected variations details asynchronously.
        /// </summary>
        /// <param name="soundFilePath">The sound file path.</param>
        /// <returns>
        ///     A <see cref="Task" /> represents the creation operation.
        /// </returns>
        [NotNull]
        private Task CreateSoundFileAsync([NotNull] string soundFilePath)
        {
            return Task.Factory.StartNew(
                () =>
                    {
                        // Compress all sounds streams into a single array.
                        var soundFilesStreams = new List<byte[]>();
                        foreach (var selectedVariationsDetail in this.SelectedVariationsDetails)
                        {
                            soundFilesStreams.Add(selectedVariationsDetail.GetSoundStreams());
                        }

                        // Save byte array as sound file (.WAV).
                        var soundFileWriter = new SoundFilesWriter();
                        soundFileWriter.CreateSoundFile(soundFilePath, soundFilesStreams);
                    },
                base.CancellationToken);
        }

        /// <summary>
        ///     Loads the variations headers from data source asynchronously.
        /// </summary>
        /// <returns>
        ///     A <see cref="Task" /> represents the loading operation.
        /// </returns>
        private async Task LoadVariationsHeadersAsync()
        {
            // Notify the user interface.
            this.LongRunningTaskDescription = NotificationsLayer.PreparingUserInterfaceMessage;
            this.IsPerformingLongRunningTask = true;

            try
            {
                await this.DataEntities.VariationsHeaders.ToListAsync(base.CancellationToken).ContinueWith(
                    loadingTask =>
                        {
                            this.StoredVariationsHeaders =
                                new ObservableCollection<VariationsHeader>(loadingTask.Result);
                        },
                    base.CancellationToken,
                    TaskContinuationOptions.OnlyOnRanToCompletion,
                    TaskScheduler.Current).ConfigureAwait(false);
            }
            finally
            {
                this.IsPerformingLongRunningTask = false;
                this.LongRunningTaskDescription = string.Empty;
            }
        }

        /// <summary>
        ///     Plays the passed sound stream as a sound asynchronously.
        /// </summary>
        /// <param name="soundStream">The sound as <see langword="byte" /> array.</param>
        /// <returns>
        ///     A <see cref="Task" /> represent the audio playing operation.
        /// </returns>
        [NotNull]
        private Task PlaySoundStreamAsync([CanBeNull] byte[] soundStream)
        {
            // Throw exception if the user canceled the task.
            this.CancellationToken.ThrowIfCancellationRequested();

            // Return a completed task if the passed stream is null.
            if (soundStream == null)
            {
                return Task.CompletedTask;
            }

            // Return a completed task if the passed stream is empty.
            if (soundStream.Length == 0)
            {
                return Task.CompletedTask;
            }

            // Play the sound using a separate task.
            return Task.Factory.StartNew(
                () =>
                    {
                        using (var soundPlayer = new SoundPlayer(new MemoryStream(soundStream)))
                        {
                            soundPlayer.PlaySync();
                        }
                    },
                this.CancellationToken);
        }

        /// <summary>
        ///     Saves the selected variations details as a sound file asynchronously.
        /// </summary>
        /// <returns>
        ///     A <see cref="Task" /> represents the saving operation.
        /// </returns>
        private async Task SaveAsSoundFileAsync()
        {
            // Notify the user interface.
            this.IsPerformingLongRunningTask = true;

            // Hold the values of SaveFileDialog.
            bool? saveFileDialogResult = false;
            var saveFileDialogFullName = string.Empty;

            try
            {
                // Display the SaveFileDialog on the main thread.
                Application.Current.Dispatcher.Invoke(
                    () =>
                        {
                            var saveFileDialog = new SaveFileDialog
                                                     {
                                                         ValidateNames = true,
                                                         OverwritePrompt = true,
                                                         CheckPathExists = true,
                                                         RestoreDirectory = true,
                                                         Filter = "wav files (*.wav)|*.wav",
                                                         FileName =
                                                             this.SelectedVariationsHeaders[0].FullName
                                                             ?? string.Empty
                                                     };
                            saveFileDialogResult = saveFileDialog.ShowDialog();
                            saveFileDialogFullName = saveFileDialog.FileName;
                        });

                // Create the sound file in a separate task.
                if (saveFileDialogResult == true)
                {
                    await this.CreateSoundFileAsync(saveFileDialogFullName).ConfigureAwait(false);
                }
            }
            finally
            {
                this.IsPerformingLongRunningTask = false;
            }
        }

        /// <summary>
        ///     Occurs when the collection of the selected variations headers changed.
        /// </summary>
        /// <param name="sender">The <paramref name="sender" />.</param>
        /// <param name="e">The <see cref="NotifyCollectionChangedEventArgs" /> instance containing the event data.</param>
        private async void SelectedVariationsHeadersChangedAsync(
            [NotNull] object sender,
            [NotNull] NotifyCollectionChangedEventArgs e)
        {
            // Handle only the new and the old VariationsHeader.
            if (!((e.NewItems?[0] ?? e.OldItems?[0]) is VariationsHeader variationsHeader))
            {
                return;
            }

            // Cancel resuming sound playing.
            this.IsUtteringPaused = false;

            // Notify the user interface.
            this.IsPerformingLongRunningTask = true;

            try
            {
                await Task.Factory.StartNew(
                    () =>
                        {
                            Application.Current.Dispatcher.Invoke(
                                () =>
                                    {
                                        switch (e.Action)
                                        {
                                            case NotifyCollectionChangedAction.Add:
                                                foreach (var variationsDetail in variationsHeader.VariationsDetails)
                                                {
                                                    this.SelectedVariationsDetails.Add(variationsDetail);
                                                }

                                                break;

                                            case NotifyCollectionChangedAction.Remove:
                                                foreach (var variationsDetail in variationsHeader.VariationsDetails)
                                                {
                                                    this.SelectedVariationsDetails.Remove(variationsDetail);
                                                }

                                                break;
                                        }
                                    });
                        },
                    base.CancellationToken,
                    TaskCreationOptions.None,
                    TaskScheduler.Current).ConfigureAwait(false);
            }
            finally
            {
                this.IsPerformingLongRunningTask = false;
            }
        }

        /// <summary>
        ///     Utters the variations in the variations detail asynchronously.
        /// </summary>
        /// <param name="variationsDetail">The variations detail.</param>
        /// <returns>
        ///     A <see cref="Task" /> represents the uttering operation.
        /// </returns>
        private async Task UtterVariationsDetailAsync([NotNull] VariationsDetail variationsDetail)
        {
            try
            {
                // Continue if the variation is not uttered as much as declared.
                while (variationsDetail.TimesNumberUttered < variationsDetail.PronunciationTimes)
                {
                    // Notify that the uttering has been resumed.
                    this.IsUtteringPaused = false;

                    // Notify that uttering has been started.
                    this.IsUtteringVariationsDetail = true;

                    // Hold the variation detail which begin uttering.
                    this.BeginUtteringVariationsDetail = variationsDetail;

                    // Play The Variations Using Arabic.
                    await this.PlaySoundStreamAsync(variationsDetail.ArabicPronunciation).ConfigureAwait(false);

                    // Play The Variations Using Norwegian.
                    await this.PlaySoundStreamAsync(variationsDetail.NorwegianPronunciation).ConfigureAwait(false);

                    // Play The Variations Example Using Norwegian.
                    await this.PlaySoundStreamAsync(variationsDetail.NorwegianExamplePronunciation)
                        .ConfigureAwait(false);

                    // Increase the value of the TimesNumberUttered.
                    variationsDetail.TimesNumberUttered += 1;
                }
            }
            finally
            {
                // Notify that uttering has been done.
                this.IsUtteringVariationsDetail = false;

                // Release the last uttered variations.
                this.BeginUtteringVariationsDetail = null;
            }
        }

        /// <summary>
        ///     Utters the variations details of the selected variations headers asynchronously.
        /// </summary>
        /// <returns>
        ///     A <see cref="Task" /> represent the uttering operation.
        /// </returns>
        private async Task UtterVariationsDetailsAsync()
        {
            // Initialize a new instance of the CancellationToken.
            if (this.CancellationToken.IsCancellationRequested)
            {
                this.CancellationToken = new CancellationToken();
            }

            // Loop on the selected variations headers. 
            foreach (var selectedVariationsHeader in this.SelectedVariationsHeaders)
            {
                // Continue if the header is not uttered as much as declared.
                while (selectedVariationsHeader.TimesNumberUttered < selectedVariationsHeader.PronunciationTimes)
                {
                    // Utter variations detail in a separated task as much as declared where it has been paused.
                    foreach (var variationsDetail in selectedVariationsHeader.VariationsDetails)
                    {
                        await this.UtterVariationsDetailAsync(variationsDetail).ConfigureAwait(false);
                    }

                    // Increase the value of the TimesNumberUttered.
                    selectedVariationsHeader.TimesNumberUttered += 1;

                    // Reset the value of TimesNumberUttered until the last rotation on the header.
                    if (selectedVariationsHeader.TimesNumberUttered < selectedVariationsHeader.PronunciationTimes)
                    {
                        foreach (var variationsDetail in selectedVariationsHeader.VariationsDetails)
                        {
                            variationsDetail.TimesNumberUttered = 0;
                        }
                    }
                }
            }
        }

        /// <summary>
        ///     Utters the selected variations details from beginning asynchronously.
        /// </summary>
        /// <returns>
        ///     A <see cref="Task" /> represents the uttering operation.
        /// </returns>
        [NotNull]
        private Task UtterVariationsDetailsFromBeginningAsync()
        {
            // Reset the value of TimesNumberUttered for both headers and details.
            foreach (var variationsHeader in this.SelectedVariationsHeaders)
            {
                variationsHeader.TimesNumberUttered = 0;
                foreach (var variationsDetail in variationsHeader.VariationsDetails)
                {
                    variationsDetail.TimesNumberUttered = 0;
                }
            }

            // Utter the variations details of the selected headers.
            return this.UtterVariationsDetailsAsync();
        }
    }
}