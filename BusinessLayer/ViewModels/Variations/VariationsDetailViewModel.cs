﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="VariationsDetailViewModel.cs" company="None">
// Last Modified: 12/10/2018
// Author: Ahmed Ghanam.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace WordsBooklet.BusinessLayer.ViewModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.Diagnostics;
    using System.IO;
    using System.Linq;
    using System.Media;
    using System.Reflection;
    using System.Threading;
    using System.Threading.Tasks;
    using System.Windows;

    using WordsBooklet.BusinessLayer.Infrastructure;
    using WordsBooklet.BusinessLayer.Models;

    /// <inheritdoc />
    /// <summary>
    ///     Handling the <seealso cref="VariationsDetail" /> data model.
    /// </summary>
    /// <seealso cref="NotificationsLayer{T}" />
    public sealed class VariationsDetailViewModel : NotificationsLayer<VariationsDetail>
    {
        /// <summary>
        ///     Represents the properties names of the changed variations.
        /// </summary>
        [NotNull]
        private readonly List<string> changedVariationsList = new List<string>();

        /// <summary>
        ///     The cognitive services instance.
        /// </summary>
        [CanBeNull]
        private CognitiveServices cognitiveServices;

        /// <summary>
        ///     Represents the <see langword="public" /> properties of current data Model.
        /// </summary>
        [CanBeNull]
        private PropertyInfo[] modelProperties;

        /// <summary>
        ///     Represents the current data Model type.
        /// </summary>
        [CanBeNull]
        private Type modelType;

        /// <inheritdoc />
        /// <summary>
        ///     Initializes a new instance of the <see cref="VariationsDetailViewModel" /> class.
        /// </summary>
        public VariationsDetailViewModel()
        {
        }

        /// <inheritdoc />
        /// <summary>
        ///     Initializes a new instance of the <see cref="VariationsDetailViewModel" /> class.
        /// </summary>
        /// <param name="variationsDetail">The variations detail to be set as the inner Model.</param>
        internal VariationsDetailViewModel([NotNull] VariationsDetail variationsDetail)
        {
            this.SetInnerModel(variationsDetail);
        }

        /// <summary>
        ///     Gets or sets the character set of variations using the Arabic language.
        /// </summary>
        /// <value>
        ///     The character set of variations using the Arabic language.
        /// </value>
        [CanBeNull]
        public string ArabicCharacterSet
        {
            get => this.Model.ArabicCharacterSet;
            set
            {
                this.Model.ArabicCharacterSet = value;

                this.OnPropertyChanged(this.Model.ArabicCharacterSet, nameof(this.Model.ArabicCharacterSet));
            }
        }

        /// <summary>
        ///     Gets or sets the character set of variations using the Norwegian (Bokmål) language.
        /// </summary>
        /// <value>
        ///     The character set of variations using the Norwegian (Bokmål) language.
        /// </value>
        [CanBeNull]
        public string NorwegianCharacterSet
        {
            get => this.Model.NorwegianCharacterSet;
            set
            {
                this.Model.NorwegianCharacterSet = value;

                this.OnPropertyChanged(this.Model.NorwegianCharacterSet, nameof(this.Model.NorwegianCharacterSet));
            }
        }

        /// <summary>
        ///     Gets or sets the character set of the example using the Norwegian (Bokmål) language.
        /// </summary>
        /// <value>
        ///     The character set of the example using the Norwegian (Bokmål) language.
        /// </value>
        [CanBeNull]
        public string NorwegianExampleCharacterSet
        {
            get => this.Model.NorwegianExampleCharacterSet;
            set
            {
                this.Model.NorwegianExampleCharacterSet = value;

                this.OnPropertyChanged(
                    this.Model.NorwegianExampleCharacterSet,
                    nameof(this.Model.NorwegianExampleCharacterSet));
            }
        }

        /// <summary>
        ///     Gets or sets the count of times to repeat pronunciation of current variations.
        /// </summary>
        /// <value>
        ///     The count of times to repeat pronunciation of current variations.
        /// </value>
        public byte PronunciationTimes
        {
            get => this.Model.PronunciationTimes;
            set
            {
                this.Model.PronunciationTimes = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets the variation serial number in the current variations collection.
        /// </summary>
        /// <value>
        ///     The variation serial number in the current variations collection.
        /// </value>
        public short SerialNumber
        {
            get => this.Model.SerialNumber;
            internal set
            {
                this.Model.SerialNumber = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets the speed of the speaking rate.
        /// </summary>
        /// <value>
        ///     The speed of the speaking rate.
        /// </value>
        [NotNull]
        public string SpeakingRate
        {
            get => this.Model.SpeakingRate;
            set
            {
                this.Model.SpeakingRate = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets a value indicating whether <see langword="this" />
        ///     instance should play the sound once it is available or not.
        /// </summary>
        /// <value>
        ///     <see langword="true" /> if <see langword="this" /> instance should
        ///     play the sound once it is available; otherwise, <see langword="false" />.
        /// </value>
        private bool AutoPlaySound { get; set; }

        /// <summary>
        ///     Converts variations from the inner data Model to sound streams and pronounce them asynchronously.
        /// </summary>
        /// <param name="cognitiveServicesInstance">The cognitive services instance.</param>
        /// <returns>
        ///     A <see cref="Task{TResult}" /> represents the converted data Model.
        /// </returns>
        /// <exception cref="ValidationException">The required variations are null or empty.</exception>
        [ItemNotNull]
        internal async Task<VariationsDetail> ConvertInnerModelAsync(
            [NotNull] CognitiveServices cognitiveServicesInstance)
        {
            // Initialize the cognitive services.
            if (this.cognitiveServices == null)
            {
                this.cognitiveServices = cognitiveServicesInstance;
            }

            // Convert The Variation Using Arabic To Sound Stream.
            this.Model.ArabicPronunciation = await this.ConvertInnerModelAsync(
                                                 Languages.Arabic,
                                                 nameof(this.Model.ArabicCharacterSet),
                                                 this.Model.ArabicCharacterSet,
                                                 this.Model.ArabicPronunciation).ConfigureAwait(false);

            // Validate The Variation Using Arabic.
            if (this.Model.ArabicPronunciation.Length == 0)
            {
                throw new ValidationException();
            }

            // Convert The Variation Using Norwegian To Sound Stream.
            this.Model.NorwegianPronunciation = await this.ConvertInnerModelAsync(
                                                    Languages.Bokmål,
                                                    nameof(this.Model.NorwegianCharacterSet),
                                                    this.Model.NorwegianCharacterSet,
                                                    this.Model.NorwegianPronunciation).ConfigureAwait(false);

            // Validate The Variation Using Norwegian.
            if (this.Model.NorwegianPronunciation.Length == 0)
            {
                throw new ValidationException();
            }

            // Convert The Norwegian Example To Sound Stream.
            this.Model.NorwegianExamplePronunciation = await this.ConvertInnerModelAsync(
                                                               Languages.Bokmål,
                                                               nameof(this.Model.NorwegianExampleCharacterSet),
                                                               this.Model.NorwegianExampleCharacterSet,
                                                               this.Model.NorwegianExamplePronunciation)
                                                           .ConfigureAwait(false);

            // Return the inner Model holding all filled properties.
            return this.Model;
        }

        /// <summary>
        ///     Determines whether the user confirmed to delete the inner Model or not.
        /// </summary>
        /// <returns>
        ///     <see langword="true" /> if the user confirmed to delete the inner Model; otherwise, <see langword="false" />.
        /// </returns>
        internal bool IsUserConfirmedDeletingModel()
        {
            // Holds the value of the confirmation.
            var isDeletingConfirmed = false;

            // Ask the user to confirm deleting the inner Model using the main thread.
            Application.Current.Dispatcher.Invoke(
                () => { isDeletingConfirmed = this.IsUserConfirmedDeletingModel(this.Model); });

            // Return the value of the confirmation.
            return isDeletingConfirmed;
        }

        /// <summary>
        ///     Tests the speaking rate of the inner model asynchronously.
        /// </summary>
        /// <param name="cognitiveServicesInstance">The cognitive services instance.</param>
        /// <returns>
        ///     A <see cref="Task" /> represents the testing operation.
        /// </returns>
        /// <exception cref="ValidationException">The required variations are null or empty.</exception>
        internal async Task TestSpeakingRateAsync([NotNull] CognitiveServices cognitiveServicesInstance)
        {
            // Auto play the sound once results are available.
            this.AutoPlaySound = true;

            try
            {
                // Convert the inner Model to a sound streams if its not converted, and play the sound stream.
                await this.ConvertInnerModelAsync(cognitiveServicesInstance).ConfigureAwait(false);
            }
            finally
            {
                // Stop auto playing the sound once its done.
                this.AutoPlaySound = false;
            }
        }

        /// <inheritdoc />
        /// <summary>
        ///     Releases unmanaged and - optionally - managed resources.
        /// </summary>
        /// <param name="disposing">
        ///     <see langword="true" /> to release both managed and unmanaged resources; <see langword="false" /> to release only
        ///     unmanaged resources.
        /// </param>
        /// <exception cref="AggregateException">
        ///     An aggregate exception containing all the exceptions thrown by the registered
        ///     callbacks on the associated <see cref="CancellationToken" />.
        /// </exception>
        protected override void Dispose(bool disposing)
        {
            // Call base implementation, Always first.
            base.Dispose(disposing);

            if (!disposing)
            {
                return;
            }

            // Clear Model type.
            this.modelType = null;

            // Clear Model properties collection.
            this.modelProperties = null;

            // Remove the value of cognitive services instance.
            this.cognitiveServices = null;

            // Clear the changed properties names list.
            this.changedVariationsList.Clear();
        }

        /// <inheritdoc />
        /// <summary>
        ///     Called when a property value changed.
        /// </summary>
        /// <param name="sender">The <paramref name="sender" />.</param>
        /// <param name="e">The <see cref="PropertyChangedEventArgs" /> instance containing the event data.</param>
        protected override void OnPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            switch (e.PropertyName)
            {
                // Changing any variation will result in re-producing its audio stream only.
                case nameof(this.Model.ArabicCharacterSet):
                case nameof(this.Model.NorwegianCharacterSet):
                case nameof(this.Model.NorwegianExampleCharacterSet):

                    // Remove the changed variation from changed variations list, don't hold empty properties.
                    if (this.changedVariationsList.Contains(e.PropertyName))
                    {
                        this.changedVariationsList.Remove(e.PropertyName);
                    }

                    // Get the value of the changed property.
                    var changedPropertyValue = this.GetPropertyValue(e.PropertyName);

                    // If the changed variation is not null, insert it into the changed variations list.
                    if (!string.IsNullOrEmpty(changedPropertyValue))
                    {
                        this.changedVariationsList.Add(e.PropertyName);
                    }

                    break;

                // Changing speaking rate will result in re-producing the audio stream for all variations.
                case nameof(this.Model.SpeakingRate):

                    // Clear the changed variations list.
                    this.changedVariationsList.Clear();

                    // Get properties names which is a string and not empty or null.
                    var propertiesCarryingValues = this.GetPropertiesNameCarryingValues();

                    // Add properties names to the changed variations list.
                    this.changedVariationsList.AddRange(propertiesCarryingValues);

                    break;
            }
        }

        /// <summary>
        ///     Converts the passed variation from string to a sound stream and play it asynchronously.
        /// </summary>
        /// <param name="speakingLanguage">The speaking language.</param>
        /// <param name="targetPropertyName">The variation property name.</param>
        /// <param name="targetPropertyValue">The variation property value.</param>
        /// <param name="currentSoundStreamBytes">The current sound stream.</param>
        /// <returns>
        ///     A <see cref="Task{TResult}" /> represents the sound stream as a byte array.
        /// </returns>
        private async Task<byte[]> ConvertInnerModelAsync(
            Languages speakingLanguage,
            [NotNull] string targetPropertyName,
            [NotNull] string targetPropertyValue,
            [NotNull] byte[] currentSoundStreamBytes)
        {
            // Throw exception if the user canceled the whole task.
            this.CancellationToken.ThrowIfCancellationRequested();

            // Do not convert empty variations.
            if (string.IsNullOrWhiteSpace(targetPropertyValue) || this.cognitiveServices == null)
            {
                return Array.Empty<byte>();
            }

            // Do not convert not changed variations.
            if (this.changedVariationsList.Contains(targetPropertyName))
            {
                // Start measuring the pronouncing time.
                var pronouncingElapsedTime = Stopwatch.StartNew();

                // Build and send a pronunciation request.
                await this.cognitiveServices
                    .ConvertToSoundAsync(targetPropertyValue.Trim(), this.Model.SpeakingRate, speakingLanguage)
                    .ContinueWith(
                        async conversionTask =>
                            {
                                // Stop measuring the pronouncing time.
                                pronouncingElapsedTime.Stop();

                                // Hold the conversion results.
                                currentSoundStreamBytes = conversionTask.Result;

                                // If no conversion results, return.
                                if (currentSoundStreamBytes.Length == 0)
                                {
                                    return;
                                }

                                // Remove the converted variation from the changed variations collection.
                                this.changedVariationsList.Remove(targetPropertyName);

                                // Measuring the total pronouncing time opposite the ability to pronounce 40 words per minute.
                                var waitingMilliseconds = (int)(1500 - pronouncingElapsedTime.ElapsedMilliseconds);

                                // Applying the rule 'User can only pronounce 40 words per minute'.
                                await Task.Delay(
                                    waitingMilliseconds > 0 ? waitingMilliseconds : 0,
                                    this.CancellationToken).ConfigureAwait(false);
                            },
                        this.CancellationToken,
                        TaskContinuationOptions.OnlyOnRanToCompletion,
                        TaskScheduler.Current).Unwrap().ConfigureAwait(false);
            }

            // Return the conversion results if the user did not request playing sounds.
            if (!this.AutoPlaySound)
            {
                return currentSoundStreamBytes;
            }

            // Play the sound if the user requested playing sounds and it is not empty.
            if (currentSoundStreamBytes.Length > 0)
            {
                await this.PlaySoundStreamAsync(currentSoundStreamBytes).ConfigureAwait(false);
            }

            // Return the conversion results.
            return currentSoundStreamBytes;
        }

        /// <summary>
        ///     Gets properties names which carrying values.
        /// </summary>
        /// <returns>
        ///     A <see cref="IEnumerable{T}" /> holding properties names.
        /// </returns>
        [NotNull]
        private IEnumerable<string> GetPropertiesNameCarryingValues()
        {
            // Initialize the Model type.
            if (this.modelType == null)
            {
                this.modelType = this.Model.GetType();
            }

            // Initialize the public Model properties.
            if (this.modelProperties == null)
            {
                this.modelProperties = this.modelType.GetProperties();
            }

            // Return every property matching rules.
            return (from modelPropertyInfo in this.modelProperties
                    where modelPropertyInfo.CanWrite && modelPropertyInfo.PropertyType == typeof(string)
                                                     && !string.IsNullOrWhiteSpace(
                                                         Convert.ToString(modelPropertyInfo.GetValue(this.Model)))
                                                     && !Convert.ToString(modelPropertyInfo.GetValue(this.Model))
                                                         .Contains("%")
                    select modelPropertyInfo.Name).ToList();
        }

        /// <summary>
        ///     Gets the value of the passed property name.
        /// </summary>
        /// <param name="propertyName">The property name.</param>
        /// <returns>
        ///     The value of the passed property name.
        /// </returns>
        [NotNull]
        private string GetPropertyValue([NotNull] string propertyName)
        {
            // Initialize the Model type.
            if (this.modelType == null)
            {
                this.modelType = this.Model.GetType();
            }

            // Get property value using reflection.
            var matchedPropertyInfo = this.modelType.GetProperty(propertyName);
            var valueOfMatchedPropertyInfo = matchedPropertyInfo?.GetValue(this.Model);
            return Convert.ToString(valueOfMatchedPropertyInfo);
        }

        /// <summary>
        ///     Plays the passed sound stream asynchronously.
        /// </summary>
        /// <param name="soundStream">The variation as <see langword="byte" /> array.</param>
        /// <returns>
        ///     A <see cref="Task" /> represent the playing operation.
        /// </returns>
        [NotNull]
        private Task PlaySoundStreamAsync([NotNull] byte[] soundStream)
        {
            // Throw exception if the user canceled the whole conversion task.
            this.CancellationToken.ThrowIfCancellationRequested();

            // Do not play the sound if the stream is empty.
            if (soundStream.Length == 0)
            {
                return Task.CompletedTask;
            }

            // Play the sound using a separate task.
            return Task.Factory.StartNew(
                () =>
                    {
                        using (var soundPlayer = new SoundPlayer(new MemoryStream(soundStream)))
                        {
                            soundPlayer.PlaySync();
                        }
                    },
                this.CancellationToken);
        }

        /// <summary>
        ///     Sets the inner Model using the passed VariationsDetail.
        /// </summary>
        /// <param name="variationsDetail">The variations detail.</param>
        private void SetInnerModel([NotNull] VariationsDetail variationsDetail)
        {
            // Sets the inner Model.
            this.Model = variationsDetail;

            // Notify both the user interface and changed variations list.
            this.OnPropertyChanged(string.Empty);
        }
    }
}