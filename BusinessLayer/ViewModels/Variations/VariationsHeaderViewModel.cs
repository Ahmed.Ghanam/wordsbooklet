﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="VariationsHeaderViewModel.cs" company="None">
// Last Modified: 12/10/2018
// Author: Ahmed Ghanam.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace WordsBooklet.BusinessLayer.ViewModels
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Collections.Specialized;
    using System.Data.Entity;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using System.Windows;

    using WordsBooklet.BusinessLayer.Infrastructure;
    using WordsBooklet.BusinessLayer.Infrastructure.Notifications.Resources;
    using WordsBooklet.BusinessLayer.Models;
    using WordsBooklet.BusinessLayer.ViewModels.Resources;

    /// <inheritdoc />
    /// <summary>
    ///     Handling the <seealso cref="VariationsHeader" /> data model.
    /// </summary>
    /// <seealso cref="NotificationsLayer{T}" />
    public sealed class VariationsHeaderViewModel : NotificationsLayer<VariationsHeader>
    {
        /// <summary>
        ///     The cognitive services instance.
        /// </summary>
        [NotNull]
        private readonly CognitiveServices cognitiveServices;

        /// <summary>
        ///     The back field for the <see cref="IsTestingSpeakingRate" /> property.
        /// </summary>
        private bool isTestingSpeakingRate;

        /// <summary>
        ///     The back field for the <see cref="VariationsWordbooks" /> property.
        /// </summary>
        [CanBeNull]
        private ObservableCollection<Wordbook> variationsWordbooks;

        /// <inheritdoc />
        /// <summary>
        ///     Initializes a new instance of the <see cref="VariationsHeaderViewModel" /> class.
        /// </summary>
        /// <exception cref="OverflowException">
        ///     the call back value is less than <see cref="TimeSpan.MinValue" />
        ///     or greater than <see cref="TimeSpan.MaxValue" />
        ///     .-or-
        ///     the call back value is <see cref="Double.PositiveInfinity" />.-or- the call back value is
        ///     <see cref="Double.NegativeInfinity" />.
        /// </exception>
        /// <exception cref="ArgumentOutOfRangeException">
        ///     The number of milliseconds in the value of call back value is negative
        ///     and not equal to <see cref="Timeout.Infinite" />, or is greater than <see cref="Int32.MaxValue" />.
        /// </exception>
        /// <exception cref="ArgumentException"> the call back value is equal to <see cref="Double.NaN" />. </exception>
        /// <exception cref="ArgumentNullException">The call back value parameter is <see langword="null" />. </exception>
        public VariationsHeaderViewModel()
        {
            // Set the initial values of the inner Model properties.
            this.Model.PronunciationTimes = 1;

            // Initialize the cognitive services instance.
            this.cognitiveServices = new CognitiveServices();
            this.cognitiveServices.OnErrorOccurred += this.OnConversionErrorOccurred;

            // Set the VariationsDetailViewModels collection changed event handler.
            this.VariationsDetailViewModels.CollectionChanged += this.VariationDetailViewModelsChanged;
        }

        /// <summary>
        ///     Gets a command to delete a view model of variations detail.
        /// </summary>
        /// <value>
        ///     The command to delete a view model of variations detail.
        /// </value>
        [NotNull]
        public AsyncCommand DeleteVariationsDetailViewModelCommand =>
            new AsyncCommand(this.DeleteVariationsDetailViewModelAsync);

        /// <summary>
        ///     Gets a value indicating whether the user is testing the speaking rate of a variations detail or not.
        /// </summary>
        /// <value>
        ///     <see langword="true" /> if the user is testing the speaking rate of a variations detail; otherwise,
        ///     <see langword="false" />.
        /// </value>
        public bool IsTestingSpeakingRate
        {
            get => this.isTestingSpeakingRate;

            private set
            {
                this.isTestingSpeakingRate = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets a command to load the Wordbooks which can hold variations from the data source asynchronously.
        /// </summary>
        /// <value>
        ///     The command to load the Wordbooks which can hold variation from the data source asynchronously.
        /// </value>
        [NotNull]
        public AsyncCommand LoadWordbooksForVariationsCommand => new AsyncCommand(this.LoadWordbooksForVariationsAsync);

        /// <summary>
        ///     Gets or sets the current variations header name.
        /// </summary>
        /// <value>
        ///     The current variations header name.
        /// </value>
        [CanBeNull]
        public string Name
        {
            get => this.Model.Name;

            set
            {
                this.Model.Name = value;
                this.OnPropertyChanged(this.Model.Name, nameof(this.Model.Name));
            }
        }

        /// <summary>
        ///     Gets or sets the pronunciation times of the current variations header.
        /// </summary>
        /// <value>
        ///     The pronunciation times of the current variations header.
        /// </value>
        public byte PronunciationTimes
        {
            get => this.Model.PronunciationTimes;

            set
            {
                this.Model.PronunciationTimes = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets the current variations header remarks.
        /// </summary>
        /// <value>
        ///     The current variations header remarks.
        /// </value>
        [CanBeNull]
        public string Remarks
        {
            get => this.Model.Remarks;

            set
            {
                this.Model.Remarks = value;
                this.OnPropertyChanged(this.Model.Remarks, nameof(this.Model.Remarks));
            }
        }

        /// <summary>
        ///     Gets a command to roll the user inputs back.
        /// </summary>
        /// <value>
        ///     The command to roll the user inputs back.
        /// </value>
        [NotNull]
        public RelayCommand RollBackUserInputsCommand =>
            new RelayCommand(
                canExecute => this.HasErrors || !string.IsNullOrEmpty(this.Model.Name)
                                             || !string.IsNullOrEmpty(this.Model.Remarks)
                                             || this.VariationsDetailViewModels.Any(),
                execute => this.RollBackDataChanges());

        /// <summary>
        ///     Gets a command to insert or update the current <seealso cref="VariationsHeader" />.
        /// </summary>
        /// <value>
        ///     The command to insert or update the current <seealso cref="VariationsHeader" />.
        /// </value>
        [NotNull]
        public AsyncCommand SaveVariationsHeaderDataCommand =>
            new AsyncCommand(
                canExecute => this.Model.Wordbook != null && !string.IsNullOrWhiteSpace(this.Model.Name)
                                                          && this.IsModelHasModified()
                                                          && this.VariationsDetailViewModels.Any(
                                                              variationsDetailViewModel =>
                                                                  variationsDetailViewModel.IsModelHasModified()),
                this.SaveVariationsHeaderDataAsync);

        /// <summary>
        ///     Gets a command to open a dialog box to search and select a single variations header.
        /// </summary>
        /// <value>
        ///     The command to open a dialog box to search and select a single variations header.
        /// </value>
        [NotNull]
        public AsyncCommand SearchVariationsHeadersCommand => new AsyncCommand(this.SearchVariationsHeadersAsync);

        /// <summary>
        ///     Gets a command to tests the speaking rate of a view model of variations detail asynchronously.
        /// </summary>
        /// <value>
        ///     The command to tests the speaking rate of a view model of variations detail
        /// </value>
        [NotNull]
        public AsyncCommand TestSpeakingRateCommand => new AsyncCommand(this.TestSpeakingRateAsync);

        /// <summary>
        ///     Gets the view models of the variations details which the current header holding.
        /// </summary>
        /// <value>
        ///     The view models of the variations details which the current header holding.
        /// </value>
        [NotNull]
        public ObservableCollection<VariationsDetailViewModel> VariationsDetailViewModels { get; } =
            new ObservableCollection<VariationsDetailViewModel>();

        /// <summary>
        ///     Gets the wordbooks which can hold variations which stored into the data source.
        /// </summary>
        /// <value>
        ///     The wordbooks which can hold variations which stored into the data source.
        /// </value>
        [CanBeNull]
        public ObservableCollection<Wordbook> VariationsWordbooks
        {
            get => this.variationsWordbooks;

            private set
            {
                this.variationsWordbooks = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets the main wordbook of the current variations header.
        /// </summary>
        /// <value>
        ///     The main wordbook of the current variations header.
        /// </value>
        [CanBeNull]
        public Wordbook Wordbook
        {
            get => this.Model.Wordbook;

            set
            {
                // Set the selected word category object.
                this.Model.Wordbook = value;

                // Set the selected word category identifier.
                this.Model.WordbookId = this.Model.Wordbook?.Id ?? 0;

                // Validate the user inputs and notify the user interface.
                this.OnPropertyChanged(this.Model.Wordbook, nameof(this.Model.Wordbook));
            }
        }

        /// <inheritdoc />
        /// <summary>
        ///     Releases unmanaged and - optionally - managed resources.
        /// </summary>
        /// <param name="disposing">
        ///     <see langword="true" /> to release both managed and unmanaged resources; <see langword="false" /> to release only
        ///     unmanaged resources.
        /// </param>
        /// <exception cref="ObjectDisposedException">This <see cref="CancellationTokenSource" /> has been disposed.</exception>
        /// <exception cref="AggregateException">
        ///     An aggregate exception containing all the exceptions thrown by the registered
        ///     callbacks on the associated <see cref="CancellationToken" />.
        /// </exception>
        protected override void Dispose(bool disposing)
        {
            // Call base implementation, Always first.
            base.Dispose(disposing);

            if (!disposing)
            {
                return;
            }

            // Dispose related commands.
            this.TestSpeakingRateCommand.Dispose();
            this.SearchVariationsHeadersCommand.Dispose();
            this.SaveVariationsHeaderDataCommand.Dispose();
            this.LoadWordbooksForVariationsCommand.Dispose();
            this.DeleteVariationsDetailViewModelCommand.Dispose();

            // Remove event listeners and dispose the cognitive services instance.
            this.cognitiveServices.OnErrorOccurred -= this.OnConversionErrorOccurred;
            this.cognitiveServices.Dispose();

            // Dispose every view model of variations detail.
            foreach (var variationsDetailViewModel in this.VariationsDetailViewModels)
            {
                variationsDetailViewModel.Dispose();
            }

            // Remove event listeners.
            this.VariationsDetailViewModels.CollectionChanged -= this.VariationDetailViewModelsChanged;
        }

        /// <inheritdoc />
        /// <summary>
        ///     Invoked if inserting the data model succeeded.
        /// </summary>
        /// <param name="sender">The control raise the event.</param>
        /// <param name="e">The <see cref="OnSaveChangesEventArgs" /> instance containing the event data.</param>
        /// <exception cref="Exception">A delegate callback throws an exception.</exception>
        protected override void OnInsertingModelSucceeded(object sender, OnSaveChangesEventArgs e)
        {
            // Call the base initialization.
            base.OnInsertingModelSucceeded(sender, e);

            // Prepare the user interface for new inputs.
            this.RollBackUserInputsCommand.Execute();
        }

        /// <inheritdoc />
        /// <summary>
        ///     Invoked if updating the data model succeeded.
        /// </summary>
        /// <param name="sender">The control raise the event.</param>
        /// <param name="e">The <see cref="OnSaveChangesEventArgs" /> instance containing the event data.</param>
        /// <exception cref="Exception">A delegate callback throws an exception.</exception>
        protected override void OnUpdatingModelSucceeded(object sender, OnSaveChangesEventArgs e)
        {
            // Call the base initialization.
            base.OnUpdatingModelSucceeded(sender, e);

            // Prepare the user interface for new inputs.
            this.RollBackUserInputsCommand.Execute();
        }

        /// <inheritdoc />
        /// <summary>
        ///     Roll the user changes back and set every Model to its original values.
        /// </summary>
        /// <exception cref="AggregateException">
        ///     An aggregate exception containing all the exceptions thrown by the registered
        ///     callbacks on the associated <see cref="CancellationToken" />.
        /// </exception>
        /// <exception cref="ObjectDisposedException">This <see cref="CancellationTokenSource" /> has been disposed.</exception>
        /// <exception cref="Exception">A <see langword="delegate" /> callback throws an exception.</exception>
        protected override void RollBackDataChanges()
        {
            // Roll back any data changes.
            if (this.IsModelHasModified())
            {
                base.RollBackDataChanges();
            }

            // Dispose every VariationsDetailViewModels.
            foreach (var variationDetailViewModel in this.VariationsDetailViewModels)
            {
                variationDetailViewModel.Dispose();
            }

            // Clear the VariationsDetailViewModels.
            this.VariationsDetailViewModels.Clear();

            // Initialize a new instance of the corresponding Model with the current customized values.
            this.Model = new VariationsHeader
                             {
                                 Wordbook = this.Model.Wordbook,
                                 CreationDateTime = DateTime.Now,
                                 LastUpdateDateTime = DateTime.Now,
                                 PronunciationTimes = this.Model.PronunciationTimes
                             };

            // Clear any existing validation errors.
            this.ClearValidationErrors();

            // Update the user interface depending on the new Model, and invoke property changed events.
            this.OnPropertyChanged(string.Empty);
        }

        /// <summary>
        ///     Attaches variations details to the current Model.
        /// </summary>
        /// <param name="variationsDetails">The variations details.</param>
        private void AttachVariationsDetails([NotNull] IEnumerable<VariationsDetail> variationsDetails)
        {
            // Add the variations details to the current Model using the main thread.
            Application.Current.Dispatcher.Invoke(
                () =>
                    {
                        foreach (var variationsDetail in variationsDetails)
                        {
                            this.VariationsDetailViewModels.Add(new VariationsDetailViewModel(variationsDetail));
                        }
                    });
        }

        /// <summary>
        ///     Convert details of variations to a sound stream and attach them to the current data Model asynchronously.
        /// </summary>
        /// <param name="progress">The action to report the <paramref name="progress" /> to the user.</param>
        /// <returns>
        ///     A <see cref="Task" /> represents the conversion operation.
        /// </returns>
        private async Task ConvertVariationsDetailsToSoundStreamsAsync([NotNull] IProgress<int> progress)
        {
            // Clear existing VariationsDetails.
            this.Model.VariationsDetails.Clear();

            // Defines and reports the conversion progress percentage.
            var convertedVariationIndex = 0;
            progress.Report(convertedVariationIndex);

            // Convert variations from string to sound stream.
            foreach (var variationsDetailViewModel in this.VariationsDetailViewModels)
            {
                // Every variation will be checked to not convert it multiple times.
                await variationsDetailViewModel.ConvertInnerModelAsync(this.cognitiveServices).ContinueWith(
                    conversionTask =>
                        {
                            // Attach the converted variations to the VariationsDetails collection.
                            this.Model.VariationsDetails.Add(conversionTask.Result);

                            // Increases and reports the conversion progress percentage.
                            convertedVariationIndex++;
                            progress.Report(convertedVariationIndex);
                        },
                    this.CancellationToken,
                    TaskContinuationOptions.OnlyOnRanToCompletion,
                    TaskScheduler.Current).ConfigureAwait(false);
            }
        }

        /// <summary>
        ///     Deletes a view model of variations detail asynchronous.
        /// </summary>
        /// <param name="variationsDetailViewModel">The variations detail view model.</param>
        /// <returns>
        ///     A <see cref="Task" /> represents the deleting operation.
        /// </returns>
        private async Task DeleteVariationsDetailViewModelAsync([NotNull] object variationsDetailViewModel)
        {
            // Cast the passed object to VariationsDetailViewModel.
            var deletingVariationsDetailViewModel = (VariationsDetailViewModel)variationsDetailViewModel;

            // Check the value of required verbs.
            if (string.IsNullOrWhiteSpace(deletingVariationsDetailViewModel.ArabicCharacterSet)
                || string.IsNullOrWhiteSpace(deletingVariationsDetailViewModel.NorwegianCharacterSet))
            {
                return;
            }

            // Ask the user to confirm deleting the VariationsDetailViewModel.
            if (!deletingVariationsDetailViewModel.IsUserConfirmedDeletingModel())
            {
                return;
            }

            // Delete the inner Model from the data source.
            if (deletingVariationsDetailViewModel.Model.Id != 0)
            {
                // Notify the user interface.
                this.IsPerformingLongRunningTask = true;

                // Update the VariationsCount and VariationsDetails.
                this.Model.VariationsCount -= 1;
                this.Model.LastUpdateDateTime = DateTime.Now;
                this.DataEntities.VariationsDetails.Remove(deletingVariationsDetailViewModel.Model);

                try
                {
                    // Save the changes on the data source.
                    await this.DataEntities.SaveChangesAsync(this.CancellationToken).ConfigureAwait(false);
                }
                finally
                {
                    // Notify the user interface.
                    this.IsPerformingLongRunningTask = false;
                }
            }

            // Remove the deleted VariationsDetailViewModel from the collection and notify the user. 
            Application.Current.Dispatcher.Invoke(
                () =>
                    {
                        this.VariationsDetailViewModels.Remove(deletingVariationsDetailViewModel);
                        this.OnDeletingModelSucceeded(
                            this,
                            new OnSaveChangesEventArgs { Entity = deletingVariationsDetailViewModel.Model });
                    });
        }

        /// <summary>
        ///     Checks the name of current variations header against duplication.
        /// </summary>
        /// <returns>
        ///     A <see cref="Task{TResult}" /> represents the checking operation.
        /// </returns>
        private async Task<bool> IsDuplicatedNameAsync()
        {
            // Hold the checking results.
            var isDuplicatedName = false;

            // Check whether the name is duplicated or not.
            await this.DataEntities.VariationsHeaders
                .Where(
                    variationsHeader => variationsHeader.Id != this.Model.Id
                                        && variationsHeader.Name == this.Model.Name.Trim()
                                        && variationsHeader.WordbookId == this.Model.WordbookId)
                .ToListAsync(this.CancellationToken).ContinueWith(
                    checkingDuplicationTask =>
                        {
                            // Hold the checking results.
                            isDuplicatedName = checkingDuplicationTask.Result.Any();

                            // Display or remove an error to notify the user.
                            if (isDuplicatedName)
                            {
                                this.SetError(
                                    string.Format(ValidationMessages.DuplicatedName, this.Model.Name),
                                    nameof(this.Model.Name));
                            }
                            else
                            {
                                this.RemoveErrors(nameof(this.Model.Name));
                            }
                        },
                    this.CancellationToken,
                    TaskContinuationOptions.OnlyOnRanToCompletion,
                    TaskScheduler.Current).ConfigureAwait(false);

            // Return the checking results.
            return isDuplicatedName;
        }

        /// <summary>
        ///     Loads the Wordbooks which can hold variations from the data source asynchronously.
        /// </summary>
        /// <returns>
        ///     A <see cref="Task" /> represents the loading operation.
        /// </returns>
        private async Task LoadWordbooksForVariationsAsync()
        {
            // Notify the user interface.
            this.LongRunningTaskDescription = NotificationsLayer.PreparingUserInterfaceMessage;
            this.IsPerformingLongRunningTask = true;

            try
            {
                await this.DataEntities.Wordbooks
                    .Where(wordbook => wordbook.Type == (byte)WordbookTypes.Unlike && !wordbook.IsFolder)
                    .ToListAsync(this.CancellationToken).ContinueWith(
                        readingTask =>
                            {
                                this.VariationsWordbooks = new ObservableCollection<Wordbook>(readingTask.Result);
                            },
                        this.CancellationToken,
                        TaskContinuationOptions.OnlyOnRanToCompletion,
                        TaskScheduler.Current).ConfigureAwait(false);
            }
            finally
            {
                this.IsPerformingLongRunningTask = false;
                this.LongRunningTaskDescription = string.Empty;
            }
        }

        /// <summary>
        ///     Called when a text to speech request has been failed and an error has been occurred.
        /// </summary>
        /// <param name="sender">The <paramref name="sender" />.</param>
        /// <param name="e">The <see cref="GenericEventArgs{Exception}" /> instance containing the event data.</param>
        private void OnConversionErrorOccurred([NotNull] object sender, [NotNull] GenericEventArgs<Exception> e)
        {
            this.DisplayDesktopAlert(
                NotificationsLayer.GeneralExceptionsHeader,
                string.Format(NotificationsLayer.GeneralExceptionsMessage, e.EventData.Message),
                AlertTypes.Error);
        }

        /// <summary>
        ///     Reports the <paramref name="progress" /> percentage for the conversion tasks.
        /// </summary>
        /// <param name="progress">The <paramref name="progress" /> percentage value.</param>
        private void ReportConversionProgress(int progress)
        {
            // Display the index of the converted variation.
            this.LongRunningTaskDescription = string.Format(
                NotificationsLayer.StringToSoundConversionIsInProgressMessage,
                progress,
                this.VariationsDetailViewModels.Count);
        }

        /// <summary>
        ///     Saves the data of the current <seealso cref="VariationsHeader" /> asynchronously.
        /// </summary>
        /// <returns>
        ///     A <see cref="Task" /> represents the Insert or the Update operations.
        /// </returns>
        private async Task SaveVariationsHeaderDataAsync()
        {
            // Notify the user interface.
            this.IsPerformingLongRunningTask = true;

            try
            {
                // Check and notify the user if the name is duplicated.
                if (!await this.IsDuplicatedNameAsync().ConfigureAwait(false))
                {
                    // Update the VariationsCount and LastUpdateDateTime.
                    this.Model.LastUpdateDateTime = DateTime.Now;
                    this.Model.VariationsCount = (short)this.VariationsDetailViewModels.Count;

                    // Convert inner variations from string to sound streams and attach them to the inner Model.
                    await this.ConvertVariationsDetailsToSoundStreamsAsync(
                        new Progress<int>(this.ReportConversionProgress)).ConfigureAwait(false);

                    // Inserts or updates the data Model asynchronously.
                    switch (this.Model.Id)
                    {
                        case 0:
                            this.Model.CreationDateTime = DateTime.Now;
                            await this.InsertAsync().ConfigureAwait(false);
                            break;

                        default:
                            await this.UpdateAsync().ConfigureAwait(false);
                            break;
                    }
                }
            }
            finally
            {
                // Notify the user interface.
                this.IsPerformingLongRunningTask = false;
                this.LongRunningTaskDescription = string.Empty;
            }
        }

        /// <summary>
        ///     Displays a dialog box to edit a variations header from the search result.
        /// </summary>
        /// <param name="searchingWindowType">The type of searching window.</param>
        /// <returns>
        ///     A <see cref="Task" /> represents the selecting operation.
        /// </returns>
        private async Task SearchVariationsHeadersAsync([NotNull] object searchingWindowType)
        {
            // Is the selected data model not the current data model.
            var userSelectedDifferentModel = false;

            // Create and display the searching dialog on the main thread.
            Application.Current.Dispatcher.Invoke(
                () =>
                    {
                        // Display the searching data dialog.
                        DialogService.ShowDialog((Type)searchingWindowType);

                        // Roll back current data if the user has selected a different data Model.
                        if (DialogService.SelectedModelId != 0)
                        {
                            if (!this.Model.Id.Equals(DialogService.SelectedModelId))
                            {
                                userSelectedDifferentModel = true;
                                this.RollBackUserInputsCommand.Execute();
                            }
                            else
                            {
                                this.DisplayDesktopAlert(
                                    NotificationsLayer.DataUpdating,
                                    string.Format(NotificationsLayer.CanEditDataModelFromInputFields, this.Model.Name),
                                    AlertTypes.Information);
                            }
                        }
                    });

            // If the selected data model not the current data model.
            if (userSelectedDifferentModel)
            {
                try
                {
                    // Notify the user interface.
                    this.IsPerformingLongRunningTask = true;

                    // Search for data using the selected Model identifier.
                    this.Model = await this.DataEntities.VariationsHeaders
                                     .Include(variationsHeaders => variationsHeaders.VariationsDetails).SingleAsync(
                                         variationsHeaders => variationsHeaders.Id == DialogService.SelectedModelId,
                                         this.CancellationToken).ConfigureAwait(false);

                    // Fill the variations details view model.
                    this.AttachVariationsDetails(this.Model.VariationsDetails);
                }
                finally
                {
                    // Notify the user interface.
                    this.OnPropertyChanged(string.Empty);
                    this.IsPerformingLongRunningTask = false;
                }
            }
        }

        /// <summary>
        ///     Tests the speaking rate for the passed view model of variations detail asynchronously.
        /// </summary>
        /// <param name="variationsDetailViewModel">The variations detail view model.</param>
        /// <returns>
        ///     A <see cref="Task" /> represents the testing operation.
        /// </returns>
        private async Task TestSpeakingRateAsync([NotNull] object variationsDetailViewModel)
        {
            try
            {
                // Notify the user interface.
                this.IsTestingSpeakingRate = true;

                // Cast the passed object to VariationsDetailViewModel.
                var castedVariationsDetailViewModel = (VariationsDetailViewModel)variationsDetailViewModel;

                // Convert the inner Model to a sound streams if its not converted, then play the sound stream.
                await castedVariationsDetailViewModel.TestSpeakingRateAsync(this.cognitiveServices)
                    .ConfigureAwait(false);
            }
            finally
            {
                // Notify the user interface.
                this.IsTestingSpeakingRate = false;
            }
        }

        /// <summary>
        ///     Occurs when the variations detail view models collection changed.
        /// </summary>
        /// <param name="sender">The <paramref name="sender" />.</param>
        /// <param name="e">The <see cref="NotifyCollectionChangedEventArgs" /> instance containing the event data.</param>
        private void VariationDetailViewModelsChanged(
            [NotNull] object sender,
            [NotNull] NotifyCollectionChangedEventArgs e)
        {
            switch (e.Action)
            {
                case NotifyCollectionChangedAction.Add:

                    // Escape the first VariationsDetailViewModel.
                    if (e.NewStartingIndex > 0)
                    {
                        // Get the new VariationsDetailViewModel only.
                        if (e.NewItems[0] is VariationsDetailViewModel variationsDetailViewModel
                            && variationsDetailViewModel.Model.Id == 0)
                        {
                            // Repeat values of SpeakingRate and PronunciationTimes properties.
                            variationsDetailViewModel.SpeakingRate =
                                this.VariationsDetailViewModels[e.NewStartingIndex - 1].SpeakingRate;
                            variationsDetailViewModel.PronunciationTimes =
                                this.VariationsDetailViewModels[e.NewStartingIndex - 1].PronunciationTimes;
                        }
                    }

                    break;
            }

            // Reorder serial number for variations detail on all actions.
            byte firstSerial = 1;
            foreach (var variationsDetailViewModel in this.VariationsDetailViewModels)
            {
                variationsDetailViewModel.SerialNumber = firstSerial++;
            }
        }
    }
}