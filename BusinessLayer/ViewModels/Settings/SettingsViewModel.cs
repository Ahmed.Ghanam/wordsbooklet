﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SettingsViewModel.cs" company="None">
// Last Modified: 09/10/2018
// Author: Ahmed Ghanam.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace WordsBooklet.BusinessLayer.ViewModels
{
    using System;
    using System.Collections.ObjectModel;
    using System.Data.Entity;
    using System.Diagnostics;
    using System.Linq;
    using System.Threading.Tasks;
    using System.Windows;

    using Telerik.Windows.Controls;

    using WordsBooklet.BusinessLayer.Infrastructure;
    using WordsBooklet.BusinessLayer.Infrastructure.Notifications.Resources;
    using WordsBooklet.BusinessLayer.Models;

    /// <inheritdoc />
    /// <summary>
    ///     Handling the <seealso cref="Setting" /> data models.
    /// </summary>
    /// <seealso cref="NotificationsLayer{T}" />
    public sealed class SettingsViewModel : NotificationsLayer<Setting>
    {
        /// <summary>
        ///     The back field for the <see cref="CognitiveServicesKey" /> property.
        /// </summary>
        [CanBeNull]
        private string cognitiveServicesKey;

        /// <summary>
        ///     The back field for the <see cref="CurrentSettings" /> property.
        /// </summary>
        [CanBeNull]
        private ObservableCollection<Setting> currentSettings;

        /// <summary>
        ///     The back field for the <see cref="SystemLanguage" /> property.
        /// </summary>
        private Languages systemLanguage;

        /// <summary>
        ///     Gets or sets the cognitive services key.
        /// </summary>
        /// <value>
        ///     The cognitive services key.
        /// </value>
        [CanBeNull]
        public string CognitiveServicesKey
        {
            get => this.cognitiveServicesKey;
            set
            {
                this.cognitiveServicesKey = value;
                this.ValidateValue(this.CognitiveServicesKey, nameof(this.CognitiveServicesKey));
            }
        }

        /// <summary>
        ///     Gets a command to load the values of current settings from the data source asynchronously.
        /// </summary>
        /// <value>
        ///     The command to load the values of current settings from the data source asynchronously.
        /// </value>
        [NotNull]
        public AsyncCommand LoadSettingsCommand => new AsyncCommand(this.LoadSettingsAsync);

        /// <summary>
        ///     Gets a command to roll the user inputs back.
        /// </summary>
        /// <value>
        ///     The command to roll the user inputs back.
        /// </value>
        [NotNull]
        public RelayCommand RollBackUserInputsCommand =>
            new RelayCommand(canExecute => this.IsSettingsValuesChanged(), execute => this.RollBackDataChanges());

        /// <summary>
        ///     Gets a command to insert or update the value of all <seealso cref="Setting" />s.
        /// </summary>
        /// <value>
        ///     The command to insert or update the value of all <seealso cref="Setting" />s.
        /// </value>
        [NotNull]
        public AsyncCommand SaveSettingsCommand =>
            new AsyncCommand(canExecute => !this.HasErrors && this.IsSettingsValuesChanged(), this.SaveSettingsAsync);

        /// <summary>
        ///     Gets or sets the system language.
        /// </summary>
        /// <value>
        ///     The system language.
        /// </value>
        public Languages SystemLanguage
        {
            get => this.systemLanguage;

            set
            {
                this.systemLanguage = value;
                this.ValidateValue(this.SystemLanguage, nameof(this.SystemLanguage));
            }
        }

        /// <summary>
        ///     Gets or sets the loaded values of current settings which stored into the data source.
        /// </summary>
        /// <value>
        ///     The values of current settings which stored into the data source.
        /// </value>
        [CanBeNull]
        private ObservableCollection<Setting> CurrentSettings
        {
            get => this.currentSettings;
            set
            {
                this.currentSettings = value;
                this.OnPropertyChanged();
            }
        }

        /// <inheritdoc />
        /// <summary>
        ///     Roll the user changes back and set every Model to its original values.
        /// </summary>
        /// <exception cref="Exception">A <see langword="delegate" /> callback throws an exception.</exception>
        protected override void RollBackDataChanges()
        {
            // Displays the values of current settings to the user interface.
            this.DisplaySettings();

            // Clear any existing validation errors.
            this.ClearValidationErrors();
        }

        /// <summary>
        ///     Determines whether user has confirmed to restart the application.
        /// </summary>
        /// <returns>
        ///     <see langword="true" /> if user has confirmed to restart the application; otherwise, <see langword="false" />.
        /// </returns>
        private static bool IsUserConfirmedRestartingApplication()
        {
            // Confirmation results.
            bool? isConfirmed = false;

            // Build the dialog parameter
            var dialogParameters = new DialogParameters
                                       {
                                           Header = NotificationsLayer.Restarting,
                                           OkButtonContent = NotificationsLayer.YesRestart,
                                           CancelButtonContent = NotificationsLayer.Cancel,
                                           DialogStartupLocation = WindowStartupLocation.CenterScreen,
                                           Content = NotificationsLayer.RestartingConfirmationMessage,
                                           Closed = (result, eventArgs) => isConfirmed = eventArgs.DialogResult
                                       };

            // Set the right to left styles only if needed.
            if (CommonSettings.FlowDirection == FlowDirection.RightToLeft)
            {
                dialogParameters.WindowStyle = Application.Current.Resources["RadWindowRightToLeftStyle"] as Style;
                dialogParameters.ContentStyle = Application.Current.Resources["RadConfirmRightToLeftStyle"] as Style;
            }

            // Display the delete confirmation dialog.
            RadWindow.Confirm(dialogParameters);

            // Return the confirmation results.
            return isConfirmed ?? false;
        }

        /// <summary>
        ///     Displays the values of current settings which has been read from the data source.
        /// </summary>
        private void DisplaySettings()
        {
            // Displays the value of CognitiveServicesKey.
            this.CognitiveServicesKey = this.CurrentSettings?.First(
                setting => setting.Code == Convert.ToInt16(SettingsCodes.CognitiveServicesKey).ToString()).SavedValue;

            // Displays the value of SystemLanguage.
            this.SystemLanguage = (Languages)Convert.ToInt16(
                this.CurrentSettings
                    ?.First(setting => setting.Code == Convert.ToInt16(SettingsCodes.SystemLanguage).ToString())
                    .SavedValue);
        }

        /// <summary>
        ///     Determines whether values of current settings has changed or not.
        /// </summary>
        /// <returns>
        ///     <see langword="true" /> if values of current settings has changed; otherwise, <see langword="false" />.
        /// </returns>
        private bool IsSettingsValuesChanged()
        {
            return this.CognitiveServicesKey != this.CurrentSettings?.First(
                           setting => setting.Code == Convert.ToInt16(SettingsCodes.CognitiveServicesKey).ToString())
                       .SavedValue || this.SystemLanguage != (Languages)Convert.ToInt16(
                       this.CurrentSettings?.First(
                               setting => setting.Code == Convert.ToInt16(SettingsCodes.SystemLanguage).ToString())
                           .SavedValue);
        }

        /// <summary>
        ///     Loads values of current settings from the data source asynchronously.
        /// </summary>
        /// <returns>
        ///     A <see cref="Task" /> represents the loading operation.
        /// </returns>
        private async Task LoadSettingsAsync()
        {
            // Notify the user interface.
            this.LongRunningTaskDescription = NotificationsLayer.PreparingUserInterfaceMessage;
            this.IsPerformingLongRunningTask = true;

            try
            {
                // Read values of all settings.
                await this.DataEntities.Settings.ToListAsync().ContinueWith(
                    readingTask =>
                        {
                            // Holds the current values of all settings.
                            this.CurrentSettings = new ObservableCollection<Setting>(readingTask.Result);

                            // Displays the values of current settings to the user interface.
                            this.DisplaySettings();
                        }).ConfigureAwait(false);
            }
            finally
            {
                this.IsPerformingLongRunningTask = false;
                this.LongRunningTaskDescription = string.Empty;
            }
        }

        /// <summary>
        ///     Saves the values of the current <seealso cref="Setting" />s asynchronously.
        /// </summary>
        /// <returns>
        ///     A <see cref="Task" /> represents the Insert or the Update operations.
        /// </returns>
        private async Task SaveSettingsAsync()
        {
            // Ask the user to confirm restarting the application.
            var isUserConfirmedRestartingApplication = false;
            Application.Current.Dispatcher.Invoke(
                () => { isUserConfirmedRestartingApplication = IsUserConfirmedRestartingApplication(); });

            // If the user didn't confirm to restart the application.
            if (!isUserConfirmedRestartingApplication)
            {
                return;
            }

            // Notify the user interface.
            this.IsPerformingLongRunningTask = true;

            try
            {
                // Updates the language used for the whole system.
                await this.UpdateSystemLanguageSettingAsync().ConfigureAwait(false);

                // Updates the cognitive services key.
                await this.UpdateCognitiveServicesKeySettingAsync().ConfigureAwait(false);
            }
            finally
            {
                // Start a new instance from the same path.
                Process.Start(Application.ResourceAssembly.Location);

                // Close current instance.
                Application.Current.Dispatcher.Invoke(() => { Application.Current.Shutdown(); });
            }
        }

        /// <summary>
        ///     Updates the cognitive services key.
        /// </summary>
        /// <returns>
        ///     A <see cref="Task" /> represents the Update operation.
        /// </returns>
        private async Task UpdateCognitiveServicesKeySettingAsync()
        {
            // Creates a new data model holding new setting value.
            this.Model =
                this.CurrentSettings?.First(
                    setting => setting.Code == Convert.ToInt16(SettingsCodes.CognitiveServicesKey).ToString())
                ?? throw new InvalidOperationException();

            // Save new value only.
            if (!this.Model.SavedValue.Equals(this.CognitiveServicesKey))
            {
                this.Model.SavedValue = this.CognitiveServicesKey;
                await this.UpdateAsync().ConfigureAwait(false);
            }

            // Clear data model.
            this.Model = new Setting();
        }

        /// <summary>
        ///     Updates the language used for the whole system.
        /// </summary>
        /// <returns>
        ///     A <see cref="Task" /> represents the Update operation.
        /// </returns>
        private async Task UpdateSystemLanguageSettingAsync()
        {
            // Creates a new data model holding new setting value.
            this.Model =
                this.CurrentSettings?.First(
                    setting => setting.Code == Convert.ToInt16(SettingsCodes.SystemLanguage).ToString())
                ?? throw new InvalidOperationException();

            // Save new value only.
            if (!this.Model.SavedValue.Equals(Convert.ToInt16(this.SystemLanguage).ToString()))
            {
                this.Model.SavedValue = Convert.ToInt16(this.SystemLanguage).ToString();
                await this.UpdateAsync().ConfigureAwait(false);
            }

            // Clear data model.
            this.Model = new Setting();
        }

        /// <summary>
        ///     Validates the property value against the only property in the data model.
        /// </summary>
        /// <param name="propertyValue">The property value.</param>
        /// <param name="propertyName">The property name.</param>
        private void ValidateValue([CanBeNull] object propertyValue, [NotNull] string propertyName)
        {
            // Notify the property changed.
            this.OnPropertyChanged(propertyName);

            // Remove existing errors on the same property.
            this.RemoveErrors(propertyName);

            // Validates the property value against against the only property in the data model.
            this.OnPropertyChanged(Convert.ToString(propertyValue), nameof(this.Model.SavedValue));

            // Copy the validation errors if any to the property to validate.
            if (this.HasErrors)
            {
                foreach (var errorsMessage in this.GetErrors(nameof(this.Model.SavedValue)))
                {
                    this.SetError(Convert.ToString(errorsMessage), propertyName);
                }
            }

            // Remove existing errors from the only property in the data model.
            this.RemoveErrors(nameof(this.Model.SavedValue));
        }
    }
}