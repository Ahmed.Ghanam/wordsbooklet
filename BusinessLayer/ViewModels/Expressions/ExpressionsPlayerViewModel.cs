﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ExpressionsPlayerViewModel.cs" company="None">
// Last Modified: 12/10/2018
// Author: Ahmed Ghanam.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace WordsBooklet.BusinessLayer.ViewModels
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Collections.Specialized;
    using System.Data.Entity;
    using System.IO;
    using System.Linq;
    using System.Media;
    using System.Threading;
    using System.Threading.Tasks;
    using System.Windows;

    using Microsoft.Win32;

    using WordsBooklet.BusinessLayer.Infrastructure;
    using WordsBooklet.BusinessLayer.Infrastructure.Notifications.Resources;
    using WordsBooklet.BusinessLayer.Models;

    /// <inheritdoc />
    /// <summary>
    ///     Playing and saving the sound of the <seealso cref="ExpressionsHeader" /> data model.
    /// </summary>
    /// <seealso cref="NotificationsLayer{T}" />
    public class ExpressionsPlayerViewModel : NotificationsLayer<ExpressionsHeader>
    {
        /// <summary>
        ///     The back field for the <see cref="BeginUtteringExpressionsDetail" /> property.
        /// </summary>
        [CanBeNull]
        private ExpressionsDetail beginUtteringExpressionsDetail;

        /// <summary>
        ///     The back field for the <see cref="IsUtteringExpressionsDetail" /> property.
        /// </summary>
        private bool isUtteringExpressionsDetail;

        /// <summary>
        ///     The back field for the <see cref="IsUtteringPaused" /> property.
        /// </summary>
        private bool isUtteringPaused;

        /// <summary>
        ///     The back field for the <see cref="StoredExpressionsHeaders" /> property.
        /// </summary>
        [CanBeNull]
        private ObservableCollection<ExpressionsHeader> storedExpressionsHeaders;

        /// <inheritdoc />
        /// <summary>
        ///     Initializes a new instance of the <see cref="ExpressionsPlayerViewModel" /> class.
        /// </summary>
        public ExpressionsPlayerViewModel()
        {
            // Set the SelectedExpressionsHeaders collection changed event handler.
            this.SelectedExpressionsHeaders.CollectionChanged += this.SelectedExpressionsHeadersChangedAsync;
        }

        /// <summary>
        ///     Gets the expressions detail which is being uttered now.
        /// </summary>
        /// <value>
        ///     The expressions detail which is being uttered now.
        /// </value>
        [CanBeNull]
        public ExpressionsDetail BeginUtteringExpressionsDetail
        {
            get => this.beginUtteringExpressionsDetail;

            private set
            {
                this.beginUtteringExpressionsDetail = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets a value indicating whether <see langword="this" /> instance is uttering a expressions detail or not.
        /// </summary>
        /// <value>
        ///     <see langword="true" /> if <see langword="this" /> instance is uttering a expressions detail otherwise,
        ///     <see langword="false" />.
        /// </value>
        public bool IsUtteringExpressionsDetail
        {
            get => this.isUtteringExpressionsDetail;

            private set
            {
                this.isUtteringExpressionsDetail = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets a command to load the expressions headers from the data source asynchronously.
        /// </summary>
        /// <value>
        ///     The command to load the expressions headers from the data source asynchronously.
        /// </value>
        [NotNull]
        public AsyncCommand LoadExpressionsHeadersCommand => new AsyncCommand(this.LoadExpressionsHeadersAsync);

        /// <summary>
        ///     Gets a command to pause uttering the selected expressions details.
        /// </summary>
        /// <value>
        ///     The command to pause uttering the selected expressions details.
        /// </value>
        [NotNull]
        public RelayCommand PauseUtteringCommand =>
            new RelayCommand(
                canExecute => this.IsUtteringExpressionsDetail && !this.IsUtteringPaused,
                execute =>
                    {
                        using (var cancellationTokenSource = new CancellationTokenSource())
                        {
                            this.IsUtteringPaused = true;
                            cancellationTokenSource.Cancel();
                            this.CancellationToken = cancellationTokenSource.Token;
                        }
                    });

        /// <summary>
        ///     Gets a command to resume uttering the selected expressions details asynchronously.
        /// </summary>
        /// <value>
        ///     The command to resume uttering the selected expressions details asynchronously.
        /// </value>
        [NotNull]
        public AsyncCommand ResumeUtteringCommand =>
            new AsyncCommand(
                canExecute => this.IsUtteringPaused && !this.IsUtteringExpressionsDetail
                                                    && this.SelectedExpressionsDetails.Any(),
                this.UtterExpressionsDetailsAsync);

        /// <summary>
        ///     Gets a command to save the selected expressions details as a sound file asynchronously.
        /// </summary>
        /// <value>
        ///     The command to save the selected expressions details as a sound file asynchronously.
        /// </value>
        [NotNull]
        public AsyncCommand SaveAsSoundFileCommand =>
            new AsyncCommand(canExecute => this.SelectedExpressionsDetails.Any(), this.SaveAsSoundFileAsync);

        /// <summary>
        ///     Gets the expressions details of the selected expressions headers.
        /// </summary>
        /// <value>
        ///     The expressions details of the selected expressions headers.
        /// </value>
        [NotNull]
        public ObservableCollection<ExpressionsDetail> SelectedExpressionsDetails { get; } =
            new ObservableCollection<ExpressionsDetail>();

        /// <summary>
        ///     Gets or sets the expressions headers which user has selected to utter.
        /// </summary>
        /// <value>
        ///     The expressions headers which user has selected to utter.
        /// </value>
        [NotNull]
        public ObservableCollection<ExpressionsHeader> SelectedExpressionsHeaders { get; set; } =
            new ObservableCollection<ExpressionsHeader>();

        /// <summary>
        ///     Gets the expressions headers stored into the data source.
        /// </summary>
        /// <value>
        ///     The expressions headers stored into the data source.
        /// </value>
        [CanBeNull]
        public ObservableCollection<ExpressionsHeader> StoredExpressionsHeaders
        {
            get => this.storedExpressionsHeaders;

            private set
            {
                this.storedExpressionsHeaders = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets a command to utter the selected expressions details from beginning asynchronously.
        /// </summary>
        /// <value>
        ///     The command to utter the selected expressions details from beginning asynchronously.
        /// </value>
        [NotNull]
        public AsyncCommand UtterExpressionsDetailsCommand =>
            new AsyncCommand(
                canExecute => !this.IsUtteringExpressionsDetail && this.SelectedExpressionsDetails.Any(),
                this.UtterExpressionsDetailsFromBeginningAsync);

        /// <summary>
        ///     Gets or sets the cancellation token for controlling tasks.
        /// </summary>
        /// <value>
        ///     The cancellation token for controlling tasks.
        /// </value>
        private new CancellationToken CancellationToken { get; set; }

        /// <summary>
        ///     Gets or sets a value indicating whether the user has paused the uttering or not.
        /// </summary>
        /// <value>
        ///     <see langword="true" /> if the user has paused the uttering; otherwise, <see langword="false" />.
        /// </value>
        private bool IsUtteringPaused
        {
            get => this.isUtteringPaused;

            set
            {
                this.isUtteringPaused = value;
                this.OnPropertyChanged();
            }
        }

        /// <inheritdoc />
        /// <summary>
        ///     Releases unmanaged and - optionally - managed resources.
        /// </summary>
        /// <param name="disposing">
        ///     <see langword="true" /> to release both managed and unmanaged resources; <see langword="false" /> to release only
        ///     unmanaged resources.
        /// </param>
        /// <exception cref="ObjectDisposedException">This <see cref="CancellationTokenSource" /> has been disposed.</exception>
        /// <exception cref="AggregateException">
        ///     An aggregate exception containing all the exceptions thrown by the registered
        ///     callbacks on the associated <see cref="CancellationToken" />.
        /// </exception>
        protected override void Dispose(bool disposing)
        {
            // Call base implementation, Always first.
            base.Dispose(disposing);

            if (!disposing)
            {
                return;
            }

            using (var cancellationTokenSource = new CancellationTokenSource())
            {
                cancellationTokenSource.Cancel();
                this.CancellationToken = cancellationTokenSource.Token;
            }

            // Dispose related commands.
            this.ResumeUtteringCommand.Dispose();
            this.SaveAsSoundFileCommand.Dispose();
            this.LoadExpressionsHeadersCommand.Dispose();
            this.UtterExpressionsDetailsCommand.Dispose();

            // Remove event listeners and clear the expressions detail collection.
            this.SelectedExpressionsHeaders.CollectionChanged -= this.SelectedExpressionsHeadersChangedAsync;
        }

        /// <summary>
        ///     Creates a sound file using the <see langword="byte" /> arrays of the selected expressions details asynchronously.
        /// </summary>
        /// <param name="soundFilePath">The sound file path.</param>
        /// <returns>
        ///     A <see cref="Task" /> represents the creation operation.
        /// </returns>
        [NotNull]
        private Task CreateSoundFileAsync([NotNull] string soundFilePath)
        {
            return Task.Factory.StartNew(
                () =>
                    {
                        // Compress all sounds streams into a single array.
                        var soundFilesStreams = new List<byte[]>();
                        foreach (var selectedExpressionsDetail in this.SelectedExpressionsDetails)
                        {
                            soundFilesStreams.Add(selectedExpressionsDetail.GetSoundStreams());
                        }

                        // Save byte array as sound file (.WAV).
                        var soundFileWriter = new SoundFilesWriter();
                        soundFileWriter.CreateSoundFile(soundFilePath, soundFilesStreams);
                    },
                base.CancellationToken);
        }

        /// <summary>
        ///     Loads the expressions headers from data source asynchronously.
        /// </summary>
        /// <returns>
        ///     A <see cref="Task" /> represents the loading operation.
        /// </returns>
        private async Task LoadExpressionsHeadersAsync()
        {
            // Notify the user interface.
            this.LongRunningTaskDescription = NotificationsLayer.PreparingUserInterfaceMessage;
            this.IsPerformingLongRunningTask = true;

            try
            {
                await this.DataEntities.ExpressionsHeaders.ToListAsync(base.CancellationToken).ContinueWith(
                    loadingTask =>
                        {
                            this.StoredExpressionsHeaders =
                                new ObservableCollection<ExpressionsHeader>(loadingTask.Result);
                        },
                    base.CancellationToken,
                    TaskContinuationOptions.OnlyOnRanToCompletion,
                    TaskScheduler.Current).ConfigureAwait(false);
            }
            finally
            {
                this.IsPerformingLongRunningTask = false;
                this.LongRunningTaskDescription = string.Empty;
            }
        }

        /// <summary>
        ///     Plays the passed sound stream as a sound asynchronously.
        /// </summary>
        /// <param name="soundStream">The sound as <see langword="byte" /> array.</param>
        /// <returns>
        ///     A <see cref="Task" /> represent the audio playing operation.
        /// </returns>
        [NotNull]
        private Task PlaySoundStreamAsync([CanBeNull] byte[] soundStream)
        {
            // Throw exception if the user canceled the task.
            this.CancellationToken.ThrowIfCancellationRequested();

            // Return a completed task if the passed stream is null.
            if (soundStream == null)
            {
                return Task.CompletedTask;
            }

            // Return a completed task if the passed stream is empty.
            if (soundStream.Length == 0)
            {
                return Task.CompletedTask;
            }

            // Play the sound using a separate task.
            return Task.Factory.StartNew(
                () =>
                    {
                        using (var soundPlayer = new SoundPlayer(new MemoryStream(soundStream)))
                        {
                            soundPlayer.PlaySync();
                        }
                    },
                this.CancellationToken);
        }

        /// <summary>
        ///     Saves the selected expressions details as a sound file asynchronously.
        /// </summary>
        /// <returns>
        ///     A <see cref="Task" /> represents the saving operation.
        /// </returns>
        private async Task SaveAsSoundFileAsync()
        {
            // Notify the user interface.
            this.IsPerformingLongRunningTask = true;

            // Hold the values of SaveFileDialog.
            bool? saveFileDialogResult = false;
            var saveFileDialogFullName = string.Empty;

            try
            {
                // Display the SaveFileDialog on the main thread.
                Application.Current.Dispatcher.Invoke(
                    () =>
                        {
                            var saveFileDialog = new SaveFileDialog
                                                     {
                                                         ValidateNames = true,
                                                         OverwritePrompt = true,
                                                         CheckPathExists = true,
                                                         RestoreDirectory = true,
                                                         Filter = "wav files (*.wav)|*.wav",
                                                         FileName =
                                                             this.SelectedExpressionsHeaders[0].FullName
                                                             ?? string.Empty
                                                     };
                            saveFileDialogResult = saveFileDialog.ShowDialog();
                            saveFileDialogFullName = saveFileDialog.FileName;
                        });

                // Create the sound file in a separate task.
                if (saveFileDialogResult == true)
                {
                    await this.CreateSoundFileAsync(saveFileDialogFullName).ConfigureAwait(false);
                }
            }
            finally
            {
                this.IsPerformingLongRunningTask = false;
            }
        }

        /// <summary>
        ///     Occurs when the collection of the selected expressions headers changed.
        /// </summary>
        /// <param name="sender">The <paramref name="sender" />.</param>
        /// <param name="e">The <see cref="NotifyCollectionChangedEventArgs" /> instance containing the event data.</param>
        private async void SelectedExpressionsHeadersChangedAsync(
            [NotNull] object sender,
            [NotNull] NotifyCollectionChangedEventArgs e)
        {
            // Handle only the new and the old ExpressionsHeader.
            if (!((e.NewItems?[0] ?? e.OldItems?[0]) is ExpressionsHeader expressionsHeader))
            {
                return;
            }

            // Cancel resuming sound playing.
            this.IsUtteringPaused = false;

            // Notify the user interface.
            this.IsPerformingLongRunningTask = true;

            try
            {
                await Task.Factory.StartNew(
                    () =>
                        {
                            Application.Current.Dispatcher.Invoke(
                                () =>
                                    {
                                        switch (e.Action)
                                        {
                                            case NotifyCollectionChangedAction.Add:
                                                foreach (var expressionsDetail in expressionsHeader.ExpressionsDetails)
                                                {
                                                    this.SelectedExpressionsDetails.Add(expressionsDetail);
                                                }

                                                break;

                                            case NotifyCollectionChangedAction.Remove:
                                                foreach (var expressionsDetail in expressionsHeader.ExpressionsDetails)
                                                {
                                                    this.SelectedExpressionsDetails.Remove(expressionsDetail);
                                                }

                                                break;
                                        }
                                    });
                        },
                    base.CancellationToken,
                    TaskCreationOptions.None,
                    TaskScheduler.Current).ConfigureAwait(false);
            }
            finally
            {
                this.IsPerformingLongRunningTask = false;
            }
        }

        /// <summary>
        ///     Utters the expressions in the expressions detail asynchronously.
        /// </summary>
        /// <param name="expressionsDetail">The expressions detail.</param>
        /// <returns>
        ///     A <see cref="Task" /> represents the uttering operation.
        /// </returns>
        private async Task UtterExpressionsDetailAsync([NotNull] ExpressionsDetail expressionsDetail)
        {
            try
            {
                // Continue if the expression is not uttered as much as declared.
                while (expressionsDetail.TimesNumberUttered < expressionsDetail.PronunciationTimes)
                {
                    // Notify that the uttering has been resumed.
                    this.IsUtteringPaused = false;

                    // Notify that uttering has been started.
                    this.IsUtteringExpressionsDetail = true;

                    // Hold the expression detail which begin uttering.
                    this.BeginUtteringExpressionsDetail = expressionsDetail;

                    // Play The Expressions Using Arabic.
                    await this.PlaySoundStreamAsync(expressionsDetail.ArabicPronunciation).ConfigureAwait(false);

                    // Play The Expressions Using Norwegian.
                    await this.PlaySoundStreamAsync(expressionsDetail.NorwegianPronunciation).ConfigureAwait(false);

                    // Play The Expressions Example Using Norwegian.
                    await this.PlaySoundStreamAsync(expressionsDetail.NorwegianExamplePronunciation)
                        .ConfigureAwait(false);

                    // Increase the value of the TimesNumberUttered.
                    expressionsDetail.TimesNumberUttered += 1;
                }
            }
            finally
            {
                // Notify that uttering has been done.
                this.IsUtteringExpressionsDetail = false;

                // Release the last uttered expressions.
                this.BeginUtteringExpressionsDetail = null;
            }
        }

        /// <summary>
        ///     Utters the expressions details of the selected expressions headers asynchronously.
        /// </summary>
        /// <returns>
        ///     A <see cref="Task" /> represent the uttering operation.
        /// </returns>
        private async Task UtterExpressionsDetailsAsync()
        {
            // Initialize a new instance of the CancellationToken.
            if (this.CancellationToken.IsCancellationRequested)
            {
                this.CancellationToken = new CancellationToken();
            }

            // Loop on the selected expressions headers. 
            foreach (var selectedExpressionsHeader in this.SelectedExpressionsHeaders)
            {
                // Continue if the header is not uttered as much as declared.
                while (selectedExpressionsHeader.TimesNumberUttered < selectedExpressionsHeader.PronunciationTimes)
                {
                    // Utter expressions detail in a separated task as much as declared where it has been paused.
                    foreach (var expressionsDetail in selectedExpressionsHeader.ExpressionsDetails)
                    {
                        await this.UtterExpressionsDetailAsync(expressionsDetail).ConfigureAwait(false);
                    }

                    // Increase the value of the TimesNumberUttered.
                    selectedExpressionsHeader.TimesNumberUttered += 1;

                    // Reset the value of TimesNumberUttered until the last rotation on the header.
                    if (selectedExpressionsHeader.TimesNumberUttered < selectedExpressionsHeader.PronunciationTimes)
                    {
                        foreach (var expressionsDetail in selectedExpressionsHeader.ExpressionsDetails)
                        {
                            expressionsDetail.TimesNumberUttered = 0;
                        }
                    }
                }
            }
        }

        /// <summary>
        ///     Utters the selected expressions details from beginning asynchronously.
        /// </summary>
        /// <returns>
        ///     A <see cref="Task" /> represents the uttering operation.
        /// </returns>
        [NotNull]
        private Task UtterExpressionsDetailsFromBeginningAsync()
        {
            // Reset the value of TimesNumberUttered for both headers and details.
            foreach (var expressionsHeader in this.SelectedExpressionsHeaders)
            {
                expressionsHeader.TimesNumberUttered = 0;
                foreach (var expressionsDetail in expressionsHeader.ExpressionsDetails)
                {
                    expressionsDetail.TimesNumberUttered = 0;
                }
            }

            // Utter the expressions details of the selected headers.
            return this.UtterExpressionsDetailsAsync();
        }
    }
}