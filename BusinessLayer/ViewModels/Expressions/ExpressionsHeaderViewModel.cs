﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ExpressionsHeaderViewModel.cs" company="None">
// Last Modified: 12/10/2018
// Author: Ahmed Ghanam.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace WordsBooklet.BusinessLayer.ViewModels
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Collections.Specialized;
    using System.Data.Entity;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using System.Windows;

    using WordsBooklet.BusinessLayer.Infrastructure;
    using WordsBooklet.BusinessLayer.Infrastructure.Notifications.Resources;
    using WordsBooklet.BusinessLayer.Models;
    using WordsBooklet.BusinessLayer.ViewModels.Resources;

    /// <inheritdoc />
    /// <summary>
    ///     Handling the <seealso cref="ExpressionsHeader" /> data model.
    /// </summary>
    /// <seealso cref="NotificationsLayer{T}" />
    public sealed class ExpressionsHeaderViewModel : NotificationsLayer<ExpressionsHeader>
    {
        /// <summary>
        ///     The cognitive services instance.
        /// </summary>
        [NotNull]
        private readonly CognitiveServices cognitiveServices;

        /// <summary>
        ///     The back field for the <see cref="ExpressionsWordbooks" /> property.
        /// </summary>
        [CanBeNull]
        private ObservableCollection<Wordbook> expressionsWordbooks;

        /// <summary>
        ///     The back field for the <see cref="IsTestingSpeakingRate" /> property.
        /// </summary>
        private bool isTestingSpeakingRate;

        /// <inheritdoc />
        /// <summary>
        ///     Initializes a new instance of the <see cref="ExpressionsHeaderViewModel" /> class.
        /// </summary>
        /// <exception cref="OverflowException">
        ///     the call back value is less than <see cref="TimeSpan.MinValue" />
        ///     or greater than <see cref="TimeSpan.MaxValue" />
        ///     .-or-
        ///     the call back value is <see cref="Double.PositiveInfinity" />.-or- the call back value is
        ///     <see cref="Double.NegativeInfinity" />.
        /// </exception>
        /// <exception cref="ArgumentOutOfRangeException">
        ///     The number of milliseconds in the value of call back value is negative
        ///     and not equal to <see cref="Timeout.Infinite" />, or is greater than <see cref="Int32.MaxValue" />.
        /// </exception>
        /// <exception cref="ArgumentException"> the call back value is equal to <see cref="Double.NaN" />. </exception>
        /// <exception cref="ArgumentNullException">The call back value parameter is <see langword="null" />. </exception>
        public ExpressionsHeaderViewModel()
        {
            // Set the initial values of the inner Model properties.
            this.Model.PronunciationTimes = 1;

            // Initialize the cognitive services instance.
            this.cognitiveServices = new CognitiveServices();
            this.cognitiveServices.OnErrorOccurred += this.OnConversionErrorOccurred;

            // Set the ExpressionsDetailViewModels collection changed event handler.
            this.ExpressionsDetailViewModels.CollectionChanged += this.ExpressionsDetailViewModelsChanged;
        }

        /// <summary>
        ///     Gets a command to delete a view model of expressions detail.
        /// </summary>
        /// <value>
        ///     The command to delete a view model of expressions detail.
        /// </value>
        [NotNull]
        public AsyncCommand DeleteExpressionsDetailViewModelCommand =>
            new AsyncCommand(this.DeleteExpressionsDetailViewModelAsync);

        /// <summary>
        ///     Gets the view models of the expressions details which the current header holding.
        /// </summary>
        /// <value>
        ///     The view models of the expressions details which the current header holding.
        /// </value>
        [NotNull]
        public ObservableCollection<ExpressionsDetailViewModel> ExpressionsDetailViewModels { get; } =
            new ObservableCollection<ExpressionsDetailViewModel>();

        /// <summary>
        ///     Gets the wordbooks which can hold expressions which stored into the data source.
        /// </summary>
        /// <value>
        ///     The wordbooks which can hold expressions which stored into the data source.
        /// </value>
        [CanBeNull]
        public ObservableCollection<Wordbook> ExpressionsWordbooks
        {
            get => this.expressionsWordbooks;

            private set
            {
                this.expressionsWordbooks = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets a value indicating whether the user is testing the speaking rate of a expressions detail or not.
        /// </summary>
        /// <value>
        ///     <see langword="true" /> if the user is testing the speaking rate of a expressions detail; otherwise,
        ///     <see langword="false" />.
        /// </value>
        public bool IsTestingSpeakingRate
        {
            get => this.isTestingSpeakingRate;

            private set
            {
                this.isTestingSpeakingRate = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets a command to load the Wordbooks which can hold expressions from the data source asynchronously.
        /// </summary>
        /// <value>
        ///     The command to load the Wordbooks which can hold expressions from the data source asynchronously.
        /// </value>
        [NotNull]
        public AsyncCommand LoadWordbooksForExpressionsCommand => new AsyncCommand(this.LoadWordbooksForExpressionsAsync);

        /// <summary>
        ///     Gets or sets the current expressions header name.
        /// </summary>
        /// <value>
        ///     The current expressions header name.
        /// </value>
        [CanBeNull]
        public string Name
        {
            get => this.Model.Name;

            set
            {
                this.Model.Name = value;
                this.OnPropertyChanged(this.Model.Name, nameof(this.Model.Name));
            }
        }

        /// <summary>
        ///     Gets or sets the pronunciation times of the current expressions header.
        /// </summary>
        /// <value>
        ///     The pronunciation times of the current expressions header.
        /// </value>
        public byte PronunciationTimes
        {
            get => this.Model.PronunciationTimes;

            set
            {
                this.Model.PronunciationTimes = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets the current expressions header remarks.
        /// </summary>
        /// <value>
        ///     The current expressions header remarks.
        /// </value>
        [CanBeNull]
        public string Remarks
        {
            get => this.Model.Remarks;

            set
            {
                this.Model.Remarks = value;
                this.OnPropertyChanged(this.Model.Remarks, nameof(this.Model.Remarks));
            }
        }

        /// <summary>
        ///     Gets a command to roll the user inputs back.
        /// </summary>
        /// <value>
        ///     The command to roll the user inputs back.
        /// </value>
        [NotNull]
        public RelayCommand RollBackUserInputsCommand =>
            new RelayCommand(
                canExecute => this.HasErrors || !string.IsNullOrEmpty(this.Model.Name)
                                             || !string.IsNullOrEmpty(this.Model.Remarks)
                                             || this.ExpressionsDetailViewModels.Any(),
                execute => this.RollBackDataChanges());

        /// <summary>
        ///     Gets a command to insert or update the current <seealso cref="ExpressionsHeader" />.
        /// </summary>
        /// <value>
        ///     The command to insert or update the current <seealso cref="ExpressionsHeader" />.
        /// </value>
        [NotNull]
        public AsyncCommand SaveExpressionsHeaderDataCommand =>
            new AsyncCommand(
                canExecute => this.Model.Wordbook != null && !string.IsNullOrWhiteSpace(this.Model.Name)
                                                          && this.IsModelHasModified()
                                                          && this.ExpressionsDetailViewModels.Any(
                                                              expressionsDetailViewModel =>
                                                                  expressionsDetailViewModel.IsModelHasModified()),
                this.SaveExpressionsHeaderDataAsync);

        /// <summary>
        ///     Gets a command to open a dialog box to search and select a single expressions header.
        /// </summary>
        /// <value>
        ///     The command to open a dialog box to search and select a single expressions header.
        /// </value>
        [NotNull]
        public AsyncCommand SearchExpressionsHeadersCommand => new AsyncCommand(this.SearchExpressionsHeadersAsync);

        /// <summary>
        ///     Gets a command to tests the speaking rate of a view model of expressions detail asynchronously.
        /// </summary>
        /// <value>
        ///     The command to tests the speaking rate of a view model of expressions detail
        /// </value>
        [NotNull]
        public AsyncCommand TestSpeakingRateCommand => new AsyncCommand(this.TestSpeakingRateAsync);

        /// <summary>
        ///     Gets or sets the main wordbook of the current expressions header.
        /// </summary>
        /// <value>
        ///     The main wordbook of the current expressions header.
        /// </value>
        [CanBeNull]
        public Wordbook Wordbook
        {
            get => this.Model.Wordbook;

            set
            {
                // Set the selected word category object.
                this.Model.Wordbook = value;

                // Set the selected word category identifier.
                this.Model.WordbookId = this.Model.Wordbook?.Id ?? 0;

                // Validate the user inputs and notify the user interface.
                this.OnPropertyChanged(this.Model.Wordbook, nameof(this.Model.Wordbook));
            }
        }

        /// <inheritdoc />
        /// <summary>
        ///     Releases unmanaged and - optionally - managed resources.
        /// </summary>
        /// <param name="disposing">
        ///     <see langword="true" /> to release both managed and unmanaged resources; <see langword="false" /> to release only
        ///     unmanaged resources.
        /// </param>
        /// <exception cref="ObjectDisposedException">This <see cref="CancellationTokenSource" /> has been disposed.</exception>
        /// <exception cref="AggregateException">
        ///     An aggregate exception containing all the exceptions thrown by the registered
        ///     callbacks on the associated <see cref="CancellationToken" />.
        /// </exception>
        protected override void Dispose(bool disposing)
        {
            // Call base implementation, Always first.
            base.Dispose(disposing);

            if (!disposing)
            {
                return;
            }

            // Dispose related commands.
            this.TestSpeakingRateCommand.Dispose();
            this.SearchExpressionsHeadersCommand.Dispose();
            this.SaveExpressionsHeaderDataCommand.Dispose();
            this.LoadWordbooksForExpressionsCommand.Dispose();
            this.DeleteExpressionsDetailViewModelCommand.Dispose();

            // Remove event listeners and dispose the cognitive services instance.
            this.cognitiveServices.OnErrorOccurred -= this.OnConversionErrorOccurred;
            this.cognitiveServices.Dispose();

            // Dispose every view model of expressions detail.
            foreach (var expressionsDetailViewModel in this.ExpressionsDetailViewModels)
            {
                expressionsDetailViewModel.Dispose();
            }

            // Remove event listeners.
            this.ExpressionsDetailViewModels.CollectionChanged -= this.ExpressionsDetailViewModelsChanged;
        }

        /// <inheritdoc />
        /// <summary>
        ///     Invoked if inserting the data model succeeded.
        /// </summary>
        /// <param name="sender">The control raise the event.</param>
        /// <param name="e">The <see cref="OnSaveChangesEventArgs" /> instance containing the event data.</param>
        /// <exception cref="Exception">A delegate callback throws an exception.</exception>
        protected override void OnInsertingModelSucceeded(object sender, OnSaveChangesEventArgs e)
        {
            // Call the base initialization.
            base.OnInsertingModelSucceeded(sender, e);

            // Prepare the user interface for new inputs.
            this.RollBackUserInputsCommand.Execute();
        }

        /// <inheritdoc />
        /// <summary>
        ///     Invoked if updating the data model succeeded.
        /// </summary>
        /// <param name="sender">The control raise the event.</param>
        /// <param name="e">The <see cref="OnSaveChangesEventArgs" /> instance containing the event data.</param>
        /// <exception cref="Exception">A delegate callback throws an exception.</exception>
        protected override void OnUpdatingModelSucceeded(object sender, OnSaveChangesEventArgs e)
        {
            // Call the base initialization.
            base.OnUpdatingModelSucceeded(sender, e);

            // Prepare the user interface for new inputs.
            this.RollBackUserInputsCommand.Execute();
        }

        /// <inheritdoc />
        /// <summary>
        ///     Roll the user changes back and set every Model to its original values.
        /// </summary>
        /// <exception cref="AggregateException">
        ///     An aggregate exception containing all the exceptions thrown by the registered
        ///     callbacks on the associated <see cref="CancellationToken" />.
        /// </exception>
        /// <exception cref="ObjectDisposedException">This <see cref="CancellationTokenSource" /> has been disposed.</exception>
        /// <exception cref="Exception">A <see langword="delegate" /> callback throws an exception.</exception>
        protected override void RollBackDataChanges()
        {
            // Roll back any data changes.
            if (this.IsModelHasModified())
            {
                base.RollBackDataChanges();
            }

            // Dispose every ExpressionsDetailViewModels.
            foreach (var expressionsDetailViewModels in this.ExpressionsDetailViewModels)
            {
                expressionsDetailViewModels.Dispose();
            }

            // Clear the ExpressionsDetailViewModels.
            this.ExpressionsDetailViewModels.Clear();

            // Initialize a new instance of the corresponding Model with the current customized values.
            this.Model = new ExpressionsHeader
                             {
                                 Wordbook = this.Model.Wordbook,
                                 CreationDateTime = DateTime.Now,
                                 LastUpdateDateTime = DateTime.Now,
                                 PronunciationTimes = this.Model.PronunciationTimes
                             };

            // Clear any existing validation errors.
            this.ClearValidationErrors();

            // Update the user interface depending on the new Model, and invoke property changed events.
            this.OnPropertyChanged(string.Empty);
        }

        /// <summary>
        ///     Attaches expressions details to the current Model.
        /// </summary>
        /// <param name="expressionsDetails">The expressions details.</param>
        private void AttachExpressionsDetails([NotNull] IEnumerable<ExpressionsDetail> expressionsDetails)
        {
            // Add the expressions details to the current Model using the main thread.
            Application.Current.Dispatcher.Invoke(
                () =>
                    {
                        foreach (var expressionsDetail in expressionsDetails)
                        {
                            this.ExpressionsDetailViewModels.Add(new ExpressionsDetailViewModel(expressionsDetail));
                        }
                    });
        }

        /// <summary>
        ///     Convert details of expressions to a sound stream and attach them to the current data Model asynchronously.
        /// </summary>
        /// <param name="progress">The action to report the <paramref name="progress" /> to the user.</param>
        /// <returns>
        ///     A <see cref="Task" /> represents the conversion operation.
        /// </returns>
        private async Task ConvertExpressionsDetailsToSoundStreamsAsync([NotNull] IProgress<int> progress)
        {
            // Clear existing ExpressionsDetails.
            this.Model.ExpressionsDetails.Clear();

            // Defines and reports the conversion progress percentage.
            var convertedExpressionIndex = 0;
            progress.Report(convertedExpressionIndex);

            // Convert expressions from string to sound stream.
            foreach (var expressionsDetailViewModel in this.ExpressionsDetailViewModels)
            {
                // Every expression will be checked to not convert it multiple times.
                await expressionsDetailViewModel.ConvertInnerModelAsync(this.cognitiveServices).ContinueWith(
                    conversionTask =>
                        {
                            // Attach the converted expressions to the ExpressionsDetails collection.
                            this.Model.ExpressionsDetails.Add(conversionTask.Result);

                            // Increases and reports the conversion progress percentage.
                            convertedExpressionIndex++;
                            progress.Report(convertedExpressionIndex);
                        },
                    this.CancellationToken,
                    TaskContinuationOptions.OnlyOnRanToCompletion,
                    TaskScheduler.Current).ConfigureAwait(false);
            }
        }

        /// <summary>
        ///     Deletes a view model of expressions detail asynchronous.
        /// </summary>
        /// <param name="expressionsDetailViewModel">The expressions detail view model.</param>
        /// <returns>
        ///     A <see cref="Task" /> represents the deleting operation.
        /// </returns>
        private async Task DeleteExpressionsDetailViewModelAsync([NotNull] object expressionsDetailViewModel)
        {
            // Cast the passed object to ExpressionsDetailViewModel.
            var deletingExpressionsDetailViewModel = (ExpressionsDetailViewModel)expressionsDetailViewModel;

            // Check the value of required expressions.
            if (string.IsNullOrWhiteSpace(deletingExpressionsDetailViewModel.ArabicCharacterSet)
                || string.IsNullOrWhiteSpace(deletingExpressionsDetailViewModel.NorwegianCharacterSet))
            {
                return;
            }

            // Ask the user to confirm deleting the ExpressionsDetailViewModel.
            if (!deletingExpressionsDetailViewModel.IsUserConfirmedDeletingModel())
            {
                return;
            }

            // Delete the inner Model from the data source.
            if (deletingExpressionsDetailViewModel.Model.Id != 0)
            {
                // Notify the user interface.
                this.IsPerformingLongRunningTask = true;

                // Update the ExpressionsCount and ExpressionsDetails.
                this.Model.ExpressionsCount -= 1;
                this.Model.LastUpdateDateTime = DateTime.Now;
                this.DataEntities.ExpressionsDetails.Remove(deletingExpressionsDetailViewModel.Model);

                try
                {
                    // Save the changes on the data source.
                    await this.DataEntities.SaveChangesAsync(this.CancellationToken).ConfigureAwait(false);
                }
                finally
                {
                    // Notify the user interface.
                    this.IsPerformingLongRunningTask = false;
                }
            }

            // Remove the deleted ExpressionsDetailViewModel from the collection and notify the user. 
            Application.Current.Dispatcher.Invoke(
                () =>
                    {
                        this.ExpressionsDetailViewModels.Remove(deletingExpressionsDetailViewModel);
                        this.OnDeletingModelSucceeded(
                            this,
                            new OnSaveChangesEventArgs { Entity = deletingExpressionsDetailViewModel.Model });
                    });
        }

        /// <summary>
        ///     Occurs when the expressions detail view models collection changed.
        /// </summary>
        /// <param name="sender">The <paramref name="sender" />.</param>
        /// <param name="e">The <see cref="NotifyCollectionChangedEventArgs" /> instance containing the event data.</param>
        private void ExpressionsDetailViewModelsChanged(
            [NotNull] object sender,
            [NotNull] NotifyCollectionChangedEventArgs e)
        {
            switch (e.Action)
            {
                case NotifyCollectionChangedAction.Add:

                    // Escape the first ExpressionsDetailViewModel.
                    if (e.NewStartingIndex > 0)
                    {
                        // Get the new ExpressionsDetailViewModel only.
                        if (e.NewItems[0] is ExpressionsDetailViewModel expressionsDetailViewModel
                            && expressionsDetailViewModel.Model.Id == 0)
                        {
                            // Repeat values of SpeakingRate and PronunciationTimes properties.
                            expressionsDetailViewModel.SpeakingRate =
                                this.ExpressionsDetailViewModels[e.NewStartingIndex - 1].SpeakingRate;
                            expressionsDetailViewModel.PronunciationTimes =
                                this.ExpressionsDetailViewModels[e.NewStartingIndex - 1].PronunciationTimes;
                        }
                    }

                    break;
            }

            // Reorder serial number for expressions detail on all actions.
            byte firstSerial = 1;
            foreach (var expressionsDetailViewModel in this.ExpressionsDetailViewModels)
            {
                expressionsDetailViewModel.SerialNumber = firstSerial++;
            }
        }

        /// <summary>
        ///     Checks the name of current expressions header against duplication.
        /// </summary>
        /// <returns>
        ///     A <see cref="Task{TResult}" /> represents the checking operation.
        /// </returns>
        private async Task<bool> IsDuplicatedNameAsync()
        {
            // Hold the checking results.
            var isDuplicatedName = false;

            // Check whether the name is duplicated or not.
            await this.DataEntities.ExpressionsHeaders
                .Where(
                    expressionsHeader => expressionsHeader.Id != this.Model.Id
                                         && expressionsHeader.Name == this.Model.Name.Trim()
                                         && expressionsHeader.WordbookId == this.Model.WordbookId)
                .ToListAsync(this.CancellationToken).ContinueWith(
                    checkingDuplicationTask =>
                        {
                            // Hold the checking results.
                            isDuplicatedName = checkingDuplicationTask.Result.Any();

                            // Display or remove an error to notify the user.
                            if (isDuplicatedName)
                            {
                                this.SetError(
                                    string.Format(ValidationMessages.DuplicatedName, this.Model.Name),
                                    nameof(this.Model.Name));
                            }
                            else
                            {
                                this.RemoveErrors(nameof(this.Model.Name));
                            }
                        },
                    this.CancellationToken,
                    TaskContinuationOptions.OnlyOnRanToCompletion,
                    TaskScheduler.Current).ConfigureAwait(false);

            // Return the checking results.
            return isDuplicatedName;
        }

        /// <summary>
        ///     Loads the Wordbooks which can hold expressions from the data source asynchronously.
        /// </summary>
        /// <returns>
        ///     A <see cref="Task" /> represents the loading operation.
        /// </returns>
        private async Task LoadWordbooksForExpressionsAsync()
        {
            // Notify the user interface.
            this.LongRunningTaskDescription = NotificationsLayer.PreparingUserInterfaceMessage;
            this.IsPerformingLongRunningTask = true;

            try
            {
                await this.DataEntities.Wordbooks
                    .Where(wordbook => wordbook.Type == (byte)WordbookTypes.Expression && !wordbook.IsFolder)
                    .ToListAsync(this.CancellationToken).ContinueWith(
                        readingTask =>
                            {
                                this.ExpressionsWordbooks = new ObservableCollection<Wordbook>(readingTask.Result);
                            },
                        this.CancellationToken,
                        TaskContinuationOptions.OnlyOnRanToCompletion,
                        TaskScheduler.Current).ConfigureAwait(false);
            }
            finally
            {
                this.IsPerformingLongRunningTask = false;
                this.LongRunningTaskDescription = string.Empty;
            }
        }

        /// <summary>
        ///     Called when a text to speech request has been failed and an error has been occurred.
        /// </summary>
        /// <param name="sender">The <paramref name="sender" />.</param>
        /// <param name="e">The <see cref="GenericEventArgs{Exception}" /> instance containing the event data.</param>
        private void OnConversionErrorOccurred([NotNull] object sender, [NotNull] GenericEventArgs<Exception> e)
        {
            this.DisplayDesktopAlert(
                NotificationsLayer.GeneralExceptionsHeader,
                string.Format(NotificationsLayer.GeneralExceptionsMessage, e.EventData.Message),
                AlertTypes.Error);
        }

        /// <summary>
        ///     Reports the <paramref name="progress" /> percentage for the conversion tasks.
        /// </summary>
        /// <param name="progress">The <paramref name="progress" /> percentage value.</param>
        private void ReportConversionProgress(int progress)
        {
            // Display the index of the converted expression.
            this.LongRunningTaskDescription = string.Format(
                NotificationsLayer.StringToSoundConversionIsInProgressMessage,
                progress,
                this.ExpressionsDetailViewModels.Count);
        }

        /// <summary>
        ///     Saves the data of the current <seealso cref="ExpressionsHeader" /> asynchronously.
        /// </summary>
        /// <returns>
        ///     A <see cref="Task" /> represents the Insert or the Update operations.
        /// </returns>
        private async Task SaveExpressionsHeaderDataAsync()
        {
            // Notify the user interface.
            this.IsPerformingLongRunningTask = true;

            try
            {
                // Check and notify the user if the name is duplicated.
                if (!await this.IsDuplicatedNameAsync().ConfigureAwait(false))
                {
                    // Update the ExpressionsCount and LastUpdateDateTime.
                    this.Model.LastUpdateDateTime = DateTime.Now;
                    this.Model.ExpressionsCount = (short)this.ExpressionsDetailViewModels.Count;

                    // Convert inner expressions from string to sound streams and attach them to the inner Model.
                    await this.ConvertExpressionsDetailsToSoundStreamsAsync(
                        new Progress<int>(this.ReportConversionProgress)).ConfigureAwait(false);

                    // Inserts or updates the data Model asynchronously.
                    switch (this.Model.Id)
                    {
                        case 0:
                            this.Model.CreationDateTime = DateTime.Now;
                            await this.InsertAsync().ConfigureAwait(false);
                            break;

                        default:
                            await this.UpdateAsync().ConfigureAwait(false);
                            break;
                    }
                }
            }
            finally
            {
                // Notify the user interface.
                this.IsPerformingLongRunningTask = false;
                this.LongRunningTaskDescription = string.Empty;
            }
        }

        /// <summary>
        ///     Displays a dialog box to edit a expressions header from the search result.
        /// </summary>
        /// <param name="searchingWindowType">The type of searching window.</param>
        /// <returns>
        ///     A <see cref="Task" /> represents the selecting operation.
        /// </returns>
        private async Task SearchExpressionsHeadersAsync([NotNull] object searchingWindowType)
        {
            // Is the selected data model not the current data model.
            var userSelectedDifferentModel = false;

            // Create and display the searching dialog on the main thread.
            Application.Current.Dispatcher.Invoke(
                () =>
                    {
                        // Display the searching data dialog.
                        DialogService.ShowDialog((Type)searchingWindowType);

                        // Roll back current data if the user has selected a different data Model.
                        if (DialogService.SelectedModelId != 0)
                        {
                            if (!this.Model.Id.Equals(DialogService.SelectedModelId))
                            {
                                userSelectedDifferentModel = true;
                                this.RollBackUserInputsCommand.Execute();
                            }
                            else
                            {
                                this.DisplayDesktopAlert(
                                    NotificationsLayer.DataUpdating,
                                    string.Format(NotificationsLayer.CanEditDataModelFromInputFields, this.Model.Name),
                                    AlertTypes.Information);
                            }
                        }
                    });

            // If the selected data model not the current data model.
            if (userSelectedDifferentModel)
            {
                try
                {
                    // Notify the user interface.
                    this.IsPerformingLongRunningTask = true;

                    // Search for data using the selected Model identifier.
                    this.Model = await this.DataEntities.ExpressionsHeaders
                                     .Include(expressionsHeaders => expressionsHeaders.ExpressionsDetails).SingleAsync(
                                         expressionsHeaders => expressionsHeaders.Id == DialogService.SelectedModelId,
                                         this.CancellationToken).ConfigureAwait(false);

                    // Fill the expressions details view model.
                    this.AttachExpressionsDetails(this.Model.ExpressionsDetails);
                }
                finally
                {
                    // Notify the user interface.
                    this.OnPropertyChanged(string.Empty);
                    this.IsPerformingLongRunningTask = false;
                }
            }
        }

        /// <summary>
        ///     Tests the speaking rate for the passed view model of expressions detail asynchronously.
        /// </summary>
        /// <param name="expressionsDetailViewModel">The expressions detail view model.</param>
        /// <returns>
        ///     A <see cref="Task" /> represents the testing operation.
        /// </returns>
        private async Task TestSpeakingRateAsync([NotNull] object expressionsDetailViewModel)
        {
            try
            {
                // Notify the user interface.
                this.IsTestingSpeakingRate = true;

                // Cast the passed object to ExpressionsDetailViewModel.
                var castedExpressionsDetailViewModel = (ExpressionsDetailViewModel)expressionsDetailViewModel;

                // Convert the inner Model to a sound streams if its not converted, then play the sound stream.
                await castedExpressionsDetailViewModel.TestSpeakingRateAsync(this.cognitiveServices)
                    .ConfigureAwait(false);
            }
            finally
            {
                // Notify the user interface.
                this.IsTestingSpeakingRate = false;
            }
        }
    }
}