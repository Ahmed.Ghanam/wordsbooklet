﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="NounsPlayerViewModel.cs" company="None">
// Last Modified: 07/10/2018
// Author: Ahmed Ghanam.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace WordsBooklet.BusinessLayer.ViewModels
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Collections.Specialized;
    using System.Data.Entity;
    using System.IO;
    using System.Linq;
    using System.Media;
    using System.Threading;
    using System.Threading.Tasks;
    using System.Windows;

    using Microsoft.Win32;

    using WordsBooklet.BusinessLayer.Infrastructure;
    using WordsBooklet.BusinessLayer.Infrastructure.Notifications.Resources;
    using WordsBooklet.BusinessLayer.Models;

    /// <inheritdoc />
    /// <summary>
    ///     Playing and saving the sound of the <seealso cref="NounsHeader" /> data model.
    /// </summary>
    /// <seealso cref="NotificationsLayer{T}" />
    public sealed class NounsPlayerViewModel : NotificationsLayer<NounsHeader>
    {
        /// <summary>
        ///     The back field for the <see cref="BeginUtteringNounsDetail" /> property.
        /// </summary>
        [CanBeNull]
        private NounsDetail beginUtteringNounsDetail;

        /// <summary>
        ///     The back field for the <see cref="IsUtteringNounDetail" /> property.
        /// </summary>
        private bool isUtteringNounDetail;

        /// <summary>
        ///     The back field for the <see cref="IsUtteringPaused" /> property.
        /// </summary>
        private bool isUtteringPaused;

        /// <summary>
        ///     The back field for the <see cref="StoredNounsHeaders" /> property.
        /// </summary>
        [CanBeNull]
        private ObservableCollection<NounsHeader> storedNounsHeaders;

        /// <summary>
        ///     The back field for the <see cref="UseKnownPluralArabicForm" /> property.
        /// </summary>
        private bool useKnownPluralArabicForm;

        /// <summary>
        ///     The back field for the <see cref="UseKnownPluralNorwegianForm" /> property.
        /// </summary>
        private bool useKnownPluralNorwegianForm;

        /// <summary>
        ///     The back field for the <see cref="UseKnownSingularArabicForm" /> property.
        /// </summary>
        private bool useKnownSingularArabicForm;

        /// <summary>
        ///     The back field for the <see cref="UseKnownSingularNorwegianForm" /> property.
        /// </summary>
        private bool useKnownSingularNorwegianForm;

        /// <summary>
        ///     The back field for the <see cref="UseUnknownPluralArabicForm" /> property.
        /// </summary>
        private bool useUnknownPluralArabicForm;

        /// <summary>
        ///     The back field for the <see cref="UseUnknownPluralNorwegianForm" /> property.
        /// </summary>
        private bool useUnknownPluralNorwegianForm;

        /// <inheritdoc />
        /// <summary>
        ///     Initializes a new instance of the <see cref="NounsPlayerViewModel" /> class.
        /// </summary>
        public NounsPlayerViewModel()
        {
            // Set the SelectedNounsHeaders collection changed event handler.
            this.SelectedNounsHeaders.CollectionChanged += this.SelectedNounsHeadersChangedAsync;
        }

        /// <summary>
        ///     Gets the nouns detail which is being uttered now.
        /// </summary>
        /// <value>
        ///     The nouns detail which is being uttered now.
        /// </value>
        [CanBeNull]
        public NounsDetail BeginUtteringNounsDetail
        {
            get => this.beginUtteringNounsDetail;

            private set
            {
                this.beginUtteringNounsDetail = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets a value indicating whether <see langword="this" /> instance is uttering a nouns detail or not.
        /// </summary>
        /// <value>
        ///     <see langword="true" /> if <see langword="this" /> instance is uttering a nouns detail otherwise,
        ///     <see langword="false" />.
        /// </value>
        public bool IsUtteringNounDetail
        {
            get => this.isUtteringNounDetail;

            private set
            {
                this.isUtteringNounDetail = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets a command to load the nouns headers from the data source asynchronously.
        /// </summary>
        /// <value>
        ///     The command to load the nouns headers from the data source asynchronously.
        /// </value>
        [NotNull]
        public AsyncCommand LoadNounsHeadersCommand => new AsyncCommand(this.LoadNounsHeadersAsync);

        /// <summary>
        ///     Gets a command to pause uttering the selected nouns details.
        /// </summary>
        /// <value>
        ///     The command to pause uttering the selected nouns details.
        /// </value>
        [NotNull]
        public RelayCommand PauseUtteringCommand =>
            new RelayCommand(
                canExecute => this.IsUtteringNounDetail && !this.IsUtteringPaused,
                execute =>
                    {
                        using (var cancellationTokenSource = new CancellationTokenSource())
                        {
                            this.IsUtteringPaused = true;
                            cancellationTokenSource.Cancel();
                            this.CancellationToken = cancellationTokenSource.Token;
                        }
                    });

        /// <summary>
        ///     Gets a command to resume uttering the selected nouns details asynchronously.
        /// </summary>
        /// <value>
        ///     The command to resume uttering the selected nouns details asynchronously.
        /// </value>
        [NotNull]
        public AsyncCommand ResumeUtteringCommand =>
            new AsyncCommand(
                canExecute => this.IsUtteringPaused && !this.IsUtteringNounDetail && this.SelectedNounsDetails.Any(),
                this.UtterNounsDetailsAsync);

        /// <summary>
        ///     Gets a command to save the selected nouns details as a sound file asynchronously.
        /// </summary>
        /// <value>
        ///     The command to save the selected nouns details as a sound file asynchronously.
        /// </value>
        [NotNull]
        public AsyncCommand SaveAsSoundFileCommand =>
            new AsyncCommand(canExecute => this.SelectedNounsDetails.Any(), this.SaveAsSoundFileAsync);

        /// <summary>
        ///     Gets the nouns details of the selected nouns headers.
        /// </summary>
        /// <value>
        ///     The nouns details of the selected nouns headers.
        /// </value>
        [NotNull]
        public ObservableCollection<NounsDetail> SelectedNounsDetails { get; } =
            new ObservableCollection<NounsDetail>();

        /// <summary>
        ///     Gets or sets the nouns headers which user has select to utter.
        /// </summary>
        /// <value>
        ///     The nouns headers which user has select to utter.
        /// </value>
        [NotNull]
        public ObservableCollection<NounsHeader> SelectedNounsHeaders { get; set; } =
            new ObservableCollection<NounsHeader>();

        /// <summary>
        ///     Gets the nouns headers stored into the data source.
        /// </summary>
        /// <value>
        ///     The nouns headers stored into the data source.
        /// </value>
        [CanBeNull]
        public ObservableCollection<NounsHeader> StoredNounsHeaders
        {
            get => this.storedNounsHeaders;

            private set
            {
                this.storedNounsHeaders = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets a value indicating whether the known plural Arabic form is used or not.
        /// </summary>
        /// <value>
        ///     <see langword="true" /> if the known plural Arabic form is used; otherwise, <see langword="false" />.
        /// </value>
        public bool UseKnownPluralArabicForm
        {
            get => this.useKnownPluralArabicForm;
            set
            {
                this.useKnownPluralArabicForm = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets a value indicating whether the known plural Norwegian form is used or not.
        /// </summary>
        /// <value>
        ///     <see langword="true" /> if the known plural Norwegian form is used; otherwise, <see langword="false" />.
        /// </value>
        public bool UseKnownPluralNorwegianForm
        {
            get => this.useKnownPluralNorwegianForm;
            set
            {
                this.useKnownPluralNorwegianForm = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets a value indicating whether the known singular Arabic form is used or not.
        /// </summary>
        /// <value>
        ///     <see langword="true" /> if the known singular Arabic form is used; otherwise, <see langword="false" />.
        /// </value>
        public bool UseKnownSingularArabicForm
        {
            get => this.useKnownSingularArabicForm;
            set
            {
                this.useKnownSingularArabicForm = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets a value indicating whether the known singular Norwegian form is used or not.
        /// </summary>
        /// <value>
        ///     <see langword="true" /> if the known singular Norwegian form is used; otherwise, <see langword="false" />.
        /// </value>
        public bool UseKnownSingularNorwegianForm
        {
            get => this.useKnownSingularNorwegianForm;
            set
            {
                this.useKnownSingularNorwegianForm = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets a value indicating whether the unknown plural Arabic form is used or not.
        /// </summary>
        /// <value>
        ///     <see langword="true" /> if the unknown plural Arabic form is used; otherwise, <see langword="false" />.
        /// </value>
        public bool UseUnknownPluralArabicForm
        {
            get => this.useUnknownPluralArabicForm;
            set
            {
                this.useUnknownPluralArabicForm = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets a value indicating whether the unknown plural Norwegian form is used or not.
        /// </summary>
        /// <value>
        ///     <see langword="true" /> if the unknown plural Norwegian form is used; otherwise, <see langword="false" />.
        /// </value>
        public bool UseUnknownPluralNorwegianForm
        {
            get => this.useUnknownPluralNorwegianForm;
            set
            {
                this.useUnknownPluralNorwegianForm = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets a command to utter the selected nouns details from beginning asynchronously.
        /// </summary>
        /// <value>
        ///     The command to utter the selected nouns details from beginning asynchronously.
        /// </value>
        [NotNull]
        public AsyncCommand UtterNounsDetailsCommand =>
            new AsyncCommand(
                canExecute => !this.IsUtteringNounDetail && this.SelectedNounsDetails.Any(),
                this.UtterNounsDetailsFromBeginningAsync);

        /// <summary>
        ///     Gets or sets the cancellation token for controlling tasks.
        /// </summary>
        /// <value>
        ///     The cancellation token for controlling tasks.
        /// </value>
        private new CancellationToken CancellationToken { get; set; }

        /// <summary>
        ///     Gets or sets a value indicating whether the user has paused the uttering or not.
        /// </summary>
        /// <value>
        ///     <see langword="true" /> if the user has paused the uttering; otherwise, <see langword="false" />.
        /// </value>
        private bool IsUtteringPaused
        {
            get => this.isUtteringPaused;

            set
            {
                this.isUtteringPaused = value;
                this.OnPropertyChanged();
            }
        }

        /// <inheritdoc />
        /// <summary>
        ///     Releases unmanaged and - optionally - managed resources.
        /// </summary>
        /// <param name="disposing">
        ///     <see langword="true" /> to release both managed and unmanaged resources; <see langword="false" /> to release only
        ///     unmanaged resources.
        /// </param>
        /// <exception cref="ObjectDisposedException">This <see cref="CancellationTokenSource" /> has been disposed.</exception>
        /// <exception cref="AggregateException">
        ///     An aggregate exception containing all the exceptions thrown by the registered
        ///     callbacks on the associated <see cref="CancellationToken" />.
        /// </exception>
        protected override void Dispose(bool disposing)
        {
            // Call base implementation, Always first.
            base.Dispose(disposing);

            if (!disposing)
            {
                return;
            }

            using (var cancellationTokenSource = new CancellationTokenSource())
            {
                cancellationTokenSource.Cancel();
                this.CancellationToken = cancellationTokenSource.Token;
            }

            // Dispose related commands.
            this.LoadNounsHeadersCommand.Dispose();
            this.UtterNounsDetailsCommand.Dispose();
            this.ResumeUtteringCommand.Dispose();
            this.SaveAsSoundFileCommand.Dispose();

            // Remove event listeners and clear the nouns detail collection.
            this.SelectedNounsHeaders.CollectionChanged -= this.SelectedNounsHeadersChangedAsync;
        }

        /// <summary>
        ///     Creates a sound file using the <see langword="byte" /> arrays of the selected nouns details asynchronously.
        /// </summary>
        /// <param name="soundFilePath">The sound file path.</param>
        /// <returns>
        ///     A <see cref="Task" /> represents the creation operation.
        /// </returns>
        [NotNull]
        private Task CreateSoundFileAsync([NotNull] string soundFilePath)
        {
            return Task.Factory.StartNew(
                () =>
                    {
                        // Compress all sounds streams into a single array.
                        var soundFilesStreams = new List<byte[]>();
                        foreach (var nounsListDetail in this.SelectedNounsDetails)
                        {
                            soundFilesStreams.Add(nounsListDetail.GetSoundStreams());
                        }

                        // Save byte array as sound file (.WAV).
                        var soundFileWriter = new SoundFilesWriter();
                        soundFileWriter.CreateSoundFile(soundFilePath, soundFilesStreams);
                    },
                base.CancellationToken);
        }

        /// <summary>
        ///     Loads the nouns headers from data source asynchronously.
        /// </summary>
        /// <returns>
        ///     A <see cref="Task" /> represents the loading operation.
        /// </returns>
        private async Task LoadNounsHeadersAsync()
        {
            // Notify the user interface.
            this.LongRunningTaskDescription = NotificationsLayer.PreparingUserInterfaceMessage;
            this.IsPerformingLongRunningTask = true;

            try
            {
                await this.DataEntities.NounsHeaders.ToListAsync(base.CancellationToken).ContinueWith(
                    loadingTask =>
                        {
                            this.StoredNounsHeaders = new ObservableCollection<NounsHeader>(loadingTask.Result);
                        },
                    base.CancellationToken,
                    TaskContinuationOptions.OnlyOnRanToCompletion,
                    TaskScheduler.Current).ConfigureAwait(false);
            }
            finally
            {
                this.IsPerformingLongRunningTask = false;
                this.LongRunningTaskDescription = string.Empty;
            }
        }

        /// <summary>
        ///     Plays the passed sound stream as a sound asynchronously.
        /// </summary>
        /// <param name="soundStream">The sound as <see langword="byte" /> array.</param>
        /// <returns>
        ///     A <see cref="Task" /> represent the audio playing operation.
        /// </returns>
        [NotNull]
        private Task PlaySoundStreamAsync([CanBeNull] byte[] soundStream)
        {
            // Throw exception if the user canceled the task.
            this.CancellationToken.ThrowIfCancellationRequested();

            // Return a completed task if the passed stream is null.
            if (soundStream == null)
            {
                return Task.CompletedTask;
            }

            // Return a completed task if the passed stream is empty.
            if (soundStream.Length == 0)
            {
                return Task.CompletedTask;
            }

            // Play the sound using a separate task.
            return Task.Factory.StartNew(
                () =>
                    {
                        using (var soundPlayer = new SoundPlayer(new MemoryStream(soundStream)))
                        {
                            soundPlayer.PlaySync();
                        }
                    },
                this.CancellationToken);
        }

        /// <summary>
        ///     Saves the selected nouns details as a sound file asynchronously.
        /// </summary>
        /// <returns>
        ///     A <see cref="Task" /> represents the saving operation.
        /// </returns>
        private async Task SaveAsSoundFileAsync()
        {
            // Notify the user interface.
            this.IsPerformingLongRunningTask = true;

            // Hold the values of SaveFileDialog.
            bool? saveFileDialogResult = false;
            var saveFileDialogFullName = string.Empty;

            try
            {
                // Display the SaveFileDialog on the main thread.
                Application.Current.Dispatcher.Invoke(
                    () =>
                        {
                            var saveFileDialog = new SaveFileDialog
                                                     {
                                                         ValidateNames = true,
                                                         OverwritePrompt = true,
                                                         CheckPathExists = true,
                                                         RestoreDirectory = true,
                                                         Filter = "wav files (*.wav)|*.wav",
                                                         FileName = this.SelectedNounsHeaders[0].FullName
                                                                    ?? string.Empty
                                                     };
                            saveFileDialogResult = saveFileDialog.ShowDialog();
                            saveFileDialogFullName = saveFileDialog.FileName;
                        });

                // Create the sound file in a separate task.
                if (saveFileDialogResult == true)
                {
                    await this.CreateSoundFileAsync(saveFileDialogFullName).ConfigureAwait(false);
                }
            }
            finally
            {
                this.IsPerformingLongRunningTask = false;
            }
        }

        /// <summary>
        ///     Occurs when the collection of the selected nouns headers changed.
        /// </summary>
        /// <param name="sender">The <paramref name="sender" />.</param>
        /// <param name="e">The <see cref="NotifyCollectionChangedEventArgs" /> instance containing the event data.</param>
        private async void SelectedNounsHeadersChangedAsync(
            [NotNull] object sender,
            [NotNull] NotifyCollectionChangedEventArgs e)
        {
            // Handle only the new and the old NounsHeader.
            if (!((e.NewItems?[0] ?? e.OldItems?[0]) is NounsHeader nounsHeader))
            {
                return;
            }

            // Cancel resuming sound playing.
            this.IsUtteringPaused = false;

            // Notify the user interface.
            this.IsPerformingLongRunningTask = true;

            try
            {
                await Task.Factory.StartNew(
                    () =>
                        {
                            Application.Current.Dispatcher.Invoke(
                                () =>
                                    {
                                        switch (e.Action)
                                        {
                                            case NotifyCollectionChangedAction.Add:
                                                foreach (var nounsDetail in nounsHeader.NounsDetails)
                                                {
                                                    this.SelectedNounsDetails.Add(nounsDetail);
                                                }

                                                break;

                                            case NotifyCollectionChangedAction.Remove:
                                                foreach (var nounsDetail in nounsHeader.NounsDetails)
                                                {
                                                    this.SelectedNounsDetails.Remove(nounsDetail);
                                                }

                                                break;
                                        }
                                    });

                            // Handle the displaying of noun forms.
                            this.SetUsedArabicForms(this.SelectedNounsDetails);
                            this.SetUsedNorwegianForms(this.SelectedNounsDetails);
                        },
                    base.CancellationToken,
                    TaskCreationOptions.None,
                    TaskScheduler.Current).ConfigureAwait(false);
            }
            finally
            {
                this.IsPerformingLongRunningTask = false;
            }
        }

        /// <summary>
        ///     Reflects the used Arabic forms to the user.
        /// </summary>
        /// <param name="nounsDetails">The nouns details.</param>
        private void SetUsedArabicForms([NotNull] IReadOnlyCollection<NounsDetail> nounsDetails)
        {
            this.UseKnownPluralArabicForm = nounsDetails.Any(
                nounsDetail => !string.IsNullOrWhiteSpace(nounsDetail.KnownPluralArabicCharacterSet));
            this.UseUnknownPluralArabicForm = nounsDetails.Any(
                nounsDetail => !string.IsNullOrWhiteSpace(nounsDetail.UnknownPluralArabicCharacterSet));
            this.UseKnownSingularArabicForm = nounsDetails.Any(
                nounsDetail => !string.IsNullOrWhiteSpace(nounsDetail.KnownSingularArabicCharacterSet));
        }

        /// <summary>
        ///     Reflects the used Norwegian forms to the user.
        /// </summary>
        /// <param name="nounsDetails">The nouns details.</param>
        private void SetUsedNorwegianForms([NotNull] IReadOnlyCollection<NounsDetail> nounsDetails)
        {
            this.UseKnownPluralNorwegianForm = nounsDetails.Any(
                nounsDetail => !string.IsNullOrWhiteSpace(nounsDetail.KnownPluralNorwegianCharacterSet));
            this.UseUnknownPluralNorwegianForm = nounsDetails.Any(
                nounsDetail => !string.IsNullOrWhiteSpace(nounsDetail.UnknownPluralNorwegianCharacterSet));
            this.UseKnownSingularNorwegianForm = nounsDetails.Any(
                nounsDetail => !string.IsNullOrWhiteSpace(nounsDetail.KnownSingularNorwegianCharacterSet));
        }

        /// <summary>
        ///     Utters the nouns in the nouns detail asynchronously.
        /// </summary>
        /// <param name="nounsDetail">The nouns detail.</param>
        /// <returns>
        ///     A <see cref="Task" /> represents the uttering operation.
        /// </returns>
        private async Task UtterNounsDetailAsync([NotNull] NounsDetail nounsDetail)
        {
            try
            {
                // Continue if the noun is not uttered as much as declared.
                while (nounsDetail.TimesNumberUttered < nounsDetail.PronunciationTimes)
                {
                    // Notify that the uttering has been resumed.
                    this.IsUtteringPaused = false;

                    // Notify that uttering has been started.
                    this.IsUtteringNounDetail = true;

                    // Hold the noun detail which begin uttering.
                    this.BeginUtteringNounsDetail = nounsDetail;

                    // Play The Singular Unknown Noun Using Arabic.
                    await this.PlaySoundStreamAsync(nounsDetail.UnknownSingularArabicPronunciation)
                        .ConfigureAwait(false);

                    // Play The Singular Unknown Noun Using Norwegian.
                    await this.PlaySoundStreamAsync(nounsDetail.UnknownSingularNorwegianPronunciation)
                        .ConfigureAwait(false);

                    // Play The Singular Known Noun Using Arabic.
                    await this.PlaySoundStreamAsync(nounsDetail.KnownSingularArabicPronunciation).ConfigureAwait(false);

                    // Play The Singular Known Noun Using Norwegian.
                    await this.PlaySoundStreamAsync(nounsDetail.KnownSingularNorwegianPronunciation)
                        .ConfigureAwait(false);

                    // Play The Plural Unknown Noun Using Arabic.
                    await this.PlaySoundStreamAsync(nounsDetail.UnknownPluralArabicPronunciation).ConfigureAwait(false);

                    // Play The Plural Unknown Noun Using Norwegian.
                    await this.PlaySoundStreamAsync(nounsDetail.UnknownPluralNorwegianPronunciation)
                        .ConfigureAwait(false);

                    // Play The Plural Known Noun Using Arabic.
                    await this.PlaySoundStreamAsync(nounsDetail.KnownPluralArabicPronunciation).ConfigureAwait(false);

                    // Play The Plural Known Noun Using Norwegian.
                    await this.PlaySoundStreamAsync(nounsDetail.KnownPluralNorwegianPronunciation)
                        .ConfigureAwait(false);

                    // Play The Noun Example Using Norwegian.
                    await this.PlaySoundStreamAsync(nounsDetail.NorwegianExamplePronunciation).ConfigureAwait(false);

                    // Increase the value of the TimesNumberUttered.
                    nounsDetail.TimesNumberUttered += 1;
                }
            }
            finally
            {
                // Notify that uttering has been done.
                this.IsUtteringNounDetail = false;

                // Release the last uttered noun.
                this.BeginUtteringNounsDetail = null;
            }
        }

        /// <summary>
        ///     Utters the nouns details of the selected nouns headers asynchronously.
        /// </summary>
        /// <returns>
        ///     A <see cref="Task" /> represent the uttering operation.
        /// </returns>
        private async Task UtterNounsDetailsAsync()
        {
            // Initialize a new instance of the CancellationToken.
            if (this.CancellationToken.IsCancellationRequested)
            {
                this.CancellationToken = new CancellationToken();
            }

            // Loop on the selected nouns headers. 
            foreach (var selectedNounsHeader in this.SelectedNounsHeaders)
            {
                // Continue if the header is not uttered as much as declared.
                while (selectedNounsHeader.TimesNumberUttered < selectedNounsHeader.PronunciationTimes)
                {
                    // Utter nouns detail in a separated task as much as declared where it has been paused.
                    foreach (var nounsDetail in selectedNounsHeader.NounsDetails)
                    {
                        await this.UtterNounsDetailAsync(nounsDetail).ConfigureAwait(false);
                    }

                    // Increase the value of the TimesNumberUttered.
                    selectedNounsHeader.TimesNumberUttered += 1;

                    // Reset the value of TimesNumberUttered of nouns details until the last rotation on the header.
                    if (selectedNounsHeader.TimesNumberUttered < selectedNounsHeader.PronunciationTimes)
                    {
                        foreach (var nounsDetail in selectedNounsHeader.NounsDetails)
                        {
                            nounsDetail.TimesNumberUttered = 0;
                        }
                    }
                }
            }
        }

        /// <summary>
        ///     Utters the selected nouns details from beginning asynchronously.
        /// </summary>
        /// <returns>
        ///     A <see cref="Task" /> represents the uttering operation.
        /// </returns>
        [NotNull]
        private Task UtterNounsDetailsFromBeginningAsync()
        {
            // Reset the value of TimesNumberUttered for both headers and details.
            foreach (var nounsHeader in this.SelectedNounsHeaders)
            {
                nounsHeader.TimesNumberUttered = 0;
                foreach (var nounsDetail in nounsHeader.NounsDetails)
                {
                    nounsDetail.TimesNumberUttered = 0;
                }
            }

            // Utter the nouns details of the selected headers.
            return this.UtterNounsDetailsAsync();
        }
    }
}