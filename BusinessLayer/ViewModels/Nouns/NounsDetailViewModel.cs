﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="NounsDetailViewModel.cs" company="None">
// Last Modified: 01/10/2018
// Author: Ahmed Ghanam.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace WordsBooklet.BusinessLayer.ViewModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.Diagnostics;
    using System.IO;
    using System.Linq;
    using System.Media;
    using System.Reflection;
    using System.Threading;
    using System.Threading.Tasks;
    using System.Windows;

    using WordsBooklet.BusinessLayer.Infrastructure;
    using WordsBooklet.BusinessLayer.Models;

    /// <inheritdoc />
    /// <summary>
    ///     Handling the <seealso cref="NounsDetail" /> data model.
    /// </summary>
    /// <seealso cref="NotificationsLayer{T}" />
    public sealed class NounsDetailViewModel : NotificationsLayer<NounsDetail>
    {
        /// <summary>
        ///     Represents the properties names of the changed nouns.
        /// </summary>
        [NotNull]
        private readonly List<string> changedNounsList = new List<string>();

        /// <summary>
        ///     The cognitive services instance.
        /// </summary>
        [CanBeNull]
        private CognitiveServices cognitiveServices;

        /// <summary>
        ///     Represents the <see langword="public" /> properties of the current data Model.
        /// </summary>
        [CanBeNull]
        private PropertyInfo[] modelProperties;

        /// <summary>
        ///     Represents the current data Model type.
        /// </summary>
        [CanBeNull]
        private Type modelType;

        /// <inheritdoc />
        /// <summary>
        ///     Initializes a new instance of the <see cref="NounsDetailViewModel" /> class.
        /// </summary>
        public NounsDetailViewModel()
        {
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="NounsDetailViewModel" /> class.
        /// </summary>
        /// <param name="nounsDetail">The nouns detail to be set as the inner Model.</param>
        /// <inheritdoc />
        internal NounsDetailViewModel([NotNull] NounsDetail nounsDetail)
        {
            this.SetInnerModel(nounsDetail);
        }

        /// <summary>
        ///     Gets or sets the character set of the known plural noun using the Arabic language.
        /// </summary>
        /// <value>
        ///     The character set of the known plural noun using the Arabic language.
        /// </value>
        [CanBeNull]
        public string KnownPluralArabicCharacterSet
        {
            get => this.Model.KnownPluralArabicCharacterSet;
            set
            {
                this.Model.KnownPluralArabicCharacterSet = value;

                this.OnPropertyChanged(
                    this.Model.KnownPluralArabicCharacterSet,
                    nameof(this.Model.KnownPluralArabicCharacterSet));
            }
        }

        /// <summary>
        ///     Gets or sets the character set of the known plural noun using the Norwegian (Bokmål) language.
        /// </summary>
        /// <value>
        ///     The character set of the known plural noun using the Norwegian (Bokmål) language.
        /// </value>
        [CanBeNull]
        public string KnownPluralNorwegianCharacterSet
        {
            get => this.Model.KnownPluralNorwegianCharacterSet;
            set
            {
                this.Model.KnownPluralNorwegianCharacterSet = value;

                this.OnPropertyChanged(
                    this.Model.KnownPluralNorwegianCharacterSet,
                    nameof(this.Model.KnownPluralNorwegianCharacterSet));
            }
        }

        /// <summary>
        ///     Gets or sets the character set of the singular known noun using the Arabic language.
        /// </summary>
        /// <value>
        ///     The character set of the singular known noun using the Arabic language.
        /// </value>
        [CanBeNull]
        public string KnownSingularArabicCharacterSet
        {
            get => this.Model.KnownSingularArabicCharacterSet;
            set
            {
                this.Model.KnownSingularArabicCharacterSet = value;

                this.OnPropertyChanged(
                    this.Model.KnownSingularArabicCharacterSet,
                    nameof(this.Model.KnownSingularArabicCharacterSet));
            }
        }

        /// <summary>
        ///     Gets or sets the character set of the singular known noun using the Norwegian (Bokmål) language.
        /// </summary>
        /// <value>
        ///     The character set of the singular known noun using the Norwegian (Bokmål) language.
        /// </value>
        [CanBeNull]
        public string KnownSingularNorwegianCharacterSet
        {
            get => this.Model.KnownSingularNorwegianCharacterSet;
            set
            {
                this.Model.KnownSingularNorwegianCharacterSet = value;

                this.OnPropertyChanged(
                    this.Model.KnownSingularNorwegianCharacterSet,
                    nameof(this.Model.KnownSingularNorwegianCharacterSet));
            }
        }

        /// <summary>
        ///     Gets or sets the character set of the example using the Norwegian (Bokmål) language.
        /// </summary>
        /// <value>
        ///     The character set of the example using the Norwegian (Bokmål) language.
        /// </value>
        [CanBeNull]
        public string NorwegianExampleCharacterSet
        {
            get => this.Model.NorwegianExampleCharacterSet;
            set
            {
                this.Model.NorwegianExampleCharacterSet = value;

                this.OnPropertyChanged(
                    this.Model.NorwegianExampleCharacterSet,
                    nameof(this.Model.NorwegianExampleCharacterSet));
            }
        }

        /// <summary>
        ///     Gets or sets the pronunciations times to repeat the pronunciation of current noun.
        /// </summary>
        /// <value>
        ///     The pronunciations times to repeat the pronunciation of current noun.
        /// </value>
        public byte PronunciationTimes
        {
            get => this.Model.PronunciationTimes;
            set
            {
                this.Model.PronunciationTimes = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets the noun serial number in the current nouns collection.
        /// </summary>
        /// <value>
        ///     The noun serial number in the current nouns collection.
        /// </value>
        public short SerialNumber
        {
            get => this.Model.SerialNumber;
            internal set
            {
                this.Model.SerialNumber = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets the speed of the speaking rate.
        /// </summary>
        /// <value>
        ///     The speed of the speaking rate.
        /// </value>
        [NotNull]
        public string SpeakingRate
        {
            get => this.Model.SpeakingRate;
            set
            {
                this.Model.SpeakingRate = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets the character set of the plural unknown noun using the Arabic language.
        /// </summary>
        /// <value>
        ///     The character set of the plural unknown noun using the Arabic language.
        /// </value>
        [CanBeNull]
        public string UnknownPluralArabicCharacterSet
        {
            get => this.Model.UnknownPluralArabicCharacterSet;
            set
            {
                this.Model.UnknownPluralArabicCharacterSet = value;

                this.OnPropertyChanged(
                    this.Model.UnknownPluralArabicCharacterSet,
                    nameof(this.Model.UnknownPluralArabicCharacterSet));
            }
        }

        /// <summary>
        ///     Gets or sets the character set of the plural unknown noun using the Norwegian (Bokmål) language.
        /// </summary>
        /// <value>
        ///     The character set of the plural unknown noun using the Norwegian (Bokmål) language.
        /// </value>
        [CanBeNull]
        public string UnknownPluralNorwegianCharacterSet
        {
            get => this.Model.UnknownPluralNorwegianCharacterSet;
            set
            {
                this.Model.UnknownPluralNorwegianCharacterSet = value;

                this.OnPropertyChanged(
                    this.Model.UnknownPluralNorwegianCharacterSet,
                    nameof(this.Model.UnknownPluralNorwegianCharacterSet));
            }
        }

        /// <summary>
        ///     Gets or sets the character set of the singular unknown noun using the Arabic language.
        /// </summary>
        /// <value>
        ///     The character set of the singular unknown noun using the Arabic language.
        /// </value>
        [CanBeNull]
        public string UnknownSingularArabicCharacterSet
        {
            get => this.Model.UnknownSingularArabicCharacterSet;
            set
            {
                this.Model.UnknownSingularArabicCharacterSet = value;

                this.OnPropertyChanged(
                    this.Model.UnknownSingularArabicCharacterSet,
                    nameof(this.Model.UnknownSingularArabicCharacterSet));
            }
        }

        /// <summary>
        ///     Gets or sets the character set of the singular unknown noun using the Norwegian (Bokmål) language.
        /// </summary>
        /// <value>
        ///     The character set of the singular unknown noun using the Norwegian (Bokmål) language.
        /// </value>
        [CanBeNull]
        public string UnknownSingularNorwegianCharacterSet
        {
            get => this.Model.UnknownSingularNorwegianCharacterSet;
            set
            {
                this.Model.UnknownSingularNorwegianCharacterSet = value;

                this.OnPropertyChanged(
                    this.Model.UnknownSingularNorwegianCharacterSet,
                    nameof(this.Model.UnknownSingularNorwegianCharacterSet));
            }
        }

        /// <summary>
        ///     Gets or sets a value indicating whether <see langword="this" />
        ///     instance should play the sound once it is available or not.
        /// </summary>
        /// <value>
        ///     <see langword="true" /> if <see langword="this" /> instance should
        ///     play the sound once it is available; otherwise, <see langword="false" />.
        /// </value>
        private bool AutoPlaySound { get; set; }

        /// <summary>
        ///     Converts nouns from the inner data Model to sound streams and pronounce them asynchronously.
        /// </summary>
        /// <param name="cognitiveServicesInstance">The cognitive services instance.</param>
        /// <returns>
        ///     A <see cref="Task{Results}" /> represents the converted data Model.
        /// </returns>
        /// <exception cref="ValidationException">The required nouns are null or empty.</exception>
        [ItemNotNull]
        internal async Task<NounsDetail> ConvertInnerModelAsync([NotNull] CognitiveServices cognitiveServicesInstance)
        {
            // Initialize the cognitive services layer field.
            if (this.cognitiveServices == null)
            {
                this.cognitiveServices = cognitiveServicesInstance;
            }

            // Convert The Unknown Singular Arabic To Sound Stream.
            this.Model.UnknownSingularArabicPronunciation =
                await this.ConvertInnerModelAsync(
                    Languages.Arabic,
                    nameof(this.Model.UnknownSingularArabicCharacterSet),
                    this.Model.UnknownSingularArabicCharacterSet,
                    this.Model.UnknownSingularArabicPronunciation).ConfigureAwait(false);

            // Validate The Unknown Singular Arabic Noun.
            if (this.Model.UnknownSingularArabicPronunciation.Length == 0)
            {
                throw new ValidationException();
            }

            // Convert The Unknown Singular Norwegian To Sound Stream.
            this.Model.UnknownSingularNorwegianPronunciation =
                await this.ConvertInnerModelAsync(
                    Languages.Bokmål,
                    nameof(this.Model.UnknownSingularNorwegianCharacterSet),
                    this.Model.UnknownSingularNorwegianCharacterSet,
                    this.Model.UnknownSingularNorwegianPronunciation).ConfigureAwait(false);

            // Validate The Unknown Singular Arabic Noun.
            if (this.Model.UnknownSingularNorwegianPronunciation.Length == 0)
            {
                throw new ValidationException();
            }

            // Convert The Known Singular Arabic To Sound Stream.
            this.Model.KnownSingularArabicPronunciation =
                await this.ConvertInnerModelAsync(
                    Languages.Arabic,
                    nameof(this.Model.KnownSingularArabicCharacterSet),
                    this.Model.KnownSingularArabicCharacterSet,
                    this.Model.KnownSingularArabicPronunciation).ConfigureAwait(false);

            // Convert The Known Singular Norwegian To Sound Stream.
            this.Model.KnownSingularNorwegianPronunciation =
                await this.ConvertInnerModelAsync(
                    Languages.Bokmål,
                    nameof(this.Model.KnownSingularNorwegianCharacterSet),
                    this.Model.KnownSingularNorwegianCharacterSet,
                    this.Model.KnownSingularNorwegianPronunciation).ConfigureAwait(false);

            // Convert The Unknown Plural Arabic To Sound Stream.
            this.Model.UnknownPluralArabicPronunciation =
                await this.ConvertInnerModelAsync(
                    Languages.Arabic,
                    nameof(this.Model.UnknownPluralArabicCharacterSet),
                    this.Model.UnknownPluralArabicCharacterSet,
                    this.Model.UnknownPluralArabicPronunciation).ConfigureAwait(false);

            // Convert The Unknown Plural Norwegian To Sound Stream.
            this.Model.UnknownPluralNorwegianPronunciation =
                await this.ConvertInnerModelAsync(
                    Languages.Bokmål,
                    nameof(this.Model.UnknownPluralNorwegianCharacterSet),
                    this.Model.UnknownPluralNorwegianCharacterSet,
                    this.Model.UnknownPluralNorwegianPronunciation).ConfigureAwait(false);

            // Convert The Known Plural Arabic To Sound Stream.
            this.Model.KnownPluralArabicPronunciation =
                await this.ConvertInnerModelAsync(
                    Languages.Bokmål,
                    nameof(this.Model.KnownPluralArabicCharacterSet),
                    this.Model.KnownPluralArabicCharacterSet,
                    this.Model.KnownPluralArabicPronunciation).ConfigureAwait(false);

            // Convert The Known Plural Norwegian To Sound Stream.
            this.Model.KnownPluralNorwegianPronunciation =
                await this.ConvertInnerModelAsync(
                    Languages.Bokmål,
                    nameof(this.Model.KnownPluralNorwegianCharacterSet),
                    this.Model.KnownPluralNorwegianCharacterSet,
                    this.Model.KnownPluralNorwegianPronunciation).ConfigureAwait(false);

            // Convert The Norwegian Example To Sound Stream.
            this.Model.NorwegianExamplePronunciation =
                await this.ConvertInnerModelAsync(
                    Languages.Bokmål,
                    nameof(this.Model.NorwegianExampleCharacterSet),
                    this.Model.NorwegianExampleCharacterSet,
                    this.Model.NorwegianExamplePronunciation).ConfigureAwait(false);

            // Return the inner Model holding all filled properties.
            return this.Model;
        }

        /// <summary>
        ///     Determines whether the user confirmed to delete the inner Model or not.
        /// </summary>
        /// <returns>
        ///     <see langword="true" /> if the user confirmed to delete the inner Model; otherwise, <see langword="false" />.
        /// </returns>
        internal bool IsUserConfirmedDeletingModel()
        {
            // Holds the value of the confirmation.
            var isDeletingConfirmed = false;

            // Ask the user to confirm deleting the inner Model using the main thread.
            Application.Current.Dispatcher.Invoke(
                () => { isDeletingConfirmed = this.IsUserConfirmedDeletingModel(this.Model); });

            // Return the value of the confirmation.
            return isDeletingConfirmed;
        }

        /// <summary>
        ///     Tests the speaking rate of the inner model asynchronously.
        /// </summary>
        /// <param name="cognitiveServicesInstance">The cognitive services instance.</param>
        /// <returns>
        ///     A <see cref="Task" /> represents the testing operation.
        /// </returns>
        /// <exception cref="ValidationException">The required nouns are null or empty.</exception>
        internal async Task TestSpeakingRateAsync([NotNull] CognitiveServices cognitiveServicesInstance)
        {
            // Auto play the sound once results are available.
            this.AutoPlaySound = true;

            try
            {
                // Convert the inner Model to a sound streams if its not converted, and play the sound stream.
                await this.ConvertInnerModelAsync(cognitiveServicesInstance).ConfigureAwait(false);
            }
            finally
            {
                // Stop auto playing the sound once results are available.
                this.AutoPlaySound = false;
            }
        }

        /// <inheritdoc />
        /// <summary>
        ///     Releases unmanaged and - optionally - managed resources.
        /// </summary>
        /// <param name="disposing">
        ///     <see langword="true" /> to release both managed and unmanaged resources; <see langword="false" /> to release only
        ///     unmanaged resources.
        /// </param>
        /// <exception cref="AggregateException">
        ///     An aggregate exception containing all the exceptions thrown by the registered
        ///     callbacks on the associated <see cref="CancellationToken" />.
        /// </exception>
        protected override void Dispose(bool disposing)
        {
            // Call base implementation, Always first.
            base.Dispose(disposing);

            if (!disposing)
            {
                return;
            }

            // Clear Model type.
            this.modelType = null;

            // Clear Model properties collection.
            this.modelProperties = null;

            // Remove the value of cognitive services instance.
            this.cognitiveServices = null;

            // Clear the changed properties names list.
            this.changedNounsList.Clear();
        }

        /// <inheritdoc />
        /// <summary>
        ///     Called when a property value changed.
        /// </summary>
        /// <param name="sender">The <paramref name="sender" />.</param>
        /// <param name="e">The <see cref="PropertyChangedEventArgs" /> instance containing the event data.</param>
        protected override void OnPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            switch (e.PropertyName)
            {
                // Changing any noun will result in re-producing its audio stream only.
                case nameof(this.Model.KnownPluralArabicCharacterSet):
                case nameof(this.Model.KnownPluralNorwegianCharacterSet):
                case nameof(this.Model.UnknownPluralArabicCharacterSet):
                case nameof(this.Model.UnknownPluralNorwegianCharacterSet):
                case nameof(this.Model.KnownSingularArabicCharacterSet):
                case nameof(this.Model.KnownSingularNorwegianCharacterSet):
                case nameof(this.Model.UnknownSingularArabicCharacterSet):
                case nameof(this.Model.UnknownSingularNorwegianCharacterSet):
                case nameof(this.Model.NorwegianExampleCharacterSet):

                    // Remove the changed noun from changed nouns list, don't hold empty properties.
                    if (this.changedNounsList.Contains(e.PropertyName))
                    {
                        this.changedNounsList.Remove(e.PropertyName);
                    }

                    // Get the value of the changed property.
                    var changedPropertyValue = this.GetPropertyValue(e.PropertyName);

                    // If the changed noun is not null, insert it into the changed nouns list.
                    if (!string.IsNullOrEmpty(changedPropertyValue))
                    {
                        this.changedNounsList.Add(e.PropertyName);
                    }

                    break;

                // Changing speaking rate will result in re-producing the audio stream for all nouns.
                case nameof(this.Model.SpeakingRate):

                    // Clear the changed noun list.
                    this.changedNounsList.Clear();

                    // Get properties names which is a string and not empty or null.
                    var propertiesCarryingValues = this.GetPropertiesNameCarryingValues();

                    // Add properties names to the changed nouns list.
                    this.changedNounsList.AddRange(propertiesCarryingValues);

                    break;
            }
        }

        /// <summary>
        ///     Converts the passed noun from string to a sound stream and play it asynchronously.
        /// </summary>
        /// <param name="speakingLanguage">The speaking language.</param>
        /// <param name="targetPropertyName">The noun property name.</param>
        /// <param name="targetPropertyValue">The noun property value.</param>
        /// <param name="currentSoundStreamBytes">The current sound stream.</param>
        /// <returns>
        ///     A <see cref="Task{TResult}" /> represents the sound stream as a byte array.
        /// </returns>
        private async Task<byte[]> ConvertInnerModelAsync(
            Languages speakingLanguage,
            [NotNull] string targetPropertyName,
            [NotNull] string targetPropertyValue,
            [NotNull] byte[] currentSoundStreamBytes)
        {
            // Throw exception if the user canceled the whole task.
            this.CancellationToken.ThrowIfCancellationRequested();

            // Do not convert empty nouns.
            if (string.IsNullOrWhiteSpace(targetPropertyValue) || this.cognitiveServices == null)
            {
                return Array.Empty<byte>();
            }

            // Do not convert not changed nouns.
            if (this.changedNounsList.Contains(targetPropertyName))
            {
                // Start measuring the pronouncing time.
                var pronouncingElapsedTime = Stopwatch.StartNew();

                // Build and send a pronunciation request.
                await this.cognitiveServices
                    .ConvertToSoundAsync(targetPropertyValue.Trim(), this.Model.SpeakingRate, speakingLanguage)
                    .ContinueWith(
                        async conversionTask =>
                            {
                                // Stop measuring the pronouncing time.
                                pronouncingElapsedTime.Stop();

                                // Hold the conversion results.
                                currentSoundStreamBytes = conversionTask.Result;

                                // If no conversion results, return.
                                if (currentSoundStreamBytes.Length == 0)
                                {
                                    return;
                                }

                                // Remove the converted noun from the changed nouns collection.
                                this.changedNounsList.Remove(targetPropertyName);

                                // Measuring the total pronouncing time opposite the ability to pronounce 40 words per minute.
                                var waitingMilliseconds = (int)(1500 - pronouncingElapsedTime.ElapsedMilliseconds);

                                // Applying the rule 'User can only pronounce 40 words per minute'.
                                await Task.Delay(
                                    waitingMilliseconds > 0 ? waitingMilliseconds : 0,
                                    this.CancellationToken).ConfigureAwait(false);
                            },
                        this.CancellationToken,
                        TaskContinuationOptions.OnlyOnRanToCompletion,
                        TaskScheduler.Current).Unwrap().ConfigureAwait(false);
            }

            // Return the conversion results if the user did not request playing sounds.
            if (!this.AutoPlaySound)
            {
                return currentSoundStreamBytes;
            }

            // Play the sound if the user requested playing sounds and it is not empty.
            if (currentSoundStreamBytes.Length > 0)
            {
                await this.PlaySoundStreamAsync(currentSoundStreamBytes).ConfigureAwait(false);
            }

            // Return the conversion results.
            return currentSoundStreamBytes;
        }

        /// <summary>
        ///     Gets properties names which carrying values.
        /// </summary>
        /// <returns>
        ///     A <see cref="IEnumerable{T}" /> holding properties names.
        /// </returns>
        [NotNull]
        private IEnumerable<string> GetPropertiesNameCarryingValues()
        {
            // Initialize the Model type.
            if (this.modelType == null)
            {
                this.modelType = this.Model.GetType();
            }

            // Initialize the public Model properties.
            if (this.modelProperties == null)
            {
                this.modelProperties = this.modelType.GetProperties();
            }

            // Return every property matching rules.
            return (from modelPropertyInfo in this.modelProperties
                    where modelPropertyInfo.CanWrite && modelPropertyInfo.PropertyType == typeof(string)
                                                     && !string.IsNullOrWhiteSpace(
                                                         Convert.ToString(modelPropertyInfo.GetValue(this.Model)))
                                                     && !Convert.ToString(modelPropertyInfo.GetValue(this.Model))
                                                         .Contains("%")
                    select modelPropertyInfo.Name).ToList();
        }

        /// <summary>
        ///     Gets the value of the passed property name.
        /// </summary>
        /// <param name="propertyName">The property name.</param>
        /// <returns>
        ///     The value of the passed property name.
        /// </returns>
        [NotNull]
        private string GetPropertyValue([NotNull] string propertyName)
        {
            // Initialize the Model type.
            if (this.modelType == null)
            {
                this.modelType = this.Model.GetType();
            }

            // Get property value using reflection.
            var matchedPropertyInfo = this.modelType.GetProperty(propertyName);
            var valueOfMatchedPropertyInfo = matchedPropertyInfo?.GetValue(this.Model);
            return Convert.ToString(valueOfMatchedPropertyInfo);
        }

        /// <summary>
        ///     Plays the passed sound stream asynchronously.
        /// </summary>
        /// <param name="nounAsByteArray">The noun as <see langword="byte" /> array.</param>
        /// <returns>
        ///     A <see cref="Task" /> represent the playing operation.
        /// </returns>
        [NotNull]
        private Task PlaySoundStreamAsync([NotNull] byte[] nounAsByteArray)
        {
            // Throw exception if the user canceled the whole conversion task.
            this.CancellationToken.ThrowIfCancellationRequested();

            // Do not play the sound if the stream is empty.
            if (nounAsByteArray.Length == 0)
            {
                return Task.CompletedTask;
            }

            // Play the sound using a separate task.
            return Task.Factory.StartNew(
                () =>
                    {
                        using (var soundPlayer = new SoundPlayer(new MemoryStream(nounAsByteArray)))
                        {
                            soundPlayer.PlaySync();
                        }
                    },
                this.CancellationToken);
        }

        /// <summary>
        ///     Sets the inner Model using the passed NounsDetail.
        /// </summary>
        /// <param name="nounsDetail">The nouns detail.</param>
        private void SetInnerModel([NotNull] NounsDetail nounsDetail)
        {
            // Sets the inner Model.
            this.Model = nounsDetail;

            // Notify both the user interface and changed nouns list.
            this.OnPropertyChanged(string.Empty);
        }
    }
}