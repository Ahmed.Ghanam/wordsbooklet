﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="NounsSearchViewModel.cs" company="None">
// Last Modified: 01/10/2018
// Author: Ahmed Ghanam.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace WordsBooklet.BusinessLayer.ViewModels
{
    using System;
    using System.Collections.ObjectModel;
    using System.Data.Entity;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Input;

    using WordsBooklet.BusinessLayer.Infrastructure;
    using WordsBooklet.BusinessLayer.Infrastructure.Notifications.Resources;
    using WordsBooklet.BusinessLayer.Models;

    /// <inheritdoc />
    /// <summary>
    ///     Searching about a <seealso cref="NounsHeader" /> for purposes of selecting or deleting.
    /// </summary>
    /// <seealso cref="NotificationsLayer{T}" />
    public sealed class NounsSearchViewModel : NotificationsLayer<NounsHeader>
    {
        /// <summary>
        ///     The back field for the <see cref="DialogResult" /> property.
        /// </summary>
        private bool? dialogResult;

        /// <summary>
        ///     The back field for the <see cref="NoResults" /> property.
        /// </summary>
        private bool noResults;

        /// <summary>
        ///     The back field for the <see cref="SearchEndDate" /> property.
        /// </summary>
        [CanBeNull]
        private DateTime? searchEndDate;

        /// <summary>
        ///     The back field for the <see cref="SearchName" /> property.
        /// </summary>
        [CanBeNull]
        private string searchName;

        /// <summary>
        ///     The back field for the <see cref="SearchResults" /> property.
        /// </summary>
        [CanBeNull]
        private ObservableCollection<NounsHeader> searchResults;

        /// <summary>
        ///     The back field for the <see cref="SearchStartDate" /> property.
        /// </summary>
        [CanBeNull]
        private DateTime? searchStartDate;

        /// <summary>
        ///     The back field for the <see cref="SearchWordbook" /> property.
        /// </summary>
        [CanBeNull]
        private Wordbook searchWordbook;

        /// <summary>
        ///     The back field for the <see cref="NounsWordbooks" /> property.
        /// </summary>
        [CanBeNull]
        private ObservableCollection<Wordbook> nounsWordbooks;

        /// <summary>
        ///     Gets a command to delete a nouns header and details asynchronously.
        /// </summary>
        /// <value>
        ///     The command to delete a nouns header and details asynchronously.
        /// </value>
        [NotNull]
        public AsyncCommand DeleteNounsHeaderCommand => new AsyncCommand(this.DeleteNounsHeaderAsync);

        /// <summary>
        ///     Gets or sets the DialogResult value for displaying <see langword="this" /> instance as a dialog.
        /// </summary>
        /// <value>
        ///     The DialogResult value for displaying <see langword="this" /> instance as a dialog.
        /// </value>
        public bool? DialogResult
        {
            get => this.dialogResult;

            set
            {
                this.dialogResult = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets a command to load the Wordbooks which can hold nouns from the data source asynchronously.
        /// </summary>
        /// <value>
        ///     The command to load the Wordbooks which can hold nouns from the data source asynchronously.
        /// </value>
        [NotNull]
        public AsyncCommand LoadWordbooksForNounsCommand => new AsyncCommand(this.LoadWordbooksForNounsAsync);

        /// <summary>
        ///     Gets a value indicating whether there is matches to the search or not.
        /// </summary>
        /// <value>
        ///     <see langword="false" /> if there is matches to the search; otherwise, <see langword="true" />.
        /// </value>
        public bool NoResults
        {
            get => this.noResults;

            private set
            {
                this.noResults = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets a command to return the identifier of the selected nouns header and to close the current dialog.
        /// </summary>
        /// <value>
        ///     The command to return the identifier of the selected nouns header and to close the current dialog.
        /// </value>
        [NotNull]
        public ICommand ReturnSelectedIdentifierCommand => new RelayCommand(this.ReturnSelectedIdentifier);

        /// <summary>
        ///     Gets or sets the end date for searching.
        /// </summary>
        /// <value>
        ///     The end date for searching.
        /// </value>
        [CanBeNull]
        public DateTime? SearchEndDate
        {
            get => this.searchEndDate;

            set
            {
                this.searchEndDate = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets the name for searching.
        /// </summary>
        /// <value>
        ///     The name for searching.
        /// </value>
        [CanBeNull]
        public string SearchName
        {
            get => this.searchName;

            set
            {
                this.searchName = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets a command to display the search results using the search inputs.
        /// </summary>
        /// <value>
        ///     The command to display the search results using the search inputs.
        /// </value>
        [NotNull]
        public AsyncCommand SearchNounsHeadersCommand => new AsyncCommand(this.SearchNounsHeadersAsync);

        /// <summary>
        ///     Gets the search results of using the search inputs
        /// </summary>
        /// <value>
        ///     The search results of using the search inputs.
        /// </value>
        [CanBeNull]
        public ObservableCollection<NounsHeader> SearchResults
        {
            get => this.searchResults;

            private set
            {
                this.searchResults = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets the start date for searching.
        /// </summary>
        /// <value>
        ///     The start date for searching.
        /// </value>
        [CanBeNull]
        public DateTime? SearchStartDate
        {
            get => this.searchStartDate;

            set
            {
                this.searchStartDate = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets the Wordbook for searching.
        /// </summary>
        /// <value>
        ///     The Wordbook for searching.
        /// </value>
        [CanBeNull]
        public Wordbook SearchWordbook
        {
            get => this.searchWordbook;

            set
            {
                this.searchWordbook = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets the wordbooks which can hold nouns which stored into the data source.
        /// </summary>
        /// <value>
        ///     The wordbooks which can hold nouns which stored into the data source.
        /// </value>
        [CanBeNull]
        public ObservableCollection<Wordbook> NounsWordbooks
        {
            get => this.nounsWordbooks;

            private set
            {
                this.nounsWordbooks = value;
                this.OnPropertyChanged();
            }
        }

        /// <inheritdoc />
        /// <summary>
        ///     Releases unmanaged and - optionally - managed resources.
        /// </summary>
        /// <param name="disposing">
        ///     <see langword="true" /> to release both managed and unmanaged resources; <see langword="false" /> to release only
        ///     unmanaged resources.
        /// </param>
        /// <exception cref="AggregateException">
        ///     An aggregate exception containing all the exceptions thrown by the registered
        ///     callbacks on the associated <see cref="CancellationToken" />.
        /// </exception>
        protected override void Dispose(bool disposing)
        {
            // Call base implementation, Always first.
            base.Dispose(disposing);

            if (!disposing)
            {
                return;
            }

            // Dispose related commands.
            this.DeleteNounsHeaderCommand.Dispose();
            this.SearchNounsHeadersCommand.Dispose();
            this.LoadWordbooksForNounsCommand.Dispose();
        }

        /// <summary>
        ///     Builds the query to search for nouns headers.
        /// </summary>
        /// <returns>
        ///     A <seealso cref="IQueryable{T}" /> represents the search query.
        /// </returns>
        [NotNull]
        private IQueryable<NounsHeader> BuildSearchQuery()
        {
            // By default select all nouns lists headers.
            var searchingQuery = from nounsHeaders in this.DataEntities.NounsHeaders select nounsHeaders;

            // Add searching by the noun category.
            if (this.SearchWordbook != null)
            {
                searchingQuery = searchingQuery.Where(
                    nounsListHeader => nounsListHeader.WordbookId == this.SearchWordbook.Id);
            }

            // Add searching by the name.
            if (!string.IsNullOrWhiteSpace(this.SearchName))
            {
                searchingQuery =
                    searchingQuery.Where(nounsListHeader => nounsListHeader.Name.Contains(this.SearchName));
            }

            // Add searching by the start date for the creation date.
            if (this.SearchStartDate.HasValue)
            {
                searchingQuery = searchingQuery.Where(
                    nounsListHeader => nounsListHeader.CreationDateTime.Date >= this.SearchStartDate.Value.Date);
            }

            // Add searching by the end date for the creation date.
            if (this.SearchEndDate.HasValue)
            {
                searchingQuery = searchingQuery.Where(
                    nounsListHeader => nounsListHeader.CreationDateTime.Date <= this.SearchEndDate.Value.Date);
            }

            // Return the built select query.
            return searchingQuery;
        }

        /// <summary>
        ///     Deletes a singular nouns header and its details asynchronously.
        /// </summary>
        /// <param name="nounsHeader">The nouns header.</param>
        /// <returns>
        ///     A <see cref="Task" /> represents the deleting operation.
        /// </returns>
        [NotNull]
        private Task DeleteNounsHeaderAsync([NotNull] object nounsHeader)
        {
            // Cast the passed object to a NounsHeader.
            var nounsHeaderModel = (NounsHeader)nounsHeader;

            // Ask the user to confirm deleting the NounsHeader data model.
            return this.DeleteAsync(nounsHeaderModel).ContinueWith(
                deletingTask =>
                    {
                        // Remove the deleted NounsHeader from collection.
                        if (deletingTask.Result)
                        {
                            Application.Current.Dispatcher.Invoke(
                                () => { this.SearchResults?.Remove(nounsHeaderModel); });
                        }
                    },
                this.CancellationToken,
                TaskContinuationOptions.OnlyOnRanToCompletion,
                TaskScheduler.Current);
        }

        /// <summary>
        ///     Loads the Wordbooks which can hold nouns from the data source asynchronously.
        /// </summary>
        /// <returns>
        ///     A <see cref="Task" /> represents the loading operation.
        /// </returns>
        private async Task LoadWordbooksForNounsAsync()
        {
            // Notify the user interface.
            this.LongRunningTaskDescription = NotificationsLayer.PreparingUserInterfaceMessage;
            this.IsPerformingLongRunningTask = true;

            try
            {
                await this.DataEntities.Wordbooks
                    .Where(wordbook => wordbook.Type == (byte)WordbookTypes.Noun && !wordbook.IsFolder)
                    .ToListAsync(this.CancellationToken).ContinueWith(
                        readingTask =>
                            {
                                this.NounsWordbooks = new ObservableCollection<Wordbook>(readingTask.Result);
                            },
                        this.CancellationToken,
                        TaskContinuationOptions.OnlyOnRanToCompletion,
                        TaskScheduler.Current).ConfigureAwait(false);
            }
            finally
            {
                this.IsPerformingLongRunningTask = false;
                this.LongRunningTaskDescription = string.Empty;
            }
        }

        /// <summary>
        ///     Closes the dialog and return the identifier of the selected NounsHeader.
        /// </summary>
        /// <param name="nounsHeader">The selected NounsHeader.</param>
        private void ReturnSelectedIdentifier([CanBeNull] object nounsHeader)
        {
            // Assign the selected nouns header to the generic model property.
            this.Model = (NounsHeader)nounsHeader ?? throw new InvalidOperationException();

            // Set the dialog result value.
            this.DialogResult = true;

            // Close current dialog and return the identifier.
            DialogService.CloseDialog(this.Model.Id);
        }

        /// <summary>
        ///     Searches for nouns headers using the search inputs asynchronously.
        /// </summary>
        /// <returns>
        ///     A <see cref="Task" /> represents the searching operation.
        /// </returns>
        private async Task SearchNounsHeadersAsync()
        {
            // Notify the user interface.
            this.LongRunningTaskDescription = NotificationsLayer.SearchingForDataMessage;
            this.IsPerformingLongRunningTask = true;

            try
            {
                // Build the search query and search for data.
                await this.BuildSearchQuery().ToListAsync(this.CancellationToken).ContinueWith(
                    searchingTask =>
                        {
                            this.NoResults = searchingTask.Result.Count == 0;
                            this.SearchResults = searchingTask.Result.Count == 0
                                                     ? null
                                                     : new ObservableCollection<NounsHeader>(searchingTask.Result);
                        },
                    this.CancellationToken,
                    TaskContinuationOptions.OnlyOnRanToCompletion,
                    TaskScheduler.Current).ConfigureAwait(false);
            }
            finally
            {
                // Notify the user interface.
                this.IsPerformingLongRunningTask = false;
                this.LongRunningTaskDescription = string.Empty;
            }
        }
    }
}