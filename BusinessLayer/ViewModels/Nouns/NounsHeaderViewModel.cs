﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="NounsHeaderViewModel.cs" company="None">
// Last Modified: 06/10/2018
// Author: Ahmed Ghanam.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace WordsBooklet.BusinessLayer.ViewModels
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Collections.Specialized;
    using System.ComponentModel;
    using System.Data.Entity;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using System.Windows;

    using WordsBooklet.BusinessLayer.Infrastructure;
    using WordsBooklet.BusinessLayer.Infrastructure.Notifications.Resources;
    using WordsBooklet.BusinessLayer.Models;
    using WordsBooklet.BusinessLayer.ViewModels.Resources;

    /// <inheritdoc />
    /// <summary>
    ///     Handling the <seealso cref="NounsHeader" /> data model.
    /// </summary>
    /// <seealso cref="NotificationsLayer{T}" />
    public sealed class NounsHeaderViewModel : NotificationsLayer<NounsHeader>
    {
        /// <summary>
        ///     The cognitive services instance.
        /// </summary>
        [NotNull]
        private readonly CognitiveServices cognitiveServices;

        /// <summary>
        ///     The back field for the <see cref="IsTestingSpeakingRateOfNounsDetail" /> property.
        /// </summary>
        private bool isTestingSpeakingRateOfNounsDetail;

        /// <summary>
        ///     The back field for the <see cref="NounsWordbooks" /> property.
        /// </summary>
        [CanBeNull]
        private ObservableCollection<Wordbook> nounsWordbooks;

        /// <summary>
        ///     The back field for the <see cref="UseKnownPluralArabicForm" /> property.
        /// </summary>
        private bool useKnownPluralArabicForm;

        /// <summary>
        ///     The back field for the <see cref="UseKnownPluralNorwegianForm" /> property.
        /// </summary>
        private bool useKnownPluralNorwegianForm;

        /// <summary>
        ///     The back field for the <see cref="UseKnownSingularArabicForm" /> property.
        /// </summary>
        private bool useKnownSingularArabicForm;

        /// <summary>
        ///     The back field for the <see cref="UseKnownSingularNorwegianForm" /> property.
        /// </summary>
        private bool useKnownSingularNorwegianForm;

        /// <summary>
        ///     The back field for the <see cref="UseUnknownPluralArabicForm" /> property.
        /// </summary>
        private bool useUnknownPluralArabicForm;

        /// <summary>
        ///     The back field for the <see cref="UseUnknownPluralNorwegianForm" /> property.
        /// </summary>
        private bool useUnknownPluralNorwegianForm;

        /// <inheritdoc />
        /// <summary>
        ///     Initializes a new instance of the <see cref="NounsHeaderViewModel" /> class.
        /// </summary>
        /// <exception cref="OverflowException">
        ///     the call back value is less than <see cref="TimeSpan.MinValue" />
        ///     or greater than <see cref="TimeSpan.MaxValue" />
        ///     .-or-
        ///     the call back value is <see cref="Double.PositiveInfinity" />.-or- the call back value is
        ///     <see cref="Double.NegativeInfinity" />.
        /// </exception>
        /// <exception cref="ArgumentOutOfRangeException">
        ///     The number of milliseconds in the value of call back value is negative
        ///     and not equal to <see cref="Timeout.Infinite" />, or is greater than <see cref="Int32.MaxValue" />.
        /// </exception>
        /// <exception cref="ArgumentException"> the call back value is equal to <see cref="Double.NaN" />. </exception>
        /// <exception cref="ArgumentNullException">The call back value parameter is <see langword="null" />. </exception>
        public NounsHeaderViewModel()
        {
            // Set the initial values of the inner Model properties.
            this.Model.PronunciationTimes = 1;

            // Initialize the cognitive services instance.
            this.cognitiveServices = new CognitiveServices();
            this.cognitiveServices.OnErrorOccurred += this.OnConversionErrorOccurred;

            // Set the NounsDetailViewModels collection changed event handler.
            this.NounsDetailViewModels.CollectionChanged += this.NounsDetailViewModelsChanged;
        }

        /// <summary>
        ///     Gets a command to delete a view model of nouns detail.
        /// </summary>
        /// <value>
        ///     The command to delete a view model of nouns detail.
        /// </value>
        [NotNull]
        public AsyncCommand DeleteNounsDetailViewModelCommand => new AsyncCommand(this.DeleteNounsDetailViewModelAsync);

        /// <summary>
        ///     Gets a value indicating whether the user is testing the speaking rate of a noun detail or not.
        /// </summary>
        /// <value>
        ///     <see langword="true" /> if the user is testing the speaking rate of a noun detail; otherwise,
        ///     <see langword="false" />.
        /// </value>
        public bool IsTestingSpeakingRateOfNounsDetail
        {
            get => this.isTestingSpeakingRateOfNounsDetail;

            private set
            {
                this.isTestingSpeakingRateOfNounsDetail = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets a command to load the Wordbooks which can hold nouns from the data source asynchronously.
        /// </summary>
        /// <value>
        ///     The command to load the Wordbooks which can hold nouns from the data source asynchronously.
        /// </value>
        [NotNull]
        public AsyncCommand LoadWordbooksForNounsCommand => new AsyncCommand(this.LoadWordbooksForNounsAsync);

        /// <summary>
        ///     Gets or sets the current nouns header name.
        /// </summary>
        /// <value>
        ///     The current nouns header name.
        /// </value>
        [CanBeNull]
        public string Name
        {
            get => this.Model.Name;

            set
            {
                this.Model.Name = value;
                this.OnPropertyChanged(this.Model.Name, nameof(this.Model.Name));
            }
        }

        /// <summary>
        ///     Gets the view models of the nouns details which the current header holding.
        /// </summary>
        /// <value>
        ///     The view models of the nouns details which the current header holding.
        /// </value>
        [NotNull]
        public ObservableCollection<NounsDetailViewModel> NounsDetailViewModels { get; } =
            new ObservableCollection<NounsDetailViewModel>();

        /// <summary>
        ///     Gets the wordbooks which can hold nouns which stored into the data source.
        /// </summary>
        /// <value>
        ///     The wordbooks which can hold nouns which stored into the data source.
        /// </value>
        [CanBeNull]
        public ObservableCollection<Wordbook> NounsWordbooks
        {
            get => this.nounsWordbooks;

            private set
            {
                this.nounsWordbooks = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets the pronunciation times of the current nouns header.
        /// </summary>
        /// <value>
        ///     The pronunciation times of the current nouns header.
        /// </value>
        public byte PronunciationTimes
        {
            get => this.Model.PronunciationTimes;

            set
            {
                this.Model.PronunciationTimes = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets the current nouns header remarks.
        /// </summary>
        /// <value>
        ///     The current nouns header remarks.
        /// </value>
        [CanBeNull]
        public string Remarks
        {
            get => this.Model.Remarks;

            set
            {
                this.Model.Remarks = value;
                this.OnPropertyChanged(this.Model.Remarks, nameof(this.Model.Remarks));
            }
        }

        /// <summary>
        ///     Gets a command to roll the user inputs back.
        /// </summary>
        /// <value>
        ///     The command to roll the user inputs back.
        /// </value>
        [NotNull]
        public RelayCommand RollBackUserInputsCommand =>
            new RelayCommand(
                canExecute => this.HasErrors || !string.IsNullOrEmpty(this.Model.Name)
                                             || !string.IsNullOrEmpty(this.Model.Remarks)
                                             || this.NounsDetailViewModels.Any(),
                execute => this.RollBackDataChanges());

        /// <summary>
        ///     Gets a command to insert or update the current <seealso cref="NounsHeader" />.
        /// </summary>
        /// <value>
        ///     The command to insert or update the current <seealso cref="NounsHeader" />.
        /// </value>
        [NotNull]
        public AsyncCommand SaveNounsHeaderDataCommand =>
            new AsyncCommand(
                canExecute => this.Model.Wordbook != null && !string.IsNullOrWhiteSpace(this.Model.Name)
                                                          && this.IsModelHasModified()
                                                          && this.NounsDetailViewModels.Any(
                                                              nounDetailViewModel =>
                                                                  nounDetailViewModel.IsModelHasModified()),
                this.SaveNounsHeaderDataAsync);

        /// <summary>
        ///     Gets a command to open a dialog box to search and select a single nouns header.
        /// </summary>
        /// <value>
        ///     The command to open a dialog box to search and select a single nouns header.
        /// </value>
        [NotNull]
        public AsyncCommand SearchNounsHeadersCommand => new AsyncCommand(this.SearchNounsHeadersAsync);

        /// <summary>
        ///     Gets a command to tests the speaking rate of a view model of nouns detail asynchronously.
        /// </summary>
        /// <value>
        ///     The command to tests the speaking rate of a view model of nouns detail
        /// </value>
        [NotNull]
        public AsyncCommand TestSpeakingRateOfNounsDetailCommand =>
            new AsyncCommand(this.TestSpeakingRateAsync);

        /// <summary>
        ///     Gets or sets a value indicating whether the known plural Arabic form is used or not.
        /// </summary>
        /// <value>
        ///     <see langword="true" /> if the known plural Arabic form is used; otherwise, <see langword="false" />.
        /// </value>
        public bool UseKnownPluralArabicForm
        {
            get => this.useKnownPluralArabicForm;
            set
            {
                this.useKnownPluralArabicForm = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets a value indicating whether the known plural Norwegian form is used or not.
        /// </summary>
        /// <value>
        ///     <see langword="true" /> if the known plural Norwegian form is used; otherwise, <see langword="false" />.
        /// </value>
        public bool UseKnownPluralNorwegianForm
        {
            get => this.useKnownPluralNorwegianForm;
            set
            {
                this.useKnownPluralNorwegianForm = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets a value indicating whether the known singular Arabic form is used or not.
        /// </summary>
        /// <value>
        ///     <see langword="true" /> if the known singular Arabic form is used; otherwise, <see langword="false" />.
        /// </value>
        public bool UseKnownSingularArabicForm
        {
            get => this.useKnownSingularArabicForm;
            set
            {
                this.useKnownSingularArabicForm = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets a value indicating whether the known singular Norwegian form is used or not.
        /// </summary>
        /// <value>
        ///     <see langword="true" /> if the known singular Norwegian form is used; otherwise, <see langword="false" />.
        /// </value>
        public bool UseKnownSingularNorwegianForm
        {
            get => this.useKnownSingularNorwegianForm;
            set
            {
                this.useKnownSingularNorwegianForm = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets a value indicating whether the unknown plural Arabic form is used or not.
        /// </summary>
        /// <value>
        ///     <see langword="true" /> if the unknown plural Arabic form is used; otherwise, <see langword="false" />.
        /// </value>
        public bool UseUnknownPluralArabicForm
        {
            get => this.useUnknownPluralArabicForm;
            set
            {
                this.useUnknownPluralArabicForm = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets a value indicating whether the unknown plural Norwegian form is used or not.
        /// </summary>
        /// <value>
        ///     <see langword="true" /> if the unknown plural Norwegian form is used; otherwise, <see langword="false" />.
        /// </value>
        public bool UseUnknownPluralNorwegianForm
        {
            get => this.useUnknownPluralNorwegianForm;
            set
            {
                this.useUnknownPluralNorwegianForm = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets the main wordbook of the current nouns header.
        /// </summary>
        /// <value>
        ///     The main wordbook of the current nouns header.
        /// </value>
        [CanBeNull]
        public Wordbook Wordbook
        {
            get => this.Model.Wordbook;

            set
            {
                // Set the selected word category object.
                this.Model.Wordbook = value;

                // Set the selected word category identifier.
                this.Model.WordbookId = this.Model.Wordbook?.Id ?? 0;

                // Validate the user inputs and notify the user interface.
                this.OnPropertyChanged(this.Model.Wordbook, nameof(this.Model.Wordbook));
            }
        }

        /// <inheritdoc />
        /// <summary>
        ///     Releases unmanaged and - optionally - managed resources.
        /// </summary>
        /// <param name="disposing">
        ///     <see langword="true" /> to release both managed and unmanaged resources; <see langword="false" /> to release only
        ///     unmanaged resources.
        /// </param>
        /// <exception cref="ObjectDisposedException">This <see cref="CancellationTokenSource" /> has been disposed.</exception>
        /// <exception cref="AggregateException">
        ///     An aggregate exception containing all the exceptions thrown by the registered
        ///     callbacks on the associated <see cref="CancellationToken" />.
        /// </exception>
        protected override void Dispose(bool disposing)
        {
            // Call base implementation, Always first.
            base.Dispose(disposing);

            if (!disposing)
            {
                return;
            }

            // Dispose related commands.
            this.SaveNounsHeaderDataCommand.Dispose();
            this.SearchNounsHeadersCommand.Dispose();
            this.LoadWordbooksForNounsCommand.Dispose();
            this.DeleteNounsDetailViewModelCommand.Dispose();
            this.TestSpeakingRateOfNounsDetailCommand.Dispose();

            // Remove event listeners and dispose the cognitive services instance.
            this.cognitiveServices.OnErrorOccurred -= this.OnConversionErrorOccurred;
            this.cognitiveServices.Dispose();

            // Dispose every view model of nouns detail.
            foreach (var nounsDetailViewModel in this.NounsDetailViewModels)
            {
                nounsDetailViewModel.Dispose();
            }

            // Remove event listeners.
            this.NounsDetailViewModels.CollectionChanged -= this.NounsDetailViewModelsChanged;
        }

        /// <inheritdoc />
        /// <summary>
        ///     Invoked if inserting the data model succeeded.
        /// </summary>
        /// <param name="sender">The control raise the event.</param>
        /// <param name="e">The <see cref="OnSaveChangesEventArgs" /> instance containing the event data.</param>
        /// <exception cref="Exception">A delegate callback throws an exception.</exception>
        protected override void OnInsertingModelSucceeded(object sender, OnSaveChangesEventArgs e)
        {
            // Call the base initialization.
            base.OnInsertingModelSucceeded(sender, e);

            // Prepare the user interface for new inputs.
            this.RollBackUserInputsCommand.Execute();
        }

        /// <inheritdoc />
        /// <summary>
        ///     Called when a property value changed.
        /// </summary>
        /// <param name="sender">The <paramref name="sender" />.</param>
        /// <param name="e">The <see cref="PropertyChangedEventArgs" /> instance containing the event data.</param>
        protected override void OnPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            // Displaying and hiding used nouns formers based on the user selections.
            switch (e.PropertyName)
            {
                case nameof(this.UseKnownPluralArabicForm):
                case nameof(this.UseKnownSingularArabicForm):
                case nameof(this.UseUnknownPluralArabicForm):
                    this.UseKnownPluralNorwegianForm =
                        this.UseKnownPluralArabicForm || this.UseKnownPluralNorwegianForm;
                    this.UseUnknownPluralNorwegianForm =
                        this.UseUnknownPluralArabicForm || this.UseUnknownPluralNorwegianForm;
                    this.UseKnownSingularNorwegianForm =
                        this.UseKnownSingularArabicForm || this.UseKnownSingularNorwegianForm;
                    break;
            }
        }

        /// <inheritdoc />
        /// <summary>
        ///     Invoked if updating the data model succeeded.
        /// </summary>
        /// <param name="sender">The control raise the event.</param>
        /// <param name="e">The <see cref="OnSaveChangesEventArgs" /> instance containing the event data.</param>
        /// <exception cref="Exception">A delegate callback throws an exception.</exception>
        protected override void OnUpdatingModelSucceeded(object sender, OnSaveChangesEventArgs e)
        {
            // Call the base initialization.
            base.OnUpdatingModelSucceeded(sender, e);

            // Prepare the user interface for new inputs.
            this.RollBackUserInputsCommand.Execute();
        }

        /// <inheritdoc />
        /// <summary>
        ///     Roll the user changes back and set every Model to its original values.
        /// </summary>
        /// <exception cref="AggregateException">
        ///     An aggregate exception containing all the exceptions thrown by the registered
        ///     callbacks on the associated <see cref="CancellationToken" />.
        /// </exception>
        /// <exception cref="ObjectDisposedException">This <see cref="CancellationTokenSource" /> has been disposed.</exception>
        /// <exception cref="Exception">A <see langword="delegate" /> callback throws an exception.</exception>
        protected override void RollBackDataChanges()
        {
            // Roll back any data changes.
            if (this.IsModelHasModified())
            {
                base.RollBackDataChanges();
            }

            // Dispose every NounsDetailViewModel.
            foreach (var nounsDetailViewModel in this.NounsDetailViewModels)
            {
                nounsDetailViewModel.Dispose();
            }

            // Clear the NounsDetailViewModels.
            this.NounsDetailViewModels.Clear();

            // Initialize a new instance of the corresponding Model with the current customized values.
            this.Model = new NounsHeader
                             {
                                 Wordbook = this.Model.Wordbook,
                                 CreationDateTime = DateTime.Now,
                                 LastUpdateDateTime = DateTime.Now,
                                 PronunciationTimes = this.Model.PronunciationTimes
                             };

            // Clear any existing validation errors.
            this.ClearValidationErrors();

            // Update the user interface depending on the new Model, and invoke property changed events.
            this.OnPropertyChanged(string.Empty);
        }

        /// <summary>
        ///     Attaches the nouns details to the current Model.
        /// </summary>
        /// <param name="nounsDetails">The nouns details.</param>
        private void AttachNounsDetails([NotNull] IEnumerable<NounsDetail> nounsDetails)
        {
            // Add the noun details to the current Model using the main thread.
            Application.Current.Dispatcher.Invoke(
                () =>
                    {
                        foreach (var nounsDetail in nounsDetails)
                        {
                            this.NounsDetailViewModels.Add(new NounsDetailViewModel(nounsDetail));
                        }
                    });
        }

        /// <summary>
        ///     Convert details of nouns to a sound stream and attach them to the current data Model asynchronously.
        /// </summary>
        /// <param name="progress">The action to report the <paramref name="progress" /> to the user.</param>
        /// <returns>
        ///     A <see cref="Task" /> represents the conversion operation.
        /// </returns>
        private async Task ConvertNounsDetailsToSoundStreamsAsync([NotNull] IProgress<int> progress)
        {
            // Clear existing NounsDetails.
            this.Model.NounsDetails.Clear();

            // Defines and reports the conversion progress percentage.
            var convertedNounIndex = 0;
            progress.Report(convertedNounIndex);

            // Convert nouns from string to sound stream.
            foreach (var nounsDetailViewModel in this.NounsDetailViewModels)
            {
                // Every noun will be checked to not convert it multiple times.
                await nounsDetailViewModel.ConvertInnerModelAsync(this.cognitiveServices).ContinueWith(
                    conversionTask =>
                        {
                            // Attach the converted noun to the NounsDetails collection.
                            this.Model.NounsDetails.Add(conversionTask.Result);

                            // Increases and reports the conversion progress percentage.
                            convertedNounIndex++;
                            progress.Report(convertedNounIndex);
                        },
                    this.CancellationToken,
                    TaskContinuationOptions.OnlyOnRanToCompletion,
                    TaskScheduler.Current).ConfigureAwait(false);
            }
        }

        /// <summary>
        ///     Deletes a view model of nouns detail asynchronous.
        /// </summary>
        /// <param name="nounsDetailViewModel">The nouns detail view model.</param>
        /// <returns>
        ///     A <see cref="Task" /> represents the deleting operation.
        /// </returns>
        private async Task DeleteNounsDetailViewModelAsync([NotNull] object nounsDetailViewModel)
        {
            // Cast the passed object to NounsDetailViewModel.
            var deletingNounsDetailViewModel = (NounsDetailViewModel)nounsDetailViewModel;

            // Check the value of required nouns.
            if (string.IsNullOrWhiteSpace(deletingNounsDetailViewModel.UnknownSingularArabicCharacterSet) ||
                string.IsNullOrWhiteSpace(deletingNounsDetailViewModel.UnknownSingularNorwegianCharacterSet))
            {
                return;
            }

            // Ask to user to confirm deleting the NounsDetailViewModel.
            if (!deletingNounsDetailViewModel.IsUserConfirmedDeletingModel())
            {
                return;
            }

            // Delete the inner Model from the data source.
            if (deletingNounsDetailViewModel.Model.Id != 0)
            {
                // Notify the user interface.
                this.IsPerformingLongRunningTask = true;

                // Update the NounsHeader and NounsDetails.
                this.Model.NounsCount -= 1;
                this.Model.LastUpdateDateTime = DateTime.Now;
                this.DataEntities.NounsDetails.Remove(deletingNounsDetailViewModel.Model);

                try
                {
                    // Save the changes on the data source.
                    await this.DataEntities.SaveChangesAsync(this.CancellationToken).ConfigureAwait(false);
                }
                finally
                {
                    // Notify the user interface.
                    this.IsPerformingLongRunningTask = false;
                }
            }

            // Remove the deleted NounsDetailViewModel from the collection and notify the user. 
            Application.Current.Dispatcher.Invoke(
                () =>
                    {
                        this.NounsDetailViewModels.Remove(deletingNounsDetailViewModel);
                        this.OnDeletingModelSucceeded(
                            this,
                            new OnSaveChangesEventArgs { Entity = deletingNounsDetailViewModel.Model });
                    });
        }

        /// <summary>
        ///     Checks the name of current nouns header against duplication.
        /// </summary>
        /// <returns>
        ///     A <see cref="Task{TResult}" /> represents the checking operation.
        /// </returns>
        private async Task<bool> IsDuplicatedNameAsync()
        {
            // Hold the checking results.
            var isDuplicatedName = false;

            // Check whether the name is duplicated or not.
            await this.DataEntities.NounsHeaders
                .Where(
                    nounsHeader => nounsHeader.Id != this.Model.Id && nounsHeader.Name == this.Model.Name.Trim()
                                                                   && nounsHeader.WordbookId == this.Model.WordbookId)
                .ToListAsync(this.CancellationToken).ContinueWith(
                    checkingDuplicationTask =>
                        {
                            // Hold the checking results.
                            isDuplicatedName = checkingDuplicationTask.Result.Any();

                            // Display or remove an error to notify the user.
                            if (isDuplicatedName)
                            {
                                this.SetError(
                                    string.Format(ValidationMessages.DuplicatedName, this.Model.Name),
                                    nameof(this.Model.Name));
                            }
                            else
                            {
                                this.RemoveErrors(nameof(this.Model.Name));
                            }
                        },
                    this.CancellationToken,
                    TaskContinuationOptions.OnlyOnRanToCompletion,
                    TaskScheduler.Current).ConfigureAwait(false);

            // Return the checking results.
            return isDuplicatedName;
        }

        /// <summary>
        ///     Loads the Wordbooks which can hold nouns from the data source asynchronously.
        /// </summary>
        /// <returns>
        ///     A <see cref="Task" /> represents the loading operation.
        /// </returns>
        private async Task LoadWordbooksForNounsAsync()
        {
            // Notify the user interface.
            this.LongRunningTaskDescription = NotificationsLayer.PreparingUserInterfaceMessage;
            this.IsPerformingLongRunningTask = true;

            try
            {
                await this.DataEntities.Wordbooks
                    .Where(wordbook => wordbook.Type == (byte)WordbookTypes.Noun && !wordbook.IsFolder)
                    .ToListAsync(this.CancellationToken).ContinueWith(
                        readingTask =>
                            {
                                this.NounsWordbooks = new ObservableCollection<Wordbook>(readingTask.Result);
                            },
                        this.CancellationToken,
                        TaskContinuationOptions.OnlyOnRanToCompletion,
                        TaskScheduler.Current).ConfigureAwait(false);
            }
            finally
            {
                this.IsPerformingLongRunningTask = false;
                this.LongRunningTaskDescription = string.Empty;
            }
        }

        /// <summary>
        ///     Occurs when the nouns detail view models collection changed.
        /// </summary>
        /// <param name="sender">The <paramref name="sender" />.</param>
        /// <param name="e">The <see cref="NotifyCollectionChangedEventArgs" /> instance containing the event data.</param>
        private void NounsDetailViewModelsChanged([NotNull] object sender, [NotNull] NotifyCollectionChangedEventArgs e)
        {
            switch (e.Action)
            {
                case NotifyCollectionChangedAction.Add:

                    // Escape the first NounsDetailViewModel.
                    if (e.NewStartingIndex > 0)
                    {
                        // Get the new NounsDetailViewModels only.
                        if (e.NewItems[0] is NounsDetailViewModel nounsDetailViewModel
                            && nounsDetailViewModel.Model.Id == 0)
                        {
                            // Repeat values of SpeakingRate and PronunciationTimes properties.
                            nounsDetailViewModel.SpeakingRate =
                                this.NounsDetailViewModels[e.NewStartingIndex - 1].SpeakingRate;
                            nounsDetailViewModel.PronunciationTimes = this.NounsDetailViewModels[e.NewStartingIndex - 1]
                                .PronunciationTimes;
                        }
                    }

                    break;
            }

            // Reorder serial number for nouns detail on all actions.
            byte firstSerial = 1;
            foreach (var nounsDetailViewModel in this.NounsDetailViewModels)
            {
                nounsDetailViewModel.SerialNumber = firstSerial++;
            }
        }

        /// <summary>
        ///     Called when a text to speech request has been failed and an error has been occurred.
        /// </summary>
        /// <param name="sender">The <paramref name="sender" />.</param>
        /// <param name="e">The <see cref="GenericEventArgs{Exception}" /> instance containing the event data.</param>
        private void OnConversionErrorOccurred([NotNull] object sender, [NotNull] GenericEventArgs<Exception> e)
        {
            this.DisplayDesktopAlert(
                NotificationsLayer.GeneralExceptionsHeader,
                string.Format(NotificationsLayer.GeneralExceptionsMessage, e.EventData.Message),
                AlertTypes.Error);
        }

        /// <summary>
        ///     Reports the <paramref name="progress" /> percentage for the conversion tasks.
        /// </summary>
        /// <param name="progress">The <paramref name="progress" /> percentage value.</param>
        private void ReportNounsConversionProgress(int progress)
        {
            // Display the index of the converted noun.
            this.LongRunningTaskDescription = string.Format(
                NotificationsLayer.StringToSoundConversionIsInProgressMessage,
                progress,
                this.NounsDetailViewModels.Count);
        }

        /// <summary>
        ///     Saves the data of the current <seealso cref="NounsHeader" /> asynchronously.
        /// </summary>
        /// <returns>
        ///     A <see cref="Task" /> represents the Insert or the Update operations.
        /// </returns>
        private async Task SaveNounsHeaderDataAsync()
        {
            // Notify the user interface.
            this.IsPerformingLongRunningTask = true;

            try
            {
                // Check and notify the user if the name is duplicated.
                if (!await this.IsDuplicatedNameAsync().ConfigureAwait(false))
                {
                    // Update the nouns count and last update date and time.
                    this.Model.LastUpdateDateTime = DateTime.Now;
                    this.Model.NounsCount = (short)this.NounsDetailViewModels.Count;

                    // Convert inner nouns from string to sound streams and attach them to the inner Model.
                    await this.ConvertNounsDetailsToSoundStreamsAsync(
                        new Progress<int>(this.ReportNounsConversionProgress)).ConfigureAwait(false);

                    // Inserts or updates the data Model asynchronously.
                    switch (this.Model.Id)
                    {
                        case 0:
                            this.Model.CreationDateTime = DateTime.Now;
                            await this.InsertAsync().ConfigureAwait(false);
                            break;

                        default:
                            await this.UpdateAsync().ConfigureAwait(false);
                            break;
                    }
                }
            }
            finally
            {
                // Notify the user interface.
                this.IsPerformingLongRunningTask = false;
                this.LongRunningTaskDescription = string.Empty;
            }
        }

        /// <summary>
        ///     Displays a dialog box to edit a nouns header from the search result.
        /// </summary>
        /// <param name="searchingWindowType">The type of searching window.</param>
        /// <returns>
        ///     A <see cref="Task" /> represents the selecting operation.
        /// </returns>
        private async Task SearchNounsHeadersAsync([NotNull] object searchingWindowType)
        {
            // Is the selected data model not the current data model.
            var userSelectedDifferentModel = false;

            // Create and display the searching dialog on the main thread.
            Application.Current.Dispatcher.Invoke(
                () =>
                    {
                        // Display the searching data dialog.
                        DialogService.ShowDialog((Type)searchingWindowType);

                        // Roll back current data if the user has selected a different data Model.
                        if (DialogService.SelectedModelId != 0)
                        {
                            if (!this.Model.Id.Equals(DialogService.SelectedModelId))
                            {
                                userSelectedDifferentModel = true;
                                this.RollBackUserInputsCommand.Execute();
                            }
                            else
                            {
                                this.DisplayDesktopAlert(
                                    NotificationsLayer.DataUpdating,
                                    string.Format(NotificationsLayer.CanEditDataModelFromInputFields, this.Model.Name),
                                    AlertTypes.Information);
                            }
                        }
                    });

            // If the selected data model not the current data model.
            if (userSelectedDifferentModel)
            {
                try
                {
                    // Notify the user interface.
                    this.IsPerformingLongRunningTask = true;

                    // Search for data using the selected Model identifier.
                    this.Model = await this.DataEntities.NounsHeaders.Include(nounsHeaders => nounsHeaders.NounsDetails)
                                     .SingleAsync(
                                         nounsHeader => nounsHeader.Id == DialogService.SelectedModelId,
                                         this.CancellationToken).ConfigureAwait(false);

                    // Fill the nouns details view model.
                    this.AttachNounsDetails(this.Model.NounsDetails);

                    // Handle the displaying of noun forms.
                    this.SetUsedArabicForms(this.Model.NounsDetails);
                    this.SetUsedNorwegianForms(this.Model.NounsDetails);
                }
                finally
                {
                    // Notify the user interface.
                    this.OnPropertyChanged(string.Empty);
                    this.IsPerformingLongRunningTask = false;
                }
            }
        }

        /// <summary>
        ///     Reflects the used Arabic forms to the user.
        /// </summary>
        /// <param name="nounsDetails">The nouns details.</param>
        private void SetUsedArabicForms([NotNull] IReadOnlyCollection<NounsDetail> nounsDetails)
        {
            this.UseKnownPluralArabicForm = nounsDetails.Any(
                nounsDetail => !string.IsNullOrWhiteSpace(nounsDetail.KnownPluralArabicCharacterSet));
            this.UseUnknownPluralArabicForm = nounsDetails.Any(
                nounsDetail => !string.IsNullOrWhiteSpace(nounsDetail.UnknownPluralArabicCharacterSet));
            this.UseKnownSingularArabicForm = nounsDetails.Any(
                nounsDetail => !string.IsNullOrWhiteSpace(nounsDetail.KnownSingularArabicCharacterSet));
        }

        /// <summary>
        ///     Reflects the used Norwegian forms to the user.
        /// </summary>
        /// <param name="nounsDetails">The nouns details.</param>
        private void SetUsedNorwegianForms([NotNull] IReadOnlyCollection<NounsDetail> nounsDetails)
        {
            this.UseKnownPluralNorwegianForm = nounsDetails.Any(
                nounsDetail => !string.IsNullOrWhiteSpace(nounsDetail.KnownPluralNorwegianCharacterSet));
            this.UseUnknownPluralNorwegianForm = nounsDetails.Any(
                nounsDetail => !string.IsNullOrWhiteSpace(nounsDetail.UnknownPluralNorwegianCharacterSet));
            this.UseKnownSingularNorwegianForm = nounsDetails.Any(
                nounsDetail => !string.IsNullOrWhiteSpace(nounsDetail.KnownSingularNorwegianCharacterSet));
        }

        /// <summary>
        ///     Tests the speaking rate for the passed view model of nouns detail asynchronously.
        /// </summary>
        /// <param name="nounsDetailViewModel">The nouns detail view model.</param>
        /// <returns>
        ///     A <see cref="Task" /> represents the testing operation.
        /// </returns>
        private async Task TestSpeakingRateAsync([NotNull] object nounsDetailViewModel)
        {
            try
            {
                // Notify the user interface.
                this.IsTestingSpeakingRateOfNounsDetail = true;

                // Cast the passed object to NounsDetailViewModel.
                var castedNounsDetailViewModel = (NounsDetailViewModel)nounsDetailViewModel;

                // Convert the inner Model to a sound streams if its not converted, then play the sound stream.
                await castedNounsDetailViewModel.TestSpeakingRateAsync(this.cognitiveServices).ConfigureAwait(false);
            }
            finally
            {
                // Notify the user interface.
                this.IsTestingSpeakingRateOfNounsDetail = false;
            }
        }
    }
}