﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="WordbookTypes.cs" company="None">
// Last Modified: 30/09/2018
// Author: Ahmed Ghanam.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace WordsBooklet.BusinessLayer.Models
{
    using WordsBooklet.BusinessLayer.Infrastructure;

    /// <summary>
    ///     Represents the different wordbooks’ types.
    /// </summary>
    public enum WordbookTypes : byte
    {
        /// <summary>
        ///     Represents the type 'Main'.
        /// </summary>
        [Parsing(Languages.Arabic, WordbookTypesEnumSentences.MeaningOfMainInArabic)]
        [Parsing(Languages.Bokmål, WordbookTypesEnumSentences.MeaningOfMainInNorwegian)]
        Main = 0,

        /// <summary>
        ///     Represents the type 'Verbs'.
        /// </summary>
        [Parsing(Languages.Arabic, WordbookTypesEnumSentences.MeaningOfVerbsInArabic)]
        [Parsing(Languages.Bokmål, WordbookTypesEnumSentences.MeaningOfVerbsInNorwegian)]
        Verb = 1,

        /// <summary>
        ///     Represents the type 'Nouns'.
        /// </summary>
        [Parsing(Languages.Arabic, WordbookTypesEnumSentences.MeaningOfSubstantivesInArabic)]
        [Parsing(Languages.Bokmål, WordbookTypesEnumSentences.MeaningOfSubstantivesInNorwegian)]
        Noun = 2,

        /// <summary>
        ///     Represents the type 'Unlike'.
        /// </summary>
        [Parsing(Languages.Arabic, WordbookTypesEnumSentences.MeaningOfUnlikeInArabic)]
        [Parsing(Languages.Bokmål, WordbookTypesEnumSentences.MeaningOfUnlikeInNorwegian)]
        Unlike = 3,

        /// <summary>
        ///     Represents the type 'Expressions'.
        /// </summary>
        [Parsing(Languages.Arabic, WordbookTypesEnumSentences.MeaningOfExpressionsInArabic)]
        [Parsing(Languages.Bokmål, WordbookTypesEnumSentences.MeaningOfExpressionsInNorwegian)]
        Expression = 4
    }
}