﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ExpressionsDetail.cs" company="None">
// Last Modified: 12/10/2018
// Author: Ahmed Ghanam.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace WordsBooklet.BusinessLayer.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Linq;

    using WordsBooklet.BusinessLayer.Infrastructure;

    /// <inheritdoc />
    /// <summary>
    ///     Defines the metadata type for the <see cref="ExpressionsDetail" />
    ///     which representing the data validation attributes on the desired
    ///     properties. It also has additional properties the view-model needs.
    /// </summary>
    /// <seealso cref="IEntity" />
    [MetadataType(typeof(ExpressionsDetailMetadata))]
    public partial class ExpressionsDetail : IEntity
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="ExpressionsDetail" /> class.
        /// </summary>
        public ExpressionsDetail()
        {
            this.SpeakingRate = "0.00";
            this.PronunciationTimes = 1;
        }

        /// <inheritdoc />
        /// <summary>
        ///     Gets a value indicating whether <see langword="this" /> instance is a hierarchically entity or not.
        /// </summary>
        /// <value>
        ///     <see langword="true" /> if <see langword="this" /> instance is a hierarchically entity; otherwise,
        ///     <see langword="false" />.
        /// </value>
        public bool IsHierarchicallyEntity => false;

        /// <inheritdoc />
        /// <summary>
        ///     Gets the entity name.
        /// </summary>
        /// <value>
        ///     The entity name.
        /// </value>
        public string Name => $"{this.ArabicCharacterSet} - {this.NorwegianCharacterSet}";

        /// <summary>
        ///     Gets or sets the number of times <see langword="this" /> instance has been uttered.
        /// </summary>
        /// <value>
        ///     The number of times <see langword="this" /> instance has been uttered.
        /// </value>
        internal int TimesNumberUttered { get; set; }

        /// <summary>
        ///     Gets the sound streams of inner expression in all forms.
        /// </summary>
        /// <returns>
        ///     A <see cref="List{T}" /> represents the sound streams.
        /// </returns>
        [NotNull]
        internal byte[] GetSoundStreams()
        {
            // Compress all sounds streams into a single list of bytes.
            var soundStreamList = new[]
                                      {
                                          this.ArabicPronunciation, this.NorwegianPronunciation,
                                          this.NorwegianExamplePronunciation
                                      };

            // Returns the compressed sound streams.
            return CombineByteArrays(soundStreamList);
        }

        /// <summary>
        ///     Combines bytes arrays to one instance.
        /// </summary>
        /// <param name="byteArrays">The <see langword="byte" /> arrays.</param>
        /// <returns>
        ///     An instance of a <see langword="byte" /> array.
        /// </returns>
        [NotNull]
        private static byte[] CombineByteArrays([NotNull] params byte[][] byteArrays)
        {
            var offset = 0;
            var combinedArray = new byte[byteArrays.Sum(byteArray => byteArray.Length)];
            foreach (var byteArray in byteArrays)
            {
                Buffer.BlockCopy(byteArray, 0, combinedArray, offset, byteArray.Length);
                offset += byteArray.Length;
            }

            return combinedArray;
        }
    }
}