﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ExpressionsDetailMetadata.cs" company="None">
// Last Modified: 12/10/2018
// Author: Ahmed Ghanam.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace WordsBooklet.BusinessLayer.Models
{
    using System.ComponentModel.DataAnnotations;

    using WordsBooklet.BusinessLayer.Models.Resources;

    /// <summary>
    ///     Represents validation attributes for the <see cref="ExpressionsDetail" /> data model.
    /// </summary>
    internal sealed class ExpressionsDetailMetadata
    {
        /// <summary>
        ///     Gets or sets the character set of expression using Arabic language.
        /// </summary>
        /// <value>
        ///     The character set of expression using Arabic language.
        /// </value>
        [CanBeNull]
        [Required(
            AllowEmptyStrings = false,
            ErrorMessageResourceType = typeof(ValidationMessages),
            ErrorMessageResourceName = "ExpressionCharacterSetIsMandatory")]
        [MaxLength(
            100,
            ErrorMessageResourceType = typeof(ValidationMessages),
            ErrorMessageResourceName = "CanNotExceed100Character")]
        [RegularExpression(
            @"^[\u0020\u002C\u0600-\u0605\u060C-\u061B\u0620-\u065F
                \u066B-\u06DC\u06DF-\u06E8\u06fA-\u06ff]+$",
            ErrorMessageResourceName = "OnlyArabicCharactersAreAllowed",
            ErrorMessageResourceType = typeof(ValidationMessages))]
        public string ArabicCharacterSet { get; set; }

        /// <summary>
        ///     Gets or sets the character set of expression using the Norwegian (Bokmål) language.
        /// </summary>
        /// <value>
        ///     The character set of expression using the Norwegian (Bokmål) language.
        /// </value>
        [CanBeNull]
        [Required(
            AllowEmptyStrings = false,
            ErrorMessageResourceType = typeof(ValidationMessages),
            ErrorMessageResourceName = "ExpressionCharacterSetIsMandatory")]
        [MaxLength(
            100,
            ErrorMessageResourceType = typeof(ValidationMessages),
            ErrorMessageResourceName = "CanNotExceed100Character")]
        [RegularExpression(
            @"^[\u0020\u007C\u00C0-\u00C2\u00C6\u00C8-\u00CA\u00D2-\u00D4\u00D8
                \u00E0-\u00E2\u00E5\u00E6\u00E8-\u00EA\u00F2-\u00F4\u00F8\-a-zA-Z]+$",
            ErrorMessageResourceType = typeof(ValidationMessages),
            ErrorMessageResourceName = "OnlyLatinCharactersAreAllowed")]
        public string NorwegianCharacterSet { get; set; }

        /// <summary>
        ///     Gets or sets the character set of the example using the Norwegian (Bokmål) language.
        /// </summary>
        /// <value>
        ///     The character set of the example using the Norwegian (Bokmål) language.
        /// </value>
        [CanBeNull]
        [MaxLength(
            150,
            ErrorMessageResourceType = typeof(ValidationMessages),
            ErrorMessageResourceName = "CanNotExceed150Character")]
        [RegularExpression(
            @"^[\u0020\u002C\u002E\u003A\u003B\u003F
                \u00C0-\u00C2\u00C6\u00C8-\u00CA\u00D2-\u00D4
                \u00D8\u00E0-\u00E2\u00E5\u00E6\u00E8-\u00EA
                \u00F2-\u00F4\u00F8\-a-zA-Z]+$",
            ErrorMessageResourceType = typeof(ValidationMessages),
            ErrorMessageResourceName = "OnlyLatinCharactersAreAllowed")]
        public string NorwegianExampleCharacterSet { get; set; }
    }
}