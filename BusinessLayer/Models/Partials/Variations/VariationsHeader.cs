﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="VariationsHeader.cs" company="None">
// Last Modified: 12/10/2018
// Author: Ahmed Ghanam.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace WordsBooklet.BusinessLayer.Models
{
    using System.ComponentModel.DataAnnotations;

    using WordsBooklet.BusinessLayer.Infrastructure;

    /// <inheritdoc />
    /// <summary>
    ///     Defines the metadata type for the <see cref="VariationsHeader" />
    ///     which representing the data validation attributes on the desired
    ///     properties. It also has additional properties the view-model needs.
    /// </summary>
    /// <seealso cref="IEntity" />
    [MetadataType(typeof(VariationsHeaderMetadata))]
    public partial class VariationsHeader : IEntity
    {
        /// <summary>
        ///     Gets the full name of the variations category combined with the current name.
        /// </summary>
        /// <value>
        ///     The full name of the variations category combined with the current name.
        /// </value>
        [CanBeNull]
        public string FullName => $"{this.Wordbook.FullName} » {this.Name}";

        /// <inheritdoc />
        /// <summary>
        ///     Gets a value indicating whether <see langword="this" /> instance is a hierarchically entity or not.
        /// </summary>
        /// <value>
        ///     <see langword="true" /> if <see langword="this" /> instance is a hierarchically entity; otherwise,
        ///     <see langword="false" />.
        /// </value>
        public bool IsHierarchicallyEntity => false;

        /// <summary>
        ///     Gets or sets the number of times <see langword="this" /> instance has been uttered.
        /// </summary>
        /// <value>
        ///     The number of times <see langword="this" /> instance has been uttered.
        /// </value>
        internal int TimesNumberUttered { get; set; }
    }
}