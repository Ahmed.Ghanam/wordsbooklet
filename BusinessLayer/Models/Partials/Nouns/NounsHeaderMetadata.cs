﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="NounsHeaderMetadata.cs" company="None">
// Last Modified: 01/10/2018
// Author: Ahmed Ghanam.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace WordsBooklet.BusinessLayer.Models
{
    using System.ComponentModel.DataAnnotations;

    using WordsBooklet.BusinessLayer.Models.Resources;

    /// <summary>
    ///     Represents validation attributes for the <see cref="NounsHeader" /> data model.
    /// </summary>
    internal sealed class NounsHeaderMetadata
    {
        /// <summary>
        ///     Gets or sets the nouns list header' name.
        /// </summary>
        /// <value>
        ///     The nouns list header' name.
        /// </value>
        [CanBeNull]
        [Required(
            AllowEmptyStrings = false,
            ErrorMessageResourceName = "NameIsMandatory",
            ErrorMessageResourceType = typeof(ValidationMessages))]
        [MaxLength(
            35,
            ErrorMessageResourceType = typeof(ValidationMessages),
            ErrorMessageResourceName = "CanNotExceed35Character")]
        public string Name { get; set; }

        /// <summary>
        ///     Gets or sets the nouns list header' remarks.
        /// </summary>
        /// <value>
        ///     The nouns list header' remarks.
        /// </value>
        [CanBeNull]
        [MaxLength(
            100,
            ErrorMessageResourceType = typeof(ValidationMessages),
            ErrorMessageResourceName = "CanNotExceed100Character")]
        public string Remarks { get; set; }

        /// <summary>
        ///     Gets or sets the main wordbook.
        /// </summary>
        /// <value>
        ///     The main wordbook.
        /// </value>
        [CanBeNull]
        [Required(
            ErrorMessageResourceType = typeof(ValidationMessages),
            ErrorMessageResourceName = "WordbookIsMandatory")]
        public Wordbook Wordbook { get; set; }
    }
}