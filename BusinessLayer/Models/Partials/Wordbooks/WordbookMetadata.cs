﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="WordbookMetadata.cs" company="None">
// Last Modified: 01/10/2018
// Author: Ahmed Ghanam.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace WordsBooklet.BusinessLayer.Models
{
    using System.ComponentModel.DataAnnotations;

    using WordsBooklet.BusinessLayer.Models.Resources;

    /// <summary>
    ///     Represents validation attributes for the <see cref="Wordbook" /> data model.
    /// </summary>
    internal sealed class WordbookMetadata
    {
        /// <summary>
        ///     Gets or sets the wordbook name.
        /// </summary>
        /// <value>
        ///     The wordbook name.
        /// </value>
        [CanBeNull]
        [Required(
            AllowEmptyStrings = false,
            ErrorMessageResourceName = "NameIsMandatory",
            ErrorMessageResourceType = typeof(ValidationMessages))]
        [MaxLength(
            35,
            ErrorMessageResourceType = typeof(ValidationMessages),
            ErrorMessageResourceName = "CanNotExceed35Character")]
        public string Name { get; set; }
    }
}