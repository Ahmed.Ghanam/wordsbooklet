﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Wordbook.cs" company="None">
// Last Modified: 02/10/2018
// Author: Ahmed Ghanam.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace WordsBooklet.BusinessLayer.Models
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.ComponentModel.DataAnnotations;
    using System.Linq;

    using WordsBooklet.BusinessLayer.Infrastructure;

    /// <inheritdoc />
    /// <summary>
    ///     Defines the metadata type for the <see cref="Wordbook" />
    ///     which representing the data validation attributes on the desired
    ///     properties. It also has additional properties the view-model needs.
    /// </summary>
    /// <seealso cref="IHierarchicallyEntity" />
    [MetadataType(typeof(WordbookMetadata))]
    public partial class Wordbook : IHierarchicallyEntity
    {
        /// <summary>
        ///     Gets the name of major wordbook combined with the current wordbook name.
        /// </summary>
        /// <value>
        ///     The name of major wordbook combined with the current wordbook name.
        /// </value>
        [CanBeNull]
        public string FullName
        {
            get
            {
                // Get the full name of the MajorWordbook.
                var fullName = this.MajorWordbook == null ? null : $"{this.MajorWordbook.FullName} » ";

                // Combine it with the current wordbook name.
                return $"{fullName}{this.Name}";
            }
        }

        /// <summary>
        ///     Gets a symbol describes whether the current wordbook is a folder or a file.
        /// </summary>
        /// <value>
        ///     The symbol describes whether the current wordbook is a folder or a file.
        /// </value>
        [NotNull]
        public string FolderStatusSymbol => this.IsFolder ? "√" : "ꭕ";

        /// <inheritdoc />
        /// <summary>
        ///     Gets a value indicating whether <see langword="this" /> instance is a hierarchically entity or not.
        /// </summary>
        /// <value>
        ///     <see langword="true" /> if <see langword="this" /> instance is a hierarchically entity; otherwise,
        ///     <see langword="false" />.
        /// </value>
        public bool IsHierarchicallyEntity => true;

        /// <summary>
        ///     Gets a string describes the current wordbook type.
        /// </summary>
        /// <value>
        ///     The string describes the current wordbook type.
        /// </value>
        [NotNull]
        public string ParsedWordbookType =>
            Convert.ToString(
                new ParsingAttributesConverter().Convert(
                    (WordbookTypes)this.Type,
                    typeof(WordbookTypes),
                    null,
                    CommonSettings.CultureInfo ?? throw new InvalidOperationException()));

        /// <summary>
        ///     Gets or sets the current wordbook code part in the full code chain.
        /// </summary>
        /// <value>
        ///     The current wordbook code part in the full code chain.
        /// </value>
        [NotNull]
        public string PureCodePart { get; set; }

        /// <summary>
        ///     Gets the count of the related words of the current wordbook.
        /// </summary>
        /// <value>
        ///     The count of the related words of the current wordbook.
        /// </value>
        /// <exception cref="OverflowException">
        ///     The sum is larger than <see cref="Int32.MaxValue" />.
        /// </exception>
        public long RelatedWordsCount
        {
            get
            {
                // Get the count of the related words in the current wordbook.
                long relatedWordsCount = this.NounsHeaders.Sum(nouns => nouns.NounsCount)
                                         + this.VariationsHeaders.Sum(variations => variations.VariationsCount);

                // Get the count of the related words from the sub wordbooks.
                this.GetRelatedWordsCount(this.SubWordbooks, ref relatedWordsCount);

                // Return the count of the related words.
                return relatedWordsCount;
            }
        }

        /// <inheritdoc />
        /// <summary>
        ///     Gets the sub entities related to the current data entity.
        /// </summary>
        /// <value>
        ///     The sub entities related to the current data entity.
        /// </value>
        public ObservableCollection<IHierarchicallyEntity> SubEntities =>
            new ObservableCollection<IHierarchicallyEntity>(this.SubWordbooks);

        /// <summary>
        ///     Gets the count of the sub wordbooks of the current wordbook.
        /// </summary>
        /// <value>
        ///     The count of the sub wordbooks of the current wordbook.
        /// </value>
        public long SubWordbooksCount
        {
            get
            {
                // Back field holding count value.
                long subWordbookCount = 0;

                // Get the count of the sub wordbooks.
                this.SubEntitiesCount(this.SubEntities, ref subWordbookCount);

                // Return the sub wordbooks count.
                return subWordbookCount;
            }
        }

        /// <summary>
        ///     Gets the sub wordbooks whose can contain other sub wordbooks of the current wordbook.
        /// </summary>
        /// <value>
        ///     The sub wordbooks whose can contain other sub wordbooks of the current wordbook.
        /// </value>
        [NotNull]
        public IEnumerable<Wordbook> SubParentWordbooks
        {
            get
            {
                return this.SubWordbooks.Where(wordbook => wordbook.IsFolder && wordbook.LevelNumber <= 9);
            }
        }

        /// <summary>
        ///     Gets the count of the related words.
        /// </summary>
        /// <param name="subWordbooks">The sub wordbooks.</param>
        /// <param name="relatedWordsCount">The current count of the related words.</param>
        private void GetRelatedWordsCount([NotNull] IEnumerable<Wordbook> subWordbooks, ref long relatedWordsCount)
        {
            foreach (var subWordbook in subWordbooks)
            {
                // Increment the count.
                relatedWordsCount += subWordbook.NounsHeaders.Sum(nouns => nouns.NounsCount)
                                     + this.VariationsHeaders.Sum(variations => variations.VariationsCount);

                // Loop to catch the last child recursively.
                if (subWordbook.SubEntities.Any())
                {
                    this.GetRelatedWordsCount(subWordbook.SubWordbooks, ref relatedWordsCount);
                }
            }
        }

        /// <summary>
        ///     Gets the count of the sub entities.
        /// </summary>
        /// <param name="subEntities">The hierarchically sub entities.</param>
        /// <param name="subEntitiesCount">The current count of sub entities.</param>
        private void SubEntitiesCount(
            [NotNull] IEnumerable<IHierarchicallyEntity> subEntities,
            ref long subEntitiesCount)
        {
            foreach (var subEntity in subEntities)
            {
                // Increment the count.
                subEntitiesCount++;

                // Loop to catch the last child recursively.
                if (subEntity.SubEntities.Any())
                {
                    this.SubEntitiesCount(subEntity.SubEntities, ref subEntitiesCount);
                }
            }
        }
    }
}