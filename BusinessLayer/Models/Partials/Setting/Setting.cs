﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Setting.cs" company="None">
// Last Modified: 09/10/2018
// Author: Ahmed Ghanam.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace WordsBooklet.BusinessLayer.Models
{
    using System.ComponentModel.DataAnnotations;

    using WordsBooklet.BusinessLayer.Infrastructure;

    /// <inheritdoc />
    /// <summary>
    ///     Defines the metadata type for the <see cref="Setting" />
    ///     which representing the data validation attributes on the desired
    ///     properties. It also has additional properties the view-model needs.
    /// </summary>
    /// <seealso cref="IEntity" />
    [MetadataType(typeof(SettingMetadata))]
    public partial class Setting : IEntity
    {
        /// <inheritdoc />
        /// <summary>
        ///     Gets a value indicating whether <see langword="this" /> instance is a hierarchically entity or not.
        /// </summary>
        /// <value>
        ///     <see langword="true" /> if <see langword="this" /> instance is a hierarchically entity; otherwise,
        ///     <see langword="false" />.
        /// </value>
        public bool IsHierarchicallyEntity => false;
    }
}