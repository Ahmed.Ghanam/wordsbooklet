﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SettingMetadata.cs" company="None">
// Last Modified: 07/10/2018
// Author: Ahmed Ghanam.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace WordsBooklet.BusinessLayer.Models
{
    using System.ComponentModel.DataAnnotations;

    using WordsBooklet.BusinessLayer.Models.Resources;

    /// <summary>
    ///     Represents validation attributes for the <see cref="Setting" /> data model.
    /// </summary>
    internal sealed class SettingMetadata
    {
        /// <summary>
        ///     Gets or sets the saved value of the setting key name.
        /// </summary>
        /// <value>
        ///     The saved value of the setting key name.
        /// </value>
        [CanBeNull]
        [Required(
            AllowEmptyStrings = false,
            ErrorMessageResourceName = "ValueIsMandatory",
            ErrorMessageResourceType = typeof(ValidationMessages))]
        [MaxLength(
            75,
            ErrorMessageResourceType = typeof(ValidationMessages),
            ErrorMessageResourceName = "CanNotExceed75Character")]
        public string SavedValue { get; set; }
    }
}