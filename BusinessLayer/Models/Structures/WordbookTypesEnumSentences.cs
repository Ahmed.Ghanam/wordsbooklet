﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="WordbookTypesEnumSentences.cs" company="None">
// Last Modified: 30/09/2018
// Author: Ahmed Ghanam.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace WordsBooklet.BusinessLayer.Models
{
    /// <summary>
    ///     Represents <see langword="static" /> sentences describing fields of
    ///     <see cref="WordbookTypes" /> <see langword="enum" />.
    /// </summary>
    internal struct WordbookTypesEnumSentences
    {
        /* Main type Section */

        /// <summary>
        ///     Represents the meaning of main in the Arabic language.
        /// </summary>
        internal const string MeaningOfMainInArabic = "رئيسي";

        /// <summary>
        ///     Represents the meaning of main in the Norwegian (Bokmål) language.
        /// </summary>
        internal const string MeaningOfMainInNorwegian = "Sentral";

        /* Verbs type Section */

        /// <summary>
        ///     Represents the meaning of verbs in the Arabic language.
        /// </summary>
        internal const string MeaningOfVerbsInArabic = "أفعال";

        /// <summary>
        ///     Represents the meaning of verbs in the Norwegian (Bokmål) language.
        /// </summary>
        internal const string MeaningOfVerbsInNorwegian = "Verb";

        /* Substantives type Section */

        /// <summary>
        ///     Represents the meaning of substantives in the Arabic language.
        /// </summary>
        internal const string MeaningOfSubstantivesInArabic = "أسماء";

        /// <summary>
        ///     Represents the meaning of substantives in the Norwegian (Bokmål) language.
        /// </summary>
        internal const string MeaningOfSubstantivesInNorwegian = "Substantiv";

        /* Substantives type Section */

        /// <summary>
        ///     Represents the meaning of unlike in the Arabic language.
        /// </summary>
        internal const string MeaningOfUnlikeInArabic = "مختلف";

        /// <summary>
        ///     Represents the meaning of unlike in the Norwegian (Bokmål) language.
        /// </summary>
        internal const string MeaningOfUnlikeInNorwegian = "Annerledes";

        /* Expressions type Section */

        /// <summary>
        ///     Represents the meaning of expressions in the Arabic language.
        /// </summary>
        internal const string MeaningOfExpressionsInArabic = "تعبيرات";

        /// <summary>
        ///     Represents the meaning of expressions in the Norwegian (Bokmål) language.
        /// </summary>
        internal const string MeaningOfExpressionsInNorwegian = "Uttrykk";
    }
}