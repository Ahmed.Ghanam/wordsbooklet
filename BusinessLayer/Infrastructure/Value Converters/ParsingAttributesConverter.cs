// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ParsingAttributesConverter.cs" company="None">
// Last Modified: 21/11/2018
// Author: Ahmed Ghanam.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace WordsBooklet.BusinessLayer.Infrastructure
{
    using System;
    using System.Globalization;
    using System.Linq;
    using System.Reflection;
    using System.Windows;
    using System.Windows.Data;

    /// <inheritdoc />
    /// <summary>
    ///     Represents value converter for tracking <seealso cref="ParsingAttribute" />.
    /// </summary>
    /// <seealso cref="IValueConverter" />
    public class ParsingAttributesConverter : IValueConverter
    {
        /// <summary>
        ///     Gets or sets a value indicating whether <see langword="this" />
        ///     instance should handle default expressions only or not.
        /// </summary>
        /// <value>
        ///     <see langword="true" /> if <see langword="this" />
        ///     instance should handle default expressions only; otherwise, <see langword="false" />.
        /// </value>
        public bool DefaultsOnly { get; set; }

        /// <inheritdoc />
        /// <summary>
        ///     Converts <see langword="enum" /> member name to an expression.
        /// </summary>
        /// <param name="value">The member value produced by the binding source.</param>
        /// <param name="targetType">The <see langword="enum" /> type of the binding target property.</param>
        /// <param name="parameter">The converter parameter to use.</param>
        /// <param name="culture">The culture to use in the converter.</param>
        /// <returns>
        ///     If the method returns <see langword="null" />, the <see cref="DependencyProperty.UnsetValue" /> value is used.
        /// </returns>
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return value == null ? DependencyProperty.UnsetValue : this.GetExpression(value);
        }

        /// <inheritdoc />
        /// <summary>
        ///     Converts an expression to an <see langword="enum" /> member name.
        /// </summary>
        /// <param name="value">The expression that is produced by the binding target.</param>
        /// <param name="targetType">The <see langword="enum" /> type to convert to.</param>
        /// <param name="parameter">The converter parameter to use.</param>
        /// <param name="culture">The culture to use in the converter.</param>
        /// <returns>
        ///     If the method returns <see langword="null" />, the <see cref="DependencyProperty.UnsetValue" /> value is used.
        /// </returns>
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return value == null
                       ? DependencyProperty.UnsetValue
                       : GetEnumMemberName(System.Convert.ToString(value), targetType);
        }

        /// <summary>
        ///     Gets member name based on associated <paramref name="expression" />.
        /// </summary>
        /// <param name="expression">The associated expression</param>
        /// <param name="enumType">The <see langword="enum" /> type</param>
        /// <returns>
        ///     An <see langword="enum" /> member name.
        /// </returns>
        [NotNull]
        private static string GetEnumMemberName([NotNull] string expression, [NotNull] Type enumType)
        {
            // Get every member holding ParsingAttributes.
            var enumMembers = enumType.GetMembers()
                .Where(member => member.GetCustomAttributes(typeof(ParsingAttribute)).Any());

            // Loop to catch the matched member name.
            return enumMembers.First(enumMember => IsExpressionsMatched(expression, enumMember)).Name;
        }

        /// <summary>
        ///     Compares and checks equality between two expressions.
        /// </summary>
        /// <param name="expression">The source <paramref name="expression" /></param>
        /// <param name="memberInfo">The member information holding the second expression</param>
        /// <returns>
        ///     <see langword="true" /> if both expressions are equal; otherwise <see langword="false" />
        /// </returns>
        private static bool IsExpressionsMatched([NotNull] string expression, [NotNull] MemberInfo memberInfo)
        {
            return memberInfo.GetCustomAttributes<ParsingAttribute>()
                .Any(parsingAttribute => parsingAttribute.Expression == expression);
        }

        /// <summary>
        ///     Gets the associated expressions from <see langword="enum" /> member name.
        /// </summary>
        /// <param name="memberName">The member name</param>
        /// <returns>
        ///     The associated expression.
        /// </returns>
        [NotNull]
        private object GetExpression([NotNull] object memberName)
        {
            // Get the MemberInfos using the member name.
            var memberInfos = memberName.GetType().GetMember(System.Convert.ToString(memberName));

            // Get the ParsingAttribute for the first member only.
            var parsingAttribute = memberInfos.First().GetCustomAttributes<ParsingAttribute>();

            // Return the desired expression.
            return this.DefaultsOnly
                       ? parsingAttribute.First(attribute => attribute.IsDefault).Expression
                       : parsingAttribute.First(attribute => attribute.Language == CommonSettings.SystemLanguage).Expression;
        }
    }
}