﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Gender.cs" company="None">
// Last Modified: 30/09/2018
// Author: Ahmed Ghanam.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace WordsBooklet.BusinessLayer.Infrastructure
{
    /// <summary>
    ///     Represents two gender.
    /// </summary>
    internal enum Gender
    {
        /// <summary>
        ///     Represents the male gender.
        /// </summary>
        Male,

        /// <summary>
        ///     Represents the female gender.
        /// </summary>
        Female
    }
}