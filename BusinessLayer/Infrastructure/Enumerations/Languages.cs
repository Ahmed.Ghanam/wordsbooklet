﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Languages.cs" company="None">
// Last Modified: 30/09/2018
// Author: Ahmed Ghanam.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace WordsBooklet.BusinessLayer.Infrastructure
{
    /// <summary>
    ///     Represents the supported languages names.
    /// </summary>
    public enum Languages
    {
        /// <summary>
        ///     Represents the Arabic language name.
        /// </summary>
        [Parsing(true, Arabic, LanguagesEnumSentences.MeaningOfArabicLanguageInArabic)]
        [Parsing(false, Bokmål, LanguagesEnumSentences.MeaningOfArabicLanguageInNorwegian)]
        Arabic,

        /// <summary>
        ///     Represents the Norwegian (Bokmål) language name.
        /// </summary>
        [Parsing(false, Arabic, LanguagesEnumSentences.MeaningOfNorwegianBokmålLanguageInArabic)]
        [Parsing(true, Bokmål, LanguagesEnumSentences.MeaningOfNorwegianBokmålLanguageInNorwegian)]
        Bokmål
    }
}