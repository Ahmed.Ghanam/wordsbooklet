﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SettingsCodes.cs" company="None">
// Last Modified: 30/09/2018
// Author: Ahmed Ghanam.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace WordsBooklet.BusinessLayer.Infrastructure
{
    /// <summary>
    ///     Represents codes for identifying the settings records.
    /// </summary>
    internal enum SettingsCodes
    {
        /// <summary>
        ///     Represents the setting code for the authentication key of cognitive services.
        /// </summary>
        CognitiveServicesKey = 1,

        /// <summary>
        ///     Represents the setting code for the language.
        /// </summary>
        SystemLanguage = 2
    }
}