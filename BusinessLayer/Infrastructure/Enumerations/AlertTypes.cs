﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AlertTypes.cs" company="None">
// Last Modified: 30/09/2018
// Author: Ahmed Ghanam.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace WordsBooklet.BusinessLayer.Infrastructure
{
    /// <summary>
    ///     Represents the desktop notifications types.
    /// </summary>
    internal enum AlertTypes
    {
        /// <summary>
        ///     Represents an error notification type.
        /// </summary>
        Error,

        /// <summary>
        ///     Represents a success notification type.
        /// </summary>
        Success,

        /// <summary>
        ///     Represents an information notification type.
        /// </summary>
        Information
    }
}