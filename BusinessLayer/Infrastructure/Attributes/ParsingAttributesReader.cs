// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ParsingAttributesReader.cs" company="None">
// Last Modified: 20/11/2018
// Author: Ahmed Ghanam.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace WordsBooklet.BusinessLayer.Infrastructure
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Windows.Data;

    /// <summary>
    ///     Reads expressions associated to <see langword="enum" /> members.
    /// </summary>
    public sealed class ParsingAttributesReader
    {
        /// <summary>
        ///     Gets or sets the <see langword="enum" /> type holding parsing attributes.
        /// </summary>
        /// <value>
        ///     The <see langword="enum" /> type holding parsing attributes.
        /// </value>
        [CanBeNull]
        public Type EnumType { get; set; }

        /// <summary>
        ///     Gets the associated expressions to <see langword="enum" /> members.
        /// </summary>
        /// <value>
        ///     The associated expressions to <see langword="enum" /> members.
        /// </value>
        [NotNull]
        public IEnumerable<object> Expressions =>
            this.EnumType == null ? throw new InvalidOperationException() :
            this.ValueConverter == null ? throw new InvalidOperationException() :
            GetEnumMembersValue(this.EnumType).Select(
                memberValue => this.ValueConverter.Convert(
                    memberValue,
                    this.EnumType,
                    null,
                    CommonSettings.CultureInfo));

        /// <summary>
        ///     Gets or sets the value converter for reading the expressions.
        /// </summary>
        /// <value>
        ///     The value converter for reading the expressions.
        /// </value>
        [CanBeNull]
        public IValueConverter ValueConverter { get; set; }

        /// <summary>
        ///     Gets the enumeration members value.
        /// </summary>
        /// <param name="enumerationType">The enumeration type.</param>
        /// <returns>
        ///     An <see cref="IEnumerable{T}" /> represents the enumeration members value.
        /// </returns>
        [NotNull]
        private static IEnumerable<object> GetEnumMembersValue([NotNull] IReflect enumerationType)
        {
            return enumerationType.GetFields(BindingFlags.Public | BindingFlags.Static)
                .Select(enumMember => enumMember.GetValue(null));
        }
    }
}