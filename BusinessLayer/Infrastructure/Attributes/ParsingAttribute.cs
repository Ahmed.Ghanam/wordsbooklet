﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ParsingAttribute.cs" company="None">
// Last Modified: 21/11/2018
// Author: Ahmed Ghanam.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace WordsBooklet.BusinessLayer.Infrastructure
{
    using System;

    /// <inheritdoc />
    /// <summary>
    ///     Represents an expression describes <see langword="enum" /> members using a specific language.
    /// </summary>
    /// <seealso cref="Attribute" />
    [AttributeUsage(AttributeTargets.Field, AllowMultiple = true)]
    internal class ParsingAttribute : Attribute
    {
        /// <inheritdoc />
        /// <summary>
        ///     Initializes a new instance of the <see cref="ParsingAttribute" /> class.
        /// </summary>
        /// <param name="language">The expression language.</param>
        /// <param name="expression">The expression.</param>
        internal ParsingAttribute(Languages language, [NotNull] string expression)
            : this(false, language, expression)
        {
        }

        /// <inheritdoc />
        /// <summary>
        ///     Initializes a new instance of the <see cref="ParsingAttribute" /> class.
        /// </summary>
        /// <param name="isDefault">
        ///     if set to <see langword="true" /> represents the default expression.
        /// </param>
        /// <param name="language">The expression language.</param>
        /// <param name="expression">The expression.</param>
        internal ParsingAttribute(bool isDefault, Languages language, [NotNull] string expression)
        {
            this.Language = language;
            this.IsDefault = isDefault;
            this.Expression = expression;
        }

        /// <summary>
        ///     Gets an expression describes the <see langword="enum" /> member using a specific language.
        /// </summary>
        /// <value>
        ///     The expression describes the <see langword="enum" /> member using a specific language.
        /// </value>
        [NotNull]
        internal string Expression { get; }

        /// <summary>
        ///     Gets a value indicating whether <see langword="this" /> instance is the default expression or not.
        /// </summary>
        /// <value>
        ///     <see langword="true" /> if <see langword="this" /> instance is the default expression; otherwise,
        ///     <see langword="false" />.
        /// </value>
        internal bool IsDefault { get; }

        /// <summary>
        ///     Gets the expression language.
        /// </summary>
        /// <value>
        ///     The expression language.
        /// </value>
        internal Languages Language { get; }
    }
}