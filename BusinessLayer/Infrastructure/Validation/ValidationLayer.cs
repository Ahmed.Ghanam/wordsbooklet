﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ValidationLayer.cs" company="None">
// Last Modified: 30/09/2018
// Author: Ahmed Ghanam.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace WordsBooklet.BusinessLayer.Infrastructure
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.Linq;
    using System.Reflection;

    /// <inheritdoc />
    /// <summary>
    ///     Represents a basic input validation using data annotations attributes.
    /// </summary>
    /// <typeparam name="T">
    ///     The model which refer to validation attributes.
    /// </typeparam>
    /// <seealso cref="INotifyDataErrorInfo" />
    public abstract class ValidationLayer<T> : INotifyDataErrorInfo
        where T : class, IEntity, new()
    {
        /// <inheritdoc />
        /// <summary>
        ///     Occurs if errors collection changed.
        /// </summary>
        public event EventHandler<DataErrorsChangedEventArgs> ErrorsChanged;

        /// <inheritdoc />
        /// <summary>
        ///     Gets a value indicating whether the corresponding model has any validation errors or not.
        /// </summary>
        public bool HasErrors => this.ErrorsDictionary.Any();

        /// <summary>
        ///     Gets or sets the corresponding <see cref="T" /> Model.
        /// </summary>
        /// <value>
        ///     The corresponding <see cref="T" /> Model.
        /// </value>
        [NotNull]
        protected internal T Model { get; set; } = (T)Activator.CreateInstance(typeof(T));

        /// <summary>
        ///     Gets the validation errors dictionary.
        /// </summary>
        /// <value>
        ///     The validation errors dictionary.
        /// </value>
        [NotNull]
        private Dictionary<string, List<string>> ErrorsDictionary { get; } = new Dictionary<string, List<string>>();

        /// <inheritdoc />
        /// <summary>
        ///     Gets the validation errors for a specified property or for the entire entity.
        /// </summary>
        /// <param name="propertyName">
        ///     The property name which to retrieve validation errors for. pass <see langword="null" /> or
        ///     <see cref="String.Empty" />, to retrieve entity-level errors.
        /// </param>
        /// <returns>
        ///     The validation errors collection.
        /// </returns>
        public IEnumerable GetErrors(string propertyName)
        {
            return (string.IsNullOrEmpty(propertyName) ? (IEnumerable)this.ErrorsDictionary :
                    this.ErrorsDictionary.ContainsKey(propertyName) ? this.ErrorsDictionary[propertyName] : null)
                   ?? new List<string>();
        }

        /// <summary>
        ///     Clears all the validation errors.
        /// </summary>
        /// <exception cref="Exception">A <see langword="delegate" /> callback throws an exception.</exception>
        protected void ClearValidationErrors()
        {
            // If no errors, just exit.
            if (!this.HasErrors)
            {
                return;
            }

            // Back up existing errors.
            var errorsListBackup = this.ErrorsDictionary.ToList();

            // Clear existing errors.
            this.ErrorsDictionary.Clear();

            // Invoke errors changed event on every property.
            foreach (var error in errorsListBackup)
            {
                this.ErrorsChanged?.Invoke(this, new DataErrorsChangedEventArgs(error.Key));
            }

            // Clear the backup collection.
            errorsListBackup.Clear();
        }

        /// <summary>
        ///     Removes errors message away of a model property.
        /// </summary>
        /// <param name="propertyName">The property name.</param>
        /// <exception cref="Exception">A <see langword="delegate" /> callback throws an exception.</exception>
        protected void RemoveErrors([NotNull] string propertyName)
        {
            // Return if the property has no error messages.
            if (!this.ErrorsDictionary.ContainsKey(propertyName))
            {
                return;
            }

            // Remove the error message.
            this.ErrorsDictionary.Remove(propertyName);

            // Invoke ErrorsChanged event.
            this.ErrorsChanged?.Invoke(this, new DataErrorsChangedEventArgs(propertyName));
        }

        /// <summary>
        ///     Sets an error message on a model property.
        /// </summary>
        /// <param name="errorMessage">The error message.</param>
        /// <param name="propertyName">The property name.</param>
        /// <exception cref="Exception">A <see langword="delegate" /> callback throws an exception.</exception>
        protected void SetError([NotNull] string errorMessage, [NotNull] string propertyName)
        {
            if (this.ErrorsDictionary.ContainsKey(propertyName))
            {
                // Clear previous errors.
                this.ErrorsDictionary[propertyName].Clear();

                // Assign the passed error message.
                this.ErrorsDictionary[propertyName].Add(errorMessage);
            }
            else
            {
                // Add the passed error message.
                this.ErrorsDictionary.Add(propertyName, new List<string> { errorMessage });
            }

            // Invoke ErrorsChanged event.
            this.ErrorsChanged?.Invoke(this, new DataErrorsChangedEventArgs(propertyName));
        }

        /// <summary>
        ///     Validate the passed property value using the associated validation attributes.
        /// </summary>
        /// <param name="propertyName">The property name.</param>
        /// <param name="propertyValue">The property value.</param>
        /// <exception cref="ArgumentNullException">
        ///     If the <see cref="Model" /> doesn't implement any metadata type.
        ///     or the
        ///     <param name="propertyName" />
        ///     can not be found in the metadata type.
        /// </exception>
        protected void ValidatePropertyValue([NotNull] string propertyName, [CanBeNull] object propertyValue)
        {
            // Get the associated meta data types.
            var modelMetadataType = GetMetadataType(typeof(T));
            if (modelMetadataType == null)
            {
                throw new ArgumentNullException($"The {typeof(T)} hasn't any defined metadata type");
            }

            // Get the same property from the meta data type.
            var matchedProperty = modelMetadataType.GetProperties()
                .FirstOrDefault(property => property.Name.Equals(propertyName));
            if (matchedProperty == null)
            {
                throw new ArgumentNullException(
                    $"The {propertyName} is not found in the {modelMetadataType.FullName}.");
            }

            // Remove any previous errors on the same property.
            if (this.ErrorsDictionary.ContainsKey(propertyName))
            {
                this.ErrorsDictionary.Remove(propertyName);
            }

            // Launch the associated validation attributes.
            this.LaunchValidationAttributes(propertyName, propertyValue, matchedProperty);
        }

        /// <summary>
        ///     Gets the meta data for the passed type.
        /// </summary>
        /// <param name="targetType">The type to be validate.</param>
        /// <returns>
        ///     <see cref="Type" /> or <see langword="null" /> if no associated metadata types.
        /// </returns>
        [CanBeNull]
        private static Type GetMetadataType([NotNull] Type targetType)
        {
            return targetType.GetCustomAttributes(typeof(MetadataTypeAttribute), false).OfType<MetadataTypeAttribute>()
                .Select(metadataType => metadataType.MetadataClassType).FirstOrDefault();
        }

        /// <summary>
        ///     Launches the validation attributes for specified property.
        /// </summary>
        /// <param name="propertyName">The model property name.</param>
        /// <param name="propertyValue">The model property value.</param>
        /// <param name="propertyInfo">The metadata type property information.</param>
        private void LaunchValidationAttributes(
            [NotNull] string propertyName,
            [CanBeNull] object propertyValue,
            [NotNull] PropertyInfo propertyInfo)
        {
            // Get the results for the validation attributes.
            var errorsList =
                (from validationAttribute in propertyInfo.GetCustomAttributes(false).OfType<ValidationAttribute>()
                     .ToList()
                 where !validationAttribute.IsValid(propertyValue)
                 select validationAttribute.FormatErrorMessage(propertyName)).ToList();

            // Return if no errors.
            if (!errorsList.Any())
            {
                return;
            }

            // Insert the new error(s) into the errors collection.
            this.ErrorsDictionary.Add(propertyName, errorsList);
        }
    }
}