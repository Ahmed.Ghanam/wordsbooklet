﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AsyncCommand.cs" company="None">
// Last Modified: 28/09/2018
// Author: Ahmed Ghanam.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace WordsBooklet.BusinessLayer.Infrastructure
{
    using System;
    using System.Threading.Tasks;
    using System.Windows.Input;

    using WordsBooklet.BusinessLayer.Infrastructure.Notifications.Resources;

    /// <inheritdoc cref="ICommand" />
    /// <summary>
    ///     Represents a generic command to execute a <see cref="Task" /> in an asynchronously manner.
    /// </summary>
    /// <seealso cref="ICommand" />
    /// <seealso cref="IDisposable" />
    public sealed class AsyncCommand : ICommand, IDisposable
    {
        /// <summary>
        ///     The desktop alert service.
        /// </summary>
        [NotNull]
        private readonly DesktopAlertService desktopAlertService = new DesktopAlertService();

        /// <summary>
        ///     The action determines whether can execute the corresponding <see cref="Task" /> or not.
        /// </summary>
        [CanBeNull]
        private Predicate<object> canExecuteCorrespondingTask;

        /// <summary>
        ///     Flag: Has the <c>Dispose()</c> been called ?.
        /// </summary>
        private bool disposed;

        /// <summary>
        ///     A flag : determines whether the corresponding <see cref="Task" /> is executing in present or not.
        /// </summary>
        private bool isExecutingCorrespondingTask;

        /// <summary>
        ///     The <see cref="Task" /> to execute.
        /// </summary>
        [CanBeNull]
        private Func<Task> taskToExecute;

        /// <summary>
        ///     The <see cref="Task" /> to execute which accepts an <see cref="object" /> argument.
        /// </summary>
        [CanBeNull]
        private Func<object, Task> taskWithParameterToExecute;

        /// <inheritdoc />
        /// <summary>
        ///     Initializes a new instance of the <see cref="AsyncCommand" /> class.
        /// </summary>
        /// <param name="execute">The <see cref="Task" /> to execute.</param>
        public AsyncCommand([NotNull] Func<Task> execute)
            : this(null, execute)
        {
        }

        /// <inheritdoc />
        /// <summary>
        ///     Initializes a new instance of the <see cref="AsyncCommand" /> class.
        /// </summary>
        /// <param name="execute">
        ///     The <see cref="Task" /> to execute which accepts an <see cref="object" /> argument.
        /// </param>
        public AsyncCommand([NotNull] Func<object, Task> execute)
            : this(null, execute)
        {
        }

        /// <inheritdoc />
        /// <summary>
        ///     Initializes a new instance of the <see cref="AsyncCommand" /> class.
        /// </summary>
        /// <param name="canExecute">Determines whether can execute the corresponding <see cref="Task" /> or not.</param>
        /// <param name="execute">The <see cref="Task" /> to execute.</param>
        public AsyncCommand([CanBeNull] Predicate<object> canExecute, [NotNull] Func<Task> execute)
        {
            this.taskToExecute = execute;
            this.canExecuteCorrespondingTask = canExecute;
        }

        /// <inheritdoc />
        /// <summary>
        ///     Initializes a new instance of the <see cref="AsyncCommand" /> class.
        /// </summary>
        /// <param name="canExecute">Determines whether can execute the corresponding <see cref="Task" /> or not.</param>
        /// <param name="execute">
        ///     The <see cref="Task" /> to execute which accepts an <see cref="object" /> argument.
        /// </param>
        public AsyncCommand([CanBeNull] Predicate<object> canExecute, [NotNull] Func<object, Task> execute)
        {
            this.taskWithParameterToExecute = execute;
            this.canExecuteCorrespondingTask = canExecute;
        }

        /// <summary>
        ///     Finalizes an instance of the <see cref="AsyncCommand" /> class.
        /// </summary>
        ~AsyncCommand()
        {
            this.Dispose(false);
        }

        /// <inheritdoc />
        /// <summary>
        ///     Occurs when changes occur that affect whether or not the command couldn't execute.
        /// </summary>
        public event EventHandler CanExecuteChanged
        {
            add => CommandManager.RequerySuggested += value;

            remove => CommandManager.RequerySuggested -= value;
        }

        /// <inheritdoc />
        /// <summary>
        ///     Defines the action that determines whether the command can execute in its current state or not.
        /// </summary>
        /// <param name="parameter">
        ///     Data used by the command. If the command does not require data to be passed, this object can be
        ///     <see langword="null" />.
        /// </param>
        /// <returns>
        ///     <see langword="true" /> if this command can be executed; otherwise, <see langword="false" />.
        /// </returns>
        /// <exception cref="Exception">A delegate callback throws an exception.</exception>
        public bool CanExecute(object parameter = null)
        {
            return !this.isExecutingCorrespondingTask
                   && (this.canExecuteCorrespondingTask == null || this.canExecuteCorrespondingTask(parameter));
        }

        /// <inheritdoc />
        /// <summary>
        ///     Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            // Dispose of unmanaged resources.
            this.Dispose(true);

            // Suppress finalization.
            GC.SuppressFinalize(this);
        }

        /// <inheritdoc />
        /// <summary>
        ///     Defines the action to be called when the command is invoked.
        /// </summary>
        /// <param name="parameter">
        ///     Data used by the command. If the command does not require data to be passed, this object can be
        ///     <see langword="null" />.
        /// </param>
        public void Execute(object parameter = null)
        {
            // Notify that a task is started.
            this.isExecutingCorrespondingTask = true;

            // Determines which task to execute.
            var intendedTask = parameter == null ? this.ExecuteAsync() : this.ExecuteAsync(parameter);

            try
            {
                // Execute the task.
                intendedTask.ConfigureAwait(false);
            }
            catch (AggregateException exception)
            {
                this.desktopAlertService.DisplayDesktopAlert(
                    NotificationsLayer.GeneralExceptionsHeader,
                    string.Format(NotificationsLayer.GeneralExceptionsMessage, exception.Flatten().Message),
                    AlertTypes.Error);
            }
            finally
            {
                // Notify the task is finished.
                this.isExecutingCorrespondingTask = false;
            }
        }

        /// <summary>
        ///     Releases unmanaged and - optionally - managed resources.
        /// </summary>
        /// <param name="disposing">
        ///     <see langword="true" /> to release both managed and unmanaged resources; <see langword="false" /> to release only
        ///     unmanaged resources.
        /// </param>
        private void Dispose(bool disposing)
        {
            if (this.disposed)
            {
                return;
            }

            if (disposing)
            {
                this.taskToExecute = null;
                this.desktopAlertService.Dispose();
                this.taskWithParameterToExecute = null;
                this.canExecuteCorrespondingTask = null;
            }

            this.disposed = true;
        }

        /// <summary>
        ///     Defines the action to be called when the command is invoked.
        /// </summary>
        /// <param name="parameter">Data used by the command.</param>
        /// <returns>
        ///     A <see cref="Task" /> represents the corresponding event.
        /// </returns>
        [NotNull]
        private Task ExecuteAsync([CanBeNull] object parameter)
        {
            return this.taskWithParameterToExecute == null
                       ? Task.CompletedTask
                       : Task.Factory.StartNew(
                           () => this.taskWithParameterToExecute(parameter),
                           TaskCreationOptions.AttachedToParent).Unwrap();
        }

        /// <summary>
        ///     Defines the action to be called when the command is invoked.
        /// </summary>
        /// <returns>
        ///     A <see cref="Task" /> represents the corresponding event.
        /// </returns>
        [NotNull]
        private Task ExecuteAsync()
        {
            return this.taskToExecute == null
                       ? Task.CompletedTask
                       : Task.Factory.StartNew(() => this.taskToExecute(), TaskCreationOptions.AttachedToParent)
                           .Unwrap();
        }
    }
}