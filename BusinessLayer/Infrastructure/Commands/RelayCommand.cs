﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="RelayCommand.cs" company="None">
// Last Modified: 27/09/2018
// Author: Ahmed Ghanam.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace WordsBooklet.BusinessLayer.Infrastructure
{
    using System;
    using System.Windows.Input;

    /// <inheritdoc />
    /// <summary>
    ///     Represents a basic generic command implementation.
    /// </summary>
    /// <seealso cref="ICommand" />
    public class RelayCommand : ICommand
    {
        /// <summary>
        ///     The action which determines whether can execute the corresponding action or not.
        /// </summary>
        [CanBeNull]
        private readonly Predicate<object> canExecuteCorrespondingAction;

        /// <summary>
        ///     The corresponding action to execute.
        /// </summary>
        [NotNull]
        private readonly Action<object> correspondingActionToExecute;

        /// <inheritdoc />
        /// <summary>
        ///     Initializes a new instance of the <see cref="RelayCommand" /> class.
        /// </summary>
        /// <param name="execute">The action to execute.</param>
        public RelayCommand([NotNull] Action<object> execute)
            : this(null, execute)
        {
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="RelayCommand" /> class.
        /// </summary>
        /// <param name="canExecute">The predicate determines can execute the corresponding action or not.</param>
        /// <param name="execute">The action to execute.</param>
        public RelayCommand([CanBeNull] Predicate<object> canExecute, [NotNull] Action<object> execute)
        {
            this.correspondingActionToExecute = execute;
            this.canExecuteCorrespondingAction = canExecute;
        }

        /// <inheritdoc />
        /// <summary>
        ///     Occurs when changes occur that affect whether or not the command should execute.
        /// </summary>
        public event EventHandler CanExecuteChanged
        {
            add => CommandManager.RequerySuggested += value;

            remove => CommandManager.RequerySuggested -= value;
        }

        /// <inheritdoc />
        /// <summary>
        ///     Defines the method that determines whether the command can execute in its current state or not.
        /// </summary>
        /// <param name="parameter">
        ///     Data used by the command.
        /// </param>
        /// <returns>
        ///     <see langword="true" /> if this command can be executed; otherwise, <see langword="false" />.
        /// </returns>
        /// <exception cref="Exception">A delegate callback throws an exception.</exception>
        public bool CanExecute(object parameter = null)
        {
            return this.canExecuteCorrespondingAction == null || this.canExecuteCorrespondingAction(parameter);
        }

        /// <inheritdoc />
        /// <summary>
        ///     Defines the method to be called when the command is invoked.
        /// </summary>
        /// <param name="parameter">
        ///     Data used by the command.
        /// </param>
        /// <exception cref="Exception">A delegate callback throws an exception.</exception>
        public void Execute(object parameter = null)
        {
            this.correspondingActionToExecute(parameter);
        }
    }
}