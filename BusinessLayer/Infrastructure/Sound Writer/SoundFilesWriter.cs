﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SoundFilesWriter.cs" company="None">
// Last Modified: 30/09/2018
// Author: Ahmed Ghanam.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace WordsBooklet.BusinessLayer.Infrastructure
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;

    /// <summary>
    ///     Represents method for creating a Microsoft's RIFF multimedia file(s) using sound streams..
    /// </summary>
    internal sealed class SoundFilesWriter
    {
        /// <summary>
        ///     Creates the sound file by merging the sound streams.
        /// </summary>
        /// <param name="outputPath">The output path.</param>
        /// <param name="soundFilesStreams">The sound files streams.</param>
        /// <exception cref="ArgumentNullException">Thrown when the arguments are <see langword="null" /></exception>
        internal void CreateSoundFile([NotNull] string outputPath, [NotNull] List<byte[]> soundFilesStreams)
        {
            // Handle the dirty streams only.
            var soundStreams = soundFilesStreams.Where(soundStream => soundStream?.Length > 0).ToList();
            if (soundStreams.Count == 0 || string.IsNullOrEmpty(outputPath))
            {
                throw new ArgumentNullException();
            }

            // Merge sound streams into one riff canonical form.
            var mergedRiffCanonicalForm = MergeIntoSingleForm(soundStreams);

            // Create an empty sound file using the built riff canonical form.
            CreateSoundFile(outputPath, mergedRiffCanonicalForm);

            // Write the data to the created empty sound file.
            WriteDataToSoundFile(outputPath, soundStreams);
        }

        /// <summary>
        ///     Creates an empty sound file represents the canonical form.
        /// </summary>
        /// <param name="outputPath">The output path.</param>
        /// <param name="riffCanonicalForm">The subset of Microsoft's RIFF specification of multimedia file.</param>
        private static void CreateSoundFile([NotNull] string outputPath, [NotNull] RiffCanonicalForm riffCanonicalForm)
        {
            var fileStream = new FileStream(outputPath, FileMode.Create, FileAccess.Write);
            using (var binaryWriter = new BinaryWriter(fileStream))
            {
                // Writes the chunk descriptor.
                WriteChunkDescriptor(fileStream, binaryWriter, riffCanonicalForm);

                // Writes the format sub chunk.
                WriteFormatChuck(fileStream, binaryWriter, riffCanonicalForm);

                // Writes the data sub chunk.
                WriteDataChunk(fileStream, binaryWriter, riffCanonicalForm);
            }
        }

        /// <summary>
        ///     Merges the sound streams into a single riff canonical form.
        /// </summary>
        /// <param name="soundStreams">The sound streams.</param>
        /// <returns>
        ///     A <see cref="RiffCanonicalForm" /> represents the merged riff canonical form.
        /// </returns>
        [NotNull]
        private static RiffCanonicalForm MergeIntoSingleForm([NotNull] IEnumerable<byte[]> soundStreams)
        {
            var singleRiffCanonicalForm = new RiffCanonicalForm();
            var mergedRiffCanonicalForm = new RiffCanonicalForm();
            foreach (var soundStream in soundStreams)
            {
                // Building a standard riff canonical form using the sound stream.
                singleRiffCanonicalForm = singleRiffCanonicalForm.ReadChunks(soundStream);

                // Increase the data length of the output sound file.
                mergedRiffCanonicalForm.IncreaseDataLength(singleRiffCanonicalForm.Data);
            }

            // Equalize the header data of the single riff canonical form with the output riff canonical form.
            mergedRiffCanonicalForm.EqualizeHeader(singleRiffCanonicalForm);

            // Return the merged riff canonical form.
            return mergedRiffCanonicalForm;
        }

        /// <summary>
        ///     Writes the chunk descriptor of the multimedia file.
        /// </summary>
        /// <param name="fileStream">The file stream.</param>
        /// <param name="binaryWriter">The binary writer.</param>
        /// <param name="riffCanonicalForm">The riff canonical form.</param>
        private static void WriteChunkDescriptor(
            [NotNull] Stream fileStream,
            [NotNull] BinaryWriter binaryWriter,
            [NotNull] RiffCanonicalForm riffCanonicalForm)
        {
            fileStream.Position = 0;
            binaryWriter.Write(riffCanonicalForm.ChunkId);

            fileStream.Position = 4;
            binaryWriter.Write(riffCanonicalForm.ChunkSize);

            fileStream.Position = 8;
            binaryWriter.Write(riffCanonicalForm.Format);
        }

        /// <summary>
        ///     Writes the data chunk of the multimedia file.
        /// </summary>
        /// <param name="fileStream">The file stream.</param>
        /// <param name="binaryWriter">The binary writer.</param>
        /// <param name="riffCanonicalForm">The riff canonical form.</param>
        private static void WriteDataChunk(
            [NotNull] Stream fileStream,
            [NotNull] BinaryWriter binaryWriter,
            [NotNull] RiffCanonicalForm riffCanonicalForm)
        {
            fileStream.Position = 36;
            binaryWriter.Write(riffCanonicalForm.SubChunk2Id);

            fileStream.Position = 40;
            binaryWriter.Write(riffCanonicalForm.SubChunk2Size);

            fileStream.Position = 44;
            binaryWriter.Write(riffCanonicalForm.Data);
        }

        /// <summary>
        ///     Writes the data into the new sound file.
        /// </summary>
        /// <param name="outputPath">The output path.</param>
        /// <param name="soundStreams">The sound streams.</param>
        private static void WriteDataToSoundFile(
            [NotNull] string outputPath,
            [NotNull] IEnumerable<byte[]> soundStreams)
        {
            foreach (var soundStream in soundStreams)
            {
                // Read the data chunk only.
                byte[] soundFileBytes;
                using (var memoryStream = new MemoryStream(soundStream))
                {
                    memoryStream.Position = 44;
                    soundFileBytes = new byte[memoryStream.Length - 44];
                    memoryStream.Read(soundFileBytes, 0, soundFileBytes.Length - 44);
                }

                // Append only the data chunk.
                var fileStream = new FileStream(outputPath, FileMode.Append, FileAccess.Write);
                using (var binaryWriter = new BinaryWriter(fileStream))
                {
                    binaryWriter.Write(soundFileBytes);
                }
            }
        }

        /// <summary>
        ///     Writes the format chunk of the multimedia file.
        /// </summary>
        /// <param name="fileStream">The file stream.</param>
        /// <param name="binaryWriter">The binary writer.</param>
        /// <param name="riffCanonicalForm">The riff canonical form.</param>
        private static void WriteFormatChuck(
            [NotNull] Stream fileStream,
            [NotNull] BinaryWriter binaryWriter,
            [NotNull] RiffCanonicalForm riffCanonicalForm)
        {
            fileStream.Position = 12;
            binaryWriter.Write(riffCanonicalForm.SubChunk1Id);

            fileStream.Position = 16;
            binaryWriter.Write(riffCanonicalForm.SubChunk1Size);

            fileStream.Position = 20;
            binaryWriter.Write(riffCanonicalForm.AudioFormat);

            fileStream.Position = 22;
            binaryWriter.Write(riffCanonicalForm.NumChannels);

            fileStream.Position = 24;
            binaryWriter.Write(riffCanonicalForm.SampleRate);

            fileStream.Position = 28;
            binaryWriter.Write(riffCanonicalForm.ByteRate);

            fileStream.Position = 32;
            binaryWriter.Write(riffCanonicalForm.BlockAlign);

            fileStream.Position = 34;
            binaryWriter.Write(riffCanonicalForm.BitsPerSample);
        }
    }
}