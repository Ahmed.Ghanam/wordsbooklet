﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="RiffCanonicalForm.cs" company="None">
// Last Modified: 30/09/2018
// Author: Ahmed Ghanam.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace WordsBooklet.BusinessLayer.Infrastructure
{
    using System;
    using System.IO;

    /// <summary>
    ///     Represents the subset of Microsoft's RIFF specification for the storage of multimedia files.
    /// </summary>
    internal sealed class RiffCanonicalForm
    {
        /// <summary>
        ///     The back field for the <see cref="ChunkSize" /> property.
        /// </summary>
        private long chunkSize;

        /// <summary>
        ///     Gets the audio format.
        /// </summary>
        /// <value>
        ///     The audio format.
        /// </value>
        internal short AudioFormat => 1;

        /// <summary>
        ///     Gets the bits per sample.
        /// </summary>
        /// <value>
        ///     The bits per sample.
        /// </value>
        internal short BitsPerSample { get; private set; }

        /// <summary>
        ///     Gets the block align.
        /// </summary>
        /// <value>
        ///     The block align.
        /// </value>
        internal int BlockAlign => this.NumChannels * this.BitsPerSample / 8;

        /// <summary>
        ///     Gets the <see langword="byte" /> rate.
        /// </summary>
        /// <value>
        ///     The <see langword="byte" /> rate.
        /// </value>
        internal int ByteRate => this.SampleRate * this.NumChannels * this.BitsPerSample / 8;

        /// <summary>
        ///     Gets the chunk descriptor.
        /// </summary>
        /// <value>
        ///     The chunk descriptor.
        /// </value>
        [NotNull]
        internal char[] ChunkId => new[] { 'R', 'I', 'F', 'F' };

        /// <summary>
        ///     Gets the size of the entire file in bytes minus 8 bytes
        ///     for the two fields not included in count: ChunkID and ChunkSize.
        /// </summary>
        /// <value>
        ///     The size of the entire file in bytes minus 8 bytes.
        /// </value>
        internal long ChunkSize
        {
            get => 4 + 8 + this.SubChunk1Size + 8 + this.SubChunk2Size + this.chunkSize;
            private set => this.chunkSize = value;
        }

        /// <summary>
        ///     Gets the actual sound data.
        /// </summary>
        /// <value>
        ///     The actual sound data.
        /// </value>
        internal long Data { get; private set; }

        /// <summary>
        ///     Gets the sound file format.
        /// </summary>
        /// <value>
        ///     The format of sound file.
        /// </value>
        [NotNull]
        internal char[] Format => new[] { 'W', 'A', 'V', 'E' };

        /// <summary>
        ///     Gets the number of channels.
        /// </summary>
        /// <value>
        ///     The number of channels.
        /// </value>
        internal short NumChannels { get; private set; }

        /// <summary>
        ///     Gets the sample rate.
        /// </summary>
        /// <value>
        ///     The sample rate.
        /// </value>
        internal int SampleRate { get; private set; }

        /// <summary>
        ///     Gets the sub chunk 1 descriptor.
        /// </summary>
        /// <value>
        ///     The sub chunk 1 descriptor.
        /// </value>
        [NotNull]
        internal char[] SubChunk1Id => new[] { 'f', 'm', 't', ' ' };

        /// <summary>
        ///     Gets the size of the sub chunk 1 descriptor.
        /// </summary>
        /// <value>
        ///     The size of the sub chunk 1 descriptor.
        /// </value>
        internal int SubChunk1Size => 16;

        /// <summary>
        ///     Gets the sub chunk 2 identifier.
        /// </summary>
        /// <value>
        ///     The sub chunk 2 identifier.
        /// </value>
        [NotNull]
        internal char[] SubChunk2Id => new[] { 'd', 'a', 't', 'a' };

        /// <summary>
        ///     Gets the size of the sub chunk 2.
        /// </summary>
        /// <value>
        ///     The size of the sub chunk 2.
        /// </value>
        internal long SubChunk2Size => this.Data * this.NumChannels * this.BitsPerSample / 8;

        /// <summary>
        ///     Equalizes the header of the current riff canonical form to the source riff canonical form.
        /// </summary>
        /// <param name="sourceRiffCanonicalForm">The source riff canonical form.</param>
        internal void EqualizeHeader([NotNull] RiffCanonicalForm sourceRiffCanonicalForm)
        {
            this.SampleRate = sourceRiffCanonicalForm.SampleRate;
            this.NumChannels = sourceRiffCanonicalForm.NumChannels;
            this.BitsPerSample = sourceRiffCanonicalForm.BitsPerSample;
        }

        /// <summary>
        ///     Increases the length of the inner data.
        /// </summary>
        /// <param name="increaseByLength">The new data length.</param>
        internal void IncreaseDataLength(long increaseByLength)
        {
            this.Data += increaseByLength;
        }

        /// <summary>
        ///     Reads the different chunks.
        /// </summary>
        /// <param name="soundBytes">The sound bytes.</param>
        /// <returns>
        ///     A <see cref="RiffCanonicalForm" /> contains the sound chunks data.
        /// </returns>
        /// <exception cref="IOException">An I/O error occurs. </exception>
        /// <exception cref="ObjectDisposedException">The stream is closed. </exception>
        /// <exception cref="EndOfStreamException">The end of the stream is reached. </exception>
        /// <exception cref="ArgumentNullException">
        ///     <paramref name="soundBytes" /> is <see langword="null" />.
        /// </exception>
        /// <exception cref="ArgumentOutOfRangeException">
        ///     The position is set to a negative value or a value greater than <see cref="Int32.MaxValue" />.
        /// </exception>
        [NotNull]
        internal RiffCanonicalForm ReadChunks([NotNull] byte[] soundBytes)
        {
            // Initialize a new riff canonical form.
            var riffCanonicalForm = new RiffCanonicalForm();

            // Initialize non-re-sizable stream using the sound bytes.
            var memoryStream = new MemoryStream(soundBytes);

            // Reading the ChunkSize.
            riffCanonicalForm.ChunkSize = (int)memoryStream.Length - 8;

            // Reading specific chunks from the non-re-sizable stream.
            using (var binaryReader = new BinaryReader(memoryStream))
            {
                // Reading the NumChannels.
                memoryStream.Position = 22;
                riffCanonicalForm.NumChannels = binaryReader.ReadInt16();

                // Reading the SampleRate.
                memoryStream.Position = 24;
                riffCanonicalForm.SampleRate = binaryReader.ReadInt32();

                // Reading the BitsPerSample.
                memoryStream.Position = 34;
                riffCanonicalForm.BitsPerSample = binaryReader.ReadInt16();

                // Reading the actual sound data.
                riffCanonicalForm.Data = memoryStream.Length - 44;
            }

            // Return the riff canonical form holding data.
            return riffCanonicalForm;
        }
    }
}