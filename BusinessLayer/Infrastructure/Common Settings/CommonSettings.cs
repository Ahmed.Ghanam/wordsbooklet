﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CommonSettings.cs" company="None">
// Last Modified: 08/10/2018
// Author: Ahmed Ghanam.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace WordsBooklet.BusinessLayer.Infrastructure
{
    using System;
    using System.Globalization;
    using System.Linq;
    using System.Windows;

    using Telerik.Windows.Controls;

    using WordsBooklet.BusinessLayer.Models;

    /// <summary>
    ///     Represents the values of the common settings.
    /// </summary>
    public static class CommonSettings
    {
        /// <summary>
        ///     Initializes static members of the <see cref="CommonSettings" /> class.
        /// </summary>
        static CommonSettings()
        {
            ReadConcernedSettings();
        }

        /// <summary>
        ///     Gets the screen position for the user notifications.
        /// </summary>
        /// <value>
        ///     The screen position for the user notifications.
        /// </value>
        public static AlertScreenPosition AlertScreenPosition { get; private set; }

        /// <summary>
        ///     Gets the authentication key for using the cognitive services.
        /// </summary>
        /// <value>
        ///     The authentication key for using the cognitive services.
        /// </value>
        [CanBeNull]
        public static string CognitiveServicesKey { get; private set; }

        /// <summary>
        ///     Gets the culture information for the current thread.
        /// </summary>
        /// <value>
        ///     The culture information for the current thread.
        /// </value>
        [CanBeNull]
        public static CultureInfo CultureInfo { get; private set; }

        /// <summary>
        ///     Gets the flow direction used by user interfaces at run time.
        /// </summary>
        /// <value>
        ///     The flow direction used by user interfaces at run time.
        /// </value>
        public static FlowDirection FlowDirection { get; private set; }

        /// <summary>
        ///     Gets the language used by the Resource Manager to look up culture-specific resources at run time.
        /// </summary>
        /// <value>
        ///     The language used by the Resource Manager to look up culture-specific resources at run time.
        /// </value>
        public static Languages SystemLanguage { get; private set; }

        /// <summary>
        ///     Reads the authentication key for using the cognitive services.
        /// </summary>
        /// <param name="entities">The data context.</param>
        private static void ReadCognitiveServicesKey([NotNull] WordsBookletEntities entities)
        {
            // Defines the setting code.
            var cognitiveServicesKeyCode = Convert.ToString(Convert.ToInt16(SettingsCodes.CognitiveServicesKey));

            // Reads the setting value.
            var cognitiveServicesKey =
                entities.Settings.FirstOrDefault(setting => setting.Code == cognitiveServicesKeyCode);

            // Stores the value or throws an exception if the value is null.
            CognitiveServicesKey = cognitiveServicesKey != null ? cognitiveServicesKey.SavedValue : string.Empty;
        }

        /// <summary>
        ///     Reads the values of the concerned settings.
        /// </summary>
        private static void ReadConcernedSettings()
        {
            using (var wordsBookletEntities = new WordsBookletEntities())
            {
                ReadUserInterfacesSettings(wordsBookletEntities);

                ReadCognitiveServicesKey(wordsBookletEntities);
            }
        }

        /// <summary>
        ///     Reads values of settings related to all user interfaces.
        /// </summary>
        /// <param name="entities">The data context.</param>
        private static void ReadUserInterfacesSettings([NotNull] WordsBookletEntities entities)
        {
            // Reads the value of the system language.
            var systemLanguageCode = Convert.ToString(Convert.ToInt16(SettingsCodes.SystemLanguage));
            var systemLanguage = entities.Settings.FirstOrDefault(setting => setting.Code == systemLanguageCode);

            // Use the Arabic language if the value of system language is null.
            if (systemLanguage == null)
            {
                SystemLanguage = Languages.Arabic;
            }
            else
            {
                SystemLanguage = (Languages)Convert.ToInt16(systemLanguage.SavedValue);
            }

            // Set other related properties.
            switch (SystemLanguage)
            {
                case Languages.Bokmål:
                    CultureInfo = new CultureInfo("nb-NO");
                    FlowDirection = FlowDirection.LeftToRight;
                    AlertScreenPosition = AlertScreenPosition.BottomRight;
                    break;

                default:
                    CultureInfo = new CultureInfo("ar-EG");
                    FlowDirection = FlowDirection.RightToLeft;
                    AlertScreenPosition = AlertScreenPosition.BottomLeft;
                    break;
            }
        }
    }
}