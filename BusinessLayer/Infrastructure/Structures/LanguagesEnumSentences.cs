﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="LanguagesEnumSentences.cs" company="None">
// Last Modified: 30/09/2018
// Author: Ahmed Ghanam.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace WordsBooklet.BusinessLayer.Infrastructure
{
    /// <summary>
    ///     Represents <see langword="static" /> sentences describing the
    ///     <see cref="Languages" /> <see langword="enum" /> members.
    /// </summary>
    internal struct LanguagesEnumSentences
    {
        /* Arabic language Section */

        /// <summary>
        ///     Represents the meaning of Arabic language in the Arabic language.
        /// </summary>
        internal const string MeaningOfArabicLanguageInArabic = "اللغه العربية";

        /// <summary>
        ///     Represents the meaning of Arabic language in the Norwegian (Bokmål) language.
        /// </summary>
        internal const string MeaningOfArabicLanguageInNorwegian = "Det arabiske språket";

        /* Norwegian language (Bokmål) Section */

        /// <summary>
        ///     Represents the meaning of Norwegian (Bokmål) language in the Arabic language.
        /// </summary>
        internal const string MeaningOfNorwegianBokmålLanguageInArabic = "اللغه النرويجية - بوکمال";

        /// <summary>
        ///     Represents the meaning of Norwegian (Bokmål) language in the Norwegian (Bokmål) language.
        /// </summary>
        internal const string MeaningOfNorwegianBokmålLanguageInNorwegian = "Det norske språket - Bokmål";
    }
}