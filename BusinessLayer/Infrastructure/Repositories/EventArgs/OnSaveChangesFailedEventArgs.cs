﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="OnSaveChangesFailedEventArgs.cs" company="None">
// Last Modified: 30/09/2018
// Author: Ahmed Ghanam.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace WordsBooklet.BusinessLayer.Infrastructure
{
    using System;

    /// <inheritdoc />
    /// <summary>
    ///     Represents the data model caused the exception and the occurred exception.
    /// </summary>
    /// <seealso cref="OnSaveChangesEventArgs" />
    public class OnSaveChangesFailedEventArgs : OnSaveChangesEventArgs
    {
        /// <summary>
        ///     Gets or sets the thrown exception from the SaveChanges().
        /// </summary>
        /// <value>
        ///     The thrown exception.
        /// </value>
        [CanBeNull]
        public Exception Exception { get; set; }
    }
}