﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="OnSaveChangesEventArgs.cs" company="None">
// Last Modified: 30/09/2018
// Author: Ahmed Ghanam.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace WordsBooklet.BusinessLayer.Infrastructure
{
    using System;

    /// <inheritdoc />
    /// <summary>
    ///     Represents the data model which will be saved on the data source.
    /// </summary>
    /// <seealso cref="EventArgs" />
    public class OnSaveChangesEventArgs : EventArgs
    {
        /// <summary>
        ///     Gets or sets the intended data entity.
        /// </summary>
        /// <value>
        ///     The intended data entity.
        /// </value>
        [CanBeNull]
        public IEntity Entity { get; set; }
    }
}