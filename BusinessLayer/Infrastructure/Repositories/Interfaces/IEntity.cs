﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IEntity.cs" company="None">
// Last Modified: 30/09/2018
// Author: Ahmed Ghanam.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace WordsBooklet.BusinessLayer.Infrastructure
{
    /// <summary>
    ///     Represents the basic form of every data entity.
    /// </summary>
    public interface IEntity
    {
        /// <summary>
        ///     Gets the entity identifier.
        /// </summary>
        /// <value>
        ///     The entity identifier.
        /// </value>
        long Id { get; }

        /// <summary>
        ///     Gets a value indicating whether <see langword="this" /> instance is a hierarchically entity or not.
        /// </summary>
        /// <value>
        ///     <see langword="true" /> if <see langword="this" /> instance is a hierarchically entity; otherwise,
        ///     <see langword="false" />.
        /// </value>
        bool IsHierarchicallyEntity { get; }

        /// <summary>
        ///     Gets the entity name.
        /// </summary>
        /// <value>
        ///     The entity name.
        /// </value>
        [CanBeNull]
        string Name { get; }
    }
}