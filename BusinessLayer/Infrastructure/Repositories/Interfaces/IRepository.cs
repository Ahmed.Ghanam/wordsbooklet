﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IRepository.cs" company="None">
// Last Modified: 30/09/2018
// Author: Ahmed Ghanam.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace WordsBooklet.BusinessLayer.Infrastructure
{
    using System.Threading;
    using System.Threading.Tasks;

    /// <summary>
    ///     Represents the CRUD operations' definitions.
    /// </summary>
    /// <typeparam name="TEntity">The intended data entity type</typeparam>
    internal interface IRepository<in TEntity>
        where TEntity : class, IEntity, new()
    {
        /// <summary>
        ///     Gets the cancellation token for controlling every task.
        /// </summary>
        /// <value>
        ///     The cancellation token for controlling every task.
        /// </value>
        CancellationToken CancellationToken { get; }

        /// <summary>
        ///     Deletes the passed or the inner <paramref name="model" /> asynchronously.
        /// </summary>
        /// <param name="model">The <paramref name="model" />.</param>
        /// <returns>
        ///     A <see cref="Task{Results}" /> represents the deletion operation.
        /// </returns>
        [NotNull]
        Task<bool> DeleteAsync([CanBeNull] TEntity model = null);

        /// <summary>
        ///     Inserts the inner model asynchronously.
        /// </summary>
        /// <returns>
        ///     A <see cref="Task" /> represents the insertion operation.
        /// </returns>
        [NotNull]
        Task InsertAsync();

        /// <summary>
        ///     Updates the inner model asynchronously.
        /// </summary>
        /// <returns>
        ///     A <see cref="Task" /> represents the updating operation.
        /// </returns>
        [NotNull]
        Task UpdateAsync();
    }
}