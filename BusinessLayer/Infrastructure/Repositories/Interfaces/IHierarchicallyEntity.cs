﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IHierarchicallyEntity.cs" company="None">
// Last Modified: 30/09/2018
// Author: Ahmed Ghanam.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace WordsBooklet.BusinessLayer.Infrastructure
{
    using System.Collections.ObjectModel;

    /// <inheritdoc />
    /// <summary>
    ///     Represents the basic form of every hierarchical data entity.
    /// </summary>
    /// <seealso cref="IEntity" />
    public interface IHierarchicallyEntity : IEntity
    {
        /// <summary>
        ///     Gets the sub entities related to the current data entity.
        /// </summary>
        /// <value>
        ///     The sub entities related to the current data entity.
        /// </value>
        [NotNull]
        ObservableCollection<IHierarchicallyEntity> SubEntities { get; }
    }
}