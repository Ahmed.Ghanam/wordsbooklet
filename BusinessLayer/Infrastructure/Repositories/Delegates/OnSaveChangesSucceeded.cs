﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="OnSaveChangesSucceeded.cs" company="None">
// Last Modified: 30/09/2018
// Author: Ahmed Ghanam.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace WordsBooklet.BusinessLayer.Infrastructure
{
    /// <summary>
    ///     Provides the data model which has been successfully saved on the data source.
    /// </summary>
    /// <param name="sender">The control raise the event.</param>
    /// <param name="e">
    ///     The <see cref="OnSaveChangesEventArgs" /> instance containing the event data.
    /// </param>
    public delegate void OnSaveChangesSucceeded([NotNull] object sender, [NotNull] OnSaveChangesEventArgs e);
}