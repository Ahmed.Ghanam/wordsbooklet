﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="OnSaveChangesFailed.cs" company="None">
// Last Modified: 30/09/2018
// Author: Ahmed Ghanam.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace WordsBooklet.BusinessLayer.Infrastructure
{
    /// <summary>
    ///     Provides the data model which failed to be saved on the data source and the occurred exception.
    /// </summary>
    /// <param name="sender">The control raise the event.</param>
    /// <param name="e">
    ///     The <see cref="OnSaveChangesFailedEventArgs" /> instance containing the event data.
    /// </param>
    public delegate void OnSaveChangesFailed([NotNull] object sender, [NotNull] OnSaveChangesFailedEventArgs e);
}