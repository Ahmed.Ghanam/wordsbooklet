﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="RepositoryLayer.cs" company="None">
// Last Modified: 30/09/2018
// Author: Ahmed Ghanam.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace WordsBooklet.BusinessLayer.Infrastructure
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using System.Windows;

    using WordsBooklet.BusinessLayer.Models;

    /// <inheritdoc cref="ValidationLayer{T}" />
    /// <summary>
    ///     Represents members performing CRUD operations on the data model asynchronously.
    /// </summary>
    /// <typeparam name="T">The intended data model type.</typeparam>
    /// <seealso cref="ValidationLayer{T}" />
    /// <seealso cref="IRepository{T}" />
    /// <seealso cref="IDisposable" />
    public abstract class RepositoryLayer<T> : ValidationLayer<T>, IRepository<T>, IDisposable
        where T : class, IEntity, new()
    {
        /// <summary>
        ///     Flag: Has the <c>Dispose()</c> been called ?.
        /// </summary>
        private bool disposed;

        /// <summary>
        ///     Finalizes an instance of the <see cref="RepositoryLayer{T}" /> class.
        /// </summary>
        ~RepositoryLayer()
        {
            this.Dispose(false);
        }

        /// <summary>
        ///     Occurs after failed to delete the data model.
        /// </summary>
        protected event OnSaveChangesFailed OnDeletingFailed;

        /// <summary>
        ///     Occurs when start to delete the data model.
        /// </summary>
        protected event OnSaveChangesStarted OnDeletingStarted;

        /// <summary>
        ///     Occurs if deleting data model succeeded.
        /// </summary>
        protected event OnSaveChangesSucceeded OnDeletingSucceeded;

        /// <summary>
        ///     Occurs after failed to insert the data model.
        /// </summary>
        protected event OnSaveChangesFailed OnInsertingFailed;

        /// <summary>
        ///     Occurs when start to insert the data model.
        /// </summary>
        protected event OnSaveChangesStarted OnInsertingStarted;

        /// <summary>
        ///     Occurs if inserting data model succeeded.
        /// </summary>
        protected event OnSaveChangesSucceeded OnInsertingSucceeded;

        /// <summary>
        ///     Occurs after failed to update the data model.
        /// </summary>
        protected event OnSaveChangesFailed OnUpdatingFailed;

        /// <summary>
        ///     Occurs when start to update the data model.
        /// </summary>
        protected event OnSaveChangesStarted OnUpdatingStarted;

        /// <summary>
        ///     Occurs if updating data model succeeded.
        /// </summary>
        protected event OnSaveChangesSucceeded OnUpdatingSucceeded;

        /// <inheritdoc />
        /// <summary>
        ///     Gets or sets the cancellation token for controlling every task.
        /// </summary>
        /// <value>
        ///     The cancellation token for controlling every task.
        /// </value>
        public CancellationToken CancellationToken { get; private set; }

        /// <summary>
        ///     Gets the database entities context.
        /// </summary>
        /// <value>
        ///     The database entities context.
        /// </value>
        [NotNull]
        protected WordsBookletEntities DataEntities { get; } = new WordsBookletEntities();

        /// <summary>
        ///     Gets an <see cref="IDbSet{TEntity}" /> represents the collection of all entities in the context, or that
        ///     can be queried from the database, of a given type.
        /// </summary>
        /// <value>
        ///     A collection of the of <see cref="T" /> type.
        /// </value>
        [NotNull]
        private IDbSet<T> Entities => this.DataEntities.Set<T>();

        /// <inheritdoc />
        /// <summary>
        ///     Deletes the passed or the inner data <paramref name="model" /> asynchronously.
        /// </summary>
        /// <param name="model">The <paramref name="model" /> to delete.</param>
        /// <returns>
        ///     A <see cref="Task{Result}" /> represents the deletion operation.
        /// </returns>
        public async Task<bool> DeleteAsync(T model = null)
        {
            // Decide which model will be delete, the passed one or the inner one.
            var modelToDelete = model ?? this.Model;

            // Take a confirmation from user before processing.
            // And
            // Return the confirmation results.
            var isUserConfirmedDeletingModel = false;
            var isDeletingCompletedSuccessfully = false;

            // Execute the IsUserConfirmedDeletingModel on the main thread.
            Application.Current.Dispatcher.Invoke(
                () => { isUserConfirmedDeletingModel = this.IsUserConfirmedDeletingModel(modelToDelete); });

            // Delete the data model and its children if the user confirmed deleting.
            if (isUserConfirmedDeletingModel)
            {
                await this.DeleteModelDataAsync(modelToDelete).ContinueWith(
                    deletingTask => { isDeletingCompletedSuccessfully = true; },
                    TaskContinuationOptions.OnlyOnRanToCompletion).ConfigureAwait(false);
            }

            // Return the confirmation and the SaveChanges() results.
            return isUserConfirmedDeletingModel && isDeletingCompletedSuccessfully;
        }

        /// <inheritdoc />
        /// <summary>
        ///     Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        /// <exception cref="ObjectDisposedException">This <see cref="CancellationTokenSource" /> has been disposed.</exception>
        /// <exception cref="AggregateException">
        ///     An aggregate exception containing all the exceptions thrown by the registered
        ///     callbacks on the associated <see cref="CancellationToken" />.
        /// </exception>
        public void Dispose()
        {
            // Dispose of unmanaged resources.
            this.Dispose(true);

            // Suppress finalization.
            GC.SuppressFinalize(this);
        }

        /// <inheritdoc />
        /// <summary>
        ///     Inserts the inner model asynchronously.
        /// </summary>
        /// <returns>
        ///     A <see cref="Task" /> represents the insertion operation.
        /// </returns>
        public async Task InsertAsync()
        {
            try
            {
                // Invoke OnInsertingStarted event.
                this.InvokeOnMainThread(this.OnInsertingStarted);

                // Add the model to the entities set.
                this.Entities.Add(this.Model);
                this.DataEntities.Entry(this.Model).State = EntityState.Added;

                // Save changes to the database asynchronously.
                await this.DataEntities.SaveChangesAsync(this.CancellationToken).ContinueWith(
                    saveChangesTask =>
                        {
                            // Invoke OnInsertingSucceeded event.
                            if (saveChangesTask.Result > 0)
                            {
                                this.InvokeOnMainThread(this.OnInsertingSucceeded);
                            }
                        },
                    this.CancellationToken,
                    TaskContinuationOptions.OnlyOnRanToCompletion,
                    TaskScheduler.Current).ConfigureAwait(false);
            }
            catch (AggregateException exception)
            {
                // Invoke OnInsertingFailed event.
                this.InvokeOnMainThread(this.OnInsertingFailed, this.Model, exception);
            }
        }

        /// <inheritdoc />
        /// <summary>
        ///     Updates the inner model asynchronously.
        /// </summary>
        /// <returns>
        ///     A <see cref="Task" /> represents the updating operation.
        /// </returns>
        public async Task UpdateAsync()
        {
            try
            {
                // Invoke OnUpdatingStarted event.
                this.InvokeOnMainThread(this.OnUpdatingStarted);

                // Add or update the model to/into the entities set.
                this.Entities.AddOrUpdate(this.Model);
                this.DataEntities.Entry(this.Model).State = EntityState.Modified;

                // Save changes to the database asynchronously.
                await this.DataEntities.SaveChangesAsync(this.CancellationToken).ContinueWith(
                    saveChangesTask =>
                        {
                            // Invoke OnUpdatingSucceeded event.
                            if (saveChangesTask.Result > 0)
                            {
                                this.InvokeOnMainThread(this.OnUpdatingSucceeded);
                            }
                        },
                    this.CancellationToken,
                    TaskContinuationOptions.OnlyOnRanToCompletion,
                    TaskScheduler.Current).ConfigureAwait(false);
            }
            catch (AggregateException exception)
            {
                // Invoke OnUpdatingFailed event.
                this.InvokeOnMainThread(this.OnUpdatingFailed, this.Model, exception);
            }
        }

        /// <summary>
        ///     Determines whether the inner data model has been modified or is in it original status.
        /// </summary>
        /// <returns>
        ///     <see langword="true" /> if the inner data model has been modified; otherwise, <see langword="false" />.
        /// </returns>
        protected internal bool IsModelHasModified()
        {
            return this.DataEntities.Entry(this.Model).State == EntityState.Added
                   || this.DataEntities.Entry(this.Model).State == EntityState.Deleted
                   || this.DataEntities.Entry(this.Model).State == EntityState.Detached
                   || this.DataEntities.Entry(this.Model).State == EntityState.Modified;
        }

        /// <summary>
        ///     Releases unmanaged and - optionally - managed resources.
        /// </summary>
        /// <param name="disposing">
        ///     <see langword="true" /> to release both managed and unmanaged resources; <see langword="false" /> to release only
        ///     unmanaged resources.
        /// </param>
        /// <exception cref="ObjectDisposedException">This <see cref="CancellationTokenSource" /> has been disposed.</exception>
        /// <exception cref="AggregateException">
        ///     An aggregate exception containing all the exceptions thrown by the registered
        ///     callbacks on the associated <see cref="CancellationToken" />.
        /// </exception>
        protected virtual void Dispose(bool disposing)
        {
            if (this.disposed)
            {
                return;
            }

            if (disposing)
            {
                // Cancel every running task.
                using (var cancellationTokenSource = new CancellationTokenSource())
                {
                    cancellationTokenSource.Cancel();
                    this.CancellationToken = cancellationTokenSource.Token;
                }

                // Dispose the data context object.
                this.DataEntities.Dispose();
            }

            this.disposed = true;
        }

        /// <summary>
        ///     Ask user to confirm deleting the data model by using a confirmation message.
        /// </summary>
        /// <param name="deletingModel">The data model to be deleted</param>
        /// <returns>
        ///     <see langword="true" /> if the user confirmed deleting data model; otherwise, <see langword="false" />.
        /// </returns>
        protected virtual bool IsUserConfirmedDeletingModel([NotNull] T deletingModel)
        {
            return false;
        }

        /// <summary>
        ///     Roll the user changes back and set every model back to its original values.
        /// </summary>
        protected virtual void RollBackDataChanges()
        {
            var changeTracker = this.DataEntities.ChangeTracker;
            var dataEntries = changeTracker.Entries<T>();
            var changedEntries = dataEntries.Where(dataEntry => dataEntry.State == EntityState.Unchanged);
            foreach (var entry in changedEntries)
            {
                switch (entry.State)
                {
                    case EntityState.Modified:
                        entry.CurrentValues.SetValues(entry.OriginalValues);
                        entry.State = EntityState.Unchanged;
                        break;

                    case EntityState.Added:
                        entry.State = EntityState.Detached;
                        break;

                    case EntityState.Deleted:
                        entry.State = EntityState.Unchanged;
                        break;
                }
            }
        }

        /// <summary>
        ///     Deletes the passed data <paramref name="model" /> asynchronously.
        /// </summary>
        /// <param name="model">The model to delete</param>
        /// <returns>
        ///     A <see cref="Task" /> represents the deleting operation.
        /// </returns>
        private async Task DeleteModelDataAsync([NotNull] T model)
        {
            try
            {
                // Invoke OnDeletingStarted event.
                this.InvokeOnMainThread(this.OnDeletingStarted, model);

                // Remove children entities from the main entity.
                if (model.IsHierarchicallyEntity)
                {
                    await this.DeleteSubEntitiesAsync(((IHierarchicallyEntity)model).SubEntities).ConfigureAwait(false);
                }

                // Delete the main entity.
                this.Entities.Remove(model);
                this.DataEntities.Entry(model).State = EntityState.Deleted;

                // Save changes to the database asynchronously.
                await this.DataEntities.SaveChangesAsync(this.CancellationToken).ContinueWith(
                    saveChangesTask =>
                        {
                            // Invoke OnDeletingSucceeded event.
                            if (saveChangesTask.Result > 0)
                            {
                                this.InvokeOnMainThread(this.OnDeletingSucceeded, model);
                            }
                        },
                    this.CancellationToken,
                    TaskContinuationOptions.OnlyOnRanToCompletion,
                    TaskScheduler.Current).ConfigureAwait(false);
            }
            catch (AggregateException exception)
            {
                // Invoke OnDeletingFailed event.
                this.InvokeOnMainThread(this.OnDeletingFailed, model, exception);
            }
        }

        /// <summary>
        ///     Deletes every sub entity recursively.
        /// </summary>
        /// <param name="subEntities">The sub entities.</param>
        /// <returns>
        ///     A <see cref="Task" /> represents the deleting operation.
        /// </returns>
        private async Task DeleteSubEntitiesAsync([NotNull] IEnumerable<IEntity> subEntities)
        {
            foreach (var subEntity in subEntities)
            {
                // Loop to catch the last sub entity recursively.
                if (subEntity.IsHierarchicallyEntity)
                {
                    await this.DeleteSubEntitiesAsync(((IHierarchicallyEntity)subEntity).SubEntities)
                        .ConfigureAwait(false);
                }

                // Deletes singular items.
                var entityToBeRemoved = subEntity as T ?? throw new InvalidOperationException();
                this.Entities.Remove(entityToBeRemoved);
                this.DataEntities.Entry(entityToBeRemoved).State = EntityState.Deleted;
            }
        }

        /// <summary>
        ///     Invokes an event on the main thread.
        /// </summary>
        /// <param name="eventToInvoke">The event to invoke.</param>
        /// <param name="manipulatedDataModel">The manipulated data model.</param>
        /// <param name="dataException">Information about the occurred exception.</param>
        private void InvokeOnMainThread(
            [CanBeNull] Delegate eventToInvoke,
            [CanBeNull] T manipulatedDataModel = null,
            [CanBeNull] AggregateException dataException = null)
        {
            // Return if the event does not have implementation.
            if (eventToInvoke == null)
            {
                return;
            }

            // Execute the event on the main thread.
            Application.Current.Dispatcher.Invoke(
                () =>
                    {
                        if (dataException == null)
                        {
                            eventToInvoke.DynamicInvoke(
                                this,
                                new OnSaveChangesEventArgs { Entity = manipulatedDataModel ?? this.Model });
                        }
                        else
                        {
                            eventToInvoke.DynamicInvoke(
                                this,
                                new OnSaveChangesFailedEventArgs
                                    {
                                        Exception = dataException.Flatten(),
                                        Entity = manipulatedDataModel ?? this.Model
                                    });
                        }
                    });
        }
    }
}