﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="GenericEventArgs.cs" company="None">
// Last Modified: 16/11/2018
// Author: Ahmed Ghanam.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace WordsBooklet.BusinessLayer.Infrastructure
{
    using System;

    /// <inheritdoc />
    /// <summary>
    ///     Represents a bridge delivering generic type as event arguments.
    /// </summary>
    /// <typeparam name="T">The event data type</typeparam>
    /// <seealso cref="EventArgs" />
    internal class GenericEventArgs<T> : EventArgs
    {
        /// <inheritdoc />
        /// <summary>
        ///     Initializes a new instance of the <see cref="GenericEventArgs{T}" /> class.
        /// </summary>
        /// <param name="eventData">The event data argument.</param>
        internal GenericEventArgs(T eventData)
        {
            this.EventData = eventData;
        }

        /// <summary>
        ///     Gets the event data argument.
        /// </summary>
        /// <value>
        ///     The event data argument.
        /// </value>
        internal T EventData { get; }
    }
}