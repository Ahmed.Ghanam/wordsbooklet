﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Authentication.cs" company="None">
// Last Modified: 27/09/2018
// Author: Ahmed Ghanam.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace WordsBooklet.BusinessLayer.Infrastructure
{
    using System;
    using System.IO;
    using System.Net;
    using System.Text;
    using System.Threading;
    using System.Threading.Tasks;

    /// <inheritdoc />
    /// <summary>
    ///     Gathering the required authentication for using the text to speech service.
    /// </summary>
    /// <seealso cref="IDisposable" />
    internal sealed class Authentication : IDisposable
    {
        /// <summary>
        ///     The refresh duration for renewing the access token.
        /// </summary>
        private const int RefreshDuration = 9;

        /// <summary>
        ///     The text to speech service URL.
        /// </summary>
        private const string ServiceUrl = "https://api.cognitive.microsoft.com/sts/v1.0/issueToken";

        /// <summary>
        ///     A timer to renew the access token.
        /// </summary>
        [NotNull]
        private readonly Timer accessTokenTimer;

        /// <summary>
        ///     The authentication key for using the text to speech service.
        /// </summary>
        [NotNull]
        private readonly string authenticationKey;

        /// <summary>
        ///     Flag: Has <c>Dispose()</c> already been called ?
        /// </summary>
        private bool disposed;

        /// <summary>
        ///     The access token for using the text to speech service.
        /// </summary>
        [CanBeNull]
        private string serviceAccessToken;

        /// <summary>
        ///     Initializes a new instance of the <see cref="Authentication" /> class.
        /// </summary>
        /// <param name="authenticationKey">The authentication key for using the text to speech service.</param>
        /// <exception cref="OverflowException">
        ///     the call back value is less than <see cref="TimeSpan.MinValue" /> or greater than <see cref="TimeSpan.MaxValue" />
        ///     .-or- the call back value is <see cref="Double.PositiveInfinity" />.-or- the call back value is
        ///     <see cref="Double.NegativeInfinity" />.
        /// </exception>
        /// <exception cref="ArgumentException"> the call back value is equal to <see cref="Double.NaN" />. </exception>
        /// <exception cref="ArgumentOutOfRangeException">
        ///     The number of milliseconds in the value of call back value is negative
        ///     and not equal to <see cref="Timeout.Infinite" />, or is greater than <see cref="Int32.MaxValue" />.
        /// </exception>
        /// <exception cref="ArgumentNullException">The call back value parameter is <see langword="null" />. </exception>
        internal Authentication([NotNull] string authenticationKey)
        {
            // Initialize the authentication key.
            this.authenticationKey = authenticationKey;

            // Initialize the re-newer timer.
            this.accessTokenTimer = new Timer(
                this.OnAccessTokenExpired,
                this,
                TimeSpan.FromMinutes(RefreshDuration),
                TimeSpan.FromMilliseconds(-1));
        }

        /// <summary>
        ///     Finalizes an instance of the <see cref="Authentication" /> class.
        /// </summary>
        ~Authentication()
        {
            this.Dispose(false);
        }

        /// <inheritdoc />
        /// <summary>
        ///     Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            // Dispose of unmanaged resources.
            this.Dispose(true);

            // Suppress finalization.
            GC.SuppressFinalize(this);
        }

        /// <summary>
        ///     Reads the access token for using the text to speech services.
        /// </summary>
        /// <returns>
        ///     The rights to access the service, otherwise; <see cref="string.Empty" />
        /// </returns>
        internal async Task<string> ReadAccessTokenAsync()
        {
            // Initialize the access token only once.
            if (string.IsNullOrEmpty(this.serviceAccessToken))
            {
                await this.GetAccessTokenAsync().ContinueWith(
                    accessTokenTask => { this.serviceAccessToken = accessTokenTask.Result; },
                    TaskContinuationOptions.OnlyOnRanToCompletion).ConfigureAwait(false);
            }

            // Return the initialized access token.
            return !string.IsNullOrWhiteSpace(this.serviceAccessToken) ? this.serviceAccessToken : string.Empty;
        }

        /// <summary>
        ///     Releases unmanaged and - optionally - managed resources.
        /// </summary>
        /// <param name="disposing">
        ///     <see langword="true" /> to release both managed and unmanaged resources;
        ///     <see langword="false" /> to release only unmanaged resources.
        /// </param>
        private void Dispose(bool disposing)
        {
            if (this.disposed)
            {
                return;
            }

            if (disposing)
            {
                this.accessTokenTimer.Dispose();
            }

            this.disposed = true;
        }

        /// <summary>
        ///     Gets the access token for using the cognitive services.
        /// </summary>
        /// <exception cref="ArgumentNullException">
        ///     The GetResponseStream is <see langword="null" />.
        /// </exception>
        /// <returns>
        ///     The rights to access the cognitive services, otherwise; <see cref="string.Empty" />
        /// </returns>
        private async Task<string> GetAccessTokenAsync()
        {
            // Prepare WebRequest request.
            var webRequest = WebRequest.Create(ServiceUrl);
            webRequest.Method = "POST";
            webRequest.ContentLength = 0;
            webRequest.Headers["Ocp-Apim-Subscription-Key"] = this.authenticationKey;

            using (var webResponse = await webRequest.GetResponseAsync().ConfigureAwait(false))
            {
                using (var responseStream = webResponse.GetResponseStream())
                {
                    if (responseStream == null)
                    {
                        throw new ArgumentNullException();
                    }

                    using (var memoryStream = new MemoryStream())
                    {
                        int count;

                        do
                        {
                            var buff = new byte[1024];
                            count = await responseStream.ReadAsync(buff, 0, 1024).ConfigureAwait(false);
                            memoryStream.Write(buff, 0, count);
                        }
                        while (responseStream.CanRead && count > 0);

                        return Encoding.UTF8.GetString(memoryStream.ToArray());
                    }
                }
            }
        }

        /// <summary>
        ///     Called when the access token is almost expired.
        /// </summary>
        /// <param name="stateInfo">The state information.</param>
        private void OnAccessTokenExpired([CanBeNull] object stateInfo)
        {
            try
            {
                this.GetAccessTokenAsync().ContinueWith(
                    accessTokenTask => { this.serviceAccessToken = accessTokenTask.Result; },
                    TaskContinuationOptions.OnlyOnRanToCompletion).ConfigureAwait(false);
            }
            finally
            {
                this.accessTokenTimer.Change(TimeSpan.FromMinutes(RefreshDuration), TimeSpan.FromMilliseconds(-1));
            }
        }
    }
}