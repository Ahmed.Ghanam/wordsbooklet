﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CognitiveServices.cs" company="None">
// Last Modified: 28/09/2018
// Author: Ahmed Ghanam.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace WordsBooklet.BusinessLayer.Infrastructure
{
    using System;
    using System.Net.Http;
    using System.Threading;
    using System.Threading.Tasks;
    using System.Windows;

    /// <inheritdoc />
    /// <summary>
    ///     Sends requests and handles results using the cognitive services.
    /// </summary>
    /// <seealso cref="IDisposable" />
    internal sealed class CognitiveServices : IDisposable
    {
        /// <summary>
        ///     The authentication handler instance.
        /// </summary>
        [NotNull]
        private readonly Authentication authenticationHandler;

        /// <summary>
        ///     The voice synthesis instance.
        /// </summary>
        [NotNull]
        private readonly VoiceSynthesis voiceSynthesis;

        /// <summary>
        ///     The cancellation token.
        /// </summary>
        private CancellationToken cancellationToken;

        /// <summary>
        ///     Flag: Has <c>Dispose()</c> already been called ?
        /// </summary>
        private bool disposed;

        /// <summary>
        ///     Initializes a new instance of the <see cref="CognitiveServices" /> class.
        /// </summary>
        /// <exception cref="OverflowException">
        ///     the call back value is less than <see cref="TimeSpan.MinValue" />
        ///     or greater than <see cref="TimeSpan.MaxValue" />
        ///     .-or-
        ///     the call back value is <see cref="double.PositiveInfinity" />.-or- the call back value is
        ///     <see cref="double.NegativeInfinity" />.
        /// </exception>
        /// <exception cref="ArgumentNullException">The call back value parameter is <see langword="null" />. </exception>
        /// <exception cref="ArgumentException"> the call back value is equal to <see cref="double.NaN" />. </exception>
        /// <exception cref="ArgumentOutOfRangeException">
        ///     The number of milliseconds in the value of call back value is negative
        ///     and not equal to <see cref="Timeout.Infinite" />, or is greater than <see cref="int.MaxValue" />.
        /// </exception>
        internal CognitiveServices()
        {
            this.voiceSynthesis = new VoiceSynthesis();
            this.authenticationHandler = new Authentication(
                CommonSettings.CognitiveServicesKey ?? throw new InvalidOperationException());
        }

        /// <summary>
        ///     Finalizes an instance of the <see cref="CognitiveServices" /> class.
        /// </summary>
        ~CognitiveServices()
        {
            this.Dispose(false);
        }

        /// <summary>
        ///     Occurs if an error has been occurred. e.g could be an HTTP error.
        /// </summary>
        internal event EventHandler<GenericEventArgs<Exception>> OnErrorOccurred;

        /// <inheritdoc />
        /// <summary>
        ///     Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            // Dispose of unmanaged resources.
            this.Dispose(true);

            // Suppress finalization.
            GC.SuppressFinalize(this);
        }

        /// <summary>
        ///     Converts a <paramref name="text" /> to a <see langword="byte" /> array asynchronously.
        /// </summary>
        /// <param name="text">The text to be converted.</param>
        /// <param name="speakingRate">The speaking rate.</param>
        /// <param name="pronunciationLanguage">The pronunciation language.</param>
        /// <returns>
        ///     A <see cref="Task{Result}" /> represents the pronunciation and conversion operations.
        /// </returns>
        [ItemCanBeNull]
        internal async Task<byte[]> ConvertToSoundAsync(
            [NotNull] string text,
            [NotNull] string speakingRate,
            Languages pronunciationLanguage)
        {
            // Hold the results of the conversion task.
            var conversionResult = Array.Empty<byte>();

            try
            {
                // Builds the http request.
                await this.GetRequestOptionsAsync(text, speakingRate, pronunciationLanguage).ContinueWith(
                    async httpRequestTask =>
                        {
                            // Throw exception if the user canceled the task.
                            this.cancellationToken.ThrowIfCancellationRequested();

                            // Sends the http request and get the sound stream.
                            conversionResult = await this.voiceSynthesis.SendRequestAsync(httpRequestTask.Result)
                                                   .ConfigureAwait(false);
                        },
                    TaskContinuationOptions.OnlyOnRanToCompletion).Unwrap().ConfigureAwait(false);
            }
            catch (HttpRequestException exception)
            {
                this.ExecuteOnErrorOccurredEvent(exception);
            }
            catch (AggregateException exception)
            {
                this.ExecuteOnErrorOccurredEvent(exception.Flatten());
            }
            catch (SystemException exception)
            {
                this.ExecuteOnErrorOccurredEvent(exception);
            }

            // Return the conversion process results.
            return conversionResult;
        }

        /// <summary>
        ///     Releases unmanaged and - optionally - managed resources.
        /// </summary>
        /// <param name="disposing">
        ///     <see langword="true" /> to release both managed and unmanaged resources; <see langword="false" /> to release only
        ///     unmanaged resources.
        /// </param>
        private void Dispose(bool disposing)
        {
            if (this.disposed)
            {
                return;
            }

            if (disposing)
            {
                // Cancel every running task.
                using (var cancellationTokenSource = new CancellationTokenSource())
                {
                    cancellationTokenSource.Cancel();
                    this.cancellationToken = cancellationTokenSource.Token;
                }

                // Dispose main instances.
                this.voiceSynthesis.Dispose();
                this.authenticationHandler.Dispose();
            }

            this.disposed = true;
        }

        /// <summary>
        ///     Executes the <see cref="OnErrorOccurred" /> event on the main UI thread.
        /// </summary>
        /// <param name="exception">The occurred <paramref name="exception" />.</param>
        private void ExecuteOnErrorOccurredEvent([NotNull] Exception exception)
        {
            Application.Current.Dispatcher.Invoke(
                () => { this.OnErrorOccurred?.Invoke(this, new GenericEventArgs<Exception>(exception)); });
        }

        /// <summary>
        ///     Gets the HTTP request options asynchronously.
        /// </summary>
        /// <param name="text">The <paramref name="text" /> to be pronounced.</param>
        /// <param name="speakingRate">The speaking rate.</param>
        /// <param name="pronunciationLanguage">The pronunciation language.</param>
        /// <returns>
        ///     A <see cref="Task{Result}" /> represents the HTTP request.
        /// </returns>
        [NotNull]
        private Task<RequestOptions> GetRequestOptionsAsync(
            [NotNull] string text,
            [NotNull] string speakingRate,
            Languages pronunciationLanguage)
        {
            // Throw exception if the user canceled the task.
            this.cancellationToken.ThrowIfCancellationRequested();

            // Reads the access token for using the text to speech services.
            return this.authenticationHandler.ReadAccessTokenAsync().ContinueWith(
                accessTokenTask => new RequestOptions(pronunciationLanguage)
                                       {
                                           // Text to be spoken.
                                           Text = text,

                                           // The speech speaking rate.
                                           SpeakingRate = $"{speakingRate}%",

                                           // The access token.
                                           AuthorizationToken =
                                               "Bearer " + accessTokenTask.Result
                                       },
                TaskContinuationOptions.OnlyOnRanToCompletion);
        }
    }
}