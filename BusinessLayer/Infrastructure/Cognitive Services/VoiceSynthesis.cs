// --------------------------------------------------------------------------------------------------------------------
// <copyright file="VoiceSynthesis.cs" company="None">
// Last Modified: 27/09/2018
// Author: Ahmed Ghanam.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace WordsBooklet.BusinessLayer.Infrastructure
{
    using System;
    using System.Net;
    using System.Net.Http;
    using System.Threading;
    using System.Threading.Tasks;
    using System.Xml.Linq;

    /// <inheritdoc />
    /// <summary>
    ///     Represents methods to synthesizing texts to a sound streams using the text to speech service.
    /// </summary>
    /// <seealso cref="IDisposable" />
    internal sealed class VoiceSynthesis : IDisposable
    {
        /// <summary>
        ///     The http request client.
        /// </summary>
        [NotNull]
        private readonly HttpClient httpClient;

        /// <summary>
        ///     The http request client handler.
        /// </summary>
        [NotNull]
        private readonly HttpClientHandler httpClientHandler;

        /// <summary>
        ///     The cancellation token.
        /// </summary>
        private CancellationToken cancellationToken;

        /// <summary>
        ///     Flag: Has <c>Dispose()</c> already been called ?
        /// </summary>
        private bool disposed;

        /// <summary>
        ///     Initializes a new instance of the <see cref="VoiceSynthesis" /> class.
        /// </summary>
        internal VoiceSynthesis()
        {
            this.httpClientHandler =
                new HttpClientHandler { UseProxy = false, CookieContainer = new CookieContainer() };
            this.httpClient = new HttpClient(this.httpClientHandler);
        }

        /// <summary>
        ///     Finalizes an instance of the <see cref="VoiceSynthesis" /> class.
        /// </summary>
        ~VoiceSynthesis()
        {
            this.Dispose(false);
        }

        /// <inheritdoc />
        /// <summary>
        ///     Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            // Dispose of unmanaged resources.
            this.Dispose(true);

            // Suppress finalization.
            GC.SuppressFinalize(this);
        }

        /// <summary>
        ///     Sends a request to the text to speech service and return an audio stream as a
        ///     <see langword="byte" /> array asynchronously.
        /// </summary>
        /// <param name="requestOptions">The request options.</param>
        /// <returns>
        ///     an audio stream as a <see langword="byte" /> array.
        /// </returns>
        /// <exception cref="HttpRequestException">
        ///     The request failed due to an underlying issue such as network connectivity,
        ///     server certificate validation or timeout.
        /// </exception>
        [ItemCanBeNull]
        internal async Task<byte[]> SendRequestAsync([NotNull] RequestOptions requestOptions)
        {
            // The initial return value.
            byte[] pronunciationBytes = null;

            // Initialize the http request header.
            this.InitializeRequestHeaders(requestOptions);

            // Initialize the http request message.
            using (var httpRequestMessage = InitializeRequestMessage(requestOptions))
            {
                await this.httpClient.SendAsync(
                    httpRequestMessage,
                    HttpCompletionOption.ResponseHeadersRead,
                    this.cancellationToken).ContinueWith(
                    async httpRequestTask =>
                        {
                            if (httpRequestTask.Result.IsSuccessStatusCode)
                            {
                                pronunciationBytes = await httpRequestTask.Result.Content.ReadAsByteArrayAsync()
                                                         .ConfigureAwait(false);
                            }
                        },
                    this.cancellationToken,
                    TaskContinuationOptions.OnlyOnRanToCompletion,
                    TaskScheduler.Current).Unwrap().ConfigureAwait(false);
            }

            // Return the http client request results.
            return pronunciationBytes ?? throw new InvalidOperationException();
        }

        /// <summary>
        ///     Initializes the request contents 'The JSON Requests'.
        /// </summary>
        /// <param name="requestOptions">The request options.</param>
        /// <returns>
        ///     A <see cref="String" /> represents the JSON contents.
        /// </returns>
        [NotNull]
        private static string InitializeRequestContents([NotNull] RequestOptions requestOptions)
        {
            // The speak node.
            var xmlDocument = new XDocument(
                new XElement(
                    "speak",
                    new XAttribute("version", "1.0"),
                    new XAttribute(XNamespace.Xml + "lang", "en-US"),
                    new XElement(
                        "voice",
                        new XAttribute(XNamespace.Xml + "lang", requestOptions.CultureName),
                        new XAttribute(XNamespace.Xml + "gender", new { requestOptions.VoiceGender }),
                        new XAttribute("name", requestOptions.VoiceName),
                        new XElement(
                            "prosody",
                            new XAttribute("rate", requestOptions.SpeakingRate),
                            requestOptions.Text))));

            // Return the full XDocument as string.
            return Convert.ToString(xmlDocument);
        }

        /// <summary>
        ///     Initializes the HTTP request message.
        /// </summary>
        /// <param name="requestOptions">The request options.</param>
        /// <returns>
        ///     A <see cref="HttpRequestMessage" /> represents the request message.
        /// </returns>
        [NotNull]
        private static HttpRequestMessage InitializeRequestMessage([NotNull] RequestOptions requestOptions)
        {
            return new HttpRequestMessage(HttpMethod.Post, requestOptions.RequestUri)
                       {
                           Content = new StringContent(InitializeRequestContents(requestOptions))
                       };
        }

        /// <summary>
        ///     Releases unmanaged and - optionally - managed resources.
        /// </summary>
        /// <param name="disposing">
        ///     <c>true</c> to release both managed and unmanaged resources; <c>false</c> to release only
        ///     unmanaged resources.
        /// </param>
        private void Dispose(bool disposing)
        {
            if (this.disposed)
            {
                return;
            }

            if (disposing)
            {
                // Cancel any running task.
                using (var cancellationTokenSource = new CancellationTokenSource())
                {
                    cancellationTokenSource.Cancel();
                    this.cancellationToken = cancellationTokenSource.Token;
                }

                // Dispose main instances.
                this.httpClient.Dispose();
                this.httpClientHandler.Dispose();
            }

            this.disposed = true;
        }

        /// <summary>
        ///     Initializes the http request headers.
        /// </summary>
        /// <param name="requestOptions">The request options.</param>
        private void InitializeRequestHeaders([NotNull] RequestOptions requestOptions)
        {
            // Clear previous headers.
            this.httpClient.DefaultRequestHeaders.Clear();

            // Add new headers.
            foreach (var header in requestOptions.Headers)
            {
                this.httpClient.DefaultRequestHeaders.TryAddWithoutValidation(header.Key, header.Value);
            }
        }
    }
}