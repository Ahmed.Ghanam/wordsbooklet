// --------------------------------------------------------------------------------------------------------------------
// <copyright file="RequestOptions.cs" company="None">
// Last Modified: 28/09/2018
// Author: Ahmed Ghanam.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace WordsBooklet.BusinessLayer.Infrastructure
{
    using System;
    using System.Collections.Generic;

    using Microsoft.VisualStudio.DataTools;

    /// <summary>
    ///     Represents the inputs and output options of the text to speech service requests.
    /// </summary>
    internal sealed class RequestOptions
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="RequestOptions" /> class.
        /// </summary>
        /// <param name="spokenLanguage">The language used for uttering the <see cref="Text" />.</param>
        /// <exception cref="UnexpectedException">Unknown <paramref name="spokenLanguage" />.</exception>
        internal RequestOptions(Languages spokenLanguage)
        {
            switch (spokenLanguage)
            {
                case Languages.Arabic:
                    this.CultureName = "ar-EG";
                    this.VoiceGender = Gender.Female;
                    this.VoiceName = "Microsoft Server Speech Text to Speech Voice (ar-EG, Hoda)";
                    break;

                case Languages.Bokm�l:
                    this.CultureName = "nb-NO";
                    this.VoiceGender = Gender.Female;
                    this.VoiceName = "Microsoft Server Speech Text to Speech Voice (nb-NO, HuldaRUS)";
                    break;

                default:
                    throw new UnexpectedException(nameof(spokenLanguage));
            }
        }

        /// <summary>
        ///     Gets or sets the authorization token for using the text to speech service.
        /// </summary>
        /// <value>
        ///     The authorization token for using the text to speech service.
        /// </value>
        [CanBeNull]
        internal string AuthorizationToken { private get; set; }

        /// <summary>
        ///     Gets the uttering culture name.
        /// </summary>
        /// <value>
        ///     The uttering culture name.
        /// </value>
        [NotNull]
        internal string CultureName { get; }

        /// <summary>
        ///     Gets the JSON request headers.
        /// </summary>
        /// <value>
        ///     The JSON request headers.
        /// </value>
        [NotNull]
        internal IEnumerable<KeyValuePair<string, string>> Headers
        {
            get
            {
                // The default headers attributes.
                var headerPairs = new List<KeyValuePair<string, string>>
                                      {
                                          new KeyValuePair<string, string>(
                                              "Content-Type",
                                              "application/ssml+xml"),

                                          // Attach the defined audio output format.
                                          new KeyValuePair<string, string>(
                                              "X-Microsoft-OutputFormat",
                                              this.OutputFormat),

                                          // Attach the defined authorization header.
                                          new KeyValuePair<string, string>(
                                              "Authorization",
                                              this.AuthorizationToken),

                                          // Attach a unique identifier for the application.
                                          new KeyValuePair<string, string>(
                                              "X-Search-AppId",
                                              "9AB23829D23844E3B2A089BECD2CC559"),

                                          // Attach a unique identifier for the client.
                                          new KeyValuePair<string, string>(
                                              "X-Search-ClientID",
                                              "8C1FD75A44044688B9AB0A52071472CA"),

                                          // The software originating the request
                                          new KeyValuePair<string, string>(
                                              "User-Agent",
                                              "WordsBooklet")
                                      };

                // Return the header.
                return headerPairs;
            }
        }

        /// <summary>
        ///     Gets the URI for handling the uttering requests.
        /// </summary>
        /// <value>
        ///     The URI for handling the uttering requests.
        /// </value>
        [CanBeNull]
        internal Uri RequestUri { get; } = new Uri("https://speech.platform.bing.com/synthesize");

        /// <summary>
        ///     Gets or sets the speaking rate of the uttering.
        /// </summary>
        /// <value>
        ///     The speaking rate of the uttering.
        /// </value>
        [NotNull]
        internal string SpeakingRate { get; set; } = "0.00%";

        /// <summary>
        ///     Gets or sets the text to be converted to speech.
        /// </summary>
        /// <value>
        ///     The text to be converted to speech.
        /// </value>
        [CanBeNull]
        internal string Text { get; set; }

        /// <summary>
        ///     Gets the gender of the uttering voice.
        /// </summary>
        /// <value>
        ///     The gender of the uttering voice.
        /// </value>
        internal Gender VoiceGender { get; }

        /// <summary>
        ///     Gets the name of the uttering voice.
        /// </summary>
        /// <value>
        ///     The name of the uttering voice.
        /// </value>
        [NotNull]
        internal string VoiceName { get; }

        /// <summary>
        ///     Gets the audio output format which is not streaming.
        /// </summary>
        /// <value>
        ///     The audio output format which is not streaming.
        /// </value>
        [NotNull]
        private string OutputFormat { get; } = "riff-16khz-16bit-mono-pcm";
    }
}