﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="DesktopAlertService.cs" company="None">
// Last Modified: 30/09/2018
// Author: Ahmed Ghanam.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace WordsBooklet.BusinessLayer.Infrastructure
{
    using System;
    using System.Linq;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Media;

    using Telerik.Windows.Controls;

    /// <inheritdoc />
    /// <summary>
    ///     Represents methods for organizing displaying user alerts.
    /// </summary>
    /// <seealso cref="IDisposable" />
    internal sealed class DesktopAlertService : IDisposable
    {
        /// <summary>
        ///     The desktop alert manager instance.
        /// </summary>
        [NotNull]
        private readonly RadDesktopAlertManager desktopAlertManager;

        /// <summary>
        ///     The desktop alert instance.
        /// </summary>
        [CanBeNull]
        private RadDesktopAlert desktopAlert;

        /// <summary>
        ///     Flag: Has <c>Dispose()</c> already been called ?
        /// </summary>
        private bool disposed;

        /// <summary>
        ///     Initializes a new instance of the <see cref="DesktopAlertService" /> class.
        /// </summary>
        public DesktopAlertService()
        {
            this.desktopAlertManager =
                new RadDesktopAlertManager(CommonSettings.AlertScreenPosition) { AlertsDistance = 3 };
        }

        /// <summary>
        ///     Finalizes an instance of the <see cref="DesktopAlertService" /> class.
        /// </summary>
        ~DesktopAlertService()
        {
            this.Dispose(false);
        }

        /// <summary>
        ///     Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        ///     Displays a desktop alert.
        /// </summary>
        /// <param name="title">The desktop alert <paramref name="title" />.</param>
        /// <param name="contents">The desktop alert <paramref name="contents" />.</param>
        /// <param name="type">The desktop alert <paramref name="type" />.</param>
        internal void DisplayDesktopAlert([NotNull] string title, [NotNull] string contents, AlertTypes type)
        {
            // Avoid duplicate desktop alerts..
            if (this.IsAlreadyDisplayed(title, contents))
            {
                return;
            }

            // Build the alert icon.
            var alertImage = new Image
                                 {
                                     Width = 48,
                                     Height = 48,
                                     Source = Application.Current.FindResource("DesktopAlertIcon") as ImageSource
                                 };

            // Initialize the alert instance.
            this.desktopAlert = new RadDesktopAlert
                                    {
                                        Height = 95,
                                        Icon = alertImage,
                                        ShowDuration = 7500,
                                        IconColumnWidth = 58,
                                        Header = title.Trim(),
                                        Content = contents.Trim(),
                                        IconMargin = new Thickness(10, 0, 10, 0),
                                        BorderThickness = new Thickness(2, 2, 2, 0),
                                        FlowDirection = CommonSettings.FlowDirection,
                                        VerticalContentAlignment = VerticalAlignment.Stretch,
                                        BorderBrush = new SolidColorBrush(Colors.DarkTurquoise),
                                        HorizontalContentAlignment = HorizontalAlignment.Stretch
                                    };

            // Sets the alert style.
            switch (type)
            {
                case AlertTypes.Information:
                    this.desktopAlert.Style =
                        Application.Current.FindResource("RadDesktopAlertInformationStyle") as Style;
                    break;

                case AlertTypes.Success:
                    this.desktopAlert.Style = Application.Current.FindResource("RadDesktopAlertStyle") as Style;
                    break;

                case AlertTypes.Error:
                    this.desktopAlert.Style = Application.Current.FindResource("RadDesktopAlertErrorStyle") as Style;
                    break;
            }

            // Display the desktop alert.
            this.desktopAlertManager.ShowAlert(this.desktopAlert);
        }

        /// <summary>
        ///     Releases unmanaged and - optionally - managed resources.
        /// </summary>
        /// <param name="disposing">
        ///     <see langword="true" /> to release both managed and unmanaged resources; <see langword="false" /> to release only
        ///     unmanaged resources.
        /// </param>
        private void Dispose(bool disposing)
        {
            if (this.disposed)
            {
                return;
            }

            if (!disposing)
            {
                return;
            }

            this.disposed = true;
            this.desktopAlert = null;
            this.desktopAlertManager.CloseAllAlerts();
        }

        /// <summary>
        ///     Determines whether a desktop alert already displayed to the user or not.
        /// </summary>
        /// <param name="title">The desktop alert <paramref name="title" />.</param>
        /// <param name="contents">The desktop alert <paramref name="contents" />.</param>
        /// <returns>
        ///     <see langword="true" /> if a desktop alert holding the same contents and header already exist; otherwise,
        ///     <see langword="false" />.
        /// </returns>
        private bool IsAlreadyDisplayed([NotNull] string title, [NotNull] string contents)
        {
            var duplicatedAlerts = this.desktopAlertManager.GetAllAlerts().FirstOrDefault(
                desktopAlerts => Convert.ToString(desktopAlerts.Header).Equals(title.Trim())
                                 && Convert.ToString(desktopAlerts.Content).Equals(contents.Trim()));

            return duplicatedAlerts != null;
        }
    }
}