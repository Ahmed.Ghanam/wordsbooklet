﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="NotificationsLayer.cs" company="None">
// Last Modified: 01/10/2018
// Author: Ahmed Ghanam.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace WordsBooklet.BusinessLayer.Infrastructure
{
    using System;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;
    using System.Threading;
    using System.Windows;

    using Telerik.Windows.Controls;

    using WordsBooklet.BusinessLayer.Infrastructure.Notifications.Resources;

    /// <inheritdoc cref="RepositoryLayer{T}" />
    /// <summary>
    ///     Represents members for handling tasks and user notifications.
    /// </summary>
    /// <typeparam name="T">The data model type</typeparam>
    /// <seealso cref="RepositoryLayer{T}" />
    /// <seealso cref="INotifyPropertyChanged" />
    public abstract class NotificationsLayer<T> : RepositoryLayer<T>, INotifyPropertyChanged
        where T : class, IEntity, new()
    {
        /// <summary>
        ///     The desktop alert service instance.
        /// </summary>
        [CanBeNull]
        private DesktopAlertService desktopAlertService;

        /// <summary>
        ///     The back field for the <see cref="IsPerformingLongRunningTask" /> property.
        /// </summary>
        private bool isPerformingLongRunningTask;

        /// <summary>
        ///     The back field for the <see cref="LongRunningTaskDescription" /> property.
        /// </summary>
        [CanBeNull]
        private string longRunningTaskDescription;

        /// <inheritdoc />
        /// <summary>
        ///     Initializes a new instance of the <see cref="NotificationsLayer{T}" /> class.
        /// </summary>
        protected NotificationsLayer()
        {
            // Initialize OnPropertyChanged() event.
            this.PropertyChanged += this.OnPropertyChanged;

            // Initialize different events for deleting the data model.
            this.OnDeletingFailed += this.OnDeletingModelFailed;
            this.OnDeletingStarted += this.OnDeletingModelStarted;
            this.OnDeletingSucceeded += this.OnDeletingModelSucceeded;

            // Initialize different events for inserting the data model.
            this.OnInsertingFailed += this.OnInsertingModelFailed;
            this.OnInsertingStarted += this.OnInsertingModelStarted;
            this.OnInsertingSucceeded += this.OnInsertingModelSucceeded;

            // Initialize different events for updating the data model.
            this.OnUpdatingFailed += this.OnUpdatingModelFailed;
            this.OnUpdatingStarted += this.OnUpdatingModelStarted;
            this.OnUpdatingSucceeded += this.OnUpdatingModelSucceeded;
        }

        /// <inheritdoc />
        /// <summary>
        ///     Occurs when a property value changes.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        ///     Gets a value indicating whether <see langword="this" /> instance is performing a long running task or not.
        /// </summary>
        /// <value>
        ///     <see langword="true" /> if <see langword="this" /> instance is performing a long running task; otherwise,
        ///     <see langword="false" />.
        /// </value>
        public bool IsPerformingLongRunningTask
        {
            get => this.isPerformingLongRunningTask;

            internal set
            {
                this.isPerformingLongRunningTask = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets a short description about the current running task.
        /// </summary>
        /// <value>
        ///     The short description about the current running task.
        /// </value>
        [CanBeNull]
        public string LongRunningTaskDescription
        {
            get => this.longRunningTaskDescription;

            internal set
            {
                this.longRunningTaskDescription = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Displays a desktop notifications.
        /// </summary>
        /// <param name="title">The desktop alert <paramref name="title" />.</param>
        /// <param name="contents">The desktop alert <paramref name="contents" />.</param>
        /// <param name="type">The desktop alert <paramref name="type" />.</param>
        internal void DisplayDesktopAlert([NotNull] string title, [NotNull] string contents, AlertTypes type)
        {
            // Initialize the desktop alert service object.
            if (this.desktopAlertService == null)
            {
                this.desktopAlertService = new DesktopAlertService();
            }

            // Display the desktop alert.
            this.desktopAlertService.DisplayDesktopAlert(title, contents, type);
        }

        /// <inheritdoc />
        /// <summary>
        ///     Releases unmanaged and - optionally - managed resources.
        /// </summary>
        /// <param name="disposing">
        ///     <see langword="true" /> to release both managed and unmanaged resources; <see langword="false" /> to release only
        ///     unmanaged resources.
        /// </param>
        /// <exception cref="AggregateException">
        ///     An aggregate exception containing all the exceptions thrown by the registered
        ///     callbacks on the associated <see cref="CancellationToken" />.
        /// </exception>
        protected override void Dispose(bool disposing)
        {
            // Call base implementation, Always first.
            base.Dispose(disposing);

            if (!disposing)
            {
                return;
            }

            // Close any related alert.
            this.desktopAlertService?.Dispose();

            // Remove PropertyChanged() event listeners.
            this.PropertyChanged -= this.OnPropertyChanged;

            // Remove deleting data model events listeners.
            this.OnDeletingFailed -= this.OnDeletingModelFailed;
            this.OnDeletingStarted -= this.OnDeletingModelStarted;
            this.OnDeletingSucceeded -= this.OnDeletingModelSucceeded;

            // Remove inserting data model events listeners.
            this.OnInsertingFailed -= this.OnInsertingModelFailed;
            this.OnInsertingStarted -= this.OnInsertingModelStarted;
            this.OnInsertingSucceeded -= this.OnInsertingModelSucceeded;

            // Remove updating data model events listeners.
            this.OnUpdatingFailed -= this.OnUpdatingModelFailed;
            this.OnUpdatingStarted -= this.OnUpdatingModelStarted;
            this.OnUpdatingSucceeded -= this.OnUpdatingModelSucceeded;
        }

        /// <inheritdoc />
        /// <summary>
        ///     Checks if the user confirmed deleting the data model using a confirmation message or not.
        /// </summary>
        /// <param name="dataModel">The data model to delete</param>
        /// <returns>
        ///     <see langword="true" /> if the user confirmed the deleting; otherwise, <see langword="false" />.
        /// </returns>
        protected override bool IsUserConfirmedDeletingModel(T dataModel)
        {
            // Confirmation results.
            bool? isConfirmed = false;

            // Build the dialog parameter
            var dialogParameters = new DialogParameters
                                       {
                                           OkButtonContent = NotificationsLayer.YesDelete,
                                           CancelButtonContent = NotificationsLayer.Cancel,
                                           Header = NotificationsLayer.DataDeletionConfirmation,
                                           DialogStartupLocation = WindowStartupLocation.CenterScreen,
                                           Closed =
                                               (result, eventArgs) =>
                                                   isConfirmed = eventArgs.DialogResult,
                                           Content = string.Format(
                                               NotificationsLayer.DataDeletionConfirmationMessage,
                                               dataModel.Name)
                                       };

            // Set the right to left styles only if needed.
            if (CommonSettings.FlowDirection == FlowDirection.RightToLeft)
            {
                dialogParameters.WindowStyle = Application.Current.Resources["RadWindowRightToLeftStyle"] as Style;
                dialogParameters.ContentStyle = Application.Current.Resources["RadConfirmRightToLeftStyle"] as Style;
            }

            // Display the delete confirmation dialog.
            RadWindow.Confirm(dialogParameters);

            // return the confirmation result.
            return isConfirmed ?? false;
        }

        /// <summary>
        ///     Invoked if deleting the data model succeeded.
        /// </summary>
        /// <param name="sender">The control raise the event.</param>
        /// <param name="e">The <see cref="OnSaveChangesEventArgs" /> instance containing the event data.</param>
        protected virtual void OnDeletingModelSucceeded([NotNull] object sender, [NotNull] OnSaveChangesEventArgs e)
        {
            // Clear notifications.
            this.IsPerformingLongRunningTask = false;
            this.LongRunningTaskDescription = null;

            // Build the alert message.
            var title = string.Format(NotificationsLayer.DataDeletion, e.Entity?.Name);
            var content = string.Format(NotificationsLayer.DataDeletionSucceededMessage, e.Entity?.Name);

            // Display the user notification.
            this.DisplayDesktopAlert(title, content, AlertTypes.Success);
        }

        /// <summary>
        ///     Invoked if inserting the data model succeeded.
        /// </summary>
        /// <param name="sender">The control raise the event.</param>
        /// <param name="e">The <see cref="OnSaveChangesEventArgs" /> instance containing the event data.</param>
        protected virtual void OnInsertingModelSucceeded([NotNull] object sender, [NotNull] OnSaveChangesEventArgs e)
        {
            // Clear notifications.
            this.IsPerformingLongRunningTask = false;
            this.LongRunningTaskDescription = null;

            // Build the alert message.
            var title = string.Format(NotificationsLayer.DataSaving, e.Entity?.Name);
            var content = string.Format(NotificationsLayer.DataSavingSucceededMessage, e.Entity?.Name);

            // Display the user notification.
            this.DisplayDesktopAlert(title, content, AlertTypes.Success);
        }

        /// <summary>
        ///     Called when a property value changed.
        /// </summary>
        /// <param name="sender">The <paramref name="sender" />.</param>
        /// <param name="e">The <see cref="PropertyChangedEventArgs" /> instance containing the event data.</param>
        protected virtual void OnPropertyChanged([NotNull] object sender, [NotNull] PropertyChangedEventArgs e)
        {
            // To avoid multi handling on disposing pattern, the OnPropertyChanged event
            // has been initialized here to remove its listener when calling Dispose() method.
        }

        /// <summary>
        ///     Called when a property value changed.
        /// </summary>
        /// <param name="propertyName">The changed property name.</param>
        [NotifyPropertyChangedInvocator]
        protected void OnPropertyChanged([CanBeNull] [CallerMemberName] string propertyName = "")
        {
            this.PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        /// <summary>
        ///     Validates the value of the changed property.
        /// </summary>
        /// <remarks>
        ///     Launches any data annotation attributes associated with the changed property.
        /// </remarks>
        /// <param name="propertyValue">The property value.</param>
        /// <param name="propertyName">The property name.</param>
        /// <exception cref="ArgumentNullException">
        ///     If the <see cref="T" /> doesn't implement any meta data type.
        /// </exception>
        [NotifyPropertyChangedInvocator]
        protected void OnPropertyChanged(
            [CanBeNull] object propertyValue,
            [NotNull] [CallerMemberName] string propertyName = "")
        {
            // Validate the user inputs automatically.
            this.ValidatePropertyValue(propertyName, propertyValue);

            // Invoke the OnPropertyChanged() method.
            this.OnPropertyChanged(propertyName);
        }

        /// <summary>
        ///     Invoked if updating the data model succeeded.
        /// </summary>
        /// <param name="sender">The control raise the event.</param>
        /// <param name="e">The <see cref="OnSaveChangesEventArgs" /> instance containing the event data.</param>
        protected virtual void OnUpdatingModelSucceeded([NotNull] object sender, [NotNull] OnSaveChangesEventArgs e)
        {
            // Clear notifications.
            this.IsPerformingLongRunningTask = false;
            this.LongRunningTaskDescription = null;

            // Build the alert message.
            var title = string.Format(NotificationsLayer.DataUpdating, e.Entity?.Name);
            var content = string.Format(NotificationsLayer.DataUpdatingSucceededMessage, e.Entity?.Name);

            // Display the user notification.
            this.DisplayDesktopAlert(title, content, AlertTypes.Success);
        }

        /// <summary>
        ///     Invoked if SaveChanges() throws an exception on the Deleting action.
        /// </summary>
        /// <param name="sender">The control raise the event.</param>
        /// <param name="e">The <see cref="OnSaveChangesFailedEventArgs" /> instance containing the event data.</param>
        private void OnDeletingModelFailed([NotNull] object sender, [NotNull] OnSaveChangesFailedEventArgs e)
        {
            // Alert that a delete operation has failed.
            this.IsPerformingLongRunningTask = false;
            this.LongRunningTaskDescription = null;

            // Build the alert message.
            var title = string.Format(NotificationsLayer.DataDeletion, e.Entity?.Name);
            var content = string.Format(
                NotificationsLayer.DataDeletionFailedMessage,
                e.Entity?.Name,
                e.Exception?.Message);

            // Display the user notification.
            this.DisplayDesktopAlert(title, content, AlertTypes.Error);
        }

        /// <summary>
        ///     Invoked when start to delete the data model.
        /// </summary>
        /// <param name="sender">The control raise the event.</param>
        /// <param name="e">The <see cref="OnSaveChangesEventArgs" /> instance containing the event data.</param>
        private void OnDeletingModelStarted([NotNull] object sender, [NotNull] OnSaveChangesEventArgs e)
        {
            // Build short description.
            this.LongRunningTaskDescription = string.Format(
                NotificationsLayer.DataDeletionBeganMessage,
                e.Entity?.Name);

            // Alert that a delete operation has started.
            this.IsPerformingLongRunningTask = true;
        }

        /// <summary>
        ///     Invoked if SaveChanges() throws an exception on the Inserting action.
        /// </summary>
        /// <param name="sender">The control raise the event.</param>
        /// <param name="e">The <see cref="OnSaveChangesFailedEventArgs" /> instance containing the event data.</param>
        private void OnInsertingModelFailed([NotNull] object sender, [NotNull] OnSaveChangesFailedEventArgs e)
        {
            // Alert that an insert operation has failed.
            this.IsPerformingLongRunningTask = false;
            this.LongRunningTaskDescription = null;

            // Build the alert message.
            var title = string.Format(NotificationsLayer.DataSaving, e.Entity?.Name);
            var content = string.Format(
                NotificationsLayer.DataSavingFailedMessage,
                e.Entity?.Name,
                e.Exception?.Message);

            // Display the user notification.
            this.DisplayDesktopAlert(title, content, AlertTypes.Error);
        }

        /// <summary>
        ///     Invoked when start to insert the data model.
        /// </summary>
        /// <param name="sender">The control raise the event.</param>
        /// <param name="e">The <see cref="OnSaveChangesEventArgs" /> instance containing the event data.</param>
        private void OnInsertingModelStarted([NotNull] object sender, [NotNull] OnSaveChangesEventArgs e)
        {
            // Build short description.
            this.LongRunningTaskDescription = string.Format(NotificationsLayer.DataSavingBeganMessage, e.Entity?.Name);

            // Alert that an insert operation has started.
            this.IsPerformingLongRunningTask = true;
        }

        /// <summary>
        ///     Invoked if SaveChanges() throws an exception on the Updating action.
        /// </summary>
        /// <param name="sender">The control raise the event.</param>
        /// <param name="e">The <see cref="OnSaveChangesEventArgs" /> instance containing the event data.</param>
        private void OnUpdatingModelFailed([NotNull] object sender, [NotNull] OnSaveChangesFailedEventArgs e)
        {
            // Alert that an update operation has failed.
            this.IsPerformingLongRunningTask = false;
            this.LongRunningTaskDescription = null;

            // Build the alert message.
            var title = string.Format(NotificationsLayer.DataUpdating, e.Entity?.Name);
            var content = string.Format(
                NotificationsLayer.DataUpdatingFailedMessage,
                e.Entity?.Name,
                e.Exception?.Message);

            // Display the user notification.
            this.DisplayDesktopAlert(title, content, AlertTypes.Error);
        }

        /// <summary>
        ///     Invoked when start to update the data model
        /// </summary>
        /// <param name="sender">The control raise the event.</param>
        /// <param name="e">The <see cref="OnSaveChangesEventArgs" /> instance containing the event data.</param>
        private void OnUpdatingModelStarted([NotNull] object sender, [NotNull] OnSaveChangesEventArgs e)
        {
            // build short description.
            this.LongRunningTaskDescription = string.Format(
                NotificationsLayer.DataUpdatingBeganMessage,
                e.Entity?.Name);

            // Alert that an update operation has started.
            this.IsPerformingLongRunningTask = true;
        }
    }
}