﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="DialogService.cs" company="None">
// Last Modified: 08/10/2018
// Author: Ahmed Ghanam.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace WordsBooklet.BusinessLayer.Infrastructure
{
    using System;
    using System.Windows;

    using Telerik.Windows.Controls;

    /// <summary>
    ///     Represents methods using a <seealso cref="RadWindow" /> <see cref="Type" /> as a dialog.
    /// </summary>
    internal static class DialogService
    {
        /// <summary>
        ///     Represents the dialog result of displayed <seealso cref="RadWindow" /> as a dialog.
        /// </summary>
        [CanBeNull]
        private static bool? dialogResult;

        /// <summary>
        ///     Represents the displayed <seealso cref="RadWindow" /> as a dialog.
        /// </summary>
        [CanBeNull]
        private static RadWindow dialogWindow;

        /// <summary>
        ///     Gets the selected Model identifier from the displayed dialog.
        /// </summary>
        /// <value>
        ///     The selected Model identifier from the displayed dialog.
        /// </value>
        internal static long SelectedModelId { get; private set; }

        /// <summary>
        ///     Closes the currently <seealso cref="RadWindow" /> which is displayed as dialog.
        /// </summary>
        /// <param name="chosenModelId">The chosen Model identifier.</param>
        /// <exception cref="ArgumentNullException">
        ///     The <seealso cref="RadWindow" /> is null.
        /// </exception>
        internal static void CloseDialog(long chosenModelId)
        {
            if (dialogWindow != null)
            {
                // Set the chosen Model identifier.
                SelectedModelId = chosenModelId;

                // Close the dialog window itself.
                dialogWindow.Close();

                // Dispose the DataContext object if it implements the IDisposable.
                (dialogWindow.DataContext as IDisposable)?.Dispose();

                // Save the dialog result.
                dialogResult = dialogWindow.DialogResult;

                // Remove the dialog window.
                dialogWindow = null;
            }
            else
            {
                throw new ArgumentNullException();
            }
        }

        /// <summary>
        ///     Displays a <seealso cref="RadWindow" /> as a dialog.
        /// </summary>
        /// <param name="dialogType">The <seealso cref="Type" /> represents the dialog window.</param>
        /// <returns>
        ///     <see langword="true" /> if the user accepted the dialog, otherwise; <see langword="false" />.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        ///     The passed type is not a <seealso cref="RadWindow" />.
        /// </exception>
        internal static bool? ShowDialog([NotNull] Type dialogType)
        {
            // Using the passed type for creating a window instance.
            dialogWindow = CreateDialogWindow(dialogType);
            if (dialogWindow == null)
            {
                return null;
            }

            // Reset the value of SelectedModelId.
            SelectedModelId = 0;

            // Reset the value of dialogResult.
            dialogResult = null;

            // Display the RadWindow as a dialog.
            dialogWindow.ShowDialog();

            // Return the dialog results whenever the user closes the dialog.
            return dialogResult;
        }

        /// <summary>
        ///     Creates a dialog using a window <seealso cref="Type" />.
        /// </summary>
        /// <param name="dialogType">
        ///     The <seealso cref="Type" /> represents the <see cref="RadWindow" />.
        /// </param>
        /// <exception cref="ArgumentNullException">
        ///     The passed type is not a <seealso cref="RadWindow" />.
        /// </exception>
        /// <returns>
        ///     An instance of the created <seealso cref="RadWindow" />, otherwise; throws an exception.
        /// </returns>
        [CanBeNull]
        private static RadWindow CreateDialogWindow([NotNull] Type dialogType)
        {
            // Create RadWindow instance of the passed type.
            if (!(Activator.CreateInstance(dialogType) is RadWindow windowInstance))
            {
                throw new ArgumentNullException();
            }

            // Set the RadWindow owner and style.
            windowInstance.Owner = Application.Current.MainWindow;
            windowInstance.FlowDirection = CommonSettings.FlowDirection;
            windowInstance.Style = Application.Current.FindResource("RadWindowStyle") as Style;

            // Return the created window.
            return windowInstance;
        }
    }
}